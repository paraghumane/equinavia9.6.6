﻿using klaviyo.net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;


namespace Znode.Api.Custom.Service.Service
{
    class EQShoppingCartService : ShoppingCartService
    {
        private readonly IShoppingCartMap _shoppingCartMap;

        public EQShoppingCartService() : base()
        {
            
            _shoppingCartMap = GetService<IShoppingCartMap>();
           
        }

        #region Klaviyo Integration
        //Add product in the cart.
        public override AddToCartModel AddToCartProduct(AddToCartModel shoppingCart)
        {
            AddToCartModel cartModel = base.AddToCartProduct(shoppingCart);
            //ZnodeLogging.LogMessage("Quantity" + shoppingCart.Quantity, "Nivi", TraceLevel.Error);
            //KlaviyoPlaceOrder(orderModel, model);
            return cartModel;            
        }

        private void TrackAddedToCart(AddToCartModel cartModel)
        {
            KlaviyoGateway gateway = new KlaviyoGateway("SuefjY");
        }

        #endregion
    }
}

