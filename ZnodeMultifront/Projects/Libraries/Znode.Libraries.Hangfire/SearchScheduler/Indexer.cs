﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Text;
using Znode.Engine.Api.Models;

namespace Znode.Libraries.Hangfire
{
    public class Indexer : BaseScheduler, ISchedulerProviders
    {
        #region Private Variables
        private int catalogId;
        private string indexName;
        private string domainUrl;
        private const string SchedulerName = "SearchScheduler";
        #endregion

        #region Public 

        public void InvokeMethod(ERPTaskSchedulerModel model)
        {
            if (!string.IsNullOrEmpty(model.ExeParameters))
            {
                var args = model.ExeParameters.Split(',');
                Indexer indexer = new Indexer();

                int createIndexMonitorId = 0;

                indexer.catalogId = Convert.ToInt32(args[1]);
                indexer.indexName = args[2];
                indexer.domainUrl = args[6];
                if ((args.Length > 12) && !string.IsNullOrEmpty(args[12]))
                    base.requesttimeout = int.Parse(args[12]);

                // If exe called from scheduler insert data required to create index.
                // else initialize create Index monitor ID from command line arguments.
                if (args[5] == "SchedulerInUse")
                {
                    indexer.InsertCreateIndexData(args);
                }
                else
                {
                    createIndexMonitorId = Convert.ToInt32(args[4]);
                    //Call create index method                
                    indexer.CreateIndex(indexer.catalogId, indexer.indexName, args[9], createIndexMonitorId, args[7], args[8], args[10], (args.Length > 11) ? args[11] : "", base.requesttimeout);
                }

                LogMessage(string.Join("|", args), SchedulerName);                
            }
        }
        #endregion

        #region Private Methods

        //Insert data required for creating index in database.(Portal index data and CreateIndexMonitor tables)
        //Input required PortalId, index Name.
        //Out param create index monitor ID.
        private void InsertCreateIndexData(string[] args)
        {
            string jsonString = string.Empty;
            string message = string.Empty;
            string requestPath = args[6] + "/search/insertcreateindexdata";
            try
            {
                PortalIndexModel portalIndex = new PortalIndexModel() { PublishCatalogId = catalogId, IndexName = indexName, CatalogIndexId = Convert.ToInt32(args[3]) };
                var dataBytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(portalIndex));
                var request = (HttpWebRequest)WebRequest.Create(requestPath);
                request.KeepAlive = false; // Prevents "server committed a protocol violation" error
                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = dataBytes.Length;
                request.Headers.Add($"Znode-UserId: {args[7]}");
                request.Headers.Add($"Authorization: {"Basic " + args[10]}");
                // args[11] contains token value.
                if ((args.Length > 11) && !string.IsNullOrEmpty(args[11]) && args[11] != "0")
                    request.Headers.Add($"Token: {args[11]}");

                if ((args.Length > 12) && !string.IsNullOrEmpty(args[12]))
                    base.requesttimeout = int.Parse(args[12]);

                request.Timeout = base.requesttimeout;
                using (var reqStream = request.GetRequestStream())
                {
                    reqStream.Write(dataBytes, 0, dataBytes.Length);
                }
                var result = GetResultFromResponse<PortalIndexModel>(request);

                //Log message
                LogMessage("Search index created by scheduler successfully.", SchedulerName);
            }
            catch (WebException webException)
            {
                if (CheckTokenIsInvalid(webException))
                {
                    // args[10] contains authorization header value.
                    // args[6] contains domain url.
                    args[11] = GetToken(args[6], args[10]);
                    InsertCreateIndexData(args);
                }
                else
                    LogMessage($"Search index creation failed. Exception: {webException.Message}------API called {requestPath}", SchedulerName);
            }
            catch (Exception ex)
            {
                LogMessage($"Search index creation failed. Exception: {ex.Message}------API called {requestPath}", SchedulerName);
            }
        }

        //Call search library to create Index
        //Input Index name, Portal Id, search index monitor ID.
        private void CreateIndex(int portalId, string indexName, string revisionType, int createIndexMonitorId, string userId, string serverStatusId, string headerAuthorization, string TokenValue = "", int requesttimeout = 600000)
        {
            string jsonString = string.Empty;
            string message = string.Empty;
            
            string requestPath = domainUrl + $"/search/createindex/{indexName}/{revisionType}/{portalId}/{createIndexMonitorId}/{serverStatusId}";
            try
            {
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestPath);
                request.Method = "GET";
                request.ContentType = "application/x-www-form-url-encoded";
                request.Headers.Add($"Znode-UserId: {userId}");
                request.Headers.Add($"Authorization: {"Basic " + headerAuthorization}");
                if (!string.IsNullOrEmpty(TokenValue) && TokenValue != "0")
                    request.Headers.Add($"Token: {TokenValue}");

                request.Timeout = requesttimeout;
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    Stream datastream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(datastream);
                    jsonString = reader.ReadToEnd();
                    reader.Close();
                    datastream.Close();
                    LogMessage($"{DateTime.Now}-- API Call Success.------API called {requestPath} ", SchedulerName);
                }
            }
            catch (WebException webException)
            {
                if (CheckTokenIsInvalid(webException))
                {
                    TokenValue = GetToken(domainUrl, headerAuthorization);
                    CreateIndex(portalId, indexName, revisionType, createIndexMonitorId, userId, serverStatusId, headerAuthorization, TokenValue, requesttimeout);
                }
                else
                {
                    PortalIndexModel res;
                    using (var rsp = (HttpWebResponse)webException.Response)
                    {
                        // This deserialization is used to get the error information
                        res = DeserializeResponseStream<PortalIndexModel>(rsp);
                        Console.WriteLine("Failed in get result from response");
                    }
                }
            }
            catch (Exception ex)
            {
                LogMessage($"{DateTime.Now}-- Failed: {ex.Message}", SchedulerName);
            }
        }


        private PortalIndexModel GetResultFromResponse<PortalIndexModel>(HttpWebRequest request)
        {
            PortalIndexModel res;
            using (var rsp = (HttpWebResponse)request.GetResponse())
            {
                Stream datastream = rsp.GetResponseStream();
                StreamReader reader = new StreamReader(datastream);
                string jsonString = reader.ReadToEnd();
                res = JsonConvert.DeserializeObject<PortalIndexModel>(jsonString);
            }

            return res;
        }

        private PortalIndexModel DeserializeResponseStream<PortalIndexModel>(WebResponse response)
        {
            if (response != null)
            {
                using (var body = response.GetResponseStream())
                {
                    if (body != null)
                    {
                        using (var stream = new StreamReader(body))
                        {
                            using (var jsonReader = new JsonTextReader(stream))
                            {
                                var jsonSerializer = new JsonSerializer();
                                try
                                {
                                    return jsonSerializer.Deserialize<PortalIndexModel>(jsonReader);
                                }
                                catch (JsonReaderException ex)
                                {
                                    throw new Exception(ex.Message);
                                }
                            }
                        }
                    }
                }
            }
            return default(PortalIndexModel);
        }

        #endregion
    }
}
