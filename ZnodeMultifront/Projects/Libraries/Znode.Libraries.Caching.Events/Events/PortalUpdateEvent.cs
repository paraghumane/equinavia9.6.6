﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Libraries.Caching.Events.Models;

namespace Znode.Libraries.Caching.Events
{
    public class PortalUpdateEvent : BaseCacheEvent
    {
        public PortalUpdateEventEntry[] PortalUpdateEventEntries { get; set; }
    }
}
