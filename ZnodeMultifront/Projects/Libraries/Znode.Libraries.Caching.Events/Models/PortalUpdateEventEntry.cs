﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Libraries.Caching.Events.Models
{
    public class PortalUpdateEventEntry
    {
        public string PortalDomainName { get; set; }
        public int PortalId { get; set; }
    }
}
