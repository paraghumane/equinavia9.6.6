﻿using Newtonsoft.Json;
using System;

namespace klaviyo.net.Converters
{
    public class PropertiesConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(Properties));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteStartObject();
            writer.WritePropertyName("$event_id");
            serializer.Serialize(writer, ((Properties)value).EventId);

            if (((Properties)value).Value.HasValue)
            {
                writer.WritePropertyName("$value");
                serializer.Serialize(writer, ((Properties)value).Value);
            }

            foreach (NotRequiredProperty item in ((Properties)value).NotRequiredProperties)
            {
                writer.WritePropertyName(item.Name);
                serializer.Serialize(writer, item.Value);
            }
            writer.WritePropertyName("Items");
            writer.WriteStartArray();
            foreach (KlaviyoItems item in ((Properties)value).KlaviyoItems)
            {
                // serializer.Serialize(writer, item);
                writer.WriteStartObject();
               
                writer.WritePropertyName("ProductName");
                writer.WriteValue(item.ProductName);

                writer.WritePropertyName("SKU");
                writer.WriteValue(item.SKU);
                writer.WritePropertyName("ItemPrice");
                writer.WriteValue(item.ItemPrice);
                writer.WritePropertyName("Qunatity");
                writer.WriteValue(item.Qunatity);
                writer.WritePropertyName("RowTotal");
                writer.WriteValue(item.RowTotal);
                writer.WritePropertyName("ProductURL");
                writer.WriteValue(item.ProductURL);
                writer.WritePropertyName("Description");
                writer.WriteValue(item.Description);
                writer.WritePropertyName("ImageURL");
                writer.WriteValue(item.ImageURL);

                writer.WriteEndObject();
            }
            writer.WriteEndArray();
            writer.WritePropertyName("BillingAddress");
            serializer.Serialize(writer, ((Properties)value).Billaddress);
            if (((Properties)value).ShipingAddress != null)
            {
                writer.WritePropertyName("ShippingAddress");
                serializer.Serialize(writer, ((Properties)value).ShipingAddress);
            }

            
            writer.WriteEndObject();
        }


    }
}