﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace klaviyo.net
{
    [DataContract]
    public class Properties
    {
        public Properties()
        {
            NotRequiredProperties = new List<NotRequiredProperty>();
            KlaviyoItems = new List<KlaviyoItems>();
           
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [DataMember(Name = "$event_id")]
        public string EventId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [DataMember(Name = "$value")]
        public decimal? Value { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NotRequiredProperty> NotRequiredProperties { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<KlaviyoItems> KlaviyoItems { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public KlaviyoAddress Billaddress { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public KlaviyoAddress ShipingAddress { get; set; }

    }
}