﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using Znode.Libraries.Caching;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.Cloudflare.Caching.Core
{
    internal abstract class BaseCloudflareEvictor<T> : BaseCacheEventEvictor<T> where T : BaseCacheEvent, new()
    {
        protected override string GetCacheStorageType()
        {
            return "Cloudflare";
        }
    }
}
