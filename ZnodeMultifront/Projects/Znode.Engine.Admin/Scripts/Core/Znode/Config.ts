﻿module Config {
    export const PaymentScriptUrl = $("#hdnPaymentAppUrl").val() + "/script/znodeapijs";
    export const PaymentApplicationUrl = $("#hdnPaymentAppUrl").val() + "/";
    export const PaymentTwoCoUrl = $("#hdnAdminUrl").val() + "/orders/twoco?paymentSettingId=";
}
