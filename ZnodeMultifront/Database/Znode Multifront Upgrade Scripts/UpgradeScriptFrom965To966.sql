IF EXISTS (SELECT TOP 1 1 FROM Sys.Tables WHERE Name = 'ZnodeMultifront')
BEGIN 
IF EXISTS (SELECT top 1 1 FROM ZnodeMultifront where BuildVersion = 966 and VersionName = 'Znode_Multifront_9_6_6' )
BEGIN 
PRINT 'Script is already executed....'
 SET NOEXEC ON 
END 
END
ELSE 
BEGIN 
  SET NOEXEC ON
END 


INSERT INTO [dbo].[ZnodeMultifront] ( [VersionName], [Descriptions], [MajorVersion], [MinorVersion], [LowerVersion], [BuildVersion], [PatchIndex], [CreatedBy], 
[CreatedDate], [ModifiedBy], [ModifiedDate]) 
VALUES ( N'Znode_Multifront_9_6_6', N'Upgrade GA Release by 966',9,6,6,966,0,2, GETDATE(),2, GETDATE())
GO 
SET ANSI_NULLS ON
GO


update ZnodeGlobalAttribute
set AttributeTypeId = (select top 1  AttributeTypeId from ZnodeAttributeType where AttributeTypeName = 'Text Area')
where AttributeCode = 'ContentSecurityPolicy'
----
GO
IF EXISTS(SELECT * FROM SYS.PROCEDURES WHERE NAME = 'Znode_GetOmsOrderDetail')
	DROP PROC Znode_GetOmsOrderDetail
GO

Create PROCEDURE [dbo].[Znode_GetOmsOrderDetail]
( @WhereClause NVARCHAR(MAX),
  @Rows        INT            = 100,
  @PageNo      INT            = 1,
  @Order_BY    VARCHAR(1000)  = '',
  @RowsCount   INT OUT			,
  @UserId	   INT = 0 ,
  @IsFromAdmin int=0,
  @SalesRepUserId int = 0 
 )
AS
    /*
     Summary : This procedure is used to get the oms order detils
			   Records are fetched for those users who placed the order i.e UserId is Present in ZnodeUser and  ZnodeOmsOrderDetails tables
	 Unit Testing:

    EXEC [Znode_GetOmsOrderDetail_SCT] 'PortalId =1',@Order_BY = '',@RowsCount= 0, @UserId = 0 ,@Rows = 50, @PageNo = 1

	declare @p7 int
	set @p7=4
	exec sp_executesql N'Znode_GetOmsOrderDetail_SCT @WhereClause, @Rows,@PageNo,@Order_By,@RowCount OUT,@UserId,@IsFromAdmin',N'@WhereClause nvarchar(30),@Rows int,@PageNo int,@Order_By nvarchar(14),@RowCount int output,@UserId int,@IsFromAdmin int',@WhereClause=N'(PortalId in(''1'',''4'',''5'',''6''))',@Rows=50,@PageNo=1,@Order_By=N'orderdate desc',@RowCount=@p7 output,@UserId=0,@IsFromAdmin=1
	select @p7



   */
BEGIN
    BEGIN TRY
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
		DECLARE @SQL NVARCHAR(MAX), @ProcessType  varchar(50)='Order'
		DECLARE @OrderLineItemRelationshipTypeId INT
		SET @OrderLineItemRelationshipTypeId = ( SELECT top 1 OrderLineItemRelationshipTypeId  FROM ZnodeOmsOrderLineItemRelationshipType where Name = 'AddOns' )

		----Verifying that the @SalesRepUserId is having 'Sales Rep' role
		IF NOT EXISTS
		(
			SELECT * FROM ZnodeUser ZU
			INNER JOIN AspNetZnodeUser ANZU ON ZU.UserName = ANZU.UserName
			INNER JOIN AspNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName
			INNER JOIN AspNetUserRoles ANUR ON ANU.Id = ANUR.UserId
			Where Exists(select * from AspNetRoles ANR Where Name = 'Sales Rep' AND ANUR.RoleId = ANR.Id) 
			AND ZU.UserId = @SalesRepUserId
		)   
		Begin
			SET @SalesRepUserId = 0
		End

		DECLARE @Fn_GetPaginationWhereClause VARCHAR(500) = dbo.Fn_GetPaginationWhereClause(@PageNo,@Rows),
		@Fn_GetFilterWhereClause NVARCHAR(MAX) = ''
		set @Fn_GetFilterWhereClause=dbo.Fn_GetFilterWhereClause(@WhereClause)
			
		IF @Order_BY = ''
			set @Order_BY = 'OrderDate desc'

			set @Order_BY = replace(@Order_BY,'PortalId','ZODD.PortalId')
			set @Order_BY = replace(@Order_BY,'UserName','ISNULL(RTRIM(LTRIM(ZODD.FirstName)),'''')+'' ''+ISNULL(RTRIM(LTRIM(ZODD.LastName)),'''')')
			set @Order_BY = replace(@Order_BY,'email','ZODD.Email')
			set @Order_BY = replace(@Order_BY,'OrderState','case when ZOS.IsShowToCustomer=0 and '+cast( @IsFromAdmin as varchar(50))+' = 0 then ZOSC.Description else  ZOS.Description end')
			set @Order_BY = replace(@Order_BY,'PaymentStatus','ZOPS.Name')
			set @Order_BY = replace(@Order_BY,'PublishState','ZODPS.DisplayName')
			set @Order_BY = replace(@Order_BY,'StoreName','ZP.StoreName')

			Declare @Fn_GetPagingRowId NVARCHAR(MAX) = ' DENSE_RANK()Over('+ ' Order By '+CASE WHEN Isnull(@Order_BY,'') = '' THEN 'OmsOrderId DESC' ELSE @Order_BY + ',OmsOrderId DESC' END  + ') RowId '
						
			IF OBJECT_ID('tempdb..#TBL_RowCount') is not null
				DROP TABLE #TBL_RowCount

			IF OBJECT_ID('tempdb..#Portal') is not null
				DROP TABLE #Portal
			
			Create table #TBL_RowCount(RowsCount int )
			CREATE TABLE #tbl_GetRecurciveUserId  (ID INT IDENTITY(1,1) Primary key,UserId INT,ParentUserId INT)
			INSERT INTO #tbl_GetRecurciveUserId
			SELECT UserId,ParentUserId FROM dbo.Fn_GetRecurciveUserId (CAST(@UserId AS VARCHAR(50)),@ProcessType ) FNRU
			 
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'PortalId','ZODD.PortalId')
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'UserName','ISNULL(RTRIM(LTRIM(ZODD.FirstName)),'''')+'' ''+ISNULL(RTRIM(LTRIM(ZODD.LastName)),'''')')
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'email','ZODD.Email')
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'OrderState','case when ZOS.IsShowToCustomer=0 and '+cast( @IsFromAdmin as varchar(50))+' = 0 then ZOSC.Description else  ZOS.Description end')
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'PaymentStatus','ZOPS.Name')
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'PublishState','ZODPS.DisplayName')
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'StoreName','ZP.StoreName')

			set @Order_BY = replace(@Order_BY,'ShippingPostalCode','BillingPostalCode')

			set @Fn_GetPagingRowId = replace(@Fn_GetPagingRowId,'OmsOrderId','Zoo.OmsOrderId')

			set @Rows = @PageNo * @Rows

			CREATE TABLE #Portal (PortalId int,StoreName varchar(200))
			insert into #Portal
			select PortalId,StoreName
			from ZnodePortal

		SET @SQL = '
		SELECT top '+cast(@Rows as varchar(10))+' Zoo.OmsOrderId,Zoo.OrderNumber, ZODD.PortalId,ZP.StoreName ,ZODD.CurrencyCode,
		case when ZOS.IsShowToCustomer=0 and '+cast( @IsFromAdmin as varchar(50))+' = 0 then ZOSC.Description else  ZOS.Description end  OrderState,ZODD.ShippingId,ZODD.PaymentTypeId,ZODD.PaymentSettingId
		,ZOPS.Name PaymentStatus,ZPS.Name PaymentType,CAST(1 AS BIT) ShippingStatus ,ZODD.OrderDate,ZODD.UserId,ISNULL(RTRIM(LTRIM(ZODD.FirstName)),'''')
		+'' ''+ISNULL(RTRIM(LTRIM(ZODD.LastName)),'''') UserName ,ZODD.PaymentTransactionToken ,ZODD.Total ,ZODD.OmsOrderDetailsId,ZODD.PoDocument,
		ZODD.Email ,ZODD.PhoneNumber ,ZODD.SubTotal ,ZODD.TaxCost ,ZODD.ShippingCost,ZODD.BillingPostalCode,
		ZODD.ModifiedDate AS OrderModifiedDate,  ZODD.PaymentDisplayName  ,isnull(Zoo.ExternalId,0) ExternalId,ZODD.CreditCardExpMonth,ZODD.CultureCode--,ZODD.TotalAdditionalCost
		,ZODD.CreditCardExpYear,ZODD.CardType,ZODD.CreditCardNumber,ZODD.PaymentExternalId,ZODPS.DisplayName as PublishState,
		ZOOLI.ProductName ProductName, 0 CountId, CAST (0 as bit) IsInRMA, '+@Fn_GetPagingRowId+' 
		INTO #Cte_OrderLineDescribe
		FROM ZnodeOmsOrder (nolock) ZOO 
		INNER JOIN ZnodeOmsOrderDetails (nolock) ZODD ON (ZODD.OmsOrderId = ZOO.OmsOrderId AND  ZODD.IsActive = 1)
		INNER JOIN ZnodePublishState ZODPS ON (ZODPS.PublishStateId = ZOO.PublishStateId)
		INNER JOIN #Portal ZP (nolock) ON ZODD.PortalId = ZP.PortalId
		LEFT JOIN ZnodePaymentType (nolock) ZPS ON (ZPS.PaymentTypeId = ZODD.PaymentTypeId )
		LEFT JOIN ZnodeOmsOrderStateShowToCustomer (nolock) ZOSC ON (ZOSC.OmsOrderStateId = ZODD.OmsOrderStateId)
		LEFT JOIN ZnodeOmsOrderState (nolock) ZOS ON (ZOS.OmsOrderStateId = ZODD.OmsOrderStateId)
		LEFT JOIN ZnodeOmsPaymentState (nolock) ZOPS ON (ZOPS.OmsPaymentStateId = ZODD.OmsPaymentStateId)
		LEFT JOIN ZnodeOmsOrderLineItems ZOOLI on ZOOLI.OmsOrderDetailsId = ZODD.OmsOrderDetailsId and ParentOmsOrderLineItemsId is null
		WHERE (exists(select * from ZnodeSalesRepCustomerUserPortal SalRep where SalRep.SalesRepUserId = '+cast(@SalesRepUserId as varchar(10))+' and ZODD.UserId = SalRep.CustomerUserid) or '+cast(@SalesRepUserId as varchar(10))+' = 0 )  
		AND (EXISTS (SELECT TOP 1 1 FROM #tbl_GetRecurciveUserId FNRU WHERE FNRU.UserId = ZODD.UserId ) OR '+cast(@UserId as varchar(10))+'  =0 )'
		+ @Fn_GetFilterWhereClause+' 

		Insert Into #TBL_RowCount 
		SELECT count(*)
		FROM ZnodeOmsOrder (nolock) ZOO 
		INNER JOIN ZnodeOmsOrderDetails (nolock) ZODD ON (ZODD.OmsOrderId = ZOO.OmsOrderId AND  ZODD.IsActive = 1)
		INNER JOIN ZnodePublishState ZODPS ON (ZODPS.PublishStateId = ZOO.PublishStateId)
		INNER JOIN #Portal ZP (nolock) ON ZODD.PortalId = ZP.PortalId
		LEFT JOIN ZnodePaymentType (nolock) ZPS ON (ZPS.PaymentTypeId = ZODD.PaymentTypeId )
		LEFT JOIN ZnodeOmsOrderStateShowToCustomer (nolock) ZOSC ON (ZOSC.OmsOrderStateId = ZODD.OmsOrderStateId)
		LEFT JOIN ZnodeOmsOrderState (nolock) ZOS ON (ZOS.OmsOrderStateId = ZODD.OmsOrderStateId)
		LEFT JOIN ZnodeOmsPaymentState (nolock) ZOPS ON (ZOPS.OmsPaymentStateId = ZODD.OmsPaymentStateId)
		WHERE (EXISTS(select * from ZnodeSalesRepCustomerUserPortal SalRep where SalRep.SalesRepUserId = '+cast(@SalesRepUserId as varchar(10))+' and ZODD.UserId = SalRep.CustomerUserid) or '+cast(@SalesRepUserId as varchar(10))+' = 0 )  
		AND (EXISTS (SELECT TOP 1 1 FROM #tbl_GetRecurciveUserId FNRU WHERE FNRU.UserId = ZODD.UserId ) OR '+cast(@UserId as varchar(10))+'  =0 )'
		+ @Fn_GetFilterWhereClause+' 
			
		Create index Ind_OrderLineDescribe_RowId on #Cte_OrderLineDescribe(RowId )

		SELECT OmsOrderId,OrderNumber,PortalId,StoreName,CurrencyCode,OrderState,ShippingId,
		PaymentTypeId,PaymentSettingId,PaymentStatus,PaymentType,ShippingStatus,OrderDate,UserId,UserName,PaymentTransactionToken,Total,
		ProductName OrderItem,OmsOrderDetailsId,CountId ItemCount, PoDocument AS PODocumentPath,IsInRMA ,
		Email,PhoneNumber,SubTotal,TaxCost,ShippingCost,BillingPostalCode
		,OrderModifiedDate,PaymentDisplayName, 
		ExternalId,CreditCardExpMonth,CreditCardExpYear,CardType,CreditCardNumber,PaymentExternalId,CultureCode,PublishState --TotalAdditionalCost
		FROM #Cte_OrderLineDescribe
		' + @Fn_GetPaginationWhereClause +' order by RowId '

		print @SQL
		EXEC(@SQL)
		Select @RowsCount= isnull(RowsCount  ,0) from #TBL_RowCount
		
		IF OBJECT_ID('tempdb..#TBL_RowCount') is not null
				DROP TABLE #TBL_RowCount

		IF OBJECT_ID('tempdb..#Portal') is not null
			DROP TABLE #Portal
		
    END TRY
    BEGIN CATCH
        DECLARE @Status BIT ;
		SET @Status = 0;
		DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(),
		@ErrorCall NVARCHAR(MAX)= 'EXEC Znode_GetOmsOrderDetail @WhereClause = '''+ISNULL(CAST(@WhereClause AS VARCHAR(max)),'''''')+''',@Rows='''+ISNULL(CAST(@Rows AS VARCHAR(50)),'''''')+''',@PageNo='+ISNULL(CAST(@PageNo AS VARCHAR(50)),'''')+',
		@Order_BY='+ISNULL(@Order_BY,'''''')+',@UserId = '+ISNULL(CAST(@UserId AS VARCHAR(50)),'''')+',@RowsCount='+ISNULL(CAST(@RowsCount AS VARCHAR(50)),'''')+',@IsFromAdmin='+ISNULL(CAST(@IsFromAdmin AS VARCHAR(10)),'''');
              			 
        SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                    
		  
        EXEC Znode_InsertProcedureErrorLog
		@ProcedureName = 'Znode_GetOmsOrderDetail',
		@ErrorInProcedure = 'Znode_GetOmsOrderDetail',
		@ErrorMessage = @ErrorMessage,
		@ErrorLine = @ErrorLine,
		@ErrorCall = @ErrorCall;
    END CATCH;
END;

GO
----
IF EXISTS(SELECT * FROM SYS.PROCEDURES WHERE NAME = 'Znode_GetPublishSingleProductJson')
	DROP PROC Znode_GetPublishSingleProductJson
GO
Create PROCEDURE [dbo].[Znode_GetPublishSingleProductJson]
(
	 @PublishCatalogId INT = 0 
	,@VersionId       VARCHAR(50) = 0 
	,@PimProductId    TransferId Readonly 
	,@UserId		  INT = 0 
	,@TokenId nvarchar(max)= ''	
	,@LocaleIds TransferId READONLY
	,@PublishStateId INT = 0  
	,@RevisionType varchar(50)
	,@Status bit = 0 OutPut
	
)
AS
BEGIN 
BEGIN TRY 
 SET NOCOUNT ON 

EXEC Znode_InsertUpdatePimAttributeJson 1 
EXEC Znode_InsertUpdateCustomeFieldJson 1
EXEC Znode_InsertUpdateAttributeDefaultValueJson 1 

DECLARE @PimMediaAttributeId INT = dbo.Fn_GetProductImageAttributeId()
				
Select ZPLPD.PimParentProductId, ZPLPD.PimProductId, ZPLPD.PimAttributeId, ZPAVL.AttributeValue as SKU
into #LinkProduct
FROM ZnodePimLinkProductDetail ZPLPD 
INNER JOIN ZnodePimAttributeValue ZPAV ON (ZPAV.PimProductId = ZPLPD.PimProductId)
INNER JOIN ZnodePimAttributeValueLocale ZPAVL ON ZPAV.PimAttributeValueId = ZPAVL.PimAttributeValueId
WHERE exists(select * from ZnodePimAttribute ZPA where ZPA.PimAttributeId = ZPAV.PimAttributeId and ZPA.AttributeCode = 'SKU')
and exists(select * from @PimProductId pp where ZPLPD.PimParentProductId = pp.Id)

select * into #PimProductId from @PimProductId

create index Idx_#PimProductId_Id on #PimProductId(Id)
 IF OBJECT_ID('tempdb..#Cte_BrandData') is not null
 BEGIN 
	DROP TABLE #Cte_BrandData
 END 
 

 IF OBJECT_ID('tempdb..#ProductIds') is not null
 BEGIN 
	DROP TABLE #ProductIds
 END 

			Create Table #ProductIds (PimProductId int, PublishProductId  int )
			
			--DECLARE @PimProductAttributeJson TABLE(PimAttributeJsonId INT  PRIMARY KEY ,PimAttributeId INT,LocaleId INT  )
			CREATE TABLE #PimProductAttributeJson (PimAttributeJsonId INT  PRIMARY KEY ,PimAttributeId INT,LocaleId INT  )
			DECLARE @PimDefaultValueLocale  TABLE (PimAttributeDefaultJsonId INT  PRIMARY KEY ,PimAttributeDefaultValueId INT ,LocaleId INT ) 
			DECLARE @ProductNamePimAttributeId INT = dbo.Fn_GetProductNameAttributeId(),@DefaultLocaleId INT= Dbo.Fn_GetDefaultLocaleId(),@LocaleId INT = 0 
			,@SkuPimAttributeId  INT =  dbo.Fn_GetProductSKUAttributeId() , @IsActivePimAttributeId INT =  dbo.Fn_GetProductIsActiveAttributeId()
			DECLARE @GetDate DATETIME =dbo.Fn_GetDate()
			DECLARE @TBL_LocaleId  TABLE (RowId INT IDENTITY(1,1) PRIMARY KEY  , LocaleId INT )

			DECLARE @DomainUrl varchar(max) = (select TOp 1 URL FROM ZnodeMediaConfiguration WHERE IsActive =1)

			INSERT INTO @TBL_LocaleId (LocaleId)
			SELECT  LocaleId
			FROM ZnodeLocale MT
			WHERE IsActive = 1
			AND (EXISTS (SELECT TOP 1 1  FROM @LocaleIds RT WHERE RT.Id = MT.LocaleId )
			OR NOT EXISTS (SELECT TOP 1 1 FROM @LocaleIds )) 
	
			-----to update link products newly addded and deleted from PIM
			delete ZPAP
			from ZnodePublishAssociatedProduct ZPAP
			where ZPAP.IsLink = 1
			AND not exists(select * from ZnodePimLinkProductDetail ZPPD where ZPAP.ParentPimProductId = ZPPD.PimParentProductId AND ZPAP.PimProductId = ZPPD.PimProductId)
			and exists(select * from #PimProductId PP where PP.Id = ZPAP.ParentPimProductId )

			UPDATE ZPACP SET ZPACP.DisplayOrder = ZPLPD.DisplayOrder
			from ZnodePimLinkProductDetail ZPLPD
			INNER JOIN ZnodePimCategoryProduct ZPCP ON ZPLPD.PimParentProductId = ZPCP.PimProductId
			INNER JOIN ZnodePimCategoryHierarchy ZPCH ON ZPCP.PimCategoryId = ZPCH.PimCategoryId
			INNER JOIN ZnodePublishAssociatedProduct ZPACP ON ZPCH.PimCatalogId = ZPACP.PimCatalogId and ZPLPD.PimParentProductId = ZPACP.ParentPimProductId AND ZPLPD.PimProductId = ZPACP.PimProductId 
			where exists(select * from #PimProductId PP where PP.Id = ZPLPD.PimParentProductId )
			AND ZPACP.IsLink = 1

			insert into ZnodePublishAssociatedProduct(PimCatalogId,ParentPimProductId,PimProductId,PublishStateId,IsConfigurable,IsBundle,IsGroup,IsAddOn,IsLink,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
			select distinct ZPCH.PimCatalogId, ZPLPD.PimParentProductId, ZPLPD.PimProductId, @PublishStateId, 0, 0, 0, 0, 1, ZPLPD.DisplayOrder, @UserId,@GetDate ,@UserId , @GetDate
			from ZnodePimLinkProductDetail ZPLPD
			INNER JOIN ZnodePimCategoryProduct ZPCP ON ZPLPD.PimParentProductId = ZPCP.PimProductId
			INNER JOIN ZnodePimCategoryHierarchy ZPCH ON ZPCP.PimCategoryId = ZPCH.PimCategoryId
			where exists(select * from #PimProductId PP where PP.Id = ZPLPD.PimParentProductId )
			and not exists(select * from ZnodePublishAssociatedProduct ZPACP where ZPCH.PimCatalogId = ZPACP.PimCatalogId and ZPLPD.PimParentProductId = ZPACP.ParentPimProductId AND ZPLPD.PimProductId = ZPACP.PimProductId  )
		
			-----to update config products newly addded and deleted from PIM
			delete ZPAP
			from ZnodePublishAssociatedProduct ZPAP
			where ZPAP.IsConfigurable = 1
			AND exists(select * from ZnodePimProductTypeAssociation ZPPD where ZPAP.ParentPimProductId = ZPPD.PimParentProductId )
			and exists(select * from #PimProductId PP where PP.Id = ZPAP.ParentPimProductId )

			Update ZPACP SET ZPACP.DisplayOrder = ZPLPD.DisplayOrder
			from ZnodePimProductTypeAssociation ZPLPD
			INNER JOIN ZnodePimCategoryProduct ZPCP ON ZPLPD.PimParentProductId = ZPCP.PimProductId
			INNER JOIN ZnodePimCategoryHierarchy ZPCH ON ZPCP.PimCategoryId = ZPCH.PimCategoryId
			INNER JOIN ZnodePublishAssociatedProduct ZPACP ON ZPCH.PimCatalogId = ZPACP.PimCatalogId and ZPLPD.PimParentProductId = ZPACP.ParentPimProductId AND ZPLPD.PimProductId = ZPACP.PimProductId
			where exists(select * from #PimProductId PP where PP.Id = ZPLPD.PimParentProductId )
			AND ZPACP.IsConfigurable = 1

			insert into ZnodePublishAssociatedProduct(PimCatalogId,ParentPimProductId,PimProductId,PublishStateId,IsConfigurable,IsBundle,IsGroup,IsAddOn,IsLink,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, IsDefault)
			select distinct ZPCH.PimCatalogId, ZPLPD.PimParentProductId, ZPLPD.PimProductId, @PublishStateId, 1, 0, 0, 0, 0, ZPLPD.DisplayOrder, @UserId,@GetDate ,@UserId , @GetDate, ZPLPD.IsDefault
			from ZnodePimProductTypeAssociation ZPLPD
			INNER JOIN ZnodePimCategoryProduct ZPCP ON ZPLPD.PimParentProductId = ZPCP.PimProductId
			INNER JOIN ZnodePimCategoryHierarchy ZPCH ON ZPCP.PimCategoryId = ZPCH.PimCategoryId
			where exists(select * from #PimProductId PP where PP.Id = ZPLPD.PimParentProductId )
			and not exists(select * from ZnodePublishAssociatedProduct ZPACP where ZPCH.PimCatalogId = ZPACP.PimCatalogId and ZPLPD.PimParentProductId = ZPACP.ParentPimProductId AND ZPLPD.PimProductId = ZPACP.PimProductId  )
			--group by ZPCH.PimCatalogId, ZPLPD.PimParentProductId, ZPLPD.PimProductId, ZPLPD.DisplayOrder, ZPLPD.IsDefault
			-------

			DECLARE @Counter INT =1 ,@maxCountId INT = (SELECT max(RowId) FROM @TBL_LocaleId ) 

			CREATE TABLE #TBL_PublishCatalogId (PublishCatalogId INT,PublishProductId INT,PimProductId  INT   , VersionId INT ,LocaleId INT, PriceListId INT , PortalId INT ,MaxSmallWidth NVARCHAr(max)  )
			CREATE INDEX idx_#TBL_PublishCatalogIdPimProductId on #TBL_PublishCatalogId(PimProductId)
			CREATE INDEX idx_#TBL_PublishCatalogIdPimPublishCatalogId on #TBL_PublishCatalogId(PublishCatalogId)

			INSERT INTO #TBL_PublishCatalogId 
			SELECT Distinct ZPP.PublishCatalogId , ZPP.PublishProductId,PimProductId, 0,0 ,
			(SELECT TOP 1 PriceListId FROM ZnodePriceListPortal NT 
			INNER JOIN ZnodePimCatalog ZPC on ZPC.PortalId=NT.PortalId  
			ORDER BY NT.Precedence ASC ) ,TY.PortalId,
			(SELECT TOP 1  MAX(MaxSmallWidth) FROM ZnodeGlobalMediaDisplaySetting)
			FROM ZnodePublishProduct ZPP 
			LEFT JOIN ZnodePortalCatalog TY ON (TY.PublishCatalogId = ZPP.PublishCatalogId)
			WHERE (EXISTS (SELECT TOP 1 1 FROM #PimProductId SP WHERE SP.Id = ZPP.PimProductId  
			AND  (@PublishCatalogId IS NULL OR @PublishCatalogId = 0 ))
			OR  (ZPP.PublishCatalogId = @PublishCatalogId ))
			And Exists 
			(Select TOP 1 1 from ZnodePublishVersionEntity ZPCP  where ZPCP.ZnodeCatalogId  = ZPP.PublishCatalogId AND ZPCP.IsPublishSuccess =1 )

			Insert into #ProductIds (PimProductId,PublishProductId) Select distinct PimProductId,PublishProductId from #TBL_PublishCatalogId  

             Create TABLE #TBL_ZnodeTempPublish (PimProductId INT , AttributeCode VARCHAR(300) ,AttributeValue NVARCHAR(max) ) 			
			 DECLARE @TBL_AttributeVAlueLocale TABLE(PimProductId INT,PimAttributeId INT,ZnodePimAttributeValueLocaleId INT,LocaleId INT ,AttributeValue Nvarchar(1000) )


			 INSERT INTO @TBL_AttributeValueLocale (PimProductId ,PimAttributeId ,ZnodePimAttributeValueLocaleId ,LocaleId ,AttributeValue )
			 SELECT VIR.PimProductId,PimAttributeId,ZnodePimAttributeValueLocaleId,VIR.LocaleId, ''
			 FROM View_LoadManageProductInternal VIR
			 INNER JOIN #ProductIds ZPP ON (ZPP.PimProductId = VIR.PimProductId)
			 UNION ALL 
			 SELECT VIR.PimProductId,PimAttributeId,PimProductAttributeMediaId,ZPDE.LocaleId , ''
			 FROM ZnodePimAttributeValue  VIR
			 INNER JOIN ZnodePimProductAttributeMedia ZPDE ON (ZPDE.PimAttributeValueId = VIR.PimAttributeValueId )
			 WHERE EXISTS (SELECT TOP 1 1 FROM #ProductIds ZPP WHERE (ZPP.PimProductId = VIR.PimProductId) )
			 Union All 
			 SELECT VIR.PimProductId,VIR.PimAttributeId,ZPDVL.PimAttributeDefaultValueLocaleId,ZPDVL.LocaleId ,ZPDVL.AttributeDefaultValue
			   FROM ZnodePimAttributeValue  VIR
			 INNER JOIN ZnodePimAttribute D ON ( D.PimAttributeId=VIR.PimAttributeId AND D.IsPersonalizable =1 )
			 INNER JOIN ZnodePimAttributeDefaultValue ZPADV ON ZPADV.PimAttributeId = D.PimAttributeId
			 INNER JOIN ZnodePimAttributeDefaultValueLocale ZPDVL   on (ZPADV.PimAttributeDefaultValueId = ZPDVL.PimAttributeDefaultValueId)
			 WHERE ( ZPDVL.LocaleId = @DefaultLocaleId OR ZPDVL.LocaleId = @LocaleId )
			 AND EXISTS(SELECT TOP 1 1 FROM #ProductIds ZPP WHERE (ZPP.PimProductId = VIR.PimProductId) )
			 Union All 
			 SELECT VIR.PimProductId,VIR.PimAttributeId,'','' ,''
			 FROM ZnodePimAttributeValue  VIR
			 INNER JOIN ZnodePimAttribute D ON ( D.PimAttributeId=VIR.PimAttributeId AND D.IsPersonalizable =1 )
			 WHERE  EXISTS(SELECT TOP 1 1 FROM #ProductIds ZPP WHERE (ZPP.PimProductId = VIR.PimProductId) )
		
				--insert INTO #ZnodePrice
				SELECT RetailPrice,SalesPrice,ZC.CurrencyCode,ZCC.CultureCode ,ZCC.Symbol CurrencySuffix,TYU.PublishProductId ,isnull(ZPC1.IsAllowIndexing,0) as IsAllowIndexing
				into #ZnodePrice
				FROM ZnodePrice ZP 
				INNER JOIN ZnodePriceList ZPL ON (ZPL.PriceListId = ZP.PriceListId)
				INNER JOIN ZnodeCurrency ZC oN (ZC.CurrencyId = ZPL.CurrencyId )
				INNER JOIN ZnodeCulture ZCC ON (ZCC.CultureId = ZPL.CultureId)
				INNER JOIN ZnodePublishProductDetail TY ON (TY.SKU = ZP.SKU ) 
				INNER JOIN ZnodePublishProduct TYU ON (TYU.PublishProductId = TY.PublishProductId)
				INNER JOIN ZnodePublishCatalog ZPC ON (TYU.PublishCatalogId = ZPC.PublishCatalogId)
				INNER JOIN ZnodePimCatalog ZPC1 ON (ZPC.PimCatalogId = ZPC1.PimCatalogId)
				WHERE EXISTS (SELECT TOP 1 1 FROM #TBL_PublishCatalogId TYUR WHERE TYUR.PriceListId = ZPL.PriceListId AND TYUR.PublishCatalogId = TYU.PublishCatalogId
				AND TYU.PublishProductId = TYUR.PublishProductId)
				AND TY.LocaleId = dbo.Fn_GetDefaultLocaleId()
				AND EXISTS (SELECT TOP 1 1 FROM ZnodePriceListPortal ZPLP 
				INNER JOIN ZnodePimCatalog ZPC on ZPC.PortalId=ZPLP.PortalId WHERE ZPLP.PriceListId=ZP.PriceListId )
				
				--insert INTO #ProductSKU
				SELECT ZCSD.SEOUrl , ZCDL.SEODescription,ZCDL.SEOKeywords ,ZCDL.SEOTitle, TYU.PublishProductId ,isnull(ZPC1.IsAllowIndexing,0) as IsAllowIndexing
				INTO #ProductSKU
				FROM ZnodeCMSSEODetail ZCSD 
				INNER JOIN ZnodeCMSSEODetailLocale ZCDL ON (ZCDL.CMSSEODetailId = ZCSD.CMSSEODetailId)
				INNER JOIN ZnodePublishProductDetail TY ON (TY.SKU = ZCSD.SEOCode AND ZCDL.LocaleId = TY.LocaleId) 
				INNER JOIN ZnodePublishProduct TYU ON (TYU.PublishProductId = TY.PublishProductId)
				INNER JOIN ZnodePublishCatalog ZPC ON (TYU.PublishCatalogId = ZPC.PublishCatalogId)
				INNER JOIN ZnodePimCatalog ZPC1 ON (ZPC.PimCatalogId = ZPC1.PimCatalogId)
				WHERE CMSSEOTypeId = (SELECT TOP 1 CMSSEOTypeId FROM ZnodeCMSSEOType WHERE Name = 'Product') 
				AND EXISTS (SELECT TOP 1 1  FROM #TBL_PublishCatalogId TYUR WHERE  TYUR.PublishCatalogId = TYU.PublishCatalogId
				AND TYU.PublishProductId = TYUR.PublishProductId)
				AND ZCDL.LocaleId = dbo.Fn_GetDefaultLocaleId()
				and ZCSD.PortalId = isnull(ZPC1.PortalId,0)

			
				--insert INTO #ProductImages
				SELECT  TUI.PublishCatalogId, TYU.PublishProductId , @DomainUrl +'Catalog/'  + CAST(Max(ZPC1.PortalId) AS VARCHAr(100)) + '/'+ CAST(Isnull(Max(TUI.MaxSmallWidth),'') AS VARCHAR(100)) + '/' + Isnull(RT.MediaPath,'') AS ImageSmallPath    
				,isnull(ZPC1.IsAllowIndexing,0) as IsAllowIndexing
				INTO #ProductImages
				FROM ZnodePimAttributeValue ZPAV 
				INNER JOIN ZnodePublishProduct TYU ON (TYU.PimProductId  = ZPAV.PimProductId)
				INNER JOIN ZnodePimProductAttributeMedia  RT ON ( RT.PimAttributeValueId = ZPAV.PimAttributeValueId )
				INNER JOIN #TBL_PublishCatalogId TUI ON (TUI.PublishProductId = TYU.PublishProductId AND TUI.PublishCatalogId = TYU.PublishCatalogId
						 )--AND  TUI.LocaleId = dbo.Fn_GetDefaultLocaleId()
				INNER JOIN ZnodePublishCatalog ZPC ON (TYU.PublishCatalogId = ZPC.PublishCatalogId)
				INNER JOIN ZnodePimCatalog ZPC1 ON (ZPC.PimCatalogId = ZPC1.PimCatalogId)
				WHERE  RT.LocaleId = dbo.Fn_GetDefaultLocaleId()
				AND ZPAV.PimAttributeId = (SELECT TOp 1 PimAttributeId FROM ZnodePimAttribute WHERE AttributeCode = 'ProductImage')
				group by TUI.PublishCatalogId, TYU.PublishProductId ,isnull(RT.MediaPath,''),isnull(ZPC1.IsAllowIndexing,0) 
		  -- end
	  
WHILE @Counter <= @maxCountId
BEGIN
 SET @LocaleId = (SELECT TOP 1 LocaleId FROM @TBL_LocaleId WHERE RowId = @Counter)

  INSERT INTO #PimProductAttributeJson 
  SELECT PimAttributeJsonId ,PimAttributeId,LocaleId
  FROM ZnodePimAttributeJSON
  WHERE LocaleId = @LocaleId
  
  INSERT INTO #PimProductAttributeJson 
  SELECT PimAttributeJsonId ,PimAttributeId,LocaleId
  FROM ZnodePimAttributeJSON ZPAX
  WHERE ZPAX.LocaleId = @DefaultLocaleId  
  AND NOT EXISTS (SELECT TOP 1 1 FROM #PimProductAttributeJson ZPAXI WHERE ZPAXI.PimAttributeId = ZPAX.PimAttributeId )

  INSERT INTO @PimDefaultValueLocale
  SELECT PimAttributeDefaultJsonId,PimAttributeDefaultValueId,LocaleId 
  FROM ZnodePimAttributeDefaultJson
  WHERE localeId = @LocaleId

  INSERT INTO @PimDefaultValueLocale 
   SELECT PimAttributeDefaultJsonId,PimAttributeDefaultValueId,LocaleId 
  FROM ZnodePimAttributeDefaultJson ZX
  WHERE localeId = @DefaultLocaleId
  AND NOT EXISTS (SELECT TOP 1 1 FROM @PimDefaultValueLocale TRTR WHERE TRTR.PimAttributeDefaultValueId = ZX.PimAttributeDefaultValueId)
  
 
  --DECLARE @TBL_AttributeVAlue TABLE(PimProductId INT,PimAttributeId INT,ZnodePimAttributeValueLocaleId INT  )
  --DECLARE @TBL_CustomeFiled TABLE (PimCustomeFieldJsonId INT ,CustomCode VARCHAR(300),PimProductId INT ,LocaleId INT )
  CREATE TABLE #TBL_CustomeFiled  (PimCustomeFieldJsonId INT ,CustomCode VARCHAR(300),PimProductId INT ,LocaleId INT )
  CREATE TABLE #TBL_AttributeVAlue (PimProductId INT,PimAttributeId INT,ZnodePimAttributeValueLocaleId INT  )



  INSERT INTO #TBL_CustomeFiled (PimCustomeFieldJsonId,PimProductId ,LocaleId,CustomCode)
  SELECT  PimCustomeFieldJsonId,RTR.PimProductId ,RTR.LocaleId,CustomCode
  FROM ZnodePimCustomeFieldJson RTR 
  INNER JOIN #ProductIds ZPP ON (ZPP.PimProductId = RTR.PimProductId)
  WHERE RTR.LocaleId = @LocaleId
 

  INSERT INTO #TBL_CustomeFiled (PimCustomeFieldJsonId,PimProductId ,LocaleId,CustomCode)
  SELECT  Distinct  PimCustomeFieldJsonId,ITR.PimProductId ,ITR.LocaleId,CustomCode
  FROM ZnodePimCustomeFieldJson ITR
  INNER JOIN #ProductIds ZPP ON (ZPP.PimProductId = ITR.PimProductId)
  WHERE ITR.LocaleId = @DefaultLocaleId
  AND NOT EXISTS (SELECT TOP 1 1 FROM #TBL_CustomeFiled TBL  WHERE ITR.CustomCode = TBL.CustomCode AND ITR.PimProductId = TBL.PimProductId)
  

    INSERT INTO #TBL_AttributeVAlue (PimProductId ,PimAttributeId ,ZnodePimAttributeValueLocaleId )
    SELECT Distinct  PimProductId,PimAttributeId,ZnodePimAttributeValueLocaleId
	FROM @TBL_AttributeVAlueLocale
    WHERE LocaleId = @LocaleId

    
	INSERT INTO #TBL_AttributeVAlue(PimProductId ,PimAttributeId ,ZnodePimAttributeValueLocaleId )
	SELECT VI.PimProductId,PimAttributeId,ZnodePimAttributeValueLocaleId
	FROM @TBL_AttributeVAlueLocale VI 
    WHERE VI.LocaleId = @DefaultLocaleId 
	AND NOT EXISTS (SELECT TOP 1 1 FROM #TBL_AttributeVAlue  CTE WHERE CTE.PimProductId = VI.PimProductId AND CTE.PimAttributeId = VI.PimAttributeId )
 
	------------Facet Merging Patch --------------
	IF OBJECT_ID('tempdb..#PimChildProductFacets') is not null
	BEGIN 
		DROP TABLE #PimChildProductFacets
	END 

	IF OBJECT_ID('tempdb..#PimAttributeDefaultXML') is not null
	BEGIN 
		DROP TABLE #PimAttributeDefaultXML
	END
	----Getting parent facets data
	Select  ZPPADV.PimAttributeDefaultValueId, ZPAV_Parent.PimAttributeValueId, ZPPADV.LocaleId
	Into #PimChildProductFacets
	from ZnodePimAttributeValue ZPAV_Parent
	inner join ZnodePimProductAttributeDefaultValue ZPPADV ON ZPAV_Parent.PimAttributeValueId = ZPPADV.PimAttributeValueId 
	where exists(select * from #ProductIds ZPPC where ZPAV_Parent.PimProductId = ZPPC.PimProductId )

	----Getting child facets for merging	
	insert into #PimChildProductFacets	  
	Select distinct ZPPADV.PimAttributeDefaultValueId, ZPAV_Parent.PimAttributeValueId, ZPPADV.LocaleId
	from ZnodePimAttributeValue ZPAV_Parent
	inner join ZnodePimProductTypeAssociation ZPPTA ON ZPAV_Parent.PimProductId = ZPPTA.PimParentProductId
	inner join ZnodePimAttributeValue ZPAV_Child ON ZPPTA.PimProductId = ZPAV_Child.PimProductId AND ZPAV_Parent.PimAttributeId = ZPAV_Child.PimAttributeId
	inner join ZnodePimProductAttributeDefaultValue ZPPADV ON ZPAV_Child.PimAttributeValueId = ZPPADV.PimAttributeValueId 
	where exists(select * from ZnodePimFrontendProperties ZPFP where ZPAV_Parent.PimAttributeId = ZPFP.PimAttributeId and ZPFP.IsFacets = 1)
	and exists(select * from #ProductIds ZPPC where ZPAV_Parent.PimProductId = ZPPC.PimProductId )
	and not exists(select * from ZnodePimProductAttributeDefaultValue ZPPADV1 where ZPAV_Parent.PimAttributeValueId = ZPPADV1.PimAttributeValueId 
		            and ZPPADV1.PimAttributeDefaultValueId = ZPPADV.PimAttributeDefaultValueId )

	----Merging childs facet attribute Default value XML for parent
	select  ZPADX.DefaultValueJson, ZPPADV.PimAttributeValueId, ZPPADV.LocaleId
	into #PimAttributeDefaultXML
	from #PimChildProductFacets ZPPADV		  
	inner join ZnodePimAttributeDefaultJson ZPADX ON ( ZPPADV.PimAttributeDefaultValueId = ZPADX.PimAttributeDefaultValueId )--AND ZPPADV.LocaleId = ZPADX.LocaleId)
	INNER JOIN @PimDefaultValueLocale GH ON (GH.PimAttributeDefaultJsonId = ZPADX.PimAttributeDefaultJsonId)
	------------Facet Merging Patch --------------   

	 IF OBJECT_ID('tempdb..#View_LoadManageProductInternal') is not null
	 BEGIN 
		DROP TABLE #View_LoadManageProductInternal
	 END 

	SELECT a.PimProductId ,b.AttributeValue as AttributeValue , b.LocaleId  ,a.PimAttributeId,c.AttributeCode ,b.ZnodePimAttributeValueLocaleId
	into #View_LoadManageProductInternal
	FROM ZnodePimAttributeValue a 
	INNER JOIN  ZnodePimAttributeValueLocale b ON ( b.PimAttributeValueId = a.PimAttributeValueId )
	INNER JOIN ZnodePimAttribute c ON ( c.PimAttributeId=a.PimAttributeId )
	INNER JOIN ZnodePimAttributeJSON c1   ON (c1.PimAttributeId = a.PimAttributeId )
	INNER JOIN #PimProductAttributeJson b1 ON (b1.PimAttributeJsonId = c1.PimAttributeJsonId )
	INNER JOIN #TBL_AttributeVAlue CTE ON (Cte.PimAttributeId = a.PimAttributeId AND Cte.ZnodePimAttributeValueLocaleId = b.ZnodePimAttributeValueLocaleId)
	UNION ALL
	SELECT a.PimProductId,ZPPATAV.AttributeValue AS AttributeValue  
	,ZPPATAV.LocaleId,a.PimAttributeId,c.AttributeCode  ,ZPPATAV.PimProductAttributeTextAreaValueId
	FROM ZnodePimAttributeValue a 
	INNER JOIN ZnodePimProductAttributeTextAreaValue ZPPATAV ON (ZPPATAV.PimAttributeValueId = a.PimAttributeValueId )
	INNER JOIN ZnodePimAttribute c ON ( c.PimAttributeId=a.PimAttributeId )
	INNER JOIN ZnodePimAttributeJSON c1   ON (c1.PimAttributeId = a.PimAttributeId )
	INNER JOIN #PimProductAttributeJson b1 ON (b1.PimAttributeJsonId = c1.PimAttributeJsonId )
	INNER JOIN #TBL_AttributeVAlue CTE ON (Cte.PimAttributeId = a.PimAttributeId AND Cte.ZnodePimAttributeValueLocaleId = ZPPATAV.PimProductAttributeTextAreaValueId)
	
	INSERT INTO #TBL_ZnodeTempPublish  
		SELECT  a.PimProductId,a.AttributeCode , 
			JSON_MODIFY (JSON_MODIFY (Json_Query( c.AttributeJSON  ) , '$.AttributeValues' ,  
			ISNULL(a.AttributeValue,'') ) ,'$.SelectValues',Json_Query('[]'))
			AS 'AttributeValue'
		FROM #View_LoadManageProductInternal a 
		INNER JOIN ZnodePimAttributeJSON c   ON (c.PimAttributeId = a.PimAttributeId )
		INNER JOIN #PimProductAttributeJson b ON (b.PimAttributeJsonId = c.PimAttributeJsonId )
		INNER JOIN #TBL_AttributeVAlue CTE ON (Cte.PimAttributeId = a.PimAttributeId AND Cte.ZnodePimAttributeValueLocaleId = a.ZnodePimAttributeValueLocaleId)
	UNION ALL 
			SELECT  a.PimProductId,c.AttributeCode , 
			JSON_MODIFY (JSON_MODIFY (Json_Query( c.AttributeJSON  ) , '$.AttributeValues' ,  
			ISNULL(TAVL.AttributeValue,'') ) ,'$.SelectValues',Json_Query('[]'))
			AS 'AttributeValue'
		FROM ZnodePimAttributeValue  a 
		INNER JOIN ZnodePimAttributeJSON c   ON (c.PimAttributeId = a.PimAttributeId )
		INNER JOIN #PimProductAttributeJson b ON (b.PimAttributeJsonId = c.PimAttributeJsonId )
		INNER JOIN ZnodePImAttribute ZPA  ON (ZPA.PimAttributeId = a.PimAttributeId)
		INNER JOIN #ProductIds ZPP ON (ZPP.PimProductId = a.PimProductId)
		Inner JOIN @TBL_AttributeVAlueLocale TAVL ON  (c.PimAttributeId = TAVL.PimAttributeId  and ZPP.PimProductId = TAVL.PimProductId )
		WHERE ZPA.IsPersonalizable = 1 
		AND NOT EXISTS ( SELECT TOP 1 1 FROM ZnodePimAttributeValueLocale q WHERE q.PimAttributeValueId = a.PimAttributeValueId) 
	UNION ALL 
		SELECT THB.PimProductId,THB.CustomCode,
		--'<Attributes><AttributeEntity>'+CustomeFiledJson +'</AttributeEntity></Attributes>' 
		JSON_MODIFY (Json_Query( CustomeFiledJson ) ,'$.SelectValues',Json_Query('[]')) 
		FROM ZnodePimCustomeFieldJson THB 
		INNER JOIN #TBL_CustomeFiled TRTE ON (TRTE.PimCustomeFieldJsonId = THB.PimCustomeFieldJsonId)
		UNION ALL 
		SELECT ZPAV.PimProductId,c.AttributeCode,
			JSON_MODIFY (JSON_MODIFY (c.AttributeJson,'$.AttributeValues',''), '$.SelectValues',
			Isnull((SELECT 
			Isnull(JSON_VALUE(DefaultValueJson, '$.Code'),'') Code 
			,Isnull(JSON_VALUE(DefaultValueJson, '$.LocaleId'),0) LocaleId
			,IsNull(JSON_VALUE(DefaultValueJson, '$.Value'),'') Value
			,IsNull(JSON_VALUE(DefaultValueJson, '$.AttributeDefaultValue'),'') AttributeDefaultValue
			,Isnull(JSON_VALUE(DefaultValueJson, '$.DisplayOrder'),0) DisplayOrder
			,Isnull(JSON_VALUE(DefaultValueJson, '$.IsEditable'),'false') IsEditable
			,Isnull(JSON_VALUE(DefaultValueJson, '$.SwatchText'),'') SwatchText
			,Isnull(JSON_VALUE(DefaultValueJson, '$.Path'),'') Path
			FROM #PimAttributeDefaultXML ZPADV
			WHERE (ZPADV.PimAttributeValueId = ZPAV.PimAttributeValueId) For JSON Auto 
			),'[]') 
		)  AttributeValue
		FROM ZnodePimAttributeValue ZPAV  With (NoLock)
		INNER JOIN ZnodePimAttributeJSON c   ON (c.PimAttributeId = ZPAV.PimAttributeId )
		INNER JOIN #PimProductAttributeJson b ON (b.PimAttributeJsonId = c.PimAttributeJsonId )
		INNER JOIN #ProductIds ZPP ON (ZPP.PimProductId = ZPAV.PimProductId)
		WHERE EXISTS (SELECT TOP 1 1 FROM ZnodePimProductAttributeDefaultValue ZPADVL 
		WHERE ZPADVL.PimAttributeValueId = ZPAV.PimAttributeValueId)
	UNION ALL 
		SELECT DISTINCT  ZPAV.PimProductId,c.AttributeCode,
			JSON_MODIFY (JSON_MODIFY (Json_Query( c.AttributeJson  ) , '$.AttributeValues',  
			ISNULL((Select stuff( 
			(SELECT ','+ZPPG.MediaPath 
			FROM ZnodePimProductAttributeMedia ZPPG INNER JOIN  #TBL_AttributeVAlue TBLV ON 
			(	TBLV.PimProductId=  ZPAV.PimProductId AND TBLV.PimAttributeId = ZPAV.PimAttributeId )
			WHERE ZPPG.PimProductAttributeMediaId = TBLV.ZnodePimAttributeValueLocaleId
			FOR XML PATH(''),Type).value('.', 'varchar(max)'), 1, 1, '')),'') ) ,'$.SelectValues',Json_Query('[]'))   
			AS 'AttributeEntity'
		FROM ZnodePimAttributeValue ZPAV 
		INNER JOIN ZnodePimAttributeJSON c   ON (c.PimAttributeId = ZPAV.PimAttributeId )
		INNER JOIN #PimProductAttributeJson b ON (b.PimAttributeJsonId = c.PimAttributeJsonId )
		INNER JOIN #ProductIds ZPP ON (ZPP.PimProductId = ZPAV.PimProductId)
		WHERE EXISTS (SELECT TOP 1 1 FROM ZnodePimProductAttributeMedia ZPADVL WHERE ZPADVL.PimAttributeValueId = ZPAV.PimAttributeValueId)
	UNION ALL 
		SELECT ZPLP.PimParentProductId ,c.AttributeCode, 
			JSON_MODIFY( JSON_Modify(c.AttributeJson , '$.AttributeValues' , 
			ISNULL(SUBSTRING((SELECT ','+cast( LP.SKU as varchar(600)) 
							 FROM #LinkProduct LP
							 WHERE LP.PimParentProductId = ZPLP.PimParentProductId 
							 AND LP.PimAttributeId = ZPLP.PimAttributeId
		FOR XML PATH ('') ),2,4000),'')),'$.SelectValues',Json_Query('[]'))   
	
		FROM ZnodePimLinkProductDetail ZPLP 
		INNER JOIN #TBL_PublishCatalogId ZPP ON (ZPP.PimProductId = ZPLP.PimParentProductId)
		INNER JOIN ZnodePimAttributeJSON c   ON (c.PimAttributeId = ZPLP.PimAttributeId )
		INNER JOIN #PimProductAttributeJson b ON (b.PimAttributeJsonId = c.PimAttributeJsonId )
		GROUP BY ZPLP.PimParentProductId , ZPP.PublishProductId  ,ZPLP.PimAttributeId,c.AttributeCode,c.AttributeJson,ZPP.PublishCatalogId
	UNION ALL 
		SELECT ZPAV.PimProductId,'DefaultSkuForConfigurable' ,
			JSON_MODIFY( JSON_Modify(
			REPLACE(REPLACE (c.AttributeJson,'ProductType','DefaultSkuForConfigurable'),'Product Type','Default Sku For Configurable'),
			'$.AttributeValues' , 
			ISNULL(SUBSTRING((SELECT ','+CAST(adl.AttributeValue AS VARCHAR(50)) 
		FROM ZnodePimAttributeValue ad 
		inner join ZnodePimAttributeValueLocale adl on ad.PimattributeValueId = adl.PimAttributeValueId
		INNER JOIN ZnodePimProductTypeAssociation yt ON (yt.PimProductId = ad.PimProductId)
		WHERE EXISTS (select * from #ProductIds p where yt.PimParentProductId = p.PimProductId)
		AND Ad.PimAttributeId =(select top 1 PimAttributeId from ZnodePimAttribute zpa where zpa.AttributeCode = 'SKU')
		AND yt.PimParentProductId = ZPAV.PimProductId 
		ORDER BY yt.DisplayOrder , yt.PimProductTypeAssociationId ASC FOR XML PATH ('') ),2,4000),'')),'$.SelectValues',Json_Query('[]'))   
		FROM ZnodePimAttributeValue ZPAV  
		INNER JOIN ZnodePimAttributeJSON c   ON (c.PimAttributeId = ZPAV.PimAttributeId )
		INNER JOIN #ProductIds ZPP ON (ZPP.PimProductId = ZPAV.PimProductId)
		WHERE EXISTS (SELECT TOP 1 1 FROM ZnodePimProductAttributeDefaultValue ZPADVL 
		INNER JOIN ZnodePimAttributeDefaultValue dr ON (dr.PimAttributeDefaultValueId = ZPADVL.PimAttributeDefaultValueId)
		WHERE ZPADVL.PimAttributeValueId = ZPAV.PimAttributeValueId
		AND dr.AttributeDefaultValueCode= 'ConfigurableProduct' 
		)
		AND EXISTS (select * from #PimProductAttributeJson b where b.PimAttributeJsonId = c.PimAttributeJsonId)
		AND c.AttributeCode = 'ProductType' 
	UNION ALL
		SELECT DISTINCT  UOP.PimProductId,c.AttributeCode,
			JSON_MODIFY (JSON_MODIFY (c.AttributeJson,'$.AttributeValues',''), '$.SelectValues',
			Isnull((SELECT  DISTINCT 
			Isnull(JSON_VALUE(AA.DefaultValueJson, '$.Code'),'') Code 
			,Isnull(JSON_VALUE(AA.DefaultValueJson, '$.LocaleId'),0) LocaleId
			,Isnull(JSON_VALUE(AA.DefaultValueJson, '$.Value'),'') Value
			,Isnull(JSON_VALUE(AA.DefaultValueJson, '$.AttributeDefaultValue'),'') AttributeDefaultValue
			,Isnull(JSON_VALUE(AA.DefaultValueJson, '$.DisplayOrder'),0) DisplayOrder
			,Isnull(JSON_VALUE(AA.DefaultValueJson, '$.IsEditable'),'false') IsEditable
			,Isnull(JSON_VALUE(AA.DefaultValueJson, '$.SwatchText'),'') SwatchText
			,Isnull(JSON_VALUE(AA.DefaultValueJson, '$.Path'),'') Path 
			,ISNULL(ZPA.DisplayOrder,0)  AS VariantDisplayOrder 
			,ISNULL(ZPAVL_SKU.AttributeValue,'')   AS VariantSKU 
			--,Isnull(ZM.Path,'') 
		,ZM.Path AS VariantImagePath 
		FROM ZnodePimAttributeDefaultJson AA 
		INNER JOIN ZnodePimProductAttributeDefaultValue ZPADV ON ( ZPADV.PimAttributeDefaultValueId = AA.PimAttributeDefaultValueId )
		INNER JOIN ZnodePimAttributeValue ZPAV1 ON (ZPAV1.PimAttributeValueId= ZPADV.PimAttributeValueId )
		-- check/join for active variants 
		INNER JOIN ZnodePimAttributeValue ZPAV ON (ZPAV.PimProductId =ZPAV1.PimProductId)
		INNER JOIN ZnodePimAttributeValueLocale ZPAVL ON (ZPAV.PimAttributevalueid = ZPAVL.PimAttributeValueId AND ZPAVL.AttributeValue = 'True')
		INNER JOIN ZnodePimProductTypeAssociation YUP ON (YUP.PimProductId = ZPAV1.PimProductId)
		-- SKU
		INNER JOIN ZnodePimAttributeValue ZPAV_SKU ON(YUP.PimProductId = ZPAV_SKU.PimProductId)
		INNER JOIN ZnodePimAttributeValueLocale ZPAVL_SKU ON (ZPAVL_SKU.PimAttributeValueId = ZPAV_SKU.PimAttributeValueId)
		LEFT JOIN ZnodePimAttribute ZPA ON (ZPA.PimattributeId = ZPAV1.PimAttributeId)
		--VariantImagePath
		LEFT  JOIN ZnodePimAttributeValue ZPAV12 ON (ZPAV12.PimProductId= YUP.PimProductId  AND ZPAV12.PimAttributeId = @PimMediaAttributeId ) 
		LEFT JOIN ZnodePimProductAttributeMedia ZPAVM ON (ZPAVM.PimAttributeValueId= ZPAV12.PimAttributeValueId ) 
		LEFT JOIN ZnodeMedia ZM ON (ZM.MediaId = ZPAVM.MediaId)
		WHERE (YUP.PimParentProductId  = UOP.PimProductId AND ZPAV1.pimAttributeId = UOP.PimAttributeId )
		-- Active Variants
		AND ZPAV.PimAttributeId = (SELECT TOP 1 PimAttributeId FROM ZnodePimAttribute WHERE AttributeCode = 'IsActive')
		-- VariantSKU
		AND ZPAV_SKU.PimAttributeId = (SELECT PimAttributeId FROM ZnodePimAttribute WHERE AttributeCode = 'SKU')
		For JSON Auto 
		),'[]')) 
				
		--</AttributeEntity></Attributes>' 
		FROM ZnodePimConfigureProductAttribute UOP 
		INNER JOIN ZnodePimAttributeJSON c   ON (c.PimAttributeId = UOP.PimAttributeId )
		WHERE  exists(select * from #TBL_PublishCatalogId PPCP1 where UOP.PimProductId = PPCP1.PimProductId )
		AND EXISTS (select * from #PimProductAttributeJson b where b.PimAttributeJsonId = c.PimAttributeJsonId)

			-------------configurable attribute 
			---------------------------------------------------------------------
			
			If (@RevisionType like '%Preview%'  OR @RevisionType like '%Production%'  ) 
				Delete from ZnodePublishProductEntity where SKU  in (select SKU from #TBL_PublishCatalogId
				A inner join ZnodePublishProductDetail B on A.PublishProductId   =B.PublishProductId   )
				AND LocaleId = @LocaleId
				AND VersionId in (SELECT VersionId FROM ZnodePublishVersionEntity where RevisionType = 'PREVIEW')
			If (@RevisionType like '%Production%' OR @RevisionType = 'None')
				Delete from ZnodePublishProductEntity where SKU  in (select SKU from #TBL_PublishCatalogId
				A inner join ZnodePublishProductDetail B on A.PublishProductId   =B.PublishProductId   )
				AND LocaleId = @LocaleId
				AND VersionId in (SELECT VersionId FROM ZnodePublishVersionEntity where RevisionType = 'PRODUCTION')

			Insert into ZnodePublishProductEntity (
					VersionId, --1
					IndexId, --2 
					ZnodeProductId,ZnodeCatalogId, --3
					SKU,LocaleId, --4 
					Name,ZnodeCategoryIds, --5
					IsActive, -- 6 
					Attributes, -- 7 
					Brands, -- 9
					CategoryName, --9
					CatalogName,DisplayOrder, --10 
					RevisionType,AssociatedProductDisplayOrder, --11
					ProductIndex,--12
					SalesPrice,RetailPrice,CultureCode,CurrencySuffix,CurrencyCode,SeoDescription,SeoKeywords,SeoTitle,SeoUrl,ImageSmallPath,SKULower --13 
					)
 			SELECT distinct ZPVE.VersionId, --1 
			CAST(ISNULL(ZPCP.ProductIndex,1) AS VARCHAr(100)) + CAST(ISNULL(ZPC.PublishCategoryId,'')  AS VARCHAR(50))  + 
			CAST(Isnull(ZPP.PublishCatalogId ,'')  AS VARCHAR(50)) + CAST( @LocaleId AS VARCHAR(50)) IndexId, --2 
			CAST(ZPP.PublishProductId AS VARCHAR(50)) PublishProductId,CAST(ZPP.PublishCatalogId  AS VARCHAR(50)) PublishCatalogId,  --3 
			CAST(ISNULL(ZPPDFG.SKU ,'') AS NVARCHAR(2000)) SKU,CAST( Isnull(@LocaleId ,'') AS VARCHAR(50)) LocaleId, -- 4 
			CAST(isnull(ZPPDFG.ProductName,'') AS NVARCHAR(2000) )  ProductName ,CAST(ISNULL(ZPCD.PublishCategoryId,'')  AS VARCHAR(50)) PublishCategoryId  -- 5 
			,CAST(ISNULL(ZPPDFG.IsActive ,'0') AS VARCHAR(50)) IsActive , --6 
			'[' +
				(Select STUFF((SELECT distinct ','+ AttributeValue from #TBL_ZnodeTempPublish TY WHERE TY.PimProductId = ZPP.PimProductId   
				FOR XML Path ('')) ,1,1,'')  ) 
			+ ']' xmlvalue,  -- 7 
			'[]' Brands  --8 
			,CAST(isnull(PublishCategoryName,'') AS NVARCHAR(2000)) CategoryName  --9
			,CAST(Isnull(CatalogName,'')  AS NVARCHAR(2000)) CatalogName,CAST(ISNULL(ZPCCF.DisplayOrder,'') AS VARCHAR(50)) DisplayOrder  -- 10  
			,ZPVE.RevisionType RevisionType , 0 AssociatedProductDisplayOrder,-- pending  -- 11 
			Isnull(ZPCP.ProductIndex,1),  -- 12 

			Case When TBZP.IsAllowIndexing = 1 then  ISNULL(CAST(SalesPrice  AS varchar(500)),'') else '' end SalesPrice , 
			Case When TBZP.IsAllowIndexing = 1 then  ISNULL(CAST(RetailPrice  AS varchar(500)),'') else '' end RetailPrice , 
			Case When TBZP.IsAllowIndexing = 1 then  ISNULL(CultureCode ,'') else '' end CultureCode , 
			Case When TBZP.IsAllowIndexing = 1 then  ISNULL(CurrencySuffix ,'') else '' end CurrencySuffix , 
			Case When TBZP.IsAllowIndexing = 1 then  ISNULL(CurrencyCode ,'') else '' end CurrencyCode , 
			Case When TBZP.IsAllowIndexing = 1 then  ISNULL(SEODescription,'') else '' end SEODescriptionForIndex,
			Case When TBZP.IsAllowIndexing = 1 then  ISNULL(SEOKeywords,'') else '' end SEOKeywords,
			Case When TBZP.IsAllowIndexing = 1 then  ISNULL(SEOTitle,'') else '' end SEOTitle,
			Case When TBZP.IsAllowIndexing = 1 then  ISNULL(SEOUrl ,'') else '' end SEOUrl,
			Case When TBZP.IsAllowIndexing = 1 then  ISNULL(ImageSmallPath,'') else '' end ImageSmallPath,
			CAST(ISNULL(LOWER(ZPPDFG.SKU) ,'') AS NVARCHAR(100)) Lower_SKU -- 13
	FROM  #TBL_PublishCatalogId zpp
	INNER JOIN ZnodePublishCatalog ZPCV ON (ZPCV.PublishCatalogId = ZPP.PublishCatalogId)
	INNER JOIN ZnodePublishProductDetail ZPPDFG ON (ZPPDFG.PublishProductId =  ZPP.PublishProductId)
	INNER JOIN ZnodePublishVersionEntity ZPVE ON (ZPVE.ZnodeCatalogId  = ZPP.PublishCatalogId AND ZPVE.IsPublishSuccess =1 AND ZPVE.LocaleId = @LocaleId )
	LEFT JOIN #ZnodePrice TBZP ON (TBZP.PublishProductId = ZPP.PublishProductId)
	LEFT JOIN #ProductSKU TBPS ON (TBPS.PublishProductId = ZPP.PublishProductId)
	LEFT JOIN #ProductImages TBPI ON (TBPI.PublishProductId = ZPP.PublishProductId  )
	LEFT JOIN ZnodePublishCategoryProduct ZPCP ON (ZPCP.PublishProductId = ZPP.PublishProductId AND ZPCP.PublishCatalogId = ZPP.PublishCatalogId)
	LEFT JOIN ZnodePublishCategory ZPC ON (ZPC.PublishCatalogId = ZPCP.PublishCatalogId AND   ZPC.PublishCategoryId = ZPCP.PublishCategoryId)
	LEFT JOIN ZnodePimCategoryProduct ZPCCF ON (ZPCCF.PimCategoryId = ZPC.PimCategoryId  AND ZPCCF.PimProductId = ZPP.PimProductId )
	LEFT JOIN ZnodePimCategoryHierarchy ZPCH ON (ZPCH.PimCatalogId = ZPCV.PimCatalogId AND  ZPCH.PimCategoryHierarchyId =  ZPC.PimCategoryHierarchyId) 
	LEFT JOIN ZnodePublishCategoryDetail ZPCD ON (ZPCD.PublishCategoryId = ZPCP.PublishCategoryId AND ZPCD.LocaleId = @LocaleId )
	WHERE ZPPDFG.LocaleId = @LocaleId
		--AND zpp.LocaleId = @LocaleId
	AND 
		(
			(ZPVE.RevisionType =  Case when  (@RevisionType like '%Preview%'  OR @RevisionType like '%Production%' ) then 'Preview' End ) 
			OR 
			(ZPVE.RevisionType =  Case when (@RevisionType like '%Production%' OR @RevisionType = 'None') then  'Production'  end )
		)


	DELETE FROM #TBL_ZnodeTempPublish
	IF OBJECT_ID('tempdb..#PimProductAttributeJson') is not null
	 BEGIN 
		DELETE FROM #PimProductAttributeJson
	 END
	 IF OBJECT_ID('tempdb..#TBL_CustomeFiled') is not null
	 BEGIN 
	 DROP TABLE #TBL_CustomeFiled
	 END
	 IF OBJECT_ID('tempdb..#TBL_AttributeVAlue') is not null
	 BEGIN 
	 DROP TABLE #TBL_AttributeVAlue
	 END
 
	DELETE FROM @PimDefaultValueLocale
SET @Counter = @counter + 1 
END

SET @Status =1 

END TRY 
BEGIN CATCH 
	SET @Status =0  
	 SELECT 1 AS ID,@Status AS Status;   
	 DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), 
		@ErrorLine VARCHAR(100)= ERROR_LINE(),
		@ErrorCall NVARCHAR(MAX)= 'EXEC [Znode_GetPublishSingleProductJson] 
		@PublishCatalogId = '+CAST(@PublishCatalogId  AS VARCHAR	(max))+',@UserId='+CAST(@UserId AS VARCHAR(50))+',@Status='+CAST(@Status AS VARCHAR(10))
				
	EXEC Znode_InsertProcedureErrorLog
		@ProcedureName = 'Znode_GetPublishSingleProductJson',
		@ErrorInProcedure = @Error_procedure,
		@ErrorMessage = @ErrorMessage,
		@ErrorLine = @ErrorLine,
		@ErrorCall = @ErrorCall;
END CATCH
END
GO
-----
IF EXISTS(SELECT * FROM SYS.PROCEDURES WHERE NAME = 'Znode_RevertOrderInventory')
	DROP PROC Znode_RevertOrderInventory
GO
CREATE PROCEDURE [dbo].[Znode_RevertOrderInventory]
(   
	@OmsOrderDetailsId INT,
    @OmsOrderLineItemIds VARCHAR(2000) = '',
    @Status BIT OUT,
    @UserId INT,
	@IsRevertAll BIT = 0
)
AS 
   /* Summary: this proceedure is used to revert the order  inventory in case of order revert
      Unit Testing:
	  begin tran
	  EXEC  Znode_RevertOrderInventory 
      rollback tran
    */
BEGIN
BEGIN TRAN ZROI;
BEGIN TRY
		SET NOCOUNT ON;
		DECLARE @GetDate DATETIME = dbo.Fn_GetDate();
		DECLARE @Revert NVARCHAR(MAX), @SQL NVARCHAR(MAX)
		CREATE TABLE  #OmsOrderLineItemsId (OmsOrderLineItemsId INT);
		--Getting line item ids
		INSERT INTO #OmsOrderLineItemsId
		SELECT item
		FROM dbo.split(@OmsOrderLineItemIds, ',');

		--Getting parent line item ids for downloadable product
		INSERT INTO #OmsOrderLineItemsId
		SELECT ZOOLI.ParentOmsOrderLineItemsId 
		FROM ZnodeOmsOrderLineItems ZOOLI WITH (NOLOCK)
		INNER JOIN #OmsOrderLineItemsId OLI on ZOOLI.OmsOrderLineItemsId=OLI.OmsOrderLineItemsId
		WHERE EXISTS (SELECT * FROM ZnodeOmsDownloadableProductKey Z where Z.OmsOrderLineItemsId=ZOOLI.ParentOmsOrderLineItemsId)
		AND NOT EXISTS (SELECT * FROM #OmsOrderLineItemsId O where O.OmsOrderLineItemsId=ZOOLI.ParentOmsOrderLineItemsId)

		--Getting parent line item ids for downloadable product on basis of OmsOrderDetailsId
		INSERT INTO #OmsOrderLineItemsId
		SELECT ZOOLI.OmsOrderLineItemsId 
		FROM ZnodeOmsOrderLineItems ZOOLI WITH (NOLOCK)
		WHERE ZOOLI.OmsOrderDetailsId = @OmsOrderDetailsId and ZOOLI.OrderLineItemStateId=40
		AND EXISTS (SELECT * FROM ZnodeOmsDownloadableProductKey Z WHERE Z.OmsOrderLineItemsId=ZOOLI.OmsOrderLineItemsId)
		AND NOT EXISTS (SELECT * FROM #OmsOrderLineItemsId O WHERE O.OmsOrderLineItemsId=ZOOLI.OmsOrderLineItemsId)

		--Updating the used key for revert order of downloadable product
		IF EXISTS(SELECT * FROM #OmsOrderLineItemsId)
		BEGIN
			UPDATE ZnodePimDownloadableProductKey
			SET IsUsed = 0,	
				ModifiedDate= GETDATE()
			WHERE EXISTS (SELECT TOP 1 1 FROM ZnodeOmsDownloadableProductKey ZODPK 
				INNER JOIN #OmsOrderLineItemsId OOLI ON OOLI.OmsOrderLineItemsId= ZODPK.OmsOrderLineItemsId
				WHERE ZnodePimDownloadableProductKey.PimDownloadableProductKeyId = ZODPK.PimDownloadableProductKeyId)
		END
		
		--Reverting the quantity of produucts after return
		SET @SQL = '
			
		UPDATE ZI
		SET
			ZI.Quantity = ZI.Quantity + ZOOW.Quantity,
			ZI.MOdifiedBy = '''+CAST(@UserId AS VARCHAR(1000))+''',
			ZI.ModifiedDate = '''+CAST(@GetDate AS VARCHAR(1000))+'''
		FROM ZnodeOmsOrderWarehouse ZOOW
			INNER JOIN ZnodeOmsOrderLineItems ZOOLI ON(ZOOLI.OmsOrderLineItemsId = ZOOW.OmsOrderLineItemsId)
			INNER JOIN ZnodeInventory ZI ON(ZI.WarehouseId = ZOOW.WarehouseId
											AND ZI.SKU = ZOOLI.SKU)
		WHERE 
		ZOOLI.OmsOrderDetailsId = '+CAST(@OmsOrderDetailsId AS VARCHAR(1000))+'
			AND EXISTS
		(
			SELECT TOP 1 1     FROM #OmsOrderLineItemsId SP WHERE Sp.OmsOrderLineItemsId = ZOOLI.OmsOrderLineItemsId OR Sp.OmsOrderLineItemsId = ZOOLI.ParentOmsOrderLineItemsId OR '''+@OmsOrderLineItemIds+''' = ''''
		)  
		' +CASE WHEN @IsRevertAll = 0 THEN 'AND NOT EXISTS   (SELECT TOP  1 1 FROM ZnodeOmsOrderState OOS WHERE OOS.OrderStateName = ''RETURNED'' AND OOS.OmsOrderStateId = ZOOLI.OrderLineItemStateId) ' ELSE '' END

		EXEC(@SQL)

		SET @Status = 1;
		SELECT 1 ID, CAST(1 AS BIT) Status;

COMMIT TRAN ZROI;
END TRY
BEGIN CATCH
    DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(),
	@ErrorCall NVARCHAR(MAX)= 'EXEC Znode_RevertOrderInventory @OmsOrderDetailsId = '+CAST(@OmsOrderDetailsId AS VARCHAR(200))+',@OmsOrderLineItemIds='+@OmsOrderLineItemIds+',@UserId='+CAST(@UserId AS VARCHAR(200))+',@Status='+CAST(@Status AS VARCHAR(200));
    SET @Status = 0;
    SELECT 0 AS ID,
        CAST(0 AS BIT) AS Status;
	ROLLBACK TRAN ZROI;
    EXEC Znode_InsertProcedureErrorLog
        @ProcedureName = 'Znode_RevertOrderInventory',
        @ErrorInProcedure = @Error_procedure,
        @ErrorMessage = @ErrorMessage,
        @ErrorLine = @ErrorLine,
        @ErrorCall = @ErrorCall;
END CATCH;
END
GO
IF EXISTS(SELECT * FROM SYS.PROCEDURES WHERE NAME = 'Znode_UpdatePimDownloadableProductKey')
	DROP PROC Znode_UpdatePimDownloadableProductKey
GO
CREATE PROCEDURE [dbo].[Znode_UpdatePimDownloadableProductKey]
(   
	@OrderLineItemDataModel XML,
	@Status INT = 0 OUT
)
AS
  /* 
	Summary :- This Procedure is used to update the Znode_UpdatePimDownloadableProductKey 
				
	
   */
BEGIN 
BEGIN TRY
BEGIN TRAN UpdatePimDownloadableProductKey;
SET NOCOUNT ON;
	CREATE TABLE #SKU (Sku INT);
			 
	IF OBJECT_ID('tempdb..#SkuQuantityUpdate') IS NOT NULL 
		DROP TABLE #SkuQuantityUpdate
    --Getting xml data into table
	SELECT  Tbl.Col.value ( 'OmsOrderLineItemsId[1]' , 'INT') AS OmsOrderLineItemsId
		,Tbl.Col.value ( 'ParentOmsOrderLineItemsId[1]' , 'INT') AS ParentOmsOrderLineItemsId,
		Tbl.Col.value ( 'Sku[1]' , 'NVARCHAR(max)') AS Sku
		,Tbl.Col.value ( 'Quantity[1]' , 'Numeric(28,6)') AS Quantity
	INTO #SkuQuantityUpdate
	FROM @OrderLineItemDataModel.nodes ( '//ArrayOfOrderLineItemDataModel/OrderLineItemDataModel'  ) AS Tbl(Col)

	ALTER TABLE #SkuQuantityUpdate ADD OmsLineItemId INT,PimDownloadableProductKeyId INT;

	UPDATE #SkuQuantityUpdate
	SET OmsLineItemId = OmsOrderLineItemsId;

	--Updating the ParentOmsOrderLineItemsId
	UPDATE OLI
	SET OmsLineItemId =  ZOOLI.ParentOmsOrderLineItemsId,PimDownloadableProductKeyId=Z.PimDownloadableProductKeyId
	FROM ZnodeOmsOrderLineItems ZOOLI WITH (NOLOCK)
	INNER JOIN #SkuQuantityUpdate OLI ON ZOOLI.OmsOrderLineItemsId=OLI.OmsOrderLineItemsId
	INNER JOIN ZnodeOmsDownloadableProductKey Z WITH (NOLOCK) ON Z.OmsOrderLineItemsId=ZOOLI.ParentOmsOrderLineItemsId
	WHERE NOT EXISTS (SELECT * FROM #SkuQuantityUpdate O WHERE O.OmsOrderLineItemsId=ZOOLI.ParentOmsOrderLineItemsId)

    IF OBJECT_ID('tempdb..#FinalProductKey') IS NOT NULL 
		DROP TABLE #FinalProductKey
		
	--Getting the product key of downloadable products
	SELECT ZPDPK.PimDownloadableProductKeyId,S.Quantity, RANK() OVER(PARTITION BY ZPDPK.PimDownloadableProductKeyId ORDER BY ZPDPK.PimDownloadableProductKeyId DESC) RNK  
	INTO #FinalProductKey 
	FROM ZnodePimDownloadableProductKey ZPDPK WITH (NOLOCK)
	INNER JOIN ZnodePimDownloadableProduct ZPDP WITH (NOLOCK) ON ZPDP.PimDownloadableProductId = ZPDPK.PimDownloadableProductId
	INNER JOIN #SkuQuantityUpdate S ON  ZPDP.SKU=S.Sku AND S.PimDownloadableProductKeyId = ZPDPK.PimDownloadableProductKeyId

	--Update IsUsed for downloadable products for return 
	UPDATE ZnodePimDownloadableProductKey
	SET IsUsed = 0		
	WHERE PimDownloadableProductKeyId IN (SELECT ISNULL(PimDownloadableProductKeyId,0) FROM #FinalProductKey WHERE RNK <= Quantity )

	SET @Status = 1
	SELECT 1 AS ID,CAST(@Status AS BIT) AS Status; 

COMMIT TRAN UpdatePimDownloadableProductKey;
END TRY
BEGIN CATCH
           	     
	DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(),
	@ErrorCall NVARCHAR(MAX)= 'EXEC Znode_UpdatePimDownloadableProductKey @OrderLineItemDataModel = '+ cast(@OrderLineItemDataModel as varchar(2000));
    SET @Status = 0          			 
	SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                    
	ROLLBACK TRAN UpdatePimDownloadableProductKey;	  
	EXEC Znode_InsertProcedureErrorLog
	@ProcedureName = 'Znode_UpdatePimDownloadableProductKey',
	@ErrorInProcedure = @Error_procedure,
	@ErrorMessage = @ErrorMessage,
	@ErrorLine = @ErrorLine,
	@ErrorCall = @ErrorCall;
END CATCH;
END
GO
-----

IF EXISTS(SELECT * FROM SYS.PROCEDURES WHERE NAME = 'Znode_InsertUpdatePimCatalogProductDetailJson')
	DROP PROC Znode_InsertUpdatePimCatalogProductDetailJson
GO

CREATE PROCEDURE [dbo].[Znode_InsertUpdatePimCatalogProductDetailJson] 
(
	@PublishCatalogId INT = 0, 
	@LocaleId TransferId READONLY, 
	@UserId INT = 0   
)
AS 

--declare @LocaleId TransferId
--INSERT INTO @LocaleId
--SELECT 1
--exec [Znode_InsertUpdatePimCatalogProductDetailJson] @PublishCatalogId=3,@LocaleId=@LocaleId,@UserId=2

--declare @LocaleId TransferId
--INSERT INTO @LocaleId
--SELECT 1
----union 
----SELECT 4
----union 
----SELECT 2
--exec [Znode_POC_InsertUpdatePimCatalogProductDetail] @PublishCatalogId=3,@LocaleId=@LocaleId,@UserId=2
BEGIN 
 BEGIN TRY 

  SET NOCOUNT ON 
       DECLARE @LocaleId_In INT = 0 , @DefaultLocaleId INT = dbo.FN_GETDefaultLocaleId()
			   ,@Date DATETIME = dbo.fn_GetDate()
	   DECLARE @PimMediaAttributeId INT = dbo.Fn_GetProductImageAttributeId()		   

	   CREATE TABLE #PimDefaultValueLocale  (PimAttributeDefaultJsonId INT  PRIMARY KEY ,PimAttributeDefaultValueId INT ,LocaleId INT, DefaultValueJson	nvarchar(max) )

	   CREATE TABLE #AttributeValueLocale  (Id int Identity,  PimProductId INT, AttributeCode Varchar(300), AttributeValue varchar(max), AttributeEntity varchar(max), LocaleId int )

	    SELECT ZPAV.PimProductId, ZPP.PublishProductId, ZPAVL.LocaleId
		INTO #ProductLocaleWise
		FROM ZnodePimAttributeValue ZPAV 
		INNER JOIN ZnodePimAttributeValueLocale ZPAVL ON ZPAV.PimAttributeValueId = ZPAVL.PimAttributeValueId
		INNER JOIN ZnodePublishProduct ZPP ON ZPAV.PimProductId = ZPP.PimProductId
		WHERE ZPAV.PimAttributeId = (SELECT PimAttributeId FROM ZnodePimAttribute WHERE AttributeCode = 'SKU')

		SELECT BTM.PimProductId , ZPCPD.PublishProductId, ZPCPD.PublishCatalogId,BTM.ModifiedDate
		INTO #ProductAttributeXML
		FROM ZnodePublishProductAttributeJson BTM 
		INNER JOIN ZnodePublishProduct ZPP1 ON BTM.PimProductId = ZPP1.PimProductId
		INNER JOIN ZnodePublishCatalogProductDetail ZPCPD ON ZPP1.PublishProductId = ZPCPD.PublishProductId AND ZPCPD.PublishCatalogId = ZPP1.PublishCatalogId 
		WHERE ZPCPD.PublishCatalogId =  @PublishCatalogId 

	    -------- Products Attribute modified 
		SELECT DISTINCT ZPP.PublishProductId,  ZPCC.PimCategoryHierarchyId 
		INTO #ModifiedProducts
		FROM ZnodePublishProduct  ZPP
		INNER JOIN ZnodePimProduct ZPPI ON (ZPPI.PimProductId = ZPP.PimProductId)
		INNER JOIN ZnodePimAttributeValue ZPAV ON (ZPAV.PimProductId = ZPP.PimProductId )
		INNER JOIN ZnodePimAttribute ZPA ON (ZPA.PimAttributeId = ZPAV.PimAttributeId)
		INNER JOIN ZnodePublishCatalog ZPC ON (ZPC.PublishCatalogId = ZPP.PublishCatalogId)
		LEFT JOIN ZnodePublishCategoryProduct ZPCC  ON (ZPP.PublishProductId = ZPCC.PublishProductId AND ZPCC.PublishCatalogId = ZPP.PublishCatalogId)
		LEFT JOIN ZnodePublishCategory ZPPC ON (ISNULL(ZPPC.PimCategoryHierarchyId,0) = ISNULL(ZPCC.PimCategoryHierarchyId,0) AND ZPPC.PublishCategoryId = ZPCC.PublishCategoryId)
		WHERE ZPP.PublishCatalogId =  @PublishCatalogId 
		AND EXISTS(SELECT * FROM ZnodePimFamilyGroupMapper ZPFGM WHERE (ZPFGM.PimAttributeFamilyId = ZPPI.PimAttributeFamilyId AND ZPFGM.PimAttributeId = ZPAV.PimAttributeId))
		AND EXISTS (SELECT TOP 1 1 FROM #ProductAttributeXML BTM WHERE BTM.PimProductId = ZPP.PimProductId AND BTM.PublishCatalogId = ZPP.PublishCatalogId
						AND (BTM.ModifiedDate < ZPAV.ModifiedDate OR BTM.ModifiedDate < ZPA.ModifiedDate)   ) 
		
		-------- Products not published  
		INSERT INTO #ModifiedProducts
		SELECT ZPP.PublishProductId,  ZPCC.PimCategoryHierarchyId 
		FROM ZnodePublishProduct  ZPP
		INNER JOIN ZnodePimProduct ZPPI ON (ZPPI.PimProductId = ZPP.PimProductId)
		INNER JOIN ZnodePublishCatalog ZPC ON (ZPC.PublishCatalogId = ZPP.PublishCatalogId)
		LEFT JOIN ZnodePublishCategoryProduct ZPCC  ON (ZPP.PublishProductId = ZPCC.PublishProductId AND ZPCC.PublishCatalogId = ZPP.PublishCatalogId)
		LEFT JOIN ZnodePublishCategory ZPPC ON (ISNULL(ZPPC.PimCategoryHierarchyId,0) = ISNULL(ZPCC.PimCategoryHierarchyId,0) AND ZPPC.PublishCategoryId = ZPCC.PublishCategoryId)
		WHERE ZPP.PublishCatalogId =  @PublishCatalogId 
		AND EXISTS(SELECT * FROM ZnodePimFamilyGroupMapper ZPFGM WHERE (ZPFGM.PimAttributeFamilyId = ZPPI.PimAttributeFamilyId ))--AND ZPFGM.PimAttributeId = ZPAV.PimAttributeId))
		AND EXISTS(SELECT * FROM ZnodePimProduct ZPP1 INNER JOIN ZnodePublishState ZPS ON ZPP1.PublishStateId = ZPS.PublishStateId
					WHERE StateName <> 'Publish' AND ZPP.PimProductId = ZPP1.PimProductId )	
			
		-------- Products associated to catalog or category or modified catalog category products
		INSERT INTO #ModifiedProducts		
		SELECT ZPP.PublishProductId,  ZPCC.PimCategoryHierarchyId 
		FROM ZnodePublishProduct  ZPP
		INNER JOIN ZnodePimProduct ZPPI ON (ZPPI.PimProductId = ZPP.PimProductId)
		INNER JOIN ZnodePublishCatalog ZPC ON (ZPC.PublishCatalogId = ZPP.PublishCatalogId)
		INNER JOIN ZnodePimCategoryProduct ZPCC1 ON  ZPP.PimProductId = ZPCC1.PimProductId 
		INNER JOIN ZnodePimCategoryHierarchy ZPCH ON ZPCC1.PimCategoryId = ZPCH.PimCategoryId AND ZPC.PimCatalogId = ZPCH.PimCatalogId 
		LEFT JOIN ZnodePublishCategoryProduct ZPCC  ON (ZPP.PublishProductId = ZPCC.PublishProductId AND ZPCC.PublishCatalogId = ZPP.PublishCatalogId)
		LEFT JOIN ZnodePublishCategory ZPPC ON (ISNULL(ZPPC.PimCategoryHierarchyId,0) = ISNULL(ZPCC.PimCategoryHierarchyId,0) AND ZPPC.PublishCategoryId = ZPCC.PublishCategoryId)
		WHERE ZPP.PublishCatalogId =  @PublishCatalogId 
		AND EXISTS(SELECT * FROM ZnodePimFamilyGroupMapper ZPFGM WHERE (ZPFGM.PimAttributeFamilyId = ZPPI.PimAttributeFamilyId ))--AND ZPFGM.PimAttributeId = ZPAV.PimAttributeId))
		AND EXISTS (SELECT TOP 1 1 FROM #ProductAttributeXML BTM WHERE BTM.PimProductId = ZPCC1.PimProductId AND BTM.PublishCatalogId = ZPP.PublishCatalogId
						AND (BTM.ModifiedDate < ZPCC1.ModifiedDate )   ) 

		-------- Link Product modified 
		INSERT INTO #ModifiedProducts	
		SELECT ZPP.PublishProductId,  ZPCC.PimCategoryHierarchyId 
		FROM ZnodePublishProduct  ZPP
		INNER JOIN ZnodePimProduct ZPPI ON (ZPPI.PimProductId = ZPP.PimProductId)
		INNER JOIN ZnodePimLinkProductDetail ZPAV ON (ZPAV.PimParentProductId = ZPP.PimProductId )
		INNER JOIN ZnodePublishCatalog ZPC ON (ZPC.PublishCatalogId = ZPP.PublishCatalogId)
		LEFT JOIN ZnodePublishCategoryProduct ZPCC  ON (ZPP.PublishProductId = ZPCC.PublishProductId AND ZPCC.PublishCatalogId = ZPP.PublishCatalogId)
		LEFT JOIN ZnodePublishCategory ZPPC ON (ISNULL(ZPPC.PimCategoryHierarchyId,0) = ISNULL(ZPCC.PimCategoryHierarchyId,0) AND ZPPC.PublishCategoryId = ZPCC.PublishCategoryId)
		WHERE ZPP.PublishCatalogId =  @PublishCatalogId 
		--AND EXISTS(SELECT * FROM ZnodePimFamilyGroupMapper ZPFGM WHERE (ZPFGM.PimAttributeFamilyId = ZPPI.PimAttributeFamilyId AND ZPFGM.PimAttributeId = ZPAV.PimAttributeId))
		AND EXISTS (SELECT TOP 1 1 FROM #ProductAttributeXML BTM WHERE BTM.PimProductId = ZPP.PimProductId AND BTM.PublishCatalogId = ZPP.PublishCatalogId
						AND (BTM.ModifiedDate < ZPAV.ModifiedDate)   ) 

		--------Associated child Products (varients, Group) not published	
		INSERT INTO #ModifiedProducts	
		SELECT ZPP.PublishProductId,  ZPCC.PimCategoryHierarchyId 
		FROM ZnodePublishProduct  ZPP
		INNER JOIN ZnodePimProduct ZPPI ON (ZPPI.PimProductId = ZPP.PimProductId)
		INNER JOIN ZnodePimProductTypeAssociation ZPAV ON (ZPAV.PimProductId = ZPP.PimProductId )
		INNER JOIN ZnodePublishCatalog ZPC ON (ZPC.PublishCatalogId = ZPP.PublishCatalogId)
		LEFT JOIN ZnodePublishCategoryProduct ZPCC  ON (ZPP.PublishProductId = ZPCC.PublishProductId AND ZPCC.PublishCatalogId = ZPP.PublishCatalogId)
		LEFT JOIN ZnodePublishCategory ZPPC ON (ISNULL(ZPPC.PimCategoryHierarchyId,0) = ISNULL(ZPCC.PimCategoryHierarchyId,0) AND ZPPC.PublishCategoryId = ZPCC.PublishCategoryId)
		WHERE ZPP.PublishCatalogId =  @PublishCatalogId 
		AND EXISTS(SELECT * FROM ZnodePimProduct ZPP1 INNER JOIN ZnodePublishState ZPS ON ZPP1.PublishStateId = ZPS.PublishStateId
					WHERE StateName <> 'Publish' AND ZPAV.PimProductId = ZPP1.PimProductId )


		--------Link child Products (Bundle) not published 	
		INSERT INTO #ModifiedProducts
		SELECT ZPP.PublishProductId,  ZPCC.PimCategoryHierarchyId 
		FROM ZnodePublishProduct  ZPP
		INNER JOIN ZnodePimProduct ZPPI ON (ZPPI.PimProductId = ZPP.PimProductId)
		INNER JOIN ZnodePimLinkProductDetail ZPAV ON (ZPAV.PimProductId = ZPP.PimProductId )
		INNER JOIN ZnodePublishCatalog ZPC ON (ZPC.PublishCatalogId = ZPP.PublishCatalogId)
		LEFT JOIN ZnodePublishCategoryProduct ZPCC  ON (ZPP.PublishProductId = ZPCC.PublishProductId AND ZPCC.PublishCatalogId = ZPP.PublishCatalogId)
		LEFT JOIN ZnodePublishCategory ZPPC ON (ISNULL(ZPPC.PimCategoryHierarchyId,0) = ISNULL(ZPCC.PimCategoryHierarchyId,0) AND ZPPC.PublishCategoryId = ZPCC.PublishCategoryId)
		WHERE ZPP.PublishCatalogId =  @PublishCatalogId 
		AND EXISTS(SELECT * FROM ZnodePimProduct ZPP1 INNER JOIN ZnodePublishState ZPS ON ZPP1.PublishStateId = ZPS.PublishStateId
					WHERE StateName <> 'Publish' AND ZPAV.PimProductId = ZPP1.PimProductId )

		----Getting products of newly added category hierarchy 
		INSERT INTO #ModifiedProducts		
		SELECT ZPP.PublishProductId,  ZPCC.PimCategoryHierarchyId
		FROM ZnodePublishProduct  ZPP
		INNER JOIN ZnodePimProduct ZPPI ON (ZPPI.PimProductId = ZPP.PimProductId)
		INNER JOIN ZnodePublishCatalog ZPC ON (ZPC.PublishCatalogId = ZPP.PublishCatalogId)
		INNER JOIN ZnodePimCategoryProduct ZPCC1 ON  ZPP.PimProductId = ZPCC1.PimProductId 
		INNER JOIN ZnodePimCategoryHierarchy ZPCH ON ZPCC1.PimCategoryId = ZPCH.PimCategoryId AND ZPC.PimCatalogId = ZPCH.PimCatalogId 
		LEFT JOIN ZnodePublishCategoryProduct ZPCC  ON (ZPP.PublishProductId = ZPCC.PublishProductId AND ZPCC.PublishCatalogId = ZPP.PublishCatalogId)
		LEFT JOIN ZnodePublishCategory ZPPC ON (ISNULL(ZPPC.PimCategoryHierarchyId,0) = ISNULL(ZPCC.PimCategoryHierarchyId,0) AND ZPPC.PublishCategoryId = ZPCC.PublishCategoryId)
		WHERE ZPP.PublishCatalogId =  @PublishCatalogId 
		AND EXISTS(SELECT * FROM ZnodePimFamilyGroupMapper ZPFGM WHERE (ZPFGM.PimAttributeFamilyId = ZPPI.PimAttributeFamilyId ))--AND ZPFGM.PimAttributeId = ZPAV.PimAttributeId))
		AND NOT EXISTS(SELECT * FROM ZnodePublishCatalogProductDetail ZPCPPD WHERE ZPCPPD.PimCategoryHierarchyId =  ZPCH.PimCategoryHierarchyId AND ZPCPPD.PublishCatalogId = ZPP.PublishCatalogId ) 

		---------------------Category associated to catalog or category or modified catalog
		SELECT ZPCH.PimCategoryId, ZPC1.PublishCategoryId, ZPCH.PimCategoryHierarchyId
		INTO #ModifiedCategory
		FROM ZnodePimCategoryHierarchy ZPCH 
		INNER JOIN ZnodePublishCategory ZPC1 ON ZPCH.PimCategoryId = ZPC1.PimCategoryId 
        WHERE ZPC1.PublishCatalogId =  @PublishCatalogId 
		AND EXISTS (SELECT TOP 1 1 FROM ZnodePublishCatalogProductDetail BTM  
		WHERE BTM.PublishCatalogId = ZPC1.PublishCatalogId AND (BTM.ModifiedDate < ZPCH.ModifiedDate )   )
		AND NOT EXISTS(SELECT * FROM #ModifiedProducts MP WHERE  ISNULL(ZPCH.PimCategoryHierarchyId,0) = ISNULL(MP.PimCategoryHierarchyId,0))

		-------- Category associated to catalog or category or modified catalog
		INSERT INTO #ModifiedProducts		
		SELECT ZPP.PublishProductId,  ZPCC.PimCategoryHierarchyId 
		FROM ZnodePublishProduct  ZPP
		INNER JOIN ZnodePimProduct ZPPI ON (ZPPI.PimProductId = ZPP.PimProductId)
		INNER JOIN ZnodePublishCatalog ZPC ON (ZPC.PublishCatalogId = ZPP.PublishCatalogId)
		INNER JOIN ZnodePimCategoryProduct ZPCC1 ON  ZPP.PimProductId = ZPCC1.PimProductId 
		INNER JOIN ZnodePimCategoryHierarchy ZPCH ON ZPCC1.PimCategoryId = ZPCH.PimCategoryId AND ZPC.PimCatalogId = ZPCH.PimCatalogId 
		LEFT JOIN ZnodePublishCategoryProduct ZPCC  ON (ZPP.PublishProductId = ZPCC.PublishProductId AND ZPCC.PublishCatalogId = ZPP.PublishCatalogId)
		LEFT JOIN ZnodePublishCategory ZPPC ON (ISNULL(ZPPC.PimCategoryHierarchyId,0) = ISNULL(ZPCC.PimCategoryHierarchyId,0) AND ZPPC.PublishCategoryId = ZPCC.PublishCategoryId)
		WHERE ZPP.PublishCatalogId =  @PublishCatalogId 
		AND EXISTS(SELECT * FROM ZnodePimFamilyGroupMapper ZPFGM WHERE (ZPFGM.PimAttributeFamilyId = ZPPI.PimAttributeFamilyId ))
		AND EXISTS (SELECT TOP 1 1 FROM #ModifiedCategory BTM WHERE BTM.PimCategoryHierarchyId = ZPCH.PimCategoryHierarchyId  ) 
		------------------

		--Getting all products of catalog for publish first time 
		SELECT ZPP.PublishProductId,  ZPAV.PimAttributeId, ZPP.PublishCatalogId , ZPCC.PimCategoryHierarchyId , ZPCC.PublishCategoryId,
		       ZPAV.PimAttributeValueId, ZPC.CatalogName ,ZPP.PimProductId ,ZPA.AttributeCode				
		INTO #ZnodePublishCategoryProduct
		FROM ZnodePublishProduct  ZPP
		INNER JOIN ZnodePimProduct ZPPI ON (ZPPI.PimProductId = ZPP.PimProductId)
		INNER JOIN ZnodePimAttributeValue ZPAV ON (ZPAV.PimProductId = ZPP.PimProductId )
		INNER JOIN ZnodePimAttribute ZPA ON (ZPA.PimAttributeId = ZPAV.PimAttributeId)
		INNER JOIN ZnodePublishCatalog ZPC ON (ZPC.PublishCatalogId = ZPP.PublishCatalogId)
		LEFT JOIN ZnodePublishCategoryProduct ZPCC  ON (ZPP.PublishProductId = ZPCC.PublishProductId AND ZPCC.PublishCatalogId = ZPP.PublishCatalogId)
		LEFT JOIN ZnodePublishCategory ZPPC ON (ISNULL(ZPPC.PimCategoryHierarchyId,0) = ISNULL(ZPCC.PimCategoryHierarchyId,0) AND ZPPC.PublishCategoryId = ZPCC.PublishCategoryId)
		WHERE ZPP.PublishCatalogId =  @PublishCatalogId 
		AND EXISTS(SELECT * FROM ZnodePimFamilyGroupMapper ZPFGM WHERE (ZPFGM.PimAttributeFamilyId = ZPPI.PimAttributeFamilyId AND ZPFGM.PimAttributeId = ZPAV.PimAttributeId))
		AND NOT EXISTS (SELECT TOP 1 1 FROM #ProductAttributeXML BTM WHERE BTM.PimProductId = ZPP.PimProductId AND BTM.PublishCatalogId = ZPP.PublishCatalogId)
		
		--Getting all products of catalog for publish which are modified after last publish
		INSERT INTO #ZnodePublishCategoryProduct 
		SELECT ZPP.PublishProductId,  ZPAV.PimAttributeId, ZPP.PublishCatalogId , ZPCC.PimCategoryHierarchyId , ZPCC.PublishCategoryId
			   ,ZPAV.PimAttributeValueId, ZPC.CatalogName--,CASE WHEN ZPCC.PublishProductId IS NULL THEN 1 ELSE  dense_rank()Over(ORDER BY ZPCC.PimCategoryHierarchyId,ZPCC.PublishProductId) END  ProductIndex 	
			   ,ZPP.PimProductId ,ZPA.AttributeCode				
		FROM ZnodePublishProduct  ZPP
		INNER JOIN ZnodePimProduct ZPPI ON (ZPPI.PimProductId = ZPP.PimProductId)
		INNER JOIN ZnodePimAttributeValue ZPAV ON (ZPAV.PimProductId = ZPP.PimProductId )
		INNER JOIN ZnodePimAttribute ZPA ON (ZPA.PimAttributeId = ZPAV.PimAttributeId)
		INNER JOIN ZnodePublishCatalog ZPC ON (ZPC.PublishCatalogId = ZPP.PublishCatalogId)
		LEFT JOIN ZnodePublishCategoryProduct ZPCC  ON (ZPP.PublishProductId = ZPCC.PublishProductId AND ZPCC.PublishCatalogId = ZPP.PublishCatalogId)
		LEFT JOIN ZnodePublishCategory ZPPC ON (ZPPC.PimCategoryHierarchyId = ZPCC.PimCategoryHierarchyId AND ZPPC.PublishCategoryId = ZPCC.PublishCategoryId)
		WHERE ZPP.PublishCatalogId =  @PublishCatalogId 
		AND EXISTS(SELECT * FROM ZnodePimFamilyGroupMapper ZPFGM WHERE (ZPFGM.PimAttributeFamilyId = ZPPI.PimAttributeFamilyId AND ZPFGM.PimAttributeId = ZPAV.PimAttributeId))
		AND EXISTS (SELECT * FROM #ModifiedProducts MP WHERE ZPP.PublishProductId = MP.PublishProductId 
			AND ISNULL(ZPCC.PimCategoryHierarchyId,0) = ISNULL(MP.PimCategoryHierarchyId,0) ) 
		AND NOT EXISTS(SELECT * FROM #ZnodePublishCategoryProduct ZPCP WHERE ZPP.PublishProductId = ZPCP.PublishProductId AND
			ZPAV.PimAttributeId=ZPCP.PimAttributeId AND ZPP.PublishCatalogId=ZPCP.PublishCatalogId 
			AND ISNULL(ZPCC.PimCategoryHierarchyId,0)= ISNULL(ZPCP.PimCategoryHierarchyId,0))

		CREATE INDEX IDX_#ZnodePublishCategoryProduct_PimProductId ON #ZnodePublishCategoryProduct(PimProductId)
		CREATE INDEX IDX_#ZnodePublishCategoryProduct_PublishCategoryId ON #ZnodePublishCategoryProduct(PublishCategoryId)

		CREATE INDEX IDX_#ZnodePublishCategoryProduct_PimAttributeValueId ON #ZnodePublishCategoryProduct(PimAttributeValueId)
		CREATE INDEX IDX_#ZnodePublishCategoryProduct_PimAttributeId ON #ZnodePublishCategoryProduct(PimAttributeId)
		 
		------Getting All Link Product Details
		SELECT ZPLPD.PimParentProductId, ZPLPD.PimProductId, ZPLPD.PimAttributeId, ZPAVL.AttributeValue as SKU
		INTO #LinkProduct
		FROM ZnodePimLinkProductDetail ZPLPD 
		INNER JOIN ZnodePimAttributeValue ZPAV ON (ZPAV.PimProductId = ZPLPD.PimProductId)
		INNER JOIN ZnodePimAttributeValueLocale ZPAVL ON ZPAV.PimAttributeValueId = ZPAVL.PimAttributeValueId
		WHERE EXISTS(SELECT * FROM ZnodePimAttribute ZPA WHERE ZPA.PimAttributeId = ZPAV.PimAttributeId AND ZPA.AttributeCode = 'SKU')
		
		 ----Getting products link product value entity
	     INSERT INTO #AttributeValueLocale ( PimProductId, AttributeCode, AttributeValue, AttributeEntity, LocaleId )
	     SELECT ZPLP.PimParentProductId ,ZPAX.AttributeCode, '' AttributeValue , 
		 JSON_MODIFY( JSON_Modify(ZPAX.AttributeJson , '$.AttributeValues' , 
		 ISNULL(SUBSTRING ( (SELECT ','+cast( LP.SKU as varchar(600))
							FROM #LinkProduct LP
							WHERE LP.PimParentProductId = ZPLP.PimParentProductId 
							AND LP.PimAttributeId = ZPLP.PimAttributeId FOR XML PATH('')),2,8000),'') ),'$.SelectValues',Json_Query('[]'))   

							, ZPAX.LocaleId
		 FROM ZnodePimLinkProductDetail ZPLP
		 INNER JOIN ZnodePimAttributeJSON ZPAX ON (ZPAX.PimAttributeId = ZPLP.PimAttributeId )
		 WHERE EXISTS(SELECT * FROM #ZnodePublishCategoryProduct PPCP  WHERE (ZPLP.PimParentProductId = PPCP.PimProductId ))
		 GROUP BY ZPLP.PimParentProductId ,ZPAX.AttributeCode , ZPAX.AttributeJSON,ZPAX.LocaleId,ZPAX.AttributeCode,ZPLP.PimAttributeId


		  ----Getting product attribute value entity
	      INSERT INTO #AttributeValueLocale ( PimProductId, AttributeCode, AttributeValue, AttributeEntity, LocaleId )
		  SELECT PPCP.PimProductId , ZPA.AttributeCode,ZPAVL.AttributeValue ,
					JSON_MODIFY(
					JSON_MODIFY (Json_Query( ZPAX.AttributeJSON  ) , '$.AttributeValues' ,  ISNULL(ZPAVL.AttributeValue,'') )    
					,'$.SelectValues',Json_Query('[]'))   
					AS 'AttributeEntity', 
				 ZPAVL.LocaleId
		  FROM ZnodePimAttributeValue PPCP
		  INNER JOIN ZnodePimAttribute ZPA ON (ZPA.PimAttributeId = PPCP.PimAttributeId)
		  INNER JOIN ZnodePimAttributeValueLocale ZPAVL ON (PPCP.PimAttributeValueId =ZPAVL.PimAttributeValueId)
		  INNER JOIN ZnodePimAttributeJSON ZPAX ON (ZPAX.PimAttributeId = ZPA.PimAttributeId AND ZPAX.LocaleId = ZPAVL.LocaleId)
		  WHERE EXISTS(SELECT * FROM #ZnodePublishCategoryProduct PPCP1  WHERE PPCP1.PimProductId = PPCP.PimProductId)--(PPCP1.PimAttributeValueId =PPCP.PimAttributeValueId) AND (ZPA.PimAttributeId = PPCP1.PimAttributeId))
		  AND NOT EXISTS(SELECT * FROM #AttributeValueLocale AVL WHERE PPCP.PimProductId = AVL.PimProductId AND ZPA.AttributeCode = AVL.AttributeCode AND ZPAVL.LocaleId = AVL.LocaleId )
		  AND NOT EXISTS(SELECT * FROM ZnodePimConfigureProductAttribute UOP WHERE ZPAX.PimAttributeId = UOP.PimAttributeId AND PPCP.PimProductId = UOP.PimProductId )

		  ----Getting product attribute value entity getting for other locale with default attribute json
		  INSERT INTO #AttributeValueLocale ( PimProductId, AttributeCode, AttributeValue, AttributeEntity, LocaleId )
		  SELECT PPCP.PimProductId , ZPA.AttributeCode,ZPAVL.AttributeValue ,
					JSON_MODIFY(
					JSON_MODIFY (Json_Query( ZPAX.AttributeJSON  ) , '$.AttributeValues' ,  ISNULL(ZPAVL.AttributeValue,'') )    
					,'$.SelectValues',Json_Query('[]'))   
					AS 'AttributeEntity', 
				 ZPAVL.LocaleId
		  FROM ZnodePimAttributeValue PPCP
		  INNER JOIN ZnodePimAttribute ZPA ON (ZPA.PimAttributeId = PPCP.PimAttributeId)
		  INNER JOIN ZnodePimAttributeValueLocale ZPAVL ON (PPCP.PimAttributeValueId =ZPAVL.PimAttributeValueId)
		  INNER JOIN ZnodePimAttributeJSON ZPAX ON (ZPAX.PimAttributeId = ZPA.PimAttributeId)
		  WHERE ZPAX.LocaleId = @DefaultLocaleId AND ZPAVL.LocaleId <> @DefaultLocaleId AND
		  EXISTS(SELECT * FROM #ZnodePublishCategoryProduct PPCP1  WHERE PPCP1.PimProductId = PPCP.PimProductId)--(PPCP1.PimAttributeValueId =PPCP.PimAttributeValueId) AND (ZPA.PimAttributeId = PPCP1.PimAttributeId))
		  AND NOT EXISTS(SELECT * FROM #AttributeValueLocale AVL WHERE PPCP.PimProductId = AVL.PimProductId AND ZPA.AttributeCode = AVL.AttributeCode AND ZPAVL.LocaleId = AVL.LocaleId )
		  AND NOT EXISTS(SELECT * FROM ZnodePimConfigureProductAttribute UOP WHERE ZPAX.PimAttributeId = UOP.PimAttributeId AND PPCP.PimProductId = UOP.PimProductId )


		  IF OBJECT_ID('TEMPDB..#ZnodePublishCatalogProductDetail') IS NOT NULL
			DROP TABLE #ZnodePublishCatalogProductDetail

		  IF OBJECT_ID('TEMPDB..#ZnodePublishCatalogProductDetail1') IS NOT NULL
			DROP TABLE #ZnodePublishCatalogProductDetail1

		  IF OBJECT_ID('TEMPDB..#TBL_ProductRequiredAttribute') IS NOT NULL
			DROP TABLE #TBL_ProductRequiredAttribute

		  
		CREATE TABLE #TBL_ProductRequiredAttribute (PimProductId INT,SKU VARCHAR(600),ProductName VARCHAR(600), IsActive VARCHAR(10), LocaleId INT)

		INSERT INTO #TBL_ProductRequiredAttribute(PimProductId, LocaleId)
		SELECT DISTINCT PimProductId, LocaleId FROM #AttributeValueLocale

		UPDATE #TBL_ProductRequiredAttribute 
		SET SKU = b.AttributeValue
		FROM #TBL_ProductRequiredAttribute a
		INNER JOIN #AttributeValueLocale b ON a.PimproductId = b.PimProductId AND a.LocaleId = b.LocaleId
		WHERE b.AttributeCode = 'SKU'

		UPDATE #TBL_ProductRequiredAttribute 
		SET ProductName = b.AttributeValue
		FROM #TBL_ProductRequiredAttribute a
		INNER JOIN #AttributeValueLocale b ON a.PimproductId = b.PimProductId AND a.LocaleId = b.LocaleId
		WHERE b.AttributeCode = 'ProductName'

		UPDATE #TBL_ProductRequiredAttribute 
		SET IsActive = b.AttributeValue
		FROM #TBL_ProductRequiredAttribute a
		INNER JOIN #AttributeValueLocale b ON a.PimproductId = b.PimProductId AND a.LocaleId = b.LocaleId
		WHERE b.AttributeCode = 'IsActive'

		  CREATE INDEX IDX_#TBL_ProductRequiredAttribute_PimProductId ON #TBL_ProductRequiredAttribute(PimProductId)

		  SELECT ZPI.PublishProductId, ZPI.PublishCatalogId ,TYU.PublishCategoryId,ZPI.CatalogName,ISNULL(ZPI.PimCategoryHierarchyId,0) PimCategoryHierarchyId
					,TPAR.SKU,TPAR.ProductName,TPAR.IsActive,TYU.PublishCategoryName CategoryName,ISNULL(TYU.LocaleId,TPAR.LocaleId) as LocaleId
		   INTO #ZnodePublishCatalogProductDetail
		   FROM #ZnodePublishCategoryProduct ZPI
		   INNER JOIN #TBL_ProductRequiredAttribute TPAR ON (TPAR.PimProductId = ZPI.PimProductId )
		   LEFT JOIN ZnodePublishCategoryDetail TYU ON (TYU.PublishCategoryId = ZPI.PublishCategoryId AND TPAR.LocaleId = TYU.LocaleId )
		   GROUP BY PublishProductId, PublishCatalogId ,TYU.PublishCategoryId,CatalogName,PimCategoryHierarchyId
					,SKU,ProductName,TPAR.IsActive,PublishCategoryName, TYU.LocaleId, TPAR.LocaleId

			
			CREATE INDEX IDX_#ZnodePublishCatalogProductDetail ON #ZnodePublishCatalogProductDetail(PublishProductId,PublishCatalogId,PimCategoryHierarchyId,LocaleId)

			SELECT PublishProductId,PublishCatalogId,PimCategoryHierarchyId,SKU,ProductName,CategoryName, CatalogName, LocaleId ,IsActive
			      ,CASE WHEN PublishProductId IS NULL THEN 1 ELSE Row_Number()Over(Partition by PublishProductId ORDER BY PublishProductId,PimCategoryHierarchyId) END  ProductIndex
			INTO #ZnodePublishCatalogProductDetail1
			FROM #ZnodePublishCatalogProductDetail


			INSERT INTO #ZnodePublishCatalogProductDetail1 (PublishProductId,PublishCatalogId,PimCategoryHierarchyId,SKU,ProductName,CategoryName, CatalogName, LocaleId ,IsActive,ProductIndex)
			SELECT PublishProductId,PublishCatalogId,PimCategoryHierarchyId,SKU,ProductName,CategoryName, CatalogName, b.Id ,IsActive,ProductIndex
			FROM #ZnodePublishCatalogProductDetail1 a
			CROSS APPLY @LocaleId b 
			WHERE NOT EXISTS(SELECT * FROM #ZnodePublishCatalogProductDetail1 c WHERE a.PublishProductId = c.PublishProductId AND b.Id = c.LocaleId  )
			AND a.LocaleId = @DefaultLocaleId 
			AND a.PublishCatalogId = @PublishCatalogId

			DELETE ZPCPD 
			FROM ZnodePublishCatalogProductDetail ZPCPD
			WHERE NOT EXISTS(SELECT * FROM #ProductLocaleWise ZPCPD1 WHERE ZPCPD.PublishProductId = ZPCPD1.PublishProductId 
			                 AND ZPCPD.LocaleId = ZPCPD1.LocaleId )  
			AND ZPCPD.PublishCatalogId = @PublishCatalogId

			DELETE ZPCPD 
			FROM ZnodePublishCatalogProductDetail ZPCPD
			WHERE EXISTS(SELECT * FROM #ZnodePublishCatalogProductDetail ZPCPD1 WHERE 
						ZPCPD.PublishProductId = ZPCPD1.PublishProductId 
			            AND ZPCPD.LocaleId = ZPCPD1.LocaleId 
						AND ISNULL(ZPCPD1.PimCategoryHierarchyId,0)  <> 0 ) AND ZPCPD.PimCategoryHierarchyId =0
			AND ZPCPD.PublishCatalogId = @PublishCatalogId
			
			----Update data ZnodePublishCatalogProductDetail 
			UPDATE TARGET
			SET  TARGET.ProductIndex	=SOURCE.ProductIndex
				,TARGET.ModifiedBy		= @UserId	
				,TARGET.ModifiedDate	= @Date
			FROM ZnodePublishCatalogProductDetail TARGET
			INNER JOIN #ZnodePublishCatalogProductDetail1 SOURCE
			ON (
		        SOURCE.PublishProductId = TARGET.PublishProductId
				AND SOURCE.PublishCatalogId = TARGET.PublishCatalogId 
				AND ISNULL(SOURCE.PimCategoryHierarchyId,0) = ISNULL(TARGET.PimCategoryHierarchyId,0)
				AND SOURCE.LocaleId = TARGET.LocaleId --@LocaleId_In
				)
			WHERE TARGET.PublishCatalogId = @PublishCatalogId

			----Update data ZnodePublishCatalogProductDetail 
			UPDATE TARGET
			SET  
				TARGET.ProductName		=SOURCE.ProductName
				,TARGET.CatalogName		=SOURCE.CatalogName
				,TARGET.IsActive		=case when SOURCE.IsActive in ('0','false') then 0 else 1 end 
				,TARGET.ModifiedBy		= @UserId	
				,TARGET.ModifiedDate	= @Date
			FROM ZnodePublishCatalogProductDetail TARGET
			INNER JOIN #ZnodePublishCatalogProductDetail1 SOURCE
			ON (
		        TARGET.SKU = SOURCE.SKU
				AND SOURCE.LocaleId = TARGET.LocaleId --@LocaleId_In
				)
			WHERE TARGET.PublishCatalogId = @PublishCatalogId

			
		----Update data ZnodePublishCatalogProductDetail 
		UPDATE TARGET
		SET  
			TARGET.CategoryName	=SOURCE.CategoryName
			,TARGET.ModifiedBy		= @UserId	
			,TARGET.ModifiedDate	= @Date
		FROM ZnodePublishCatalogProductDetail TARGET
		INNER JOIN #ZnodePublishCatalogProductDetail1 SOURCE
		ON (
		    TARGET.SKU = SOURCE.SKU
			AND SOURCE.LocaleId = TARGET.LocaleId 
			AND ISNULL(SOURCE.PimCategoryHierarchyId,0) = ISNULL(TARGET.PimCategoryHierarchyId,0)
			)
		WHERE ISNULL(TARGET.PimCategoryHierarchyId,0) <> 0
		AND TARGET.PublishCatalogId = @PublishCatalogId


		----Insert data ZnodePublishCatalogProductDetail 
		INSERT INTO ZnodePublishCatalogProductDetail
			( PublishProductId,PublishCatalogId,PimCategoryHierarchyId,SKU,ProductName,CategoryName, CatalogName,
				LocaleId ,IsActive,ProductIndex,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate )
		SELECT SOURCE.PublishProductId ,SOURCE.PublishCatalogId ,SOURCE.PimCategoryHierarchyId ,SOURCE.SKU ,SOURCE.ProductName
		,SOURCE.CategoryName ,SOURCE.CatalogName ,SOURCE.LocaleId ,SOURCE.IsActive ,SOURCE.ProductIndex ,@UserId ,@Date ,@UserId ,@Date
		FROM #ZnodePublishCatalogProductDetail1 SOURCE
		WHERE NOT EXISTS(SELECT * FROM ZnodePublishCatalogProductDetail TARGET WHERE SOURCE.PublishProductId = TARGET.PublishProductId
						AND SOURCE.PublishCatalogId = TARGET.PublishCatalogId 
						AND SOURCE.PimCategoryHierarchyId = TARGET.PimCategoryHierarchyId 
						AND TARGET.LocaleId = SOURCE.LocaleId )
		AND SOURCE.PublishCatalogId = @PublishCatalogId
					
		----		  
		INSERT INTO ZnodePublishCatalogProductDetail (PublishProductId,PublishCatalogId,PimCategoryHierarchyId,SKU,ProductName,CategoryName, CatalogName,
				LocaleId ,IsActive,ProductIndex,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		SELECT PublishProductId,PublishCatalogId,PimCategoryHierarchyId,SKU,ProductName,CategoryName, CatalogName,
				b.Id ,IsActive,ProductIndex,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate
		FROM ZnodePublishCatalogProductDetail a
		CROSS APPLY @LocaleId b 
		WHERE NOT EXISTS(SELECT * FROM ZnodePublishCatalogProductDetail c WHERE a.PublishProductId = c.PublishProductId AND b.Id = c.LocaleId  )
		AND a.LocaleId = @DefaultLocaleId
		AND a.PublishCatalogId = @PublishCatalogId

		DELETE ZPCPD FROM ZnodePublishCatalogProductDetail ZPCPD
		INNER JOIN ZnodePublishProduct ZPD ON ZPCPD.PublishProductId = ZPD.PublishProductId AND ZPCPD.PublishCatalogId = ZPD.PublishCatalogId
		INNER JOIN ZnodePublishCatalog ZPC ON ZPCPD.PublishCatalogId = ZPC.PublishCatalogId
		WHERE NOT EXISTS(SELECT * FROM ZnodePimCategoryProduct ZPCC 
			    INNER JOIN ZnodePimCategoryHierarchy ZPCH ON ZPCC.PimCategoryId = ZPCH.PimCategoryId WHERE ZPD.PimProductId = ZPCC.PimProductId AND ZPC.PimCatalogId = ZPCH.PimCatalogId AND ZPCPD.PimCategoryHierarchyId = ZPCH.PimCategoryHierarchyId)
		AND ZPCPD.PimCategoryHierarchyId <> 0
		AND ZPCPD.PublishCatalogId = @PublishCatalogId

		----Updating the SKU into table ZnodePublishCatalogProductDetail
		UPDATE ZnodePublishCatalogProductDetail 
		SET SKU = b.AttributeValue
		FROM ZnodePublishCatalogProductDetail a
		INNER JOIN ZnodePublishProduct ZPP ON a.PublishProductId = ZPP.PublishProductId
		INNER JOIN #AttributeValueLocale b ON ZPP.PimproductId = b.PimProductId AND a.LocaleId = b.LocaleId
		WHERE b.AttributeCode = 'SKU'
		AND a.PublishCatalogId = @PublishCatalogId

		----Updating the ProductName into table ZnodePublishCatalogProductDetail
		UPDATE ZnodePublishCatalogProductDetail 
		SET ProductName = b.AttributeValue
		FROM ZnodePublishCatalogProductDetail a
		INNER JOIN ZnodePublishProduct ZPP ON a.PublishProductId = ZPP.PublishProductId
		INNER JOIN #AttributeValueLocale b ON ZPP.PimproductId = b.PimProductId AND a.LocaleId = b.LocaleId
		WHERE b.AttributeCode = 'ProductName'
		AND a.PublishCatalogId = @PublishCatalogId

		----Updating the IsActive into table ZnodePublishCatalogProductDetail
		UPDATE ZnodePublishCatalogProductDetail 
		SET IsActive = b.AttributeValue
		FROM ZnodePublishCatalogProductDetail a
		INNER JOIN ZnodePublishProduct ZPP ON a.PublishProductId = ZPP.PublishProductId
		INNER JOIN #AttributeValueLocale b ON ZPP.PimproductId = b.PimProductId AND a.LocaleId = b.LocaleId
		WHERE b.AttributeCode = 'IsActive'
		AND a.PublishCatalogId = @PublishCatalogId

		SELECT a.PimProductId,  a.PimAttributeId
		INTO #PimProductAttributeDefaultValue
		FROM ZnodePimAttributeValue a 
		INNER JOIN ZnodePimProductAttributeDefaultValue b ON a.PimAttributeValueId = b.PimAttributeValueId 

		CREATE INDEX Idx_#PimProductAttributeDefaultValue ON #PimProductAttributeDefaultValue (PimProductId,PimAttributeId)

		INSERT INTO #PimDefaultValueLocale
		SELECT PimAttributeDefaultJsonId,PimAttributeDefaultValueId,LocaleId ,DefaultValueJson
		FROM ZnodePimAttributeDefaultJSON

		SELECT  AA.DefaultValueJson , ZPADV.PimAttributeValueId, AA.LocaleId 
		INTO #PimAttributeDefaultXML
		FROM ZnodePimAttributeDefaultJSON AA 
		INNER JOIN #PimDefaultValueLocale GH ON (GH.PimAttributeDefaultJsonId = AA.PimAttributeDefaultJsonId AND AA.LocaleId = GH.LocaleId)
		INNER JOIN ZnodePimProductAttributeDefaultValue ZPADV ON ( ZPADV.PimAttributeDefaultValueId = AA.PimAttributeDefaultValueId AND AA.LocaleId = ZPADV.LocaleId)

		----Getting child facets for merging		  
		SELECT DISTINCT ZPPADV.PimAttributeDefaultValueId, ZPAV_Parent.PimAttributeValueId, ZPPADV.LocaleId
		INTO #PimChildProductFacets
		FROM ZnodePimAttributeValue ZPAV_Parent
		INNER JOIN ZnodePimProductTypeAssociation ZPPTA ON ZPAV_Parent.PimProductId = ZPPTA.PimParentProductId
		INNER JOIN ZnodePimAttributeValue ZPAV_Child ON ZPPTA.PimProductId = ZPAV_Child.PimProductId AND ZPAV_Parent.PimAttributeId = ZPAV_Child.PimAttributeId
		INNER JOIN ZnodePimProductAttributeDefaultValue ZPPADV ON ZPAV_Child.PimAttributeValueId = ZPPADV.PimAttributeValueId 
		WHERE EXISTS(SELECT * FROM ZnodePimFrontendProperties ZPFP WHERE ZPAV_Parent.PimAttributeId = ZPFP.PimAttributeId AND ZPFP.IsFacets = 1)
		AND EXISTS(SELECT * FROM #ZnodePublishCategoryProduct ZPPC WHERE ZPAV_Parent.PimProductId = ZPPC.PimProductId )
		AND NOT EXISTS(SELECT * FROM ZnodePimProductAttributeDefaultValue ZPPADV1 WHERE ZPAV_Parent.PimAttributeValueId = ZPPADV1.PimAttributeValueId 
		                AND ZPPADV1.PimAttributeDefaultValueId = ZPPADV.PimAttributeDefaultValueId )

		----Merging childs facet attribute Default value XML for parent
		INSERT INTO #PimAttributeDefaultXML (DefaultValueJson, PimAttributeValueId, LocaleId)
		SELECT ZPADX.DefaultValueJson, ZPPADV.PimAttributeValueId, ZPPADV.LocaleId
		FROM #PimChildProductFacets ZPPADV		  
		INNER JOIN ZnodePimAttributeDefaultJSON ZPADX ON ( ZPPADV.PimAttributeDefaultValueId = ZPADX.PimAttributeDefaultValueId AND ZPPADV.LocaleId = ZPADX.LocaleId)

		CREATE INDEX Idx_#PimDefaultValueLocale ON #PimDefaultValueLocale(PimAttributeDefaultJsonId,LocaleId)

		CREATE INDEX Idx_#PimAttributeDefaultXML ON #PimAttributeDefaultXML(PimAttributeValueId,LocaleId)
		INCLUDE (DefaultValueJson)

		----Getting default attribute value entity
		INSERT INTO #AttributeValueLocale
		
		SELECT PPCP.PimProductId, PPCP.AttributeCode,'' AttributeValue,
		JSON_MODIFY (JSON_MODIFY (ZPAX.AttributeJson,'$.AttributeValues',''), '$.SelectValues',
			
				ISNULL((SELECT 
							ISNULL(JSON_VALUE(DefaultValueJson, '$.Code'),'') Code 
							,ISNULL(JSON_VALUE(DefaultValueJson, '$.LocaleId'),0) LocaleId
							,ISNULL(JSON_VALUE(DefaultValueJson, '$.Value'),'') Value
							,ISNULL(JSON_VALUE(DefaultValueJson, '$.AttributeDefaultValue'),'') AttributeDefaultValue
							,ISNULL(JSON_VALUE(DefaultValueJson, '$.DisplayOrder'),0) DisplayOrder
							,ISNULL(JSON_VALUE(DefaultValueJson, '$.IsEditable'),'false') IsEditable
							,ISNULL(JSON_VALUE(DefaultValueJson, '$.SwatchText'),'') SwatchText
							,ISNULL(JSON_VALUE(DefaultValueJson, '$.Path'),'') Path
					FROM #PimAttributeDefaultXML aa
					WHERE (aa.PimAttributeValueId = PPCP.PimAttributeValueId AND AA.LocaleId = ZPAX.LocaleId ) For JSON Auto 
				),'[]') 
				) 
			 AttributeEntity 
		 , ZPAX.LocaleId
		 FROM #ZnodePublishCategoryProduct PPCP 
		 INNER JOIN ZnodePimAttributeJSON ZPAX ON (ZPAX.PimAttributeId = PPCP.PimAttributeId)
		 WHERE 
		 NOT EXISTS(SELECT * FROM #AttributeValueLocale AVL WHERE PPCP.PimProductId = AVL.PimProductId AND PPCP.AttributeCode = AVL.AttributeCode AND ZPAX.LocaleId = AVL.LocaleId )
		 AND EXISTS(SELECT * FROM #PimProductAttributeDefaultValue a  WHERE PPCP.PimProductId = a.PimProductId AND ZPAX.PimAttributeId = a.PimAttributeId )
		 AND EXISTS(SELECT * FROM ZnodePimAttributeValue a INNER JOIN ZnodePimProductAttributeDefaultValue b ON a.PimAttributeValueId = b.PimAttributeValueId 
		            AND PPCP.PimProductId = a.PimProductId AND ZPAX.PimAttributeId = a.PimAttributeId )
		 AND NOT EXISTS(SELECT * FROM ZnodePimConfigureProductAttribute UOP WHERE ZPAX.PimAttributeId = UOP.PimAttributeId AND PPCP.PimProductId = UOP.PimProductId )

		 
		 ----Getting text attribute value entity
		 INSERT INTO #AttributeValueLocale ( PimProductId, AttributeCode, AttributeValue, AttributeEntity, LocaleId )
		 SELECT PPCP.PimProductId , ZPA.AttributeCode,'' AttributeValue ,
		 JSON_MODIFY (JSON_MODIFY (Json_Query( ZPAX.AttributeJSON  ) , '$.AttributeValues' ,  ISNULL(ZPAVL.AttributeValue,'') ) ,'$.SelectValues',Json_Query('[]'))
		    AS 'AttributeEntity', 
		 ZPAVL.LocaleId
		 FROM ZnodePimAttributeValue PPCP
		 INNER JOIN ZnodePimProductAttributeTextAreaValue ZPAVL ON (PPCP.PimAttributeValueId =ZPAVL.PimAttributeValueId)
		 INNER JOIN ZnodePimAttributeJSON ZPAX ON (ZPAX.PimAttributeId = PPCP.PimAttributeId AND ZPAX.LocaleId = ZPAVL.LocaleId)
		 INNER JOIN ZnodePimAttribute ZPA ON PPCP.PimAttributeId = ZPA.PimAttributeId
	     WHERE EXISTS(SELECT * FROM #ZnodePublishCategoryProduct PPCP1 WHERE PPCP1.PimProductId = PPCP.PimProductId) --(PPCP1.PimAttributeValueId =ZPAVL.PimAttributeValueId) AND (ZPAX.PimAttributeId = PPCP1.PimAttributeId))
		 AND NOT EXISTS(SELECT * FROM #AttributeValueLocale AVL WHERE PPCP.PimProductId = AVL.PimProductId AND ZPA.AttributeCode = AVL.AttributeCode AND ZPAVL.LocaleId = AVL.LocaleId )
		group by PPCP.PimProductId , ZPA.AttributeCode,ZPAX.AttributeJson,ZPAVL.LocaleId,ZPAVL.AttributeValue

		 ----Getting custome field value entity
		 INSERT INTO #AttributeValueLocale ( PimProductId, AttributeCode, AttributeValue, AttributeEntity, LocaleId )
 		 SELECT ZPCFX.PimProductId , ZPCFX.CustomCode, '' AttributeValue ,
		 JSON_MODIFY (Json_Query( ZPCFX.CustomeFiledJson) ,'$.SelectValues',Json_Query('[]')) 
		 AttributeEntity, 
		 ZPCFX.LocaleId
		 FROM ZnodePimCustomeFieldJSON ZPCFX 
		 WHERE EXISTS(SELECT * FROM #ZnodePublishCategoryProduct PPCP WHERE (PPCP.PimProductId = ZPCFX.PimProductId ))
		 AND NOT EXISTS(SELECT * FROM #AttributeValueLocale AVL WHERE ZPCFX.PimProductId = AVL.PimProductId AND ZPCFX.CustomCode = AVL.AttributeCode AND ZPCFX.LocaleId = AVL.LocaleId )
		 group by ZPCFX.PimProductId , ZPCFX.CustomCode, ZPCFX.CustomeFiledJson , ZPCFX.LocaleId

		  ----Getting image attribute value entity
		 INSERT INTO #AttributeValueLocale ( PimProductId, AttributeCode, AttributeValue, AttributeEntity, LocaleId )
		 SELECT PPCP.PimProductId, ZPA.AttributeCode,'' AttributeValue,
		 JSON_MODIFY (JSON_MODIFY (Json_Query( ZPAX.AttributeJSON  ) , '$.AttributeValues',  
		 ISNULL((SELECT stuff( (SELECT ','+ZPPAM.MediaPath FROM ZnodePimProductAttributeMedia ZPPAM WHERE (ZPPAM.PimAttributeValueId = PPCP.PimAttributeValueId)
				 FOR XML PATH(''),Type).value('.', 'varchar(max)'), 1, 1, '')
				 
				 ),'') ) ,'$.SelectValues',Json_Query('[]'))   
				 AS 'AttributeEntity', 
				 ZPAX.LocaleId
		 FROM ZnodePimAttributeValue PPCP 
		 INNER JOIN ZnodePimAttributeJSON ZPAX ON (ZPAX.PimAttributeId = PPCP.PimAttributeId)
		 INNER JOIN ZnodePimAttribute ZPA ON ZPA.PimAttributeId = PPCP.PimAttributeId
		 WHERE NOT EXISTS(SELECT * FROM #AttributeValueLocale AVL WHERE PPCP.PimProductId = AVL.PimProductId AND ZPA.AttributeCode = AVL.AttributeCode AND ZPAX.LocaleId = AVL.LocaleId )
		 AND EXISTS(SELECT * FROM ZnodePimProductAttributeMedia b WHERE PPCP.PimAttributeValueId = b.PimAttributeValueId )
		 AND EXISTS(SELECT * FROM #ZnodePublishCategoryProduct PPCP1 WHERE PPCP.PimProductId = PPCP1.PimProductId )
		 AND NOT EXISTS(SELECT * FROM ZnodePimConfigureProductAttribute UOP WHERE ZPAX.PimAttributeId = UOP.PimAttributeId AND PPCP.PimProductId = UOP.PimProductId )

		 -------------configurable attribute 		 
		
		INSERT INTO #AttributeValueLocale ( PimProductId, AttributeCode, AttributeValue, AttributeEntity, LocaleId )
		SELECT DISTINCT UOP.PimProductId,c.AttributeCode,'' AttributeValue ,--'<Attributes><AttributeEntity>'+
		JSON_MODIFY (ISNULL(JSON_MODIFY (c.AttributeJson,'$.AttributeValues',''),'')  ,'$.SelectValues',
			ISNULL((SELECT DISTINCT 
							ISNULL(JSON_VALUE(AA.DefaultValueJson, '$.Code'),'') Code 
							,ISNULL(JSON_VALUE(AA.DefaultValueJson, '$.LocaleId'),0) LocaleId
							,ISNULL(JSON_VALUE(AA.DefaultValueJson, '$.Value'),'') Value
							,ISNULL(JSON_VALUE(AA.DefaultValueJson, '$.AttributeDefaultValue'),'') AttributeDefaultValue
							,ISNULL(JSON_VALUE(AA.DefaultValueJson, '$.DisplayOrder'),0) DisplayOrder
							,ISNULL(JSON_VALUE(AA.DefaultValueJson, '$.IsEditable'),'false') IsEditable
							,ISNULL(JSON_VALUE(AA.DefaultValueJson, '$.SwatchText'),'') SwatchText
							,ISNULL(JSON_VALUE(AA.DefaultValueJson, '$.Path'),'') Path 
							,ISNULL(ZPA.DisplayOrder,0)  AS VariantDisplayOrder 
							,ISNULL(ZPAVL_SKU.AttributeValue,'')   AS VariantSKU 
							,ISNULL(ZM.Path,'') AS VariantImagePath 
						 FROM ZnodePimAttributeDefaultJSON AA 
						 INNER JOIN ZnodePimProductAttributeDefaultValue ZPADV ON ( ZPADV.PimAttributeDefaultValueId = AA.PimAttributeDefaultValueId )
						 INNER JOIN ZnodePimAttributeValue ZPAV1 ON (ZPAV1.PimAttributeValueId= ZPADV.PimAttributeValueId )
						 -- check/join for active variants 
						 INNER JOIN ZnodePimAttributeValue ZPAV ON (ZPAV.PimProductId =ZPAV1.PimProductId)
						 INNER JOIN ZnodePimAttributeValueLocale ZPAVL ON (ZPAV.PimAttributevalueid = ZPAVL.PimAttributeValueId AND ZPAVL.AttributeValue = 'True')
						 INNER JOIN ZnodePimProductTypeAssociation YUP ON (YUP.PimProductId = ZPAV1.PimProductId)
						 -- SKU
						 INNER JOIN ZnodePimAttributeValue ZPAV_SKU ON(YUP.PimProductId = ZPAV_SKU.PimProductId)
						 INNER JOIN ZnodePimAttributeValueLocale ZPAVL_SKU ON (ZPAVL_SKU.PimAttributeValueId = ZPAV_SKU.PimAttributeValueId)
						 LEFT JOIN ZnodePimAttributeValue ZPAV12 ON (ZPAV12.PimProductId= YUP.PimProductId  AND ZPAV12.PimAttributeId = @PimMediaAttributeId ) 
						 LEFT JOIN ZnodePimProductAttributeMedia ZPAVM ON (ZPAVM.PimAttributeValueId= ZPAV12.PimAttributeValueId ) 
						 LEFT JOIN ZnodeMedia ZM ON (ZM.MediaId = ZPAVM.MediaId)
						 LEFT JOIN ZnodePimAttribute ZPA ON (ZPA.PimattributeId = ZPAV1.PimAttributeId)
						 WHERE (YUP.PimParentProductId  = UOP.PimProductId AND ZPAV1.pimAttributeId = UOP.PimAttributeId )
						 -- Active Variants
						 AND ZPAV.PimAttributeId = (SELECT TOP 1 PimAttributeId FROM ZnodePimAttribute WHERE AttributeCode = 'IsActive')
						 -- VariantSKU
						 AND ZPAV_SKU.PimAttributeId = (SELECT PimAttributeId FROM ZnodePimAttribute WHERE AttributeCode = 'SKU')
		FOR JSON auto),'[]')) SelectValuesEntity ,
		c.LocaleId
		FROM ZnodePimConfigureProductAttribute UOP 
		INNER JOIN ZnodePimAttributeJSON c   ON (c.PimAttributeId = UOP.PimAttributeId )
		WHERE  EXISTS(SELECT * FROM #ZnodePublishCategoryProduct PPCP1 WHERE UOP.PimProductId = PPCP1.PimProductId )

		-------------configurable attribute 

		CREATE INDEX IDX_#AttributeValueLocale ON #AttributeValueLocale(PimProductId,AttributeCode,LocaleId)
		CREATE INDEX IDX_#AttributeValueLocale_Id ON #AttributeValueLocale(ID)
		 	
		DELETE ZPPAX FROM ZnodePublishProductAttributeJson ZPPAX
		WHERE exists (SELECT * FROM #AttributeValueLocale AVL WHERE ZPPAX.PimProductId = AVL.PimProductId AND AVL.LocaleId = ZPPAX.LocaleId )
		AND NOT EXISTS(SELECT * FROM #AttributeValueLocale AVL WHERE ZPPAX.PimProductId = AVL.PimProductId AND AVL.LocaleId = ZPPAX.LocaleId AND ZPPAX.AttributeCode = AVL.AttributeCode )

		DECLARE @MaxCount INT, @MinRow INT, @MaxRow INT, @Rows numeric(10,2);
		SELECT @MaxCount = COUNT(*) FROM #AttributeValueLocale;

		SELECT @Rows = 200000
        
		SELECT @MaxCount = CEILING(@MaxCount / @Rows);

		IF OBJECT_ID('tempdb..#Temp_ImportLoop') IS NOT NULL
            DROP TABLE #Temp_ImportLoop;
        
		---- To get the min AND max rows for import in loop
		;WITH cte AS 
		(
			SELECT RowId = 1, 
				   MinRow = 1, 
                   MaxRow = cast(@Rows as int)
            UNION ALL
            SELECT RowId + 1, 
                   MinRow + cast(@Rows as int), 
                   MaxRow + cast(@Rows as int)
            FROM cte
            WHERE RowId + 1 <= @MaxCount
		)
        SELECT RowId, MinRow, MaxRow
        INTO #Temp_ImportLoop
        FROM cte
		OPTION (maxrecursion 0);

		--Cursor for rows wise execution in bulk
		DECLARE cur_BulkData CURSOR LOCAL FAST_FORWARD
        FOR SELECT MinRow, MaxRow FROM #Temp_ImportLoop
		WHERE EXISTS(SELECT * FROM #AttributeValueLocale);

        OPEN cur_BulkData;
        FETCH NEXT FROM cur_BulkData INTO  @MinRow, @MaxRow;

        WHILE @@FETCH_STATUS = 0
        BEGIN
	         UPDATE ZnodePublishProductAttributeJson SET IsUpdateLocaleWise = 0 WHERE ISNULL(IsUpdateLocaleWise,0) = 1
			  ----Update Product Attribute XML
			 UPDATE ZPPAX SET ZPPAX.Attributes = AVL.AttributeEntity, ZPPAX.ModifiedBy = @UserId, ZPPAX.ModifiedDate = GETDATE() 
			        , ZPPAX.IsUpdateLocaleWise = 0
			 FROM ZnodePublishProductAttributeJson ZPPAX 
			 INNER JOIN #AttributeValueLocale AVL ON ZPPAX.PimProductId = AVL.PimProductId AND AVL.LocaleId = ZPPAX.LocaleId AND ZPPAX.AttributeCode = AVL.AttributeCode 
			 WHERE  AVL.Id BETWEEN @MinRow AND @MaxRow AND AVL.AttributeEntity is not null
		 
			 ----Insert Product Attribute XML
			 INSERT INTO ZnodePublishProductAttributeJson(PimProductId,LocaleId,AttributeCode,Attributes,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
			 SELECT AVL.PimProductId, AVL.LocaleId, AVL.AttributeCode, cast(AVL.AttributeEntity as varchar(max)), @UserId CreatedBy, GETDATE() CreatedDate, @UserId ModifiedBy, GETDATE() ModifiedDate
			 FROM #AttributeValueLocale AVL
			 WHERE NOT EXISTS(SELECT * FROM ZnodePublishProductAttributeJson ZPPAX WHERE AVL.PimProductId = ZPPAX.PimProductId AND  AVL.LocaleId = ZPPAX.LocaleId AND AVL.AttributeCode = ZPPAX.AttributeCode )
			 AND  AVL.Id BETWEEN @MinRow AND @MaxRow AND AVL.AttributeEntity is not null
			 GROUP BY AVL.PimProductId, AVL.AttributeEntity, AVL.LocaleId, AVL.AttributeCode

			 FETCH NEXT FROM cur_BulkData INTO  @MinRow, @MaxRow;
        END;
		CLOSE cur_BulkData;
		DEALLOCATE cur_BulkData;

		DELETE ZPPAX
		FROM ZnodePublishProductAttributeJson ZPPAX
		WHERE LocaleId <> @DefaultLocaleId
		AND exists( SELECT * FROM ZnodePublishProductAttributeJson ZPPAX1 WHERE ZPPAX.AttributeCode = ZPPAX1.AttributeCode AND ZPPAX.PimProductId = ZPPAX1.PimProductId )
		AND NOT EXISTS(SELECT * FROM #ProductLocaleWise AVL WHERE AVL.PimProductId = ZPPAX.PimProductId AND  AVL.LocaleId = ZPPAX.LocaleId )
		
		
		DELETE  ZPPAX
		FROM ZnodePublishProductAttributeJson ZPPAX
		WHERE NOT EXISTS(SELECT * FROM #ProductLocaleWise ZLW WHERE ZPPAX.PimProductId = ZLW.PimProductId 
			                AND ZPPAX.LocaleId = ZLW.LocaleId )

		SELECT PimProductId,Attributes Attributes,AttributeCode
		INTO #ZnodePublishProductAttributeJson
		FROM ZnodePublishProductAttributeJson 
		WHERE LocaleId = @DefaultLocaleId

		INSERT INTO ZnodePublishProductAttributeJson (PimProductId,Attributes,LocaleId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,AttributeCode)
		SELECT PimProductId,Attributes,b.id,@UserId,GETDATE(),@UserId,GETDATE(),AttributeCode
		FROM #ZnodePublishProductAttributeJson a
		CROSS APPLY @LocaleId b 
		WHERE NOT EXISTS(SELECT * FROM ZnodePublishProductAttributeJson c WHERE a.PimProductId = c.PimProductId AND b.Id = c.LocaleId AND a.AttributeCode = c.AttributeCode )
		AND b.Id <> @DefaultLocaleId
					  
		END TRY
         BEGIN CATCH
             DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(), @ErrorCall NVARCHAR(MAX)= 'EXEC Znode_InsertUpdatePimCatalogProductDetailJson @PublishCatalogId = '+CAST(@PublishCatalogId AS VARCHAR(200))+',@UserId='+CAST(@UserId AS VARCHAR(200));


             EXEC Znode_InsertProcedureErrorLog
                  @ProcedureName = 'Znode_InsertUpdatePimCatalogProductDetailJson',
                  @ErrorInProcedure = @Error_procedure,
                  @ErrorMessage = @ErrorMessage,
                  @ErrorLine = @ErrorLine,
                  @ErrorCall = @ErrorCall;
            
            
         END CATCH;
END
GO
-------------------
IF EXISTS(SELECT * FROM SYS.PROCEDURES WHERE NAME = 'Znode_ImportCustomer')
	DROP PROC Znode_ImportCustomer
GO

CREATE PROCEDURE [dbo].[Znode_ImportCustomer]
(
	  @TableName NVARCHAR(100), 
	  @Status BIT OUT, 
	  @UserId INT, 
	  @ImportProcessLogId INT, 
	  @NewGUId NVARCHAR(200), 
	  @LocaleId INT= 0,
	  @PortalId INT ,
	  @CsvColumnString NVARCHAR(max)
)
AS
	--------------------------------------------------------------------------------------
	-- Summary :  Import SEO Details
	
	-- Unit Testing : 
	--------------------------------------------------------------------------------------

BEGIN
	BEGIN TRAN A;
	BEGIN TRY
		DECLARE @MessageDisplay NVARCHAR(100), @SSQL NVARCHAR(max),@AspNetZnodeUserId NVARCHAR(256),@ASPNetUsersId NVARCHAR(256),
		@PasswordHash NVARCHAR(max),@SecurityStamp NVARCHAR(max),@RoleId NVARCHAR(256),@IsAllowGlobalLevelUserCreation NVARCHAR(10)
		Declare @ProfileId  INT
		DECLARE @FailedRecordCount BIGINT
		DECLARE @SuccessRecordCount BIGINT
		 
		SET @SecurityStamp = '0wVYOZNK4g4kKz9wNs-UHw2'
		SET @PasswordHash = 'APy4Tm1KbRG6oy7h3r85UDh/lCW4JeOi2O2Mfsb3OjkpWTp1YfucMAvvcmUqNaSOlA==';
		SELECT  @RoleId  = Id FROM AspNetRoles WHERE   NAME = 'Customer'  

		SELECT @IsAllowGlobalLevelUserCreation = FeatureValues FROM ZnodeGlobalsetting WHERE FeatureName = 'AllowGlobalLevelUserCreation'

		DECLARE @GetDate DATETIME= dbo.Fn_GetDate();
		-- Retrive RoundOff Value FROM global setting 

		-- Three type of import required three table varible for product , category and brand
		CREATE TABLE #InsertCustomer 
		( 
			RowId INT IDENTITY(1, 1) PRIMARY KEY, RowNumber INT, UserName NVARCHAR(512) ,FirstName	NVARCHAR(200),
			LastName NVARCHAR(200), BudgetAmount	NUMERIC,Email	NVARCHAR(100),PhoneNumber	NVARCHAR(100),
		    EmailOptIn	BIT	,ReferralStatus	NVARCHAR(40),IsActive	BIT	,ExternalId	NVARCHAR(max),CreatedDate DATETIME,
			ProfileName varchar(200),AccountCode NVARCHAR(100),DepartmentName varchar(300),RoleName NVARCHAR(256), GUID NVARCHAR(400)
			,PerOrderLimit INT,	PerOrderAnnualLimit NVARCHAR(100),BillingAccountNumber NVARCHAR(100),EnableUserShippingAddressSuggestion  NVARCHAR(100),
			EnablePowerBIReportOnWebStore BIT,Custom1 NVARCHAR(max),Custom2 NVARCHAR(max),Custom3 NVARCHAR(max),
			Custom4 NVARCHAR(max),Custom5 NVARCHAR(max)
		);

		SET @SSQL = 'INSERT INTO #InsertCustomer( RowNumber,' + @CsvColumnString + ',GUID)
		             SELECT RowNumber,' + @CsvColumnString + ',GUID FROM '+ @TableName;
		
		EXEC sys.sp_sqlexec @SSQL;
				
		SELECT TOP 1 @ProfileId   =  ProfileId FROM ZnodePortalprofile WHERE Portalid = @Portalid and IsDefaultRegistedProfile=1
		IF( Isnull(@ProfileId ,0) = 0 ) 
		Begin
		
		
				INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
				SELECT '62', 'Default Portal Profile', '', @NewGUId, 1 , @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
							
				UPDATE ZnodeImportProcessLog
				SET Status = dbo.Fn_GetImportStatus( 3 ), ProcessCompletedDate = @GetDate
				WHERE ImportProcessLogId = @ImportProcessLogId;
			

				SELECT @FailedRecordCount = COUNT(DISTINCT RowNumber) FROM ZnodeImportLog 
				WHERE RowNumber IS NOT NULL AND  ImportProcessLogId = @ImportProcessLogId;
				SELECT @SuccessRecordCount = 0

				UPDATE ZnodeImportProcessLog SET FailedRecordcount = @FailedRecordCount , SuccessRecordCount = @SuccessRecordCount , 
				TotalProcessedRecords = (ISNULL(@FailedRecordCount,0) + ISNULL(@SuccessRecordCount,0))
				WHERE ImportProcessLogId = @ImportProcessLogId;

				DELETE FROM #InsertCustomer 
				SET @Status = 0;

				COMMIT TRAN A;
				Return 0 
		End

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		SELECT '35', 'UserName', UserName, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
		FROM #InsertCustomer AS ii
		WHERE ii.UserName not like '%_@_%_.__%' 

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		SELECT '35', 'UserName', UserName, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
		FROM #InsertCustomer AS ii
		WHERE ii.UserName like '%;%'
				
		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		SELECT '30', 'UserName', UserName, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
		FROM #InsertCustomer AS ii
		WHERE ltrim(rtrim(ii.UserName)) IN 
		(SELECT ltrim(rtrim(UserName))  FROM #InsertCustomer GROUP BY ltrim(rtrim(UserName))  having count(*) > 1 )

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		SELECT '77', 'AccountCode', ii.AccountCode, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
		FROM #InsertCustomer ii 
		WHERE ISNULL(LTRIM(RTRIM(ii.AccountCode)),'') !='' 
		AND NOT EXISTS(SELECT * FROM ZnodeAccount za INNER JOIN ZnodePortalAccount zpa on za.AccountId = zpa.AccountId
			WHERE  ISNULL(LTRIM(RTRIM(ii.AccountCode)),'') = za.AccountCode and zpa.PortalId = @PortalId )
		AND EXISTS(SELECT ISNULL(LTRIM(RTRIM(AccountCode)),'') FROM ZnodeAccount za1 WHERE ISNULL(LTRIM(RTRIM(ii.AccountCode)),'') = za1.AccountCode );

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		SELECT '73', 'AccountCode', AccountCode, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
		FROM #InsertCustomer AS ii
		WHERE   ISNULL(LTRIM(RTRIM(ii.AccountCode)),'') !=''   and ISNULL(LTRIM(RTRIM(ii.AccountCode)),'') not IN 
		(
			SELECT ISNULL(LTRIM(RTRIM(AccountCode)),'') FROM ZnodeAccount   
		);

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		SELECT '88', 'AccountCode', AccountCode, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
		FROM #InsertCustomer AS ii
		WHERE   ISNULL(LTRIM(RTRIM(ii.AccountCode)),'') !='' and  exists
		(
			SELECT top 1 1 FROM  AspNetZnodeUser ANZU INNER JOIN ASPNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName 
		INNER JOIN ZnodeUser ZU ON ANU.ID = ZU.AspNetUserId	 
		INNER JOIN ZnodeAccount ZA on  ZU.AccountId = ZA.AccountId
		WHERE ANZU.UserName = ii.UserName and  ZA.AccountCode != ii.AccountCode
		);

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		SELECT '92', 'RoleName', RoleName, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
		FROM #InsertCustomer AS ii
		WHERE ISNULL(LTRIM(RTRIM(ii.RoleName)),'') <> '' and  ISNULL(LTRIM(RTRIM(ii.RoleName)),'') IN ('User','Manager','Administrator')
		AND EXISTS(SELECT * FROM ZnodeUser a INNER JOIN ASPNetUsers b on (b.Id = a.AspNetUserId)
				INNER JOIN AspNetZnodeUser c on (c.AspNetZnodeUserId = b.UserName)
				--INNER JOIN #InsertCustomer IC on (IC.UserName = c.UserName)						
				INNER JOIN AspNetUserRoles u on u.UserId = b.Id
				INNER JOIN AspNetRoles ZD on u.RoleId = zd.Id
				WHERE (ii.UserName = c.UserName) and ISNULL(LTRIM(RTRIM(ii.RoleName)),'') <> ZD.Name )

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		SELECT '75', 'RoleName', RoleName, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
		FROM #InsertCustomer AS ii
		WHERE   ISNULL(LTRIM(RTRIM(ii.AccountCode)),'') ='' and ISNULL(LTRIM(RTRIM(RoleName)),'') <> '' 

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		SELECT '74', 'RoleName', RoleName, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
		FROM #InsertCustomer AS ii
		WHERE --ltrim(rtrim(ii.RoleName)) not IN ('User','Manager','Administrator') and ISNULL(LTRIM(RTRIM(RoleName)),'') <> '' and
			ISNULL(LTRIM(RTRIM(RoleName)),'') <> '' and not exists (SELECT top 1 1 FROM  AspNetRoles ANR WHERE name IN ('User','Manager','Administrator') and  ANR.name =ii.RoleName)

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		SELECT '76', 'DepartmentName', DepartmentName, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
		FROM #InsertCustomer AS ii
		WHERE ISNULL(LTRIM(RTRIM(ii.DepartmentName)),'') <> ''
		AND NOT EXISTS(SELECT * FROM  ZnodeAccount ZA INNER JOIN ZnodeDepartment ZD on ZA.AccountId = ZD.AccountId
			WHERE ISNULL(LTRIM(RTRIM(ii.AccountCode)),'') = ltrim(rtrim(za.AccountCode))
			and ISNULL(LTRIM(RTRIM(ii.DepartmentName)),'') = ltrim(rtrim(ZD.DepartmentName)))
				

		UPDATE ZIL
		SET ZIL.ColumnName =   ZIL.ColumnName + ' [ UserName - ' + ISNULL(UserName,'') + ' ] '
		FROM ZnodeImportLog ZIL 
		INNER JOIN #InsertCustomer IPA ON (ZIL.RowNumber = IPA.RowNumber)
		WHERE  ZIL.ImportProcessLogId = @ImportProcessLogId AND ZIL.RowNumber IS NOT NULL

		--Note : Content page import is not required 
		
		-- END Function Validation 	
		-----------------------------------------------
		--- Delete Invalid Data after functional validatin  

		DELETE FROM #InsertCustomer
		WHERE RowNumber IN
		(
			SELECT DISTINCT 
				   RowNumber
			FROM ZnodeImportLog
			WHERE ImportProcessLogId = @ImportProcessLogId  and RowNumber is not null 
			--AND GUID = @NewGUID
		);


		-- Update Record count IN log 
        
		SELECT @FailedRecordCount = COUNT(DISTINCT RowNumber) FROM ZnodeImportLog WHERE RowNumber IS NOT NULL AND  ImportProcessLogId = @ImportProcessLogId;
		SELECT @SuccessRecordCount = count(DISTINCT RowNumber) FROM #InsertCustomer
		UPDATE ZnodeImportProcessLog SET FailedRecordcount = @FailedRecordCount , SuccessRecordCount = @SuccessRecordCount , 
		TotalProcessedRecords = (ISNULL(@FailedRecordCount,0) + ISNULL(@SuccessRecordCount,0))
		WHERE ImportProcessLogId = @ImportProcessLogId;
		-- END

		-- Insert Product Data 
				
				
				DECLARE @InsertedAspNetZnodeUser TABLE (AspNetZnodeUserId NVARCHAR(256) ,UserName NVARCHAR(512),PortalId INT )
				DECLARE @InsertedASPNetUsers TABLE (Id NVARCHAR(256) ,UserName NVARCHAR(512))
				DECLARE @InsertZnodeUser TABLE (UserId INT,AspNetUserId NVARCHAR(256),CreatedDate DATETIME )

				UPDATE ANU SET 
				ANU.PhoneNumber	= IC.PhoneNumber, ANU.LockoutEndDateUtc = case when IC.IsActive = 0 then @GetDate when IC.IsActive = 1 then null else ANU.LockoutEndDateUtc END
				FROM AspNetZnodeUser ANZU 
				INNER JOIN ASPNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName 
				INNER JOIN ZnodeUser ZU ON ANU.ID = ZU.AspNetUserId	
				INNER JOIN #InsertCustomer IC ON ANZU.UserName = IC.UserName 
				WHERE CASE WHEN @IsAllowGlobalLevelUserCreation = 'true' THEN -1 ELSE Isnull(ANZU.PortalId,0) END = CASE WHEN @IsAllowGlobalLevelUserCreation = 'true' THEN -1 ELSE Isnull(@PortalId ,0) END
				----Isnull(ANZU.PortalId,0) = Isnull(@PortalId ,0)

				UPDATE ZU SET 
				ZU.FirstName	= IC.FirstName,
				ZU.LastName		= IC.LastName,
				--ZU.MiddleName	= IC.MiddleName,
				ZU.BudgetAmount = IC.BudgetAmount,
				ZU.Email		= IC.Email,
				ZU.PhoneNumber	= IC.PhoneNumber,
				ZU.EmailOptIn	= Isnull(IC.EmailOptIn,0),
				ZU.IsActive		= IC.IsActive,
				ZU.Custom1 = IC.Custom1,
				ZU.Custom2 = IC.Custom2,
				ZU.Custom3 = IC.Custom3,
				ZU.Custom4 = IC.Custom4,
				ZU.Custom5 = IC.Custom5
				--ZU.ExternalId = ExternalId
				FROM AspNetZnodeUser ANZU INNER JOIN ASPNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName 
				INNER JOIN ZnodeUser ZU ON ANU.ID = ZU.AspNetUserId	
				INNER JOIN #InsertCustomer IC ON ANZU.UserName = IC.UserName 
				WHERE CASE WHEN @IsAllowGlobalLevelUserCreation = 'true' THEN -1 ELSE Isnull(ANZU.PortalId,0) END = CASE WHEN @IsAllowGlobalLevelUserCreation = 'true' THEN -1 ELSE Isnull(@PortalId ,0) END
				--WHERE Isnull(ANZU.PortalId,0) = Isnull(@PortalId ,0)

				INSERT INTO AspNetZnodeUser (AspNetZnodeUserId, UserName, PortalId)		
				OUTPUT INSERTED.AspNetZnodeUserId, INSERTED.UserName, INSERTED.PortalId	INTO  @InsertedAspNetZnodeUser 			 
				SELECT NEWID(),IC.UserName, @PortalId FROM #InsertCustomer IC 
				WHERE NOT EXISTS (SELECT TOP 1 1  FROM AspNetZnodeUser ANZ 
					WHERE CASE WHEN @IsAllowGlobalLevelUserCreation = 'true' THEN -1 ELSE Isnull(ANZ.PortalId,0) END = CASE WHEN @IsAllowGlobalLevelUserCreation = 'true' THEN -1 ELSE Isnull(@PortalId ,0) END 
					AND ANZ.UserName = IC.UserName)

				INSERT INTO ASPNetUsers (Id,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,
				LockoutEndDateUtc,LockOutEnabled,AccessFailedCount,PasswordChangedDate,UserName)
				OUTPUT inserted.Id, inserted.UserName INTO @InsertedASPNetUsers
				SELECT NewId(), Email,0 ,@PasswordHash,@SecurityStamp,PhoneNumber,0,0,case when A.IsActive = 0 then @GetDate else null END LockoutEndDateUtc,1 LockoutEnabled,
				0,@GetDate,AspNetZnodeUserId FROM #InsertCustomer A INNER JOIN @InsertedAspNetZnodeUser  B 
				ON A.UserName = B.UserName
				
				INSERT INTO  ZnodeUser(AspNetUserId,FirstName,LastName,CustomerPaymentGUID,Email,PhoneNumber,EmailOptIn,
				IsActive,ExternalId, CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,UserName,Custom1,Custom2,Custom3,Custom4,Custom5)
				OUTPUT Inserted.UserId, Inserted.AspNetUserId,Inserted.CreatedDate INTO @InsertZnodeUser
				SELECT IANU.Id AspNetUserId ,IC.FirstName,IC.LastName,null CustomerPaymentGUID,IC.Email
				,IC.PhoneNumber,Isnull(IC.EmailOptIn,0),IC.IsActive,IC.ExternalId, @UserId,
				CASE WHEN IC.CreatedDate IS NULL OR IC.CreatedDate = '' THEN  @Getdate ELSE IC.CreatedDate END,
				@UserId,@Getdate,IC.UserName,IC.Custom1,IC.Custom2,IC.Custom3,IC.Custom4,IC.Custom5
				FROM #InsertCustomer IC INNER JOIN 
				@InsertedAspNetZnodeUser IANZU ON IC.UserName = IANZU.UserName  INNER JOIN 
				@InsertedASPNetUsers IANU ON IANZU.AspNetZnodeUserId = IANU.UserName 
				  	     
				INSERT INTO AspNetUserRoles (UserId,RoleId)  SELECT AspNetUserId, @RoleID FROM @InsertZnodeUser 
				INSERT INTO ZnodeUserPortal (UserId,PortalId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate) 
				SELECT UserId, @PortalId , @UserId, IZU.CreatedDate,@UserId,@Getdate 
				FROM @InsertZnodeUser IZU

				INSERT INTO ZnodeAccountUserPermission(UserId,AccountPermissionAccessId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
				SELECT UserId, 4 , @UserId, @Getdate,@UserId,@Getdate 
				FROM @InsertZnodeUser IZU
		
				------------INSERT INTO ZnodeUserGlobalAttributeValue
	
				IF OBJECT_ID('tempdb..#globaldata') IS NOT NULL 
					DROP TABLE #globaldata
				
				SELECT ZU.userid,PerOrderLimit,
					PerOrderAnnualLimit,
					BillingAccountNumber,
					EnableUserShippingAddressSuggestion,
					EnablePowerBIReportOnWebStore 
				INTO #globaldata
				FROM znodeuser ZU INNER JOIN  #InsertCustomer IC on IC.Email =ZU.EMAIL
				WHERE ZU.userid IN (SELECT userid FROM @InsertZnodeUser)
				GROUP BY ZU.userid,PerOrderLimit,
									PerOrderAnnualLimit,
									BillingAccountNumber,
									EnableUserShippingAddressSuggestion,
									EnablePowerBIReportOnWebStore 

				IF OBJECT_ID('tempdb..#globaldata1') IS NOT NULL DROP TABLE #globaldata1
				SELECT * INTO #globaldata1 FROM (SELECT userid, Cast(PerOrderLimit as varchar(100)) PerOrderLimit,
				 Cast(PerOrderAnnualLimit as varchar(100)) PerOrderAnnualLimit,
				 Cast(BillingAccountNumber as varchar(100)) BillingAccountNumber,
				 Cast(case when cast(EnableUserShippingAddressSuggestion as varchar(10))= '1' or cast(EnableUserShippingAddressSuggestion as varchar(10)) ='YES' or  cast(EnableUserShippingAddressSuggestion as varchar(10)) ='True' then 'True' else 'False'  END  as varchar(100)) EnableUserShippingAddressSuggestion,
				 Cast(case when cast(EnablePowerBIReportOnWebStore as varchar(10))= '1'  or cast(EnablePowerBIReportOnWebStore as varchar(10)) ='YES' or  cast(EnablePowerBIReportOnWebStore as varchar(10)) ='True' then 'True' else 'False' END  as varchar(100)) EnablePowerBIReportOnWebStore FROM #globaldata) ab
				UNPIVOT  
				   (avalue FOR acode IN   
					  (PerOrderLimit,
				PerOrderAnnualLimit,
				BillingAccountNumber,
				EnableUserShippingAddressSuggestion,
				EnablePowerBIReportOnWebStore)  
				)AS unpvt;  

				INSERT INTO ZnodeUserGlobalAttributeValue (UserId,	GlobalAttributeId,	GlobalAttributeDefaultValueId,	AttributeValue,	CreatedBy,	CreatedDate,	ModifiedBy,	ModifiedDate)
				SELECT g.Userid,ZGA.GlobalAttributeId,null,null,@UserId,@Getdate,@UserId,@Getdate
					FROM #globaldata1 g INNER JOIN ZnodeGlobalAttribute ZGA on ZGA.AttributeCode =g.acode
					WHERE NOT EXISTS (SELECT top 1 1 FROM ZnodeUserGlobalAttributeValue ZGAV WHERE ZGAV.Userid =g.Userid and ZGA.GlobalAttributeId=ZGAV.GlobalAttributeId)

				INSERT INTO ZnodeUserGlobalAttributeValuelocale(UserGlobalAttributeValueId,	LocaleId,	AttributeValue,	CreatedBy,	CreatedDate,	ModifiedBy,	ModifiedDate,	GlobalAttributeDefaultValueId,	MediaId,	MediaPath)
				SELECT UserGlobalAttributeValueId, @LocaleId,avalue ,@UserId,@Getdate,@UserId,@Getdate,null,null,null
					FROM ZnodeUserGlobalAttributeValue ZUGAV INNER JOIN #globaldata1 g on ZUGAV.userid =g.userid
						INNER JOIN ZnodeGlobalAttribute ZGA on ZGA.AttributeCode =g.acode and ZGA.GlobalAttributeId = ZUGAV.GlobalAttributeId
						WHERE NOT EXISTS (SELECT Top 1 1 FROM ZnodeUserGlobalAttributeValuelocale z WHERE z.UserGlobalAttributeValueId = ZUGAV.UserGlobalAttributeValueId)

				---------------------------------------------------------------------------------

				declare @Profile table (ProfileId INT)

				INSERT INTO ZnodeProfile (ProfileName,ShowOnPartnerSignup,Weighting,TaxExempt,DefaultExternalAccountNo,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,ParentProfileId)
				OUTPUT inserted.ProfileId INTO @Profile(ProfileId)
				SELECT Distinct ProfileName, 0, null,0, replace(ltrim(rtrim(ProfileName)),' ','') as DefaultExternalAccountNo, @UserId,@Getdate, @UserId,@Getdate, null as ParentProfileId				
				FROM #InsertCustomer IC
				WHERE NOT EXISTS(SELECT * FROM ZnodeProfile ZP WHERE IC.ProfileName = ZP.ProfileName )
				AND ISNULL(ic.ProfileName,'') <> ''

				INSERT INTO ZnodePortalProfile (PortalId,	ProfileId,	IsDefaultAnonymousProfile,	IsDefaultRegistedProfile,	CreatedBy,	CreatedDate,	ModifiedBy,	ModifiedDate)
				SELECT @PortalId, ProfileId, 0 AS IsDefaultAnonymousProfile, 0 AS IsDefaultRegistedProfile, @UserId,@Getdate, @UserId,@Getdate
				FROM @Profile

				UPDATE ZnodeUserProfile 
				SET ProfileId = COALESCE(ZP.ProfileId,@ProfileId)
				FROM ZnodeUser a
				INNER JOIN ASPNetUsers b on (b.Id = a.AspNetUserId)
				INNER JOIN AspNetZnodeUser c on (c.AspNetZnodeUserId = b.UserName)
				INNER JOIN #InsertCustomer IC on (IC.UserName = c.UserName)
				INNER JOIN ZnodeUserProfile u ON u.UserId = a.UserId
				LEFT join ZnodeProfile ZP on IC.ProfileName = ZP.ProfileName
				--WHERE IC.ProfileName <> ''
				
				INSERT INTO ZnodeUserProfile (ProfileId,UserId,IsDefault,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
				SELECT COALESCE(ZP.ProfileId,@ProfileId)  , a.UserId, 1 , @UserId,a.CreatedDate,@UserId,@Getdate 
				FROM ZnodeUser a
				INNER JOIN ASPNetUsers b on (b.Id = a.AspNetUserId)
				INNER JOIN AspNetZnodeUser c on (c.AspNetZnodeUserId = b.UserName)
				INNER JOIN #InsertCustomer IC on (IC.UserName = c.UserName)
				LEFT join ZnodeProfile ZP on IC.ProfileName = ZP.ProfileName
				WHERE NOT EXISTS (SELECT TOP  1 1 FROM ZnodeUserProfile u WHERE u.UserId = a.UserId )
				AND EXISTS(SELECT * FROM @InsertZnodeUser IZU WHERE A.UserId = IZU.UserId)

				---to update accountid agaist user
				UPDATE ZU SET ZU.AccountId = ZA.AccountId 
				FROM AspNetZnodeUser ANZU INNER JOIN ASPNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName 
				INNER JOIN ZnodeUser ZU ON ANU.ID = ZU.AspNetUserId	 
				INNER JOIN #InsertCustomer IC ON ANZU.UserName = IC.UserName
				INNER JOIN ZnodeAccount ZA ON ZA.AccountCode = IC.AccountCode 
				--INNER JOIN @InsertZnodeUser IZU on IZU.UserId =ZU.UserId
				WHERE Isnull(ANZU.PortalId,0) = Isnull(@PortalId ,0) and isnull(IC.AccountCode,'') <> ''
				
				update ZDU set ZDU.DepartmentId = ZD.DepartmentId, ModifiedBy = @UserId, ModifiedDate = @Getdate
				FROM ZnodeUser a
				INNER JOIN ASPNetUsers b on (b.Id = a.AspNetUserId)
				INNER JOIN AspNetZnodeUser c on (c.AspNetZnodeUserId = b.UserName)
				INNER JOIN #InsertCustomer IC on (IC.UserName = c.UserName)
				INNER JOIN ZnodeDepartment ZD on IC.DepartmentName = ZD.DepartmentName
				INNER JOIN ZnodeDepartmentUser ZDU on ZDU.UserId = a.UserId
				WHERE isnull(IC.DepartmentName,'') <> ''

				INSERT INTO ZnodeDepartmentUser(UserId,DepartmentId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
				SELECT a.UserId, ZD.DepartmentId, @UserId,a.CreatedDate,@UserId,@Getdate 
				FROM ZnodeUser a
				INNER JOIN ASPNetUsers b on (b.Id = a.AspNetUserId)
				INNER JOIN AspNetZnodeUser c on (c.AspNetZnodeUserId = b.UserName)
				INNER JOIN #InsertCustomer IC on (IC.UserName = c.UserName)
				INNER JOIN ZnodeDepartment ZD on IC.DepartmentName = ZD.DepartmentName
				WHERE NOT EXISTS (SELECT TOP  1 1 FROM ZnodeDepartmentUser u WHERE u.UserId = a.UserId)
				AND isnull(IC.DepartmentName,'') <> ''
		
				update u set u.RoleId = ZD.Id
				FROM ZnodeUser a
				INNER JOIN ASPNetUsers b on (b.Id = a.AspNetUserId)
				INNER JOIN AspNetZnodeUser c on (c.AspNetZnodeUserId = b.UserName)
				INNER JOIN #InsertCustomer IC on (IC.UserName = c.UserName)
				INNER JOIN AspNetRoles ZD on IC.RoleName = ZD.Name
				INNER JOIN AspNetUserRoles u on u.UserId = b.Id
				WHERE isnull(IC.RoleName,'') <> ''
				
				INSERT INTO AspNetUserRoles(UserId,RoleId)
				SELECT b.Id as ASPNetUserId, ZD.Id as RoleId
				FROM ZnodeUser a
				INNER JOIN ASPNetUsers b on (b.Id = a.AspNetUserId)
				INNER JOIN AspNetZnodeUser c on (c.AspNetZnodeUserId = b.UserName)
				INNER JOIN #InsertCustomer IC on (IC.UserName = c.UserName)
				INNER JOIN AspNetRoles ZD on IC.RoleName = ZD.Name
				WHERE NOT EXISTS (SELECT TOP  1 1 FROM AspNetUserRoles u WHERE u.UserId = b.Id)
				AND EXISTS(SELECT * FROM @InsertZnodeUser IZU WHERE A.UserId = IZU.UserId)
				AND isnull(IC.RoleName,'') <> ''


		UPDATE ZnodeImportProcessLog
		  SET Status = dbo.Fn_GetImportStatus( 2 ), ProcessCompletedDate = @GetDate
		WHERE ImportProcessLogId = @ImportProcessLogId;

		COMMIT TRAN A;
	END TRY
	BEGIN CATCH
	ROLLBACK TRAN A;

		SET @Status = 0;
		SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE();
		
		 DECLARE @Error_procedure VARCHAR(8000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(), @ErrorCall NVARCHAR(MAX)= 'EXEC Znode_ImportCustomer @TableName = '+CAST(@TableName AS VARCHAR(max))+',@UserId = '+CAST(@UserId AS VARCHAR(50))+',@ImportProcessLogId='+CAST(@ImportProcessLogId AS VARCHAR(10))+',@PortalId='+CAST(@PortalId AS VARCHAR(10))+',@CsvColumnString='+CAST(@CsvColumnString AS VARCHAR(max));
              	
		 ---Import process updating fail due to database error
		UPDATE ZnodeImportProcessLog
		SET Status = dbo.Fn_GetImportStatus( 3 ), ProcessCompletedDate = @GetDate
		WHERE ImportProcessLogId = @ImportProcessLogId;

		---Loging error for Import process due to database error
		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		SELECT '93', '', '', @NewGUId,  @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId

		--Updating total and fail record count
		UPDATE ZnodeImportProcessLog SET FailedRecordcount = (SELECT TOP 1 RowsCount FROM Znode_ImportCsvRowCount with (nolock) WHERE ImportProcessLogId = @ImportProcessLogId) , SuccessRecordCount = 0 ,
		TotalProcessedRecords = (SELECT TOP 1 RowsCount FROM Znode_ImportCsvRowCount with (nolock) WHERE ImportProcessLogId = @ImportProcessLogId)
		WHERE ImportProcessLogId = @ImportProcessLogId;

        EXEC Znode_InsertProcedureErrorLog
		@ProcedureName = 'Znode_ImportCustomer',
		@ErrorInProcedure = @Error_procedure,
		@ErrorMessage = @ErrorMessage,
		@ErrorLine = @ErrorLine,
		@ErrorCall = @ErrorCall;
	END CATCH;
END;

Go
--------------------
IF EXISTS(SELECT * FROM SYS.PROCEDURES WHERE NAME = 'Znode_ImportCustomerAddress')
	DROP PROC Znode_ImportCustomerAddress
GO
CREATE PROCEDURE [dbo].[Znode_ImportCustomerAddress]
(
	  @TableName NVARCHAR(100), 
	  @Status BIT OUT, 
	  @UserId INT, 
	  @ImportProcessLogId INT, 
	  @NewGUId NVARCHAR(200), 
	  @LocaleId INT= 0,
	  @PortalId INT ,
	  @CsvColumnString NVARCHAR(max), 
	  @IsAccountAddress BIT = 0 
)
AS
	--------------------------------------------------------------------------------------
	-- Summary :  Import SEO Details
	
	-- Unit Testing : 
	--------------------------------------------------------------------------------------

BEGIN
	BEGIN TRAN A;
	BEGIN TRY
		DECLARE @MessageDisplay NVARCHAR(100), @SSQL NVARCHAR(max),@IsAllowGlobalLevelUserCreation NVARCHAR(10)

		DECLARE @GETDATE DATETIME= dbo.Fn_GETDATE();
		-- Retrive Value FROM global setting 
		SELECT @IsAllowGlobalLevelUserCreation = FeatureValues FROM ZnodeGlobalsetting WHERE FeatureName = 'AllowGlobalLevelUserCreation'
		-- Three type of import required three table varible for product , category AND brand

		CREATE TABLE #InsertCustomerAddress 
		( 
			RowId INT IDENTITY(1, 1) PRIMARY KEY, RowNumber INT,UserName NVARCHAR(512),FirstName VARCHAR(300),
			LastName VARCHAR(300),DisplayName NVARCHAR(1200),Address1 VARCHAR(300),Address2 VARCHAR(300),
			CountryName VARCHAR(3000),StateName VARCHAR(3000),CityName VARCHAR(3000),PostalCode VARCHAR(50),
			PhoneNumber VARCHAR(50),IsDefaultBilling BIT,IsDefaultShipping BIT	,IsActive BIT,ExternalId NVARCHAR(2000),
			CompanyName NVARCHAR(2000), GUID NVARCHAR(400)
		);
	
		SET @SSQL = ' INSERT INTO #InsertCustomerAddress ( RowNumber, ' + @CsvColumnString + ' ,GUID )
		SELECT RowNumber,' + @CsvColumnString + ',GUID FROM '+ @TableName;
		EXEC sys.sp_sqlexec @SSQL;

		-- start Functional Validation 
		----------------------------------------------
		IF (@IsAccountAddress = 0)
		BEGIN
			IF @IsAllowGlobalLevelUserCreation = 'true'
					INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
						   SELECT '19', 'UserName', UserName, @NewGUId, RowNumber, @UserId, @GETDATE, @UserId, @GETDATE, @ImportProcessLogId
						   FROM #InsertCustomerAddress AS ii
						   WHERE ii.UserName NOT IN 
						   (
							   SELECT UserName FROM AspNetZnodeUser   WHERE PortalId = @PortalId
						   );
			ELSE 
					INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
						   SELECT '19', 'UserName', UserName, @NewGUId, RowNumber, @UserId, @GETDATE, @UserId, @GETDATE, @ImportProcessLogId
						   FROM #InsertCustomerAddress AS ii
						   WHERE ii.UserName NOT IN 
						   (
							   SELECT UserName FROM AspNetZnodeUser   
						   );

					INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
							SELECT '8', 'UserName', UserName, @NewGUId, RowNumber, @UserId, @GETDATE, @UserId, @GETDATE, @ImportProcessLogId
							FROM #InsertCustomerAddress AS ii
							WHERE ISNULL(ltrim(rtrim(ii.UserName)), '') = ''

		 END

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		SELECT '8', 'StateName', StateName, @NewGUId, RowNumber, @UserId, @GETDATE, @UserId, @GETDATE, @ImportProcessLogId
		FROM #InsertCustomerAddress AS ii
		WHERE NOT EXISTS(SELECT * FROM ZnodeState ZS INNER JOIN ZnodeCountry ZC ON ZS.CountryCode = ZC.CountryCode
			where ii.CountryName = ZC.CountryName AND ZS.StateName = ISNULL(ltrim(rtrim(ii.StateName)), ''))

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		SELECT '8', 'CountryName', CountryName, @NewGUId, RowNumber, @UserId, @GETDATE, @UserId, @GETDATE, @ImportProcessLogId
		FROM #InsertCustomerAddress AS ii
		WHERE NOT EXISTS(SELECT * FROM ZnodeCountry ZC WHERE ZC.CountryName = ISNULL(ltrim(rtrim(ii.CountryName)), '') )

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		SELECT '90', 'CountryName', CountryName, @NewGUId, RowNumber, @UserId, @GETDATE, @UserId, @GETDATE, @ImportProcessLogId
		FROM #InsertCustomerAddress AS ii
		WHERE ISNULL(ltrim(rtrim(ii.CountryName)), '') <> ''
		AND NOT EXISTS(SELECT * FROM ZnodePortalCountry ZPC INNER JOIN ZnodeCountry ZC ON ZPC.CountryCode = ZC.CountryCode
		    WHERE PortalId = @PortalId AND ltrim(rtrim(ii.CountryName)) = ltrim(rtrim(ZC.CountryName)))

		 -- error log when atleast db have 
		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		SELECT '63', 'IsDefaultBilling/IsDefaultShipping', IsDefaultBilling, @NewGUId, RowNumber, @UserId, @GETDATE, @UserId, @GETDATE, @ImportProcessLogId
		FROM #InsertCustomerAddress IC WHERE  exists (
		SELECT TOP 1 1  FROM AspNetZnodeUser ANZU INNER JOIN ASPNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName 
		INNER JOIN ZnodeUser ZU ON ANU.ID = ZU.AspNetUserId	
		INNER JOIN ZnodeUserAddress ZUA ON ZUA.UserId = ZU.UserId
		INNER JOIN ZnodeAddress ZA ON ZUA.AddressId = ZA.AddressId
		where ANZU.UserName = IC.UserName AND ZA.IsDefaultBilling =IC.IsDefaultBilling 
		AND ZA.IsDefaultShipping =IC.IsDefaultShipping )

		--Note : Content page import is not required 
		
		-- End Function Validation 	
		-----------------------------------------------
		UPDATE ZIL
		SET ZIL.ColumnName =   ZIL.ColumnName + ' [ UserName - ' + ISNULL(UserName,'') + ' ] '
		FROM ZnodeImportLog ZIL 
		INNER JOIN #InsertCustomerAddress IPA ON (ZIL.RowNumber = IPA.RowNumber)
		WHERE  ZIL.ImportProcessLogId = @ImportProcessLogId AND ZIL.RowNumber IS NOT NULL


		DELETE FROM #InsertCustomerAddress
		WHERE RowNumber IN
		(
			SELECT DISTINCT 
				   RowNumber
			FROM ZnodeImportLog
			WHERE ImportProcessLogId = @ImportProcessLogId  AND RowNumber IS NOT NULL 
			--AND GUID = @NewGUID
		);

		UPDATE ZA SET ZA.IsDefaultBilling = 0
		FROM AspNetZnodeUser ANZU INNER JOIN ASPNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName 
		INNER JOIN ZnodeUser ZU ON ANU.ID = ZU.AspNetUserId	
		INNER JOIN ZnodeUserAddress ZUA ON ZUA.UserId = ZU.UserId
		INNER JOIN ZnodeAddress ZA ON ZUA.AddressId = ZA.AddressId
		INNER JOIN #InsertCustomerAddress IC ON ANZU.UserName = IC.UserName --and IC.IsDefaultBilling <> ZA.IsDefaultBilling 
		where IC.IsDefaultBilling = 1 

		UPDATE ZA SET ZA.IsDefaultShipping = 0
		FROM AspNetZnodeUser ANZU INNER JOIN ASPNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName 
		INNER JOIN ZnodeUser ZU ON ANU.ID = ZU.AspNetUserId	
		INNER JOIN ZnodeUserAddress ZUA ON ZUA.UserId = ZU.UserId
		INNER JOIN ZnodeAddress ZA ON ZUA.AddressId = ZA.AddressId
		INNER JOIN #InsertCustomerAddress IC ON ANZU.UserName = IC.UserName
		where IC.IsDefaultShipping = 1 

		DELETE FROM ZnodeImportLog  WHERE ImportProcessLogId = @ImportProcessLogId AND  ErrorDescription = '63'
		-- UPDATE Record count in log 
        DECLARE @FailedRecordCount BIGINT
		DECLARE @SuccessRecordCount BIGINT
		SELECT @FailedRecordCount = COUNT(DISTINCT RowNumber) FROM ZnodeImportLog WHERE RowNumber IS NOT NULL AND  ImportProcessLogId = @ImportProcessLogId;
		SELECT @SuccessRecordCount = count(DISTINCT RowNumber) FROM #InsertCustomerAddress
		UPDATE ZnodeImportProcessLog SET FailedRecordcount = @FailedRecordCount , SuccessRecordCount = @SuccessRecordCount ,
		TotalProcessedRecords = (ISNULL(@FailedRecordCount,0) + ISNULL(@SuccessRecordCount,0))
		WHERE ImportProcessLogId = @ImportProcessLogId;
		-- End
		
		DELETE FROM #InsertCustomerAddress 
		WHERE NOT EXISTS
			(SELECT TOP 1 1 FROM 
				(
					SELECT ROW_NUMBER() OVER(PARTITION BY UserName	
					,FirstName	,LastName	,DisplayName	,Address1	,Address2	
					,CountryName	,StateName	,CityName	,PostalCode	
					,PhoneNumber	,IsDefaultBilling,IsDefaultShipping,ExternalId	,CompanyName ORDER BY RowId desc) 
					AS rw_nmbr,RowId  FROM #InsertCustomerAddress 
				) abc WHERE rw_nmbr =1 AND abc.RowId = #InsertCustomerAddress.RowId
			)

		----------UPDATE ZnodeAddress
		DECLARE @AddressColumnString VARCHAR(1000), @WhereConditionString VARCHAR(1000), @UpdateColumnString VARCHAR(1000)

		SELECT @AddressColumnString = COALESCE(@AddressColumnString + ',', '')+a.ColumnName --COALESCE(@CsvColumnString + ' AND ', '') +'ZA.'+ColumnName+' =  IC.'+ColumnName
		FROM ZnodeImportUpdatableColumns a
		INNER JOIN INFORMATION_SCHEMA.COLUMNS b ON a.ColumnName = b.COLUMN_NAME  
		INNER JOIN dbo.Split(@CsvColumnString,',')C ON b.COLUMN_NAME = c.Item
		WHERE b.TABLE_NAME = 'ZnodeAddress' 
		AND EXISTS(SELECT * FROM ZnodeImportHead IH WHERE a.ImportHeadId = IH.ImportHeadId AND IH.Name in ('CustomerAddress','ShippingAddress'))

		SELECT @UpdateColumnString = COALESCE(@UpdateColumnString + ' , ', '') +'ZA.'+a.COLUMN_NAME+' =  IC.'+a.COLUMN_NAME  
		FROM INFORMATION_SCHEMA.COLUMNS a
		INNER JOIN dbo.Split(@CsvColumnString,',')b ON a.COLUMN_NAME = b.Item
		WHERE NOT EXISTS (SELECT * FROM dbo.Split(@AddressColumnString,',') c WHERE a.COLUMN_NAME = c.Item )
		AND a.TABLE_NAME = 'ZnodeAddress'

		SELECT @WhereConditionString = COALESCE(@WhereConditionString + ' AND ', '') +'ZA.'+item+' =  IC.'+item FROM dbo.split(@AddressColumnString,',')
		SET @WhereConditionString = Replace(@WhereConditionString , 'ZA.Address2 =  IC.Address2', 'ISNULL(ZA.Address2 ,'''')=  ISNULL(IC.Address2 ,'''')') 
		
		UPDATE a SET a.StateName =c.StateCode  
		FROM #InsertCustomerAddress a 
		INNER JOIN ZnodeCountry b ON a.CountryName = b.CountryName 
		INNER JOIN ZnodeState c ON b.CountryCode = c.CountryCode AND C.StateName = a.StateName
		
		UPDATE a SET a.CountryName= b.CountryCode FROM  #InsertCustomerAddress a INNER JOIN ZnodeCountry b ON a.CountryName = b.CountryName 
		
		CREATE TABLE #InsertedUserAddress (AddressId  NVARCHAR(256), UserId NVARCHAR(max)) 

		IF ( @IsAccountAddress = 1 )
		BEGIN
			UPDATE ZnodeAddress SET IsDefaultBilling = 0,  IsDefaultShipping = 0
			FROM AspNetZnodeUser ANZU 
			INNER JOIN ASPNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName 
			INNER JOIN ZnodeUser ZU ON ANU.ID = ZU.AspNetUserId	
			INNER JOIN ZnodeUserAddress ZUA ON ZUA.UserId = ZU.UserId
			INNER JOIN ZnodeAddress ZA ON ZUA.AddressId = ZA.AddressId
			INNER JOIN #InsertCustomerAddress IC ON ANZU.UserName = IC.UserName AND ZA.IsDefaultBilling =IC.IsDefaultBilling 
													AND ZA.IsDefaultShipping =IC.IsDefaultShipping

			SET @SSQL = '
				UPDATE ZA set ModifiedBy = '+CONVERT(VARCHAR(10), @UserId)+', ModifiedDate = GETDATE() '+CASE WHEN ISNULL(@UpdateColumnString,'') = '' THEN '' ELSE ','+@UpdateColumnString END+' 
				FROM ZnodeAddress ZA
				INNER JOIN #InsertCustomerAddress IC ON '+CASE WHEN ISNULL(@WhereConditionString,'') = '' THEN ' 1 = 0 ' ELSE 'ZA.FirstName = IC.FirstName AND 
				ZA.LastName = IC.LastName  AND 
				ZA.DisplayName	 = IC.DisplayName AND 
				ZA.CompanyName	 = IC.CompanyName AND 
				ZA.Address1 = IC.Address1' END

			EXEC (@SSQL)

			DELETE FROM #InsertCustomerAddress
			where  exists(SELECT TOP 1 1 FROM Znodeaddress ZA WHERE ZA.FirstName = #InsertCustomerAddress.FirstName AND 
								ZA.LastName = #InsertCustomerAddress.LastName  AND 
								ZA.DisplayName	 = #InsertCustomerAddress.DisplayName AND 
								ZA.CompanyName	 = #InsertCustomerAddress.CompanyName AND 
								ZA.Address1 = #InsertCustomerAddress.Address1)

			SET @SSQL = '
			INSERT INTO ZnodeAddress (FirstName,LastName,DisplayName,Address1,Address2,Address3,CountryName,
									StateName,CityName,PostalCode,PhoneNumber,
									IsDefaultBilling,IsDefaultShipping,IsActive,ExternalId,CompanyName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsShipping,IsBilling)		
			OUTPUT INSERTED.AddressId INTO  #InsertedUserAddress (AddressId) 			 
			SELECT IC.FirstName,IC.LastName,IC.DisplayName,IC.Address1,IC.Address2,null,IC.CountryName ,
			IC.StateName,
			,IC.CityName,IC.PostalCode,IC.PhoneNumber,
			ISNULL(IC.IsDefaultBilling,0),ISNULL(IC.IsDefaultShipping,0),ISNULL(IC.IsActive,0),IC.ExternalId,IC.CompanyName, '+CONVERT(VARCHAR(10), @UserId)+' , GETDATE() , '+CONVERT(VARCHAR(10), @UserId)+' ,GETDATE(), 1, 1
			FROM  #InsertCustomerAddress IC
			WHERE NOT EXISTS(SELECT * FROM ZnodeAddress ZA WHERE '+CASE WHEN ISNULL(@WhereConditionString,'') = '' THEN ' 1 = 0 ' ELSE @WhereConditionString END +')'

			EXEC (@SSQL)

			DECLARE @AccountId INT
			SELECT @AccountId = AccountId FROM ZnodeUser WHERE UserId = @UserId
			INSERT INTO ZnodeAccountAddress ( AccountId, AddressId, CreatedBy, CreatedDate,	ModifiedBy,	ModifiedDate )
			SELECT @AccountId, Addressid ,  @UserId , @GETDATE, @UserId , @GETDATE FROM #InsertedUserAddress UA
			WHERE NOT EXISTS ( SELECT * FROM ZnodeAccountAddress AA WHERE AccountId = @AccountId AND AA.Addressid = UA.Addressid )
		END
		ELSE
		BEGIN
		
			UPDATE ZnodeAddress SET IsDefaultBilling = 0,  IsDefaultShipping = 0
			FROM AspNetZnodeUser ANZU INNER JOIN ASPNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName 
			INNER JOIN ZnodeUser ZU ON ANU.ID = ZU.AspNetUserId	
			INNER JOIN ZnodeUserAddress ZUA ON ZUA.UserId = ZU.UserId
			INNER JOIN ZnodeAddress ZA ON ZUA.AddressId = ZA.AddressId
			INNER JOIN #InsertCustomerAddress IC ON ANZU.UserName = IC.UserName AND ZA.IsDefaultBilling =IC.IsDefaultBilling 
			AND ZA.IsDefaultShipping =IC.IsDefaultShipping
					
			SET @SSQL = '
				UPDATE ZA set ModifiedBy = '+CONVERT(VARCHAR(10), @UserId)+', ModifiedDate = GETDATE() '+CASE WHEN ISNULL(@UpdateColumnString,'') = '' THEN '' ELSE ','+@UpdateColumnString END+' 
				FROM ZnodeAddress ZA
				INNER JOIN #InsertCustomerAddress IC ON '+CASE WHEN ISNULL(@WhereConditionString,'') = '' THEN ' 1 = 0 ' ELSE 'ZA.FirstName = IC.FirstName AND 
				ZA.LastName = IC.LastName  AND 
				ZA.DisplayName	 = IC.DisplayName AND 
				ZA.CompanyName	 = IC.CompanyName AND 
				ZA.Address1 = IC.Address1' END

			EXEC (@SSQL)

			DELETE FROM #InsertCustomerAddress
			WHERE EXISTS(SELECT TOP 1 1 FROM Znodeaddress ZA 
			      WHERE ZA.FirstName = #InsertCustomerAddress.FirstName AND 
					ZA.LastName = #InsertCustomerAddress.LastName  AND 
					ZA.DisplayName	 = #InsertCustomerAddress.DisplayName AND 
					ZA.CompanyName	 = #InsertCustomerAddress.CompanyName AND 
					ZA.Address1 = #InsertCustomerAddress.Address1)

			SET @SSQL = '
			INSERT INTO ZnodeAddress (FirstName,LastName,DisplayName,Address1,Address2,Address3,CountryName,
										StateName,CityName,PostalCode,PhoneNumber,
										IsDefaultBilling,IsDefaultShipping,IsActive,ExternalId,CompanyName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsShipping,IsBilling)		
			OUTPUT INSERTED.AddressId, INSERTED.Address3 INTO  #InsertedUserAddress (AddressId, UserId ) 			 
			SELECT IC.FirstName,IC.LastName,IC.DisplayName,IC.Address1,IC.Address2,convert(NVARCHAR(100),ZU.UserId),IC.CountryName ,
				IC.StateName,IC.CityName,IC.PostalCode,IC.PhoneNumber,ISNULL(IC.IsDefaultBilling,0),ISNULL(IC.IsDefaultShipping,0),
				ISNULL(IC.IsActive,0),IC.ExternalId,IC.CompanyName, '+CONVERT(VARCHAR(10), @UserId)+' , GETDATE() , '+CONVERT(VARCHAR(10), @UserId)+' ,GETDATE(),1,1
			FROM AspNetZnodeUser ANZU 
			INNER JOIN ASPNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName 
			INNER JOIN ZnodeUser ZU ON ANU.ID = ZU.AspNetUserId	
			INNER JOIN #InsertCustomerAddress IC ON ANZU.UserName = IC.UserName 
			WHERE NOT EXISTS(SELECT * FROM ZnodeAddress ZA WHERE '+CASE WHEN ISNULL(@WhereConditionString,'') = '' THEN ' 1 = 0 ' ELSE @WhereConditionString END +')'
					
			EXEC (@SSQL)

			INSERT INTO ZnodeUserAddress(UserId,AddressId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
			SELECT CAST( UserId AS INT ) , Addressid , @UserId , @GETDATE, @UserId , @GETDATE FROM  #InsertedUserAddress
		END
				
		UPDATE ZA SET ZA.CountryName = ZC.CountryCode
		FROM ZnodeAddress ZA
		INNER JOIN ZnodeCountry ZC ON ltrim(rtrim(ZA.CountryName)) = ltrim(rtrim(ZC.CountryName))

		UPDATE ZA SET ZA.Address3 = null 
		FROM ZnodeAddress ZA INNER JOIN #InsertedUserAddress IUA ON ZA.AddressId = IUA.AddressId 

		
		UPDATE ZnodeImportProcessLog
		SET Status = dbo.Fn_GetImportStatus( 2 ), ProcessCompletedDate = @GETDATE
		WHERE ImportProcessLogId = @ImportProcessLogId;

		COMMIT TRAN A;
	END TRY
	BEGIN CATCH
	ROLLBACK TRAN A;

		SET @Status = 0;
		SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE();

		DECLARE @Error_procedure VARCHAR(8000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(), @ErrorCall NVARCHAR(MAX)= 'EXEC Znode_ImportCustomerAddress @TableName = '+CAST(@TableName AS VARCHAR(max)) +',@Status='+ CAST(@Status AS VARCHAR(10))+',@UserId = '+CAST(@UserId AS VARCHAR(50))+',@ImportProcessLogId='+CAST(@ImportProcessLogId AS VARCHAR(10))+',@NewGUId='+CAST(@NewGUId AS VARCHAR(200))+',@LocaleId='+CAST(@LocaleId AS VARCHAR(max)) +',@PortalId='+CAST(@PortalId AS VARCHAR(max)) +',@CsvColumnString='+CAST(@CsvColumnString AS VARCHAR(max)) +',@IsAccountAddress='+CAST(@IsAccountAddress AS VARCHAR(max));


		---Import process updating fail due to database error
		UPDATE ZnodeImportProcessLog
		SET Status = dbo.Fn_GetImportStatus( 3 ), ProcessCompletedDate = @GETDATE
		WHERE ImportProcessLogId = @ImportProcessLogId;

		---Loging error for Import process due to database error
		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		SELECT '93', '', '', @NewGUId,  @UserId, @GETDATE, @UserId, @GETDATE, @ImportProcessLogId

		--Updating total AND fail record count
		UPDATE ZnodeImportProcessLog SET FailedRecordcount = (SELECT TOP 1 RowsCount FROM Znode_ImportCsvRowCount with (nolock) WHERE ImportProcessLogId = @ImportProcessLogId) , SuccessRecordCount = 0 ,
		TotalProcessedRecords = (SELECT TOP 1 RowsCount FROM Znode_ImportCsvRowCount with (nolock) WHERE ImportProcessLogId = @ImportProcessLogId)
		WHERE ImportProcessLogId = @ImportProcessLogId;

		EXEC Znode_InsertProcedureErrorLog
		@ProcedureName = 'Znode_ImportCustomerAddress',
		@ErrorInProcedure = @Error_procedure,
		@ErrorMessage = @ErrorMessage,
		@ErrorLine = @ErrorLine,
		@ErrorCall = @ErrorCall;
		
	END CATCH;
END
Go	
---------------------------------
IF EXISTS(SELECT * FROM SYS.PROCEDURES WHERE NAME = 'Znode_GetFilterPimProductId')
	DROP PROC Znode_GetFilterPimProductId
GO

CREATE PROCEDURE [dbo].[Znode_GetFilterPimProductId]
(
  @WhereClause XML 
 ,@PimProductId TransferId READONLY 
 ,@LocaleId   INT 
)
AS 
BEGIN 
SET NOCOUNT ON 

		DECLARE  @DefaultLocaleId INT = dbo.Fn_GetDefaultLocaleID()
		DECLARE @SQL NVARCHAR(MAX)
		DECLARE @InternalProductWhereClause NVARCHAR(MAX)

		DECLARE @WorkingProcess INT = 0 

		DECLARE @TBL_FilterClause TABLE (ID INT IDENTITY(1,1),AttributeValue NVARCHAR(MAX),AttributeCode NVARCHAr(MAX),PimAttributeId INT ,AttributeTypeName VARCHAR(300),AttributeCodeOrg VARCHAR(600))

		DECLARE @WhereClauseXML XML = @WhereClause 

		SET @SQL  = ''

		IF EXISTS (SELECT TOP 1 1 FROM @WhereClauseXml.nodes ( '//ArrayOfWhereClauseModel/WhereClauseModel'  ) AS Tbl(Col) 
		WHERE Tbl.Col.value ( 'attributecode[1]' , 'NVARCHAR(MAX)')  LIKE  '% in (%')
		BEGIN 
			SET @WorkingProcess = 1

				INSERT INTO @TBL_FilterClause (AttributeValue,AttributeCode,AttributeTypeName,PimAttributeId,AttributeCodeOrg)
			SELECT  Tbl.Col.value ( 'attributevalue[1]' , 'NVARCHAR(MAX)') AS AttributeValue
			,Tbl.Col.value ( 'attributecode[1]' , 'NVARCHAR(MAX)') AS AttributeValue,ZTY.AttributeTypeName,ZPA.PimAttributeId,AttributeCode AttributeCodeOrg
			FROM @WhereClauseXml.nodes ( '//ArrayOfWhereClauseModel/WhereClauseModel'  ) AS Tbl(Col)
			LEFT JOIN  ZnodePimAttribute ZPA  ON ((Tbl.Col.value ( 'attributecode[1]' , 'NVARCHAR(MAX)')  LIKE '%in (%' OR dbo.Fn_Trim(REPLACE(REPLACE(Tbl.Col.value ( 'attributecode[1]' , 'NVARCHAR(MAX)'),' = ',''),'''','')) 
												= ZPA.AttributeCode ) AND IsCategory = 0 
			AND ( ZPA.IsShowOnGrid = 1 OR ZPA.IsConfigurable =1  )  )
			LEFT JOIN ZnodeAttributeType ZTY ON (ZTY.AttributeTypeId = ZPA.AttributeTypeId)

		END 
		ELSE 
		BEGIN 

			INSERT INTO @TBL_FilterClause (AttributeValue,AttributeCode,AttributeTypeName,PimAttributeId,AttributeCodeOrg)
			SELECT  Tbl.Col.value ( 'attributevalue[1]' , 'NVARCHAR(MAX)') AS AttributeValue
			,Tbl.Col.value ( 'attributecode[1]' , 'NVARCHAR(MAX)') AS AttributeValue,ZTY.AttributeTypeName,ZPA.PimAttributeId,AttributeCode AttributeCodeOrg
			FROM @WhereClauseXml.nodes ( '//ArrayOfWhereClauseModel/WhereClauseModel'  ) AS Tbl(Col)
			LEFT JOIN ZnodePimAttribute ZPA  ON (dbo.Fn_Trim(REPLACE(REPLACE(Tbl.Col.value ( 'attributecode[1]' , 'NVARCHAR(MAX)'),' = ',''),'''','')) 
												= ZPA.AttributeCode AND ZPA.IsCategory = 0 
			AND ( ZPA.IsShowOnGrid = 1 OR ZPA.IsConfigurable =1  )  )
			LEFT JOIN ZnodeAttributeType ZTY ON (ZTY.AttributeTypeId = ZPA.AttributeTypeId)

		END 

		CREATE TABLE #TBL_PimProductId (PimProductId INT)

		CREATE TABLE #TBL_PimProductIdDelete (PimProductId INT )

		SELECT ZPAV.PimProductId ,PimAttributeValueId ,ZPAV.CreatedDate,ZPAV.ModifiedDate,TBLA.AttributeCodeOrg AttributeCode
		INTO #TBL_AttributeValueId 
		FROM  ZnodePimAttributeValue ZPAV 
		INNER JOIN @TBL_FilterClause TBLA ON (TBLA.PimAttributeId = ZPAV.PimAttributeId)
		INNER JOIN @PimProductId YT ON (YT.Id = ZPAV.PimProductId OR NOT EXISTS (SELECT TOP 1 1 FROM @PimProductId))

		INSERT INTO #TBL_PimProductId (PimProductId )
		SELECT DISTINCT PimProductId 
		FROM #TBL_AttributeValueId

		IF @WorkingProcess =1 
		BEGIN 
				DECLARE @PimAttributeId_in TransferId 

				INSERT INTO @PimAttributeId_in 
				SELECT PimAttributeId
				FROM  @TBL_FilterClause 
				WHERE AttributeTypeName IN ('Simple Select','Multi Select') 
				AND AttributeCode LIKE '%in (%'

				CREATE TABLE #TBL_AttributeDefaultValue_in ( PimAttributeId INT ,
							AttributeDefaultValueCode VARCHAR(MAX),IsEditable INT,AttributeDefaultValue NVARCHAR(MAX),DisplayOrder INT,PimAttributeDefaultValueId INT,IsDefault Bit  )    
				INSERT INTO #TBL_AttributeDefaultValue_in(PimAttributeId,AttributeDefaultValueCode,IsEditable,AttributeDefaultValue,DisplayOrder,PimAttributeDefaultValueId,IsDefault)
				EXEC Znode_GetAttributeDefaultValueLocaleNew_TansferId @PimAttributeId_in, @LocaleId;
 
				DECLARE @WhereClauseInCom NVARCHAR(MAX) = (SELECT TOP 1 AttributeValue FROM @TBL_FilterClause WHERE AttributeCode LIKE '%in (%') 


			SET @SQL = '
			;With Cte_AttributeValue AS 
			(
			SELECT PimAttributeValueId 
			FROM ZnodePimAttributeValueLocale 
			WHERE AttributeValue '+@WhereClauseInCom+'
			UNION ALL 
			SELECT ZPADV.PimAttributeValueID 
			FROM ZnodePimProductAttributeDefaultValue ZPADV 
			INNER JOIN #TBL_AttributeDefaultValue_in TBL ON (TBL.PimAttributeDefaultValueId = ZPADV.PimAttributeDefaultValueId)
			WHERE TBL.AttributeDefaultValue '+@WhereClauseInCom+'
			)
   
			SELECT PimProductId 
			FROM #TBL_AttributeValueId ZPAV 
			INNER JOIN  Cte_AttributeValue CTAC ON (CTAC.PimAttributeValueId = ZPAV.PimAttributeVaLueId )
			GROUP BY PimProductId
			UNION ALL  
			SELECT PimProductId 
			FROM ZnodePimProduct a
			INNER JOIN ZnodePimFamilyLocale b ON (b.PimAttributeFamilyId = a.PimAttributeFamilyId) 
			WHERE b.AttributeFamilyName '+@WhereClauseInCom+'
			GROUP BY PimProductId
			UNION ALL 
			SELECT  TBLAV.PimProductId 
			FROM ZnodePimProduct TBLAV
			WHERE CASE WHEN TBLAV.IsProductPublish  IS NULL THEN ''Not Published'' 
					WHEN TBLAV.IsProductPublish = 0 THEN ''Draft''
					ELSE  ''Published'' END '+@WhereClauseInCom+'
			GROUP BY TBLAV.PimProductId 
			'
   
			DELETE FROM #TBL_PimProductIdDelete 
			INSERT INTO #TBL_PimProductIdDelete  (PimProductId)
			EXEC (@SQL)
			DELETE FROM #TBL_PimProductId
			INSERT INTO #TBL_PimProductId
			SELECT PimProductId FROM #TBL_PimProductIdDelete
			INSERT INTO #TBL_PimProductId
			SELECT -1 
			WHERE NOT EXISTS (SELECT TOP 1 1  FROM #TBL_PimProductId)

			DELETE  FROM @TBL_FilterClause WHERE AttributeCode LIKE '% in (%'

			DROP TABLE #TBL_AttributeDefaultValue_in
			SET @WorkingProcess  = 0 
   
		END 

		IF EXISTS (SELECT TOP 1 1 FROM @TBL_FilterClause WHERE AttributeCode <> '' AND ISNULL(AttributeValue,'') = '')
		BEGIN 
  
				SET  @InternalProductWhereClause = STUFF( (  SELECT ' INNER JOIN #TBL_AttributeValueId AS ZPAVL'+CAST(ID AS VARCHAR(200))+
												' ON ( TBLAV.PimProductId = ZPAVL'+CAST(ID AS VARCHAR(200))+'.PimProductId AND ZPAVL'+CAST(ID AS VARCHAR(200))+'.AttributeCode '+AttributeCode+
												' )'				
										FROM @TBL_FilterClause
										WHERE ISNULL(AttributeValue,'') = ''
										FOR XML PATH (''), TYPE).value('.', ' Nvarchar(MAX)'), 1, 0, '')
				----Change for configurable product varient page seach
				SET @SQL = ' 
							SELECT  TBLAV.PimProductId 
							FROM #TBL_AttributeValueId TBLAV '+@InternalProductWhereClause+' 
							WHERE EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId TBLP WHERE TBLP.PimProductId = TBLAV.PimProductId )
							GROUP BY TBLAV.PimProductId 
						'
  

				DELETE FROM #TBL_PimProductIdDelete 
				INSERT INTO #TBL_PimProductIdDelete  (PimProductId)
				EXEC (@SQL)
				DELETE FROM #TBL_PimProductId
				INSERT INTO #TBL_PimProductId
				SELECT PimProductId FROM #TBL_PimProductIdDelete
				INSERT INTO #TBL_PimProductId
				SELECT -1 
				WHERE NOT EXISTS (SELECT TOP 1 1  FROM #TBL_PimProductId)
				DELETE FROM @TBL_FilterClause WHERE ISNULL(AttributeValue,'') = ''
				IF NOT EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId ) AND EXISTS (SELECT TOP 1 1  FROM @PimProductId Having Max (ID) = 0 )
				BEGIN
				INSERT INTO  #TBL_PimProductId (PimProductId)
				SELECT 0 
			END
  
		END 

		IF EXISTS (SELECT TOP 1 1 FROM @TBL_FilterClause WHERE AttributeTypeName IN ('Simple Select','Multi Select') )
		BEGIN
			DECLARE @PimAttributeId TransferId 

			INSERT INTO @PimAttributeId 
			SELECT DISTINCT PimAttributeId
			FROM  @TBL_FilterClause WHERE AttributeTypeName IN ('Simple Select','Multi Select') 

			CREATE TABLE #TBL_AttributeDefaultValue ( PimAttributeId INT ,
						AttributeDefaultValueCode VARCHAR(MAX),IsEditable INT,AttributeDefaultValue NVARCHAR(MAX),DisplayOrder INT,PimAttributeDefaultValueId INT,IsDefault BIT  )    
			INSERT INTO #TBL_AttributeDefaultValue(PimAttributeId,AttributeDefaultValueCode,IsEditable,AttributeDefaultValue,DisplayOrder,PimAttributeDefaultValueId,IsDefault)
			EXEC Znode_GetAttributeDefaultValueLocaleNew_TansferId @PimAttributeId, @LocaleId;
 
			IF @DefaultLocaleId = @LocaleID AND  @WorkingProcess = 0 
			BEGIN 


				SET  @InternalProductWhereClause = STUFF( (  SELECT ' INNER JOIN Cte_AttributeValue AS ZPAVL'+CAST(ID AS VARCHAR(200))+
												' ON ( TBLAV.PimProductId = ZPAVL'+CAST(ID AS VARCHAR(200))+'.PimProductId AND ZPAVL'+CAST(ID AS VARCHAR(200))+'.AttributeCode '+AttributeCode+
												+' AND ZPAVL'+CAST(ID AS VARCHAR(200))+'.AttributeValue '+AttributeValue+' AND ZPAVL'+CAST(ID AS VARCHAR(200))+
												'.LocaleId='+CAST(@LocaleID AS VARCHAR(200))+' )'				
										FROM @TBL_FilterClause
										WHERE AttributeTypeName IN ('Simple Select','Multi Select')
										AND AttributeValue <> ''
										AND AttributeValue IS NOT NULL
										FOR XML PATH (''), TYPE).value('.', ' Nvarchar(MAX)'), 1, 0, '')

				SET @SQL = ' ;With Cte_AttributeValue AS 
							(
							SELECT TBLAV.PimAttributeValueId ,SUBSTRING((SELECT '',''+AttributeDefaultValue FROM #TBL_AttributeDefaultValue TTR 
							INNER JOIN ZnodePimProductAttributeDefaultValue ZPAVL ON (TTR.PimAttributeDefaultValueId = ZPAVL.PimAttributeDefaultValueId )
							WHERE ZPAVL.PimAttributeValueId = TBLAV.PimAttributeValueId  
							AND ZPAVL.LocaleId = '+Cast(@localeId AS VARCHAR(200))+'
							FOR XML PATH('''') ),2,4000) AttributeValue
								,  '+Cast(@localeId AS VARCHAR(200))+' LocaleId,TBLAV.AttributeCode,TBLAV.PimProductId
							FROM #TBL_AttributeValueId TBLAV
							'+CASE WHEN NOT EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId ) THEN '' 
										ELSE ' WHERE EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId TBLP WHERE TBLP.PimProductId = TBLAV.PimProductId ) ' END+'
							GROUP BY TBLAV.PimAttributeValueId,TBLAV.AttributeCode,TBLAV.PimProductId
							)
  
						SELECT  TBLAV.PimProductId
						FROM #TBL_AttributeValueId TBLAV
						'+@InternalProductWhereClause+'	GROUP BY TBLAV.PimProductId '
			
				
			END 
			ELSE IF  @WorkingProcess = 0 
			BEGIN 

				SET  @InternalProductWhereClause = 
										STUFF( (  SELECT ' INNER JOIN Cte_AttributeValue AS ZPAVL'+CAST(ID AS VARCHAR(200))+
												' ON ( TBLAV.PimProductId = ZPAVL'+CAST(ID AS VARCHAR(200))+'.PimProductId AND ZPAVL'+CAST(ID AS VARCHAR(200))+'.AttributeCode '+AttributeCode+
												' AND ZPAVL'+CAST(ID AS VARCHAR(200))+'.AttributeValue '+AttributeValue+'  )'				
										FROM @TBL_FilterClause
										WHERE AttributeTypeName IN ('Simple Select','Multi Select')
										AND AttributeValue <> ''
										AND AttributeValue IS NOT NULL
										FOR XML PATH (''), TYPE).value('.', ' Nvarchar(MAX)'), 1, 0, '')
				SET @SQL = '  			 
							SELECT TBLAV.PimAttributeValueId,ZPAVL.PimAttributeDefaultValueId , ZPAVL.LocaleId ,COUNT(*)Over(Partition By TBLAV.PimAttributeValueId ,TBLAV.PimProductId ORDER BY TBLAV.PimAttributeValueId ,TBLAV.PimProductId  ) RowId
							INTO #temp_Table 
							FROM #TBL_AttributeValueId TBLAV 
							INNER JOIN ZnodePimProductAttributeDefaultValue ZPAVL ON (ZPAVL.PimAttributeValueId = TBLAV.PimAttributeValueId)
							WHERE (ZPAVL.LocaleId = '+Cast(@localeId AS VARCHAR(200))+' OR ZPAVL.LocaleId = '+Cast(@DefaultlocaleId AS VARCHAR(200))+')
				
							;with Cte_AttributeValue AS 
							(
							SELECT TBLAV.PimAttributeValueId ,SUBSTRING((SELECT '',''+AttributeDefaultValue FROM #TBL_AttributeDefaultValue TTR 
							INNER JOIN #temp_Table  ZPAVL ON (TTR.PimAttributeDefaultValueId = ZPAVL.PimAttributeDefaultValueId )
							WHERE ZPAVL.PimAttributeValueId = TBLAV.PimAttributeValueId  
							AND ZPAVL.LocaleId = CASE WHEN ZPAVL.RowId = 2 THEN '+CAST(@LocaleId AS Varchar(300))+' ELSE '+Cast(@DefaultLocaleId AS Varchar(300))+' END  
							FOR XML PATH('''') ),2,4000) AttributeValue,TBLAV.AttributeCode ,TBLAV.PimProductId 
							FROM #TBL_AttributeValueId TBLAV
							'+CASE WHEN NOT EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId ) THEN '' 
										ELSE ' WHERE EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId TBLP WHERE TBLP.PimProductId = TBLAV.PimProductId ) ' END+'
							GROUP BY TBLAV.PimAttributeValueId,TBLAV.AttributeCode ,TBLAV.PimProductId 
							)
  
							SELECT   TBLAV.PimProductId
							FROM  #TBL_AttributeValueId TBLAV
							'+@InternalProductWhereClause+'	GROUP BY TBLAV.PimProductId '

			END 

			DELETE FROM #TBL_PimProductIdDelete 
			INSERT INTO #TBL_PimProductIdDelete  (PimProductId)
			EXEC (@SQL)
			DELETE FROM #TBL_PimProductId
			INSERT INTO #TBL_PimProductId
			SELECT PimProductId FROM #TBL_PimProductIdDelete
 
			DROP TABLE #TBL_AttributeDefaultValue

		END 

		IF EXISTS (SELECT TOP 1 1 FROM @TBL_FilterClause WHERE AttributeTypeName IN ('Text','Number','Datetime','Yes/No') )
		BEGIN  
   
				IF @DefaultLocaleId = @LocaleID AND @WorkingProcess = 0 
				BEGIN 
					SET  @InternalProductWhereClause = 
											STUFF( (  SELECT ' INNER JOIN View_PimProducttextValue AS ZPAVL'+CAST(ID AS VARCHAR(200))+
													' ON ( TBLAV.PimProductId = ZPAVL'+CAST(ID AS VARCHAR(200))+'.PimProductId AND ZPAVL'+CAST(ID AS VARCHAR(200))+'.AttributeCode '+AttributeCode+
													' AND ZPAVL'+CAST(ID AS VARCHAR(200))+'.AttributeValue '+AttributeValue+' AND ZPAVL'+CAST(ID AS VARCHAR(200))+
													'.LocaleId='+CAST(@LocaleID AS VARCHAR(200))+' )'				
											FROM @TBL_FilterClause
											WHERE AttributeTypeName IN ('Text','Number','Datetime','Yes/No')
											FOR XML PATH (''), TYPE).value('.', ' Nvarchar(MAX)'), 1, 0, '')
 
					SET @SQL = '	SELECT  TBLAV.PimProductId 
								FROM #TBL_AttributeValueId TBLAV
								'+@InternalProductWhereClause+'
								'+CASE WHEN NOT EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId ) THEN '' 
											ELSE ' WHERE EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId TBLP WHERE TBLP.PimProductId = TBLAV.PimProductId   ) ' END 
								+' GROUP BY TBLAV.PimProductId '

			END 
			ELSE IF @WorkingProcess = 0 
			BEGIN 
				SET  @InternalProductWhereClause = 
										STUFF( (  SELECT ' INNER JOIN Cte_AttributeDetails AS ZPAVL'+CAST(ID AS VARCHAR(200))+
												' ON ( TBLAV.PimProductId = ZPAVL'+CAST(ID AS VARCHAR(200))+'.PimProductId AND ZPAVL'+CAST(ID AS VARCHAR(200))+'.AttributeCode '+AttributeCode+
												' AND ZPAVL'+CAST(ID AS VARCHAR(200))+'.AttributeValue '+AttributeValue+' AND ZPAVL'+CAST(ID AS VARCHAR(200))+
												'.LocaleId = CASE WHEN ZPAVL'+CAST(ID AS VARCHAR(200))+'.RowId = 2 THEN  '+CAST(@LocaleId AS Varchar(300))+' ELSE '+Cast(@DefaultLocaleId AS Varchar(300))+' END  )'				
										FROM @TBL_FilterClause
										WHERE AttributeTypeName IN ('Text','Number','Datetime','Yes/No')
										FOR XML PATH (''), TYPE).value('.', ' Nvarchar(MAX)'), 1, 0, '')
				SET @SQL = ' 
					;With Cte_AttributeDetails AS 
					(
					SELECT TBLAV.PimProductId,ZPAVL.AttributeValue,TBLAV.AttributeCode,ZPAVL.LocaleId ,COUNT(*)Over(Partition By TBLAV.PimProductId,TBLAV.AttributeCode ORDER BY TBLAV.PimProductId,TBLAV.AttributeCode  ) RowId
					FROM #TBL_AttributeValueId TBLAV 
					INNER JOIN ZnodePimAttributeValueLocale ZPAVL ON (ZPAVL.PimAttributeValueId = TBLAV.PimAttributeValueId )
					WHERE (LocaleId = '+Cast(@DefaultLocaleId AS Varchar(300))+' OR LocaleId = '+CAST(@LocaleId AS Varchar(300))+' )'+CASE WHEN NOT EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId ) THEN '' 
										ELSE ' AND EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId TBLP WHERE TBLP.PimProductId = TBLAV.PimProductId ) ' END +'
					) 
					SELECT  TBLAV.PimProductId 
  					FROM #TBL_AttributeValueId TBLAV
					'+@InternalProductWhereClause+'
					GROUP BY TBLAV.PimProductId 
					'
			END 
			DELETE FROM #TBL_PimProductIdDelete 
			INSERT INTO #TBL_PimProductIdDelete  (PimProductId)
			EXEC (@SQL)
			DELETE FROM #TBL_PimProductId
			INSERT INTO #TBL_PimProductId
			SELECT PimProductId FROM #TBL_PimProductIdDelete

		END 

		IF EXISTS (SELECT TOP 1 1 FROM @TBL_FilterClause WHERE AttributeTypeName IN ('Text Area') )
		BEGIN    
			IF @DefaultLocaleId = @LocaleID AND @WorkingProcess = 0 
			BEGIN 
				SET  @InternalProductWhereClause = 
											STUFF( (  SELECT ' INNER JOIN View_PimProductTextAreaValue AS ZPAVL'+CAST(ID AS VARCHAR(200))+
												' ON ( TBLAV.PimProductId = ZPAVL'+CAST(ID AS VARCHAR(200))+'.PimProductId AND ZPAVL'+CAST(ID AS VARCHAR(200))+'.AttributeCode '+AttributeCode+
												' AND ZPAVL'+CAST(ID AS VARCHAR(200))+'.AttributeValue '+AttributeValue+' AND ZPAVL'+CAST(ID AS VARCHAR(200))+
												'.LocaleId='+CAST(@LocaleID AS VARCHAR(200))+' )'				
										FROM @TBL_FilterClause
										WHERE AttributeTypeName IN ('Text Area')
										FOR XML PATH (''), TYPE).value('.', ' Nvarchar(MAX)'), 1, 0, '')
 
				SET @SQL = '
							SELECT  TBLAV.PimProductId 
							FROM #TBL_AttributeValueId TBLAV
							'+@InternalProductWhereClause+CASE WHEN NOT EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId ) THEN '' 
										ELSE ' WHERE EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId TBLP WHERE TBLP.PimProductId = TBLAV.PimProductId ) ' END 
										+' GROUP BY TBLAV.PimProductId '
			END 
			ELSE IF @WorkingProcess = 0 
			BEGIN 
				SET  @InternalProductWhereClause = 
										STUFF( (  SELECT ' INNER JOIN Cte_AttributeDetails AS ZPAVL'+CAST(ID AS VARCHAR(200))+
												' ON ( TBLAV.PimProductId = ZPAVL'+CAST(ID AS VARCHAR(200))+'.PimProductId AND ZPAVL'+CAST(ID AS VARCHAR(200))+'.AttributeCode '+AttributeCode+
												' AND ZPAVL'+CAST(ID AS VARCHAR(200))+'.AttributeValue '+AttributeValue+' AND ZPAVL'+CAST(ID AS VARCHAR(200))+
												'.LocaleId = CASE WHEN ZPAVL'+CAST(ID AS VARCHAR(200))+'.RowId = 2 THEN  '+CAST(@LocaleId AS Varchar(300))+' ELSE '+Cast(@DefaultLocaleId AS Varchar(300))+' END  )'				
										FROM @TBL_FilterClause
										WHERE AttributeTypeName IN ('Text Area')
										FOR XML PATH (''), TYPE).value('.', ' Nvarchar(MAX)'), 1, 0, '')
				SET @SQL = ' 
					;With Cte_AttributeDetails AS 
					(
					SELECT TBLAV.PimProductId,TBLAV.AttributeCode,ZPAVL.AttributeValue,ZPAVL.LocaleId ,COUNT(*)Over(Partition By TBLAV.PimProductId,TBLAV.AttributeCode ORDER BY TBLAV.PimProductId,TBLAV.AttributeCode  ) RowId
					FROM #TBL_AttributeValueId TBLAV 
					INNER JOIN ZnodePimProductAttributeTextAreaValue ZPAVL ON (ZPAVL.PimAttributeValueId = TBLAV.PimAttributeValueId )
					WHERE (LocaleId = '+Cast(@DefaultLocaleId AS Varchar(300))+' OR LocaleId = '+CAST(@LocaleId AS Varchar(300))+' )
	 
					) 
					SELECT  TBLAV.PimProductId 
  					FROM #TBL_AttributeValueId TBLAV
					'+@InternalProductWhereClause+CASE WHEN NOT EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId ) THEN '' 
										ELSE ' WHERE EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId TBLP WHERE TBLP.PimProductId = TBLAV.PimProductId ) ' END 
										+' 
										GROUP BY TBLAV.PimProductId 	
										'
				END 
			DELETE FROM #TBL_PimProductIdDelete 
			INSERT INTO #TBL_PimProductIdDelete  (PimProductId)
			EXEC (@SQL)
			DELETE FROM #TBL_PimProductId
			INSERT INTO #TBL_PimProductId
			SELECT PimProductId FROM #TBL_PimProductIdDelete

		END 
		IF EXISTS (SELECT TOP 1 1 FROM @TBL_FilterClause WHERE AttributeCode  LIKE '%PublishStatus%' )
		BEGIN    
 
				SET @SQL = '
							SELECT  TBLAV.PimProductId 
							FROM ZnodePimProduct TBLAV
							WHERE CASE WHEN TBLAV.IsProductPublish  IS NULL THEN ''Not Published'' 
							WHEN TBLAV.IsProductPublish = 0 THEN ''Draft''
							ELSE  ''Published'' END '+(SELECT TOP 1 AttributeValue FROM @TBL_FilterClause WHERE AttributeCode LIKE '%PublishStatus%')+CASE WHEN NOT EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId ) THEN '' 
										ELSE ' AND EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId TBLP WHERE TBLP.PimProductId = TBLAV.PimProductId ) ' END 
										+' GROUP BY TBLAV.PimProductId '
  
				DELETE FROM #TBL_PimProductIdDelete 
				INSERT INTO #TBL_PimProductIdDelete  (PimProductId)
				EXEC (@SQL)
				DELETE FROM #TBL_PimProductId
				INSERT INTO #TBL_PimProductId
				SELECT PimProductId FROM #TBL_PimProductIdDelete

		END 
		IF EXISTS (SELECT TOP 1 1 FROM @TBL_FilterClause WHERE AttributeCode  LIKE '%AttributeFamily%' )
		BEGIN 

				;With Cte_attributeValue AS 
				(
					SELECT ZPAF.PimAttributeFamilyId,FamilyCode,AttributeFamilyName ,ZPFL.LocaleId
					FROM ZnodePimAttributeFamily ZPAF
					INNER JOIN ZnodePimFamilyLocale ZPFL ON (ZPFL.PimAttributeFamilyId = ZPAF.PimAttributeFamilyId) 
					WHERE ZPFL.LocaleId IN (@DefaultLocaleId,@LocaleId)
				) 
				, Cte_AttributeValueAttribute AS 
				(
					SELECT PimAttributeFamilyId,FamilyCode,AttributeFamilyName
					FROM Cte_attributeValue RTY 
					WHERE LocaleId = @LocaleId
				)
				, Cte_AttributeValueTht AS 
				(
					SELECT PimAttributeFamilyId,FamilyCode,AttributeFamilyName
					FROM Cte_AttributeValueAttribute
					UNION ALL 
					SELECT PimAttributeFamilyId,FamilyCode,AttributeFamilyName
					FROM Cte_attributeValue TYY  
					WHERE NOT EXISTS (SELECT TOP 1 1 FROM Cte_AttributeValueAttribute THE WHERE THE.PimAttributeFamilyId = TYY.PimAttributeFamilyId )
					AND TYY.LocaleId = @DefaultLocaleId
				)
				SELECT PimAttributeFamilyId,FamilyCode,AttributeFamilyName
				INTO #TBL_FamilyLocale
				FROM Cte_AttributeValueTht 


				SET @SQL = '
							SELECT  TBLAV.PimProductId 
							FROM ZnodePimProduct TBLAV 
							INNER JOIN #TBL_FamilyLocale THY ON (THY.PimAttributeFamilyId = TBLAV.PimAttributeFamilyId )
							WHERE AttributeFamilyName '+(SELECT TOP 1 AttributeValue FROM @TBL_FilterClause WHERE AttributeCode LIKE '%AttributeFamily%')+CASE WHEN NOT EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId ) THEN '' 
										ELSE ' AND EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId TBLP WHERE TBLP.PimProductId = TBLAV.PimProductId ) ' END 
										+' GROUP BY TBLAV.PimProductId '
  

			DELETE FROM #TBL_PimProductIdDelete 
			INSERT INTO #TBL_PimProductIdDelete  (PimProductId)
			EXEC (@SQL)
			DELETE FROM #TBL_PimProductId
			INSERT INTO #TBL_PimProductId
			SELECT PimProductId FROM #TBL_PimProductIdDelete

		END 
	
		SET @SQL = '
		IF EXISTS ( SELECT TOP 1 1 FROM tempdb..sysobjects WHERE name = ''##Temp_PimProductId'+CAST(@@SPID AS VARCHAR(500))+''' )
		BEGIN 
			DROP TABLE ##Temp_PimProductId'+CAST(@@SPID AS VARCHAR(500))+'
		END 
		CREATE TABLE ##Temp_PimProductId'+CAST(@@SPID AS VARCHAR(500))+' (PimProductId INT )
		INSERT INTO  ##Temp_PimProductId'+CAST(@@SPID AS VARCHAR(500))+'
		SELECT PimProductId 
		FROM #TBL_PimProductId
		'
		EXEC (@SQL)
		DROP TABLE #TBL_PimProductId
		DROP TABLE #TBL_AttributeValueId
		DROP TABLE #TBL_PimProductIdDelete
 END
 Go
 -------------------------------
 GO

 IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'Ind_ZnodeGlobalAttributeDefaultValue_GlobalAttributeId' AND object_id = OBJECT_ID('ZnodeGlobalAttributeDefaultValue'))
    BEGIN
       CREATE NONCLUSTERED INDEX Ind_ZnodeGlobalAttributeDefaultValue_GlobalAttributeId 
			ON [dbo].[ZnodeGlobalAttributeDefaultValue] ([GlobalAttributeId])		
    END

GO
IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'Ind_ZnodeUserGlobalAttributeValue_UserId' AND object_id = OBJECT_ID('ZnodeUserGlobalAttributeValue'))
    BEGIN
       CREATE NONCLUSTERED INDEX Ind_ZnodeUserGlobalAttributeValue_UserId 
			ON [dbo].[ZnodeUserGlobalAttributeValue] ([UserId])
    END


GO
IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'Ind_ZnodeUserGlobalAttributeValueLocale_UserGlobalAttributeValueId' AND object_id = OBJECT_ID('ZnodeUserGlobalAttributeValueLocale'))
    BEGIN
        CREATE NONCLUSTERED INDEX Ind_ZnodeUserGlobalAttributeValueLocale_UserGlobalAttributeValueId
			ON [dbo].[ZnodeUserGlobalAttributeValueLocale] ([UserGlobalAttributeValueId])
			INCLUDE ([AttributeValue],[GlobalAttributeDefaultValueId],[MediaId],[MediaPath])
    END

GO
IF EXISTS(SELECT * FROM SYS.PROCEDURES WHERE NAME = 'Znode_GetAccountGlobalAttributeValue')
	DROP PROC Znode_GetAccountGlobalAttributeValue
GO

CREATE PROCEDURE [dbo].[Znode_GetAccountGlobalAttributeValue]
(
	@EntityName NVARCHAR(200) = 0,
	@GlobalEntityValueId   INT = 0,
	@LocaleCode VARCHAR(100) = '',
	@GroupCode NVARCHAR(200) = NULL,
	@SelectedValue BIT = 0
)
AS
/*
	 Summary :- This procedure is used to get the Attribute and EntityValue attribute value as per filter pass 
	 Unit Testing 
	 BEGIN TRAN
	 EXEC [Znode_GetGlobalEntityAttributeValue] 'Store',1
	 ROLLBACK TRAN

*/	 
BEGIN
BEGIN TRY
SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @EntityValue NVARCHAR(200), @LocaleId INT

 	DECLARE @GlobalFamilyId INT
	SET  @GlobalFamilyId = (SELECT FM.GlobalAttributeFamilyId FROM ZnodeGlobalEntity GE 
		INNER JOIN ZnodeGlobalEntityFamilyMapper FM ON GE.GlobalEntityId = FM.GlobalEntityId
		WHERE GE.EntityName =  @EntityName AND  (FM.GlobalEntityValueId = @GlobalEntityValueId OR FM.GlobalEntityValueId IS NULL))
  

	DECLARE @V_MediaServerThumbnailPath VARCHAR(4000);
    SET @V_MediaServerThumbnailPath =
    (
        SELECT ISNULL(CASE WHEN CDNURL = '' THEN NULL ELSE CDNURL END,URL)+ZMSM.ThumbnailFolderName+'/'  
        FROM ZnodeMediaConfiguration ZMC 
		INNER JOIN ZnodeMediaServerMaster ZMSM ON (ZMSM.MediaServerMasterId = ZMC.MediaServerMasterId)
		WHERE IsActive = 1 
    );

	 --Getting account name
	 SELECT @EntityValue=Name FROM ZnodeAccount
	 WHERE AccountId=@GlobalEntityValueId

	IF ISNULL(@EntityValue,'')  <> ''
	BEGIN
		--Getting GlobalEntityId from @EntityName
		DECLARE @GlobalEntityId INT
		SET @GlobalEntityId =( SELECT GlobalEntityId FROM dbo.ZnodeGlobalEntity WHERE EntityName = @EntityName )

		CREATE TABLE #EntityAttributeList   
		(
			GlobalEntityId INT,EntityName NVARCHAR(300),EntityValue NVARCHAR(MAX),GlobalAttributeGroupId INT,
			GlobalAttributeId INT,AttributeTypeId INT,AttributeTypeName NVARCHAR(300),AttributeCode NVARCHAR(300) ,
			IsRequired BIT,IsLocalizable BIT,AttributeName  NVARCHAR(300) , HelpDescription NVARCHAR(MAX),DisplayOrder INT
		) 
			 
		CREATE TABLE #EntityAttributeValidationList 
		( 
			GlobalAttributeId INT, ControlName NVARCHAR(300), ValidationName NVARCHAR(300),
			SubValidationName NVARCHAR(300),RegExp NVARCHAR(300), ValidationValue NVARCHAR(300),IsRegExp Bit
		)

		CREATE TABLE #EntityAttributeValueList 
		(
			GlobalAttributeId INT,AttributeValue NVARCHAR(MAX),GlobalAttributeValueId INT,GlobalAttributeDefaultValueId INT,
			AttributeDefaultValueCode NVARCHAR(300),AttributeDefaultValue NVARCHAR(300),
			MediaId INT,MediaPath NVARCHAR(300),IsEditable bit,DisplayOrder INT 
		)

		CREATE TABLE #EntityAttributeDefaultValueList
		(
			GlobalAttributeDefaultValueId INT,GlobalAttributeId INT,AttributeDefaultValueCode NVARCHAR(300),
			AttributeDefaultValue NVARCHAR(300),RowId INT,IsEditable bit,DisplayOrder INT 
		)
		
		SET @LocaleId = (SELECT TOP 1 LocaleId FROM ZnodeLocale WHERE Code = @LocaleCode)

        --Getting account global attribute list
		INSERT INTO #EntityAttributeList
		(	
			GlobalEntityId ,EntityName ,EntityValue ,GlobalAttributeGroupId ,GlobalAttributeId ,AttributeTypeId ,
			AttributeTypeName,AttributeCode  ,IsRequired ,IsLocalizable ,AttributeName,HelpDescription,DisplayOrder  
		) 
		SELECT @GlobalEntityId,@EntityName,@EntityValue EntityValue,ww.GlobalAttributeGroupId,
			c.GlobalAttributeId,c.AttributeTypeId,q.AttributeTypeName,c.AttributeCode,c.IsRequired,
			c.IsLocalizable,f.AttributeName,c.HelpDescription,c.DisplayOrder
		FROM dbo.ZnodeGlobalGroupEntityMapper AS w 
		INNER JOIN dbo.ZnodeGlobalAttributeGroupMapper AS ww ON ww.GlobalAttributeGroupId = w.GlobalAttributeGroupId
		INNER JOIN dbo.ZnodeGlobalAttribute AS c ON ww.GlobalAttributeId = c.GlobalAttributeId
		INNER JOIN dbo.ZnodeAttributeType AS q ON c.AttributeTypeId = q.AttributeTypeId
		INNER JOIN dbo.ZnodeGlobalAttributeLocale AS f ON c.GlobalAttributeId = f.GlobalAttributeId
		WHERE w.GlobalEntityId = @GlobalEntityId AND ( f.LocaleId = isnull(@LocaleId, 0 ) or isnull(@LocaleId,0) = 0 )
		AND EXISTS(SELECT * FROM dbo.ZnodeGlobalAttributeFamily AS GAF WHERE GAF.GlobalEntityId = w.GlobalEntityId AND GAF.GlobalAttributeFamilyId = @GlobalFamilyId)
		AND EXISTS( SELECT 1 FROM ZnodeGlobalAttributeGroup g WHERE ww.GlobalAttributeGroupId = g.GlobalAttributeGroupId 
					AND (g.GroupCode = ISNULL(@GroupCode,'') OR ISNULL(@GroupCode,'') = '' ))

		--Getting globat attribute validation data
		IF EXISTS(SELECT * FROM #EntityAttributeList)
		BEGIN
			INSERT INTO #EntityAttributeValidationList(GlobalAttributeId,ControlName,ValidationName,SubValidationName,RegExp,ValidationValue,IsRegExp)
			SELECT aa.GlobalAttributeId,i.ControlName,i.Name AS ValidationName,j.ValidationName AS SubValidationName,
				j.RegExp,k.Name AS ValidationValue,CAST(CASE WHEN j.RegExp IS NULL THEN 0 ELSE 1 END AS BIT) AS IsRegExp
			FROM #EntityAttributeList aa
			INNER JOIN dbo.ZnodeGlobalAttributeValidation AS k ON k.GlobalAttributeId = aa.GlobalAttributeId
			INNER JOIN dbo.ZnodeAttributeInputValidation AS i ON k.InputValidationId = i.InputValidationId
			LEFT JOIN dbo.ZnodeAttributeInputValidationRule AS j ON k.InputValidationRuleId = j.InputValidationRuleId
		END

		----Getting globat attribute values
		INSERT INTO #EntityAttributeValueList(GlobalAttributeId,GlobalAttributeValueId,GlobalAttributeDefaultValueId,AttributeValue ,MediaId,MediaPath)
		SELECT DISTINCT GlobalAttributeId,aa.AccountGlobalAttributeValueId,bb.GlobalAttributeDefaultValueId,
			CASE WHEN bb.MediaPath IS NOT NULL THEN @V_MediaServerThumbnailPath+bb.MediaPath--+'~'+convert(NVARCHAR(10),bb.MediaId) 
			ELSE bb.AttributeValue END AS AttributeValue,	bb.MediaId,bb.MediaPath
		FROM dbo.ZnodeAccountGlobalAttributeValue aa
		INNER JOIN ZnodeAccountGlobalAttributeValueLocale bb ON bb.AccountGlobalAttributeValueId = aa.AccountGlobalAttributeValueId 
		WHERE aa.AccountId = @GlobalEntityValueId
		  
		--Updating default globat attribute data into table #EntityAttributeValueList
		IF EXISTS(SELECT * FROM #EntityAttributeValueList)
		BEGIN
			UPDATE aa
			SET AttributeDefaultValueCode= h.AttributeDefaultValueCode,
				AttributeDefaultValue=g.AttributeDefaultValue,
				GlobalAttributeDefaultValueId=g.GlobalAttributeDefaultValueId,
				AttributeValue= CASE WHEN aa.AttributeValue IS NULL THEN h.AttributeDefaultValueCode ELSE aa.AttributeValue END,
				IsEditable = ISNULL(h.IsEditable, 1),DisplayOrder = h.DisplayOrder
			FROM #EntityAttributeValueList aa
			INNER JOIN dbo.ZnodeGlobalAttributeDefaultValue h ON h.GlobalAttributeDefaultValueId = aa.GlobalAttributeDefaultValueId                                       
			INNER JOIN dbo.ZnodeGlobalAttributeDefaultValueLocale g ON h.GlobalAttributeDefaultValueId = g.GlobalAttributeDefaultValueId
         END

		--Getting globat attribute default values
		IF EXISTS(SELECT * FROM #EntityAttributeList)
		BEGIN
			INSERT INTO #EntityAttributeDefaultValueList(GlobalAttributeDefaultValueId,GlobalAttributeId,AttributeDefaultValueCode,AttributeDefaultValue ,RowId ,IsEditable ,DisplayOrder )
			SELECT h.GlobalAttributeDefaultValueId, aa.GlobalAttributeId,h.AttributeDefaultValueCode,g.AttributeDefaultValue,0,ISNULL(h.IsEditable, 1),h.DisplayOrder
			FROM #EntityAttributeList aa
			INNER JOIN dbo.ZnodeGlobalAttributeDefaultValue h ON h.GlobalAttributeId = aa.GlobalAttributeId
			INNER JOIN dbo.ZnodeGlobalAttributeDefaultValueLocale g ON h.GlobalAttributeDefaultValueId = g.GlobalAttributeDefaultValueId
		END
		 
		IF NOT EXISTS (SELECT 1 FROM #EntityAttributeList )
		BEGIN
			INSERT INTO #EntityAttributeList
			(	
				GlobalEntityId ,EntityName ,EntityValue ,GlobalAttributeGroupId ,GlobalAttributeId ,AttributeTypeId ,
				AttributeTypeName ,	AttributeCode  ,IsRequired ,IsLocalizable ,AttributeName,HelpDescription  
			) 
			SELECT qq.GlobalEntityId,qq.EntityName,@EntityValue EntityValue,0 GlobalAttributeGroupId,
				0 GlobalAttributeId,0 AttributeTypeId,''AttributeTypeName,''AttributeCode,0 IsRequired,
				0 IsLocalizable,'' AttributeName,'' HelpDescription
			FROM dbo.ZnodeGlobalEntity AS qq
			WHERE qq.EntityName=@EntityName 
		END
				

		SELECT GlobalEntityId,EntityName,EntityValue,GlobalAttributeGroupId, AA.GlobalAttributeId,AttributeTypeId,
			AttributeTypeName,AttributeCode,IsRequired,IsLocalizable,AttributeName,ControlName,ValidationName,
			SubValidationName,RegExp,ValidationValue,CAST(ISNULL(IsRegExp,0) as bit)  IsRegExp,HelpDescription,
			AttributeValue,GlobalAttributeValueId,bb.GlobalAttributeDefaultValueId,aab.AttributeDefaultValueCode,
			aab.AttributeDefaultValue,ISNULL(aab.RowId,0) AS RowId,CAST(ISNULL(aab.IsEditable,0) AS BIT) AS IsEditable
			,bb.MediaId,AA.DisplayOrder
		fROM #EntityAttributeList AA				
		LEFT JOIN #EntityAttributeDefaultValueList aab on aab.GlobalAttributeId=AA.GlobalAttributeId	
		LEFT JOIN #EntityAttributeValidationList vl on vl.GlobalAttributeId=aa.GlobalAttributeId			
		LEFT JOIN #EntityAttributeValueList BB ON BB.GlobalAttributeId=AA.GlobalAttributeId		 
			AND ( (aab.GlobalAttributeDefaultValueId=bb.GlobalAttributeDefaultValueId	)
					OR  ( bb.MediaId IS NOT NULL AND isnull(vl.ValidationName,'')='IsAllowMultiUpload' AND bb.GlobalAttributeDefaultValueId IS NULL )
					OR  ( bb.MediaId IS NULL AND bb.GlobalAttributeDefaultValueId IS NULL )
				)
		ORDER BY AA.DisplayOrder, aab.DisplayOrder
	END
	ELSE
	BEGIN
		 --Showing blank output if Account is not present
		 CREATE TABLE #TempOutput
		 ( 
			GlobalEntityId INT,EntityName VARCHAR(100),EntityValue VARCHAR(100),GlobalAttributeGroupId INT, 
			GlobalAttributeId INT,AttributeTypeId INT, AttributeTypeName VARCHAR(100),AttributeCode VARCHAR(100),
			IsRequired BIT,IsLocalizable BIT,AttributeName VARCHAR(100),ControlName VARCHAR(100),ValidationName VARCHAR(100),
			SubValidationName VARCHAR(100),RegExp VARCHAR(100),ValidationValue VARCHAR(100), IsRegExp BIT,
			HelpDescription VARCHAR(100),AttributeValue VARCHAR(100),GlobalAttributeValueId INT,
			GlobalAttributeDefaultValueId INT,AttributeDefaultValueCode VARCHAR(100),AttributeDefaultValue VARCHAR(100), 
			RowId INT, IsEditable BIT, MediaId INT,DisplayOrder INT
		)
		SELECT * FROM #TempOutput
	END

	SELECT 1 AS ID,CAST(1 AS BIT) AS Status;       
END TRY
BEGIN CATCH
	SELECT ERROR_MESSAGE()
	DECLARE @Status BIT ;
	SET @Status = 0;
	DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(),
	@ErrorLine VARCHAR(100)= ERROR_LINE(),
	@ErrorCall NVARCHAR(MAX)= null       			 
	SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                    
		 
	EXEC Znode_InsertProcedureErrorLog
	@ProcedureName = 'Znode_GetGlobalEntityValueAttributeValues',
	@ErrorInProcedure = @Error_procedure,
	@ErrorMessage = @ErrorMessage,
	@ErrorLine = @ErrorLine,
	@ErrorCall = @ErrorCall;
END CATCH;
END;
Go
-----------------------
IF EXISTS(SELECT * FROM SYS.PROCEDURES WHERE NAME = 'Znode_GetPortalGlobalAttributeValue')
	DROP PROC Znode_GetPortalGlobalAttributeValue
GO
CREATE PROCEDURE [dbo].[Znode_GetPortalGlobalAttributeValue]
(
	@EntityName NVARCHAR(200) = 0,
    @GlobalEntityValueId INT = 0,
	@LocaleCode VARCHAR(100) = '',
    @GroupCode NVARCHAR(200) = null,
	@SelectedValue BIT = 0
)
AS
/*
	 Summary :- This procedure is used to get the Attribute and EntityValue attribute value as per filter pass 
	 Unit Testing 
	 BEGIN TRAN
	 EXEC [Znode_GetGlobalEntityAttributeValue] 'Store',1
	 ROLLBACK TRAN

*/	 
BEGIN
BEGIN TRY
SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @EntityValue NVARCHAR(200), @LocaleId INT
 
	DECLARE @GlobalFamilyId INT
	SET  @GlobalFamilyId = (SELECT FM.GlobalAttributeFamilyId FROM ZnodeGlobalEntity GE 
		INNER JOIN  ZnodeGlobalEntityFamilyMapper FM ON GE.GlobalEntityId = FM.GlobalEntityId
		WHERE GE.EntityName =  @EntityName AND (FM.GlobalEntityValueId = @GlobalEntityValueId OR FM.GlobalEntityValueId IS NULL))

	DECLARE @V_MediaServerThumbnailPath VARCHAR(4000);
	--Set the MediaServerThumbnailPath
	SET @V_MediaServerThumbnailPath =
	(
		SELECT ISNULL(CASE WHEN CDNURL = '' THEN NULL ELSE CDNURL END,URL)+ZMSM.ThumbnailFolderName+'/'  
		FROM ZnodeMediaConfiguration ZMC 
		INNER JOIN ZnodeMediaServerMaster ZMSM ON (ZMSM.MediaServerMasterId = ZMC.MediaServerMasterId)
		WHERE IsActive = 1 
	);

	--Getting store name
	SELECT @EntityValue=StoreName FROM ZnodePortal
	WHERE PortalId=@GlobalEntityValueId

	IF ISNULL(@EntityValue,'')  <> ''
	BEGIN
		--Getting GlobalEntityId from @EntityName
		DECLARE @GlobalEntityId INT
		SET @GlobalEntityId =( SELECT GlobalEntityId FROM dbo.ZnodeGlobalEntity WHERE EntityName = @EntityName )

		CREATE TABLE #EntityAttributeList 
		(
			GlobalEntityId INT,EntityName NVARCHAR(300),EntityValue NVARCHAR(MAX),GlobalAttributeGroupId INT,GlobalAttributeId INT,
			AttributeTypeName NVARCHAR(300),AttributeCode NVARCHAR(300) ,IsRequired BIT,AttributeTypeId INT,IsLocalizable BIT,
			AttributeName  NVARCHAR(300) , HelpDescription NVARCHAR(MAX),AttributeGroupDisplayOrder INT,DisplayOrder int
		) 
			 
		CREATE TABLE #EntityAttributeValidationList    
		( 
			GlobalAttributeId INT, ControlName NVARCHAR(300), ValidationName NVARCHAR(300),SubValidationName NVARCHAR(300),
			RegExp NVARCHAR(300), ValidationValue NVARCHAR(300),IsRegExp Bit
		)

		CREATE TABLE #EntityAttributeValueList 
		(
			GlobalAttributeId INT,AttributeValue NVARCHAR(MAX),GlobalAttributeValueId INT,GlobalAttributeDefaultValueId INT,
			AttributeDefaultValueCode NVARCHAR(300),AttributeDefaultValue NVARCHAR(300),MediaId INT,MediaPath NVARCHAR(300),IsEditable BIT,DisplayOrder INT 
		)

		CREATE TABLE #EntityAttributeDefaultValueList 
		(
			GlobalAttributeDefaultValueId INT,GlobalAttributeId INT,AttributeDefaultValueCode NVARCHAR(300),
			AttributeDefaultValue NVARCHAR(300),RowId INT,IsEditable BIT,DisplayOrder INT 
		)

		SET @LocaleId = (SELECT top 1 LocaleId FROM ZnodeLocale WHERE Code = @LocaleCode)
	
		--Getting user global attribute list
		INSERT INTO #EntityAttributeList
		(	
			GlobalEntityId ,EntityName ,EntityValue ,GlobalAttributeGroupId ,GlobalAttributeId ,AttributeTypeId ,AttributeTypeName ,
			AttributeCode  ,IsRequired ,IsLocalizable ,AttributeName,HelpDescription ,AttributeGroupDisplayOrder,DisplayOrder 
		) 
		SELECT @GlobalEntityId,@EntityName,@EntityValue EntityValue,ww.GlobalAttributeGroupId,
			c.GlobalAttributeId,c.AttributeTypeId,q.AttributeTypeName,c.AttributeCode,c.IsRequired,
			c.IsLocalizable,f.AttributeName,c.HelpDescription,w.AttributeGroupDisplayOrder,c.DisplayOrder
		FROM dbo.ZnodeGlobalGroupEntityMapper AS w 
		INNER JOIN dbo.ZnodeGlobalAttributeGroupMapper AS ww ON ww.GlobalAttributeGroupId = w.GlobalAttributeGroupId
		INNER JOIN dbo.ZnodeGlobalAttribute AS c ON ww.GlobalAttributeId = c.GlobalAttributeId
		INNER JOIN dbo.ZnodeAttributeType AS q ON c.AttributeTypeId = q.AttributeTypeId
		INNER JOIN dbo.ZnodeGlobalAttributeLocale AS f ON c.GlobalAttributeId = f.GlobalAttributeId
		WHERE w.GlobalEntityId = @GlobalEntityId AND ( f.LocaleId = isnull(@LocaleId, 0 ) or isnull(@LocaleId,0) = 0 )
		AND EXISTS(SELECT * FROM dbo.ZnodeGlobalAttributeFamily AS GAF WHERE GAF.GlobalEntityId = w.GlobalEntityId AND GAF.GlobalAttributeFamilyId = @GlobalFamilyId) 
		AND EXISTS( SELECT 1 FROM ZnodeGlobalAttributeGroup g WHERE ww.GlobalAttributeGroupId = g.GlobalAttributeGroupId 
							AND (g.GroupCode = ISNULL(@GroupCode,'') OR ISNULL(@GroupCode,'') = '' ))

		--Getting globat attribute validation data
		IF EXISTS(SELECT * FROM #EntityAttributeList)
		BEGIN
			INSERT INTO #EntityAttributeValidationList(GlobalAttributeId,ControlName , ValidationName ,SubValidationName ,RegExp, ValidationValue,IsRegExp)
			SELECT aa.GlobalAttributeId,i.ControlName,i.Name AS ValidationName,j.ValidationName AS SubValidationName,
			j.RegExp,k.Name AS ValidationValue,CAST(CASE WHEN j.RegExp IS NULL THEN 0 ELSE 1 END AS BIT) AS IsRegExp
			FROM #EntityAttributeList aa
			INNER JOIN dbo.ZnodeGlobalAttributeValidation AS k ON k.GlobalAttributeId = aa.GlobalAttributeId
			INNER JOIN dbo.ZnodeAttributeInputValidation AS i ON k.InputValidationId = i.InputValidationId
			LEFT  JOIN dbo.ZnodeAttributeInputValidationRule AS j ON k.InputValidationRuleId = j.InputValidationRuleId
		END

		---Getting globat attribute values
		INSERT INTO #EntityAttributeValueList(GlobalAttributeId,GlobalAttributeValueId,GlobalAttributeDefaultValueId,AttributeValue ,MediaId,MediaPath)
		SELECT DISTINCT GlobalAttributeId,aa.PortalGlobalAttributeValueId,bb.GlobalAttributeDefaultValueId,
			CASE WHEN bb.MediaPath IS NOT NULL THEN  @V_MediaServerThumbnailPath+bb.MediaPath--+'~'+convert(NVARCHAR(10),bb.MediaId) 
			ELSE bb.AttributeValue END,	bb.MediaId,bb.MediaPath
		FROM  dbo.ZnodePortalGlobalAttributeValue aa
		INNER JOIN ZnodePortalGlobalAttributeValueLocale bb ON bb.PortalGlobalAttributeValueId = aa.PortalGlobalAttributeValueId 
		WHERE  PortalId=@GlobalEntityValueId

		--Updating default globat attribute data into table #EntityAttributeValueList
		IF EXISTS(SELECT * FROM #EntityAttributeValueList)
		BEGIN
			UPDATE aa
			SET AttributeDefaultValueCode= h.AttributeDefaultValueCode,
				AttributeDefaultValue=g.AttributeDefaultValue,
				GlobalAttributeDefaultValueId=g.GlobalAttributeDefaultValueId,
				AttributeValue=CASE WHEN aa.AttributeValue IS NULL THEN h.AttributeDefaultValueCode ELSE aa.AttributeValue END,
				IsEditable = ISNULL(h.IsEditable, 1),DisplayOrder = h.DisplayOrder
			FROM  #EntityAttributeValueList aa
			INNER JOIN dbo.ZnodeGlobalAttributeDefaultValue h ON h.GlobalAttributeDefaultValueId = aa.GlobalAttributeDefaultValueId                                       
			INNER JOIN dbo.ZnodeGlobalAttributeDefaultValueLocale g ON h.GlobalAttributeDefaultValueId = g.GlobalAttributeDefaultValueId
		END 

		--Getting globat attribute default values
		IF EXISTS(SELECT * FROM #EntityAttributeList)
		BEGIN
			INSERT INTO #EntityAttributeDefaultValueList(GlobalAttributeDefaultValueId,GlobalAttributeId,AttributeDefaultValueCode,	AttributeDefaultValue ,RowId ,IsEditable ,DisplayOrder )
			SELECT  h.GlobalAttributeDefaultValueId, aa.GlobalAttributeId,h.AttributeDefaultValueCode,g.AttributeDefaultValue,0,ISNULL(h.IsEditable, 1),h.DisplayOrder
			FROM  #EntityAttributeList aa
			INNER JOIN dbo.ZnodeGlobalAttributeDefaultValue h ON h.GlobalAttributeId = aa.GlobalAttributeId
			INNER JOIN dbo.ZnodeGlobalAttributeDefaultValueLocale g ON h.GlobalAttributeDefaultValueId = g.GlobalAttributeDefaultValueId
		END 

		IF NOT EXISTS (SELECT 1 FROM #EntityAttributeList )
		BEGIN
			INSERT INTO #EntityAttributeList
			(	
				GlobalEntityId ,EntityName ,EntityValue ,GlobalAttributeGroupId ,GlobalAttributeId ,AttributeTypeId ,
				AttributeTypeName ,	AttributeCode  ,IsRequired ,IsLocalizable ,AttributeName,HelpDescription  
			) 
			SELECT qq.GlobalEntityId,qq.EntityName,@EntityValue EntityValue,0 GlobalAttributeGroupId,
			0 GlobalAttributeId,0 AttributeTypeId,''AttributeTypeName,''AttributeCode,0 IsRequired,
			0 IsLocalizable,'' AttributeName,'' HelpDescription
			FROM dbo.ZnodeGlobalEntity AS qq
			WHERE qq.EntityName=@EntityName 
		END

		SELECT  GlobalEntityId,EntityName,EntityValue,GlobalAttributeGroupId,AA.GlobalAttributeId,AttributeTypeId,AttributeTypeName,
			AttributeCode,IsRequired,IsLocalizable,AttributeName,ControlName,ValidationName,SubValidationName,RegExp,
			ValidationValue,CAST(ISNULL(IsRegExp,0) AS BIT)  IsRegExp,HelpDescription,AttributeValue,GlobalAttributeValueId,
			bb.GlobalAttributeDefaultValueId,aab.AttributeDefaultValueCode,	aab.AttributeDefaultValue,isnull(aab.RowId,0)   
			RowId,cast(isnull(aab.IsEditable,0) as bit)   IsEditable,bb.MediaId,AA.DisplayOrder
		FROM #EntityAttributeList AA				
		LEFT JOIN #EntityAttributeDefaultValueList aab on aab.GlobalAttributeId=AA.GlobalAttributeId	
		LEFT JOIN #EntityAttributeValidationList vl on vl.GlobalAttributeId=aa.GlobalAttributeId			
		LEFT JOIN #EntityAttributeValueList BB ON BB.GlobalAttributeId=AA.GlobalAttributeId		 
		AND ( (aab.GlobalAttributeDefaultValueId=bb.GlobalAttributeDefaultValueId	)
			OR  ( bb.MediaId IS NOT NULL AND isnull(vl.ValidationName,'')='IsAllowMultiUpload' AND bb.GlobalAttributeDefaultValueId IS NULL )
			OR  ( bb.MediaId IS NULL AND  bb.GlobalAttributeDefaultValueId is null ))
		ORDER BY  aa.DisplayOrder, aab.DisplayOrder
	END
	ELSE
	BEGIN
		--Showing blank output if store is not present
		CREATE TABLE #TempOutput
		( 
			GlobalEntityId INT,EntityName VARCHAR(100),EntityValue VARCHAR(100),GlobalAttributeGroupId INT, 
			GlobalAttributeId INT,AttributeTypeId INT, AttributeTypeName VARCHAR(100),AttributeCode VARCHAR(100),
			IsRequired BIT,IsLocalizable BIT,AttributeName VARCHAR(100),ControlName VARCHAR(100),ValidationName VARCHAR(100),
			SubValidationName VARCHAR(100),RegExp VARCHAR(100),ValidationValue VARCHAR(100), IsRegExp BIT,
			HelpDescription VARCHAR(100),AttributeValue VARCHAR(100),GlobalAttributeValueId INT,
			GlobalAttributeDefaultValueId INT,AttributeDefaultValueCode VARCHAR(100),AttributeDefaultValue VARCHAR(100), 
			RowId INT, IsEditable BIT, MediaId INT,DisplayOrder INT
		)
		SELECT * FROM #TempOutput
	END

SELECT 1 AS ID,CAST(1 AS BIT) AS Status;       
END TRY
BEGIN CATCH
	SELECT ERROR_MESSAGE()
	DECLARE @Status BIT ;
	SET @Status = 0;
	DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(),
	@ErrorLine VARCHAR(100)= ERROR_LINE(),
	@ErrorCall NVARCHAR(MAX)= null       			 
	SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                    
		 
	EXEC Znode_InsertProcedureErrorLog
	@ProcedureName = 'Znode_GetGlobalEntityValueAttributeValues',
	@ErrorInProcedure = @Error_procedure,
	@ErrorMessage = @ErrorMessage,
	@ErrorLine = @ErrorLine,
	@ErrorCall = @ErrorCall;
END CATCH;
END
GO
-------------------------------------------------
IF EXISTS(SELECT * FROM SYS.PROCEDURES WHERE NAME = 'Znode_GetUserGlobalAttributeValue')
	DROP PROC Znode_GetUserGlobalAttributeValue
GO
CREATE PROCEDURE [dbo].[Znode_GetUserGlobalAttributeValue]
(
    @EntityName NVARCHAR(200) = 0,
    @GlobalEntityValueId INT = 0,
	@LocaleCode VARCHAR(100) = '',
    @GroupCode NVARCHAR(200) = NULL,
	@SELECTedValue BIT = 0
)
AS
/*
	 Summary :- This procedure is used to get the Attribute and EntityValue attribute value as per filter pass 
	 Unit Testing 
	 BEGIN TRAN
	 EXEC [Znode_GetGlobalEntityAttributeValue] 'USER',1111111111
	 ROLLBACK TRAN

*/	 
BEGIN
BEGIN TRY
SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @EntityValue NVARCHAR(200), @LocaleId INT

	DECLARE @GlobalFamilyId int
	SET @GlobalFamilyId = (SELECT FM.GlobalAttributeFamilyId FROM ZnodeGlobalEntity GE 
		INNER JOIN  ZnodeGlobalEntityFamilyMapper FM ON GE.GlobalEntityId = FM.GlobalEntityId
		WHERE GE.EntityName =  @EntityName and  (FM.GlobalEntityValueId = @GlobalEntityValueId or FM.GlobalEntityValueId IS NULL))
  
	DECLARE @V_MediaServerThumbnailPath VARCHAR(4000);
	--Set the MediaServerThumbnailPath
	SET @V_MediaServerThumbnailPath =
	(
		SELECT ISNULL(CASE WHEN CDNURL = '' THEN NULL ELSE CDNURL END,URL)+ZMSM.ThumbnailFolderName+'/'  
		FROM ZnodeMediaConfiguration ZMC 
		INNER JOIN ZnodeMediaServerMaster ZMSM ON (ZMSM.MediaServerMasterId = ZMC.MediaServerMasterId)
		WHERE IsActive = 1 
	);

	--Getting user name
	SELECT @EntityValue=Isnull(FirstName,'')+' '+Isnull(LastName,'')
	FROM ZnodeUser
	WHERE UserId=@GlobalEntityValueId

	--Getting GlobalEntityId from @EntityName
	DECLARE @GlobalEntityId INT
	SET @GlobalEntityId =( SELECT GlobalEntityId FROM dbo.ZnodeGlobalEntity WHERE EntityName = @EntityName )

	CREATE TABLE #EntityAttributeList
	(
		GlobalEntityId INT,EntityName NVARCHAR(300),EntityValue NVARCHAR(MAX),GlobalAttributeGroupId INT,
		GlobalAttributeId INT,AttributeTypeId INT,AttributeTypeName NVARCHAR(300), AttributeCode NVARCHAR(300) ,
		IsRequired BIT,IsLocalizable BIT,AttributeName  NVARCHAR(300) , HelpDescription NVARCHAR(MAX),DisplayOrder INT
	) 
			 
	CREATE TABLE #EntityAttributeValidationList
	( 
		GlobalAttributeId INT, ControlName NVARCHAR(300), ValidationName NVARCHAR(300),SubValidationName NVARCHAR(300),
		RegExp NVARCHAR(300), ValidationValue NVARCHAR(300),IsRegExp Bit
	)

	CREATE TABLE #EntityAttributeValueList 
	(
		GlobalAttributeId INT,AttributeValue NVARCHAR(MAX),GlobalAttributeValueId INT,GlobalAttributeDefaultValueId INT,
		AttributeDefaultValueCode NVARCHAR(300),AttributeDefaultValue NVARCHAR(300),
		MediaId INT,MediaPath NVARCHAR(300),IsEditable BIT,DisplayOrder INT 
	)

	CREATE TABLE #EntityAttributeDefaultValueList 
	(	
		GlobalAttributeDefaultValueId INT,GlobalAttributeId INT,AttributeDefaultValueCode NVARCHAR(300),
		AttributeDefaultValue NVARCHAR(300),RowId INT,IsEditable BIT,DisplayOrder INT 
	)

	SET @LocaleId = (SELECT top 1 LocaleId FROM ZnodeLocale WHERE Code = @LocaleCode)
	--Getting user global attribute list
	INSERT INTO #EntityAttributeList
	(   
		GlobalEntityId ,EntityName ,EntityValue ,GlobalAttributeGroupId ,GlobalAttributeId ,AttributeTypeId ,
		AttributeTypeName ,AttributeCode  ,IsRequired ,IsLocalizable ,AttributeName,HelpDescription,DisplayOrder 
	) 
	SELECT @GlobalEntityId,@EntityName,@EntityValue EntityValue,ww.GlobalAttributeGroupId,
		c.GlobalAttributeId,c.AttributeTypeId,q.AttributeTypeName,c.AttributeCode,c.IsRequired,
		c.IsLocalizable,f.AttributeName,c.HelpDescription,c.DisplayOrder
	FROM dbo.ZnodeGlobalGroupEntityMapper AS w 
	INNER JOIN dbo.ZnodeGlobalAttributeGroupMapper AS ww ON ww.GlobalAttributeGroupId = w.GlobalAttributeGroupId
	INNER JOIN dbo.ZnodeGlobalAttribute AS c ON ww.GlobalAttributeId = c.GlobalAttributeId
	INNER JOIN dbo.ZnodeAttributeType AS q ON c.AttributeTypeId = q.AttributeTypeId
	INNER JOIN dbo.ZnodeGlobalAttributeLocale AS f ON c.GlobalAttributeId = f.GlobalAttributeId
	WHERE w.GlobalEntityId = @GlobalEntityId AND ( f.LocaleId = isnull(@LocaleId, 0 ) or isnull(@LocaleId,0) = 0 )
	AND EXISTS(SELECT * FROM dbo.ZnodeGlobalAttributeFamily AS GAF WHERE GAF.GlobalEntityId = w.GlobalEntityId AND GAF.GlobalAttributeFamilyId = @GlobalFamilyId)
	AND EXISTS( SELECT 1 FROM ZnodeGlobalAttributeGroup g WHERE ww.GlobalAttributeGroupId = g.GlobalAttributeGroupId 
					AND (g.GroupCode = ISNULL(@GroupCode,'') OR ISNULL(@GroupCode,'') = '' ))

	--Getting globat attribute validation data
	IF EXISTS(SELECT * FROM #EntityAttributeList)
	BEGIN
		INSERT INTO #EntityAttributeValidationList(GlobalAttributeId,ControlName , ValidationName ,SubValidationName ,RegExp, ValidationValue,IsRegExp)
		SELECT aa.GlobalAttributeId,i.ControlName,i.Name AS ValidationName,j.ValidationName AS SubValidationName,
			j.RegExp,k.Name AS ValidationValue,CAST(CASE WHEN j.RegExp IS NULL THEN 0 ELSE 1 END AS BIT) AS IsRegExp	
		FROM #EntityAttributeList aa
		INNER JOIN dbo.ZnodeGlobalAttributeValidation AS k ON k.GlobalAttributeId = aa.GlobalAttributeId
		INNER JOIN dbo.ZnodeAttributeInputValidation AS i ON k.InputValidationId = i.InputValidationId
		LEFT JOIN dbo.ZnodeAttributeInputValidationRule AS j ON k.InputValidationRuleId = j.InputValidationRuleId
	END

	---Getting globat attribute values
	INSERT INTO #EntityAttributeValueList(GlobalAttributeId,GlobalAttributeValueId,GlobalAttributeDefaultValueId,AttributeValue ,MediaId,MediaPath)
	SELECT GlobalAttributeId,aa.UserGlobalAttributeValueId,bb.GlobalAttributeDefaultValueId,
		CASE WHEN bb.MediaPath IS NOT NULL THEN @V_MediaServerThumbnailPath+bb.MediaPath--+'~'+convert(NVARCHAR(10),bb.MediaId) 
		ELSE bb.AttributeValue END AS AttributeValue,bb.MediaId,bb.MediaPath
	FROM  dbo.ZnodeUserGlobalAttributeValue aa
	INNER JOIN ZnodeUserGlobalAttributeValueLocale bb ON bb.UserGlobalAttributeValueId = aa.UserGlobalAttributeValueId 
	WHERE aa.UserId=@GlobalEntityValueId

	--Updating default globat attribute data into table #EntityAttributeValueList
	IF EXISTS(SELECT * FROM #EntityAttributeValueList)
	BEGIN
		UPDATE aa
		SET AttributeDefaultValueCode= h.AttributeDefaultValueCode,
			AttributeDefaultValue=g.AttributeDefaultValue,
			GlobalAttributeDefaultValueId=g.GlobalAttributeDefaultValueId,
			AttributeValue= CASE WHEN aa.AttributeValue IS NULL THEN h.AttributeDefaultValueCode ELSE aa.AttributeValue END, 
			IsEditable = ISNULL(h.IsEditable, 1),DisplayOrder = h.DisplayOrder
		FROM  #EntityAttributeValueList aa
		INNER JOIN dbo.ZnodeGlobalAttributeDefaultValue h ON h.GlobalAttributeDefaultValueId = aa.GlobalAttributeDefaultValueId                                       
		INNER JOIN dbo.ZnodeGlobalAttributeDefaultValueLocale g ON h.GlobalAttributeDefaultValueId = g.GlobalAttributeDefaultValueId
    END

	--Getting globat attribute default values
	IF EXISTS(SELECT * FROM #EntityAttributeList)
	BEGIN
		INSERT INTO #EntityAttributeDefaultValueList(GlobalAttributeDefaultValueId,GlobalAttributeId,AttributeDefaultValueCode,AttributeDefaultValue ,RowId ,IsEditable ,DisplayOrder )
		SELECT  h.GlobalAttributeDefaultValueId, aa.GlobalAttributeId,h.AttributeDefaultValueCode,g.AttributeDefaultValue,0,ISNULL(h.IsEditable, 1),h.DisplayOrder
		FROM  #EntityAttributeList aa
		INNER JOIN dbo.ZnodeGlobalAttributeDefaultValue h ON h.GlobalAttributeId = aa.GlobalAttributeId
		INNER JOIN dbo.ZnodeGlobalAttributeDefaultValueLocale g ON h.GlobalAttributeDefaultValueId = g.GlobalAttributeDefaultValueId
	END  

	IF NOT EXISTS (SELECT 1 FROM #EntityAttributeList )
	BEGIN
		INSERT INTO #EntityAttributeList
		(
			GlobalEntityId ,EntityName ,EntityValue ,GlobalAttributeGroupId ,GlobalAttributeId ,AttributeTypeId ,
			AttributeTypeName ,	AttributeCode  ,IsRequired ,IsLocalizable ,AttributeName,HelpDescription  
		) 
		SELECT qq.GlobalEntityId,qq.EntityName,@EntityValue EntityValue,0 GlobalAttributeGroupId,
		0 GlobalAttributeId,0 AttributeTypeId,''AttributeTypeName,''AttributeCode,0 IsRequired,
		0 IsLocalizable,'' AttributeName,'' HelpDescription
		FROM dbo.ZnodeGlobalEntity AS qq
		WHERE qq.EntityName=@EntityName 
	END
				

	SELECT GlobalEntityId,EntityName,EntityValue,GlobalAttributeGroupId,AA.GlobalAttributeId,AttributeTypeId,AttributeTypeName,
		AttributeCode,IsRequired,IsLocalizable,AttributeName,ControlName,ValidationName,SubValidationName,RegExp,
		ValidationValue,cast(isnull(IsRegExp,0) as bit)  IsRegExp,HelpDescription,AttributeValue,GlobalAttributeValueId,
		bb.GlobalAttributeDefaultValueId,aab.AttributeDefaultValueCode,aab.AttributeDefaultValue,isnull(aab.RowId,0)   
		RowId,cast(isnull(aab.IsEditable,0) as bit)   IsEditable,bb.MediaId,AA.DisplayOrder
	FROM #EntityAttributeList AA				
	LEFT JOIN #EntityAttributeDefaultValueList aab on aab.GlobalAttributeId=AA.GlobalAttributeId	
	LEFT JOIN #EntityAttributeValidationList vl on vl.GlobalAttributeId=aa.GlobalAttributeId			
	LEFT JOIN #EntityAttributeValueList BB ON BB.GlobalAttributeId=AA.GlobalAttributeId		 
	AND ( (aab.GlobalAttributeDefaultValueId=bb.GlobalAttributeDefaultValueId	)
		OR  ( bb.MediaId IS NOT NULL AND ISNULL(vl.ValidationName,'')='IsAllowMultiUpload'  and bb.GlobalAttributeDefaultValueId IS NULL )
		OR  ( bb.MediaId IS NULL AND bb.GlobalAttributeDefaultValueId is null ))
	ORDER BY AA.DisplayOrder,aab.DisplayOrder

	SELECT 1 AS ID,CAST(1 AS BIT) AS Status;       
END TRY
BEGIN CATCH
	SELECT ERROR_MESSAGE()
	DECLARE @Status BIT ;
	SET @Status = 0;
	DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(),
	@ErrorLine VARCHAR(100)= ERROR_LINE(),
	@ErrorCall NVARCHAR(MAX)= null       			 
	SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                    
		 
	EXEC Znode_InsertProcedureErrorLog
	@ProcedureName = 'Znode_GetGlobalEntityValueAttributeValues',
	@ErrorInProcedure = @Error_procedure,
	@ErrorMessage = @ErrorMessage,
	@ErrorLine = @ErrorLine,
	@ErrorCall = @ErrorCall;
END CATCH;
END
GO
------------------------------
IF EXISTS(SELECT * FROM SYS.PROCEDURES WHERE NAME = 'Znode_GetBillingShippingAddress')
	DROP PROC Znode_GetBillingShippingAddress
GO
CREATE PROCEDURE [dbo].[Znode_GetBillingShippingAddress]
(
	@BillingaddressId INT   = 0,
    @OrderShipmentId  INT   = 0
)
AS 
/*
	 Summary :- This Procedure is used to get the display the shipping and billing address 
	 Unit Testig 
	 EXEC  GetBillingShippingAddress 1,1
	 
*/
BEGIN 
BEGIN TRY 
SET NOCOUNT ON ;

	IF OBJECT_ID('tempdb..#ZnodeAddress') IS NOT NULL 
		DROP TABLE #ZnodeAddress

	SELECT 0 as OmsOrderShipmentId,
		AddressId,
		FirstName,
		LastName,
		DisplayName,
		CompanyName,
		Address1,
		Address2,
		Address3,
		CountryName,
		StateName StateCode,
		CityName,
		PostalCode,
		PhoneNumber,
		Mobilenumber,
		AlternateMobileNumber,
		FaxNumber,
		IsDefaultBilling,
		IsDefaultShipping,
		IsActive,
		ExternalId,
		IsShipping,
		IsBilling,
		EmailAddress  
	INTO #ZnodeAddress  
	FROM ZnodeAddress 
	WHERE AddressId= @BillingaddressId 
	UNION ALL
	SELECT OmsOrderShipmentId,
		AddressId,
		ShipToFirstName AS FirstName,
		ShipToLastName AS LastName,
		ShipName AS DisplayName,
		ShipToCompanyName AS CompanyName,
		ShipToStreet1 AS Address1,
		ShipToStreet1 AS Address2,
		'' AS Address3,
		ShipToCountry AS CountryName,
		ShipToStateCode AS StateCode,
		ShipToCity AS CityName,
		ShipToPostalCode AS PostalCode,
		ShipToPhoneNumber AS PhoneNumber,
		'' AS Mobilenumber,
		'' AS AlternateMobileNumber,
		'' AS FaxNumber,
		CAST(0 AS BIT) AS IsDefaultBilling,
		CAST(0 AS BIT) AS IsDefaultShipping,
		Cast(1 AS BIT) AS IsActive,
		'' AS ExternalId,
		CAST(1 AS BIT) AS IsShipping,
		CAST(0 AS BIT) AS IsBilling,
		ShipToEmailId AS EmailAddress
	FROM ZnodeOmsOrderShipment
	WHERE OmsOrderShipmentId = @OrderShipmentId

	SELECT ZA.*,ZS.StateName 
	FROM #ZnodeAddress ZA
	Left JOIN ZnodeState ZS on ZS.StateCode = ZA.StateCode and ZS.CountryCode = ZA.CountryName
		
END TRY 
BEGIN CATCH 
	DECLARE @Status BIT ;
	SET @Status = 0;
	DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(), 
	@ErrorCall NVARCHAR(MAX)= 'EXEC Znode_GetBillingShippingAddress @BillingaddressId = '''+ISNULL(@BillingaddressId,'''''')+''',@orderShipmentId='+ISNULL(CAST(@orderShipmentId AS
	VARCHAR(50)),'''''')
              			 
	SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                    
		  
	EXEC Znode_InsertProcedureErrorLog
			@ProcedureName = 'Znode_GetBillingShippingAddress',
			@ErrorInProcedure = 'Znode_GetBillingShippingAddress',
			@ErrorMessage = @ErrorMessage,
			@ErrorLine = @ErrorLine,
			@ErrorCall = @ErrorCall;
END CATCH 
END
GO
----------------------------------------------------------------------
IF EXISTS(SELECT * FROM SYS.PROCEDURES WHERE NAME = 'Znode_GetBillingShippingAddress')
	DROP PROC Znode_GetBillingShippingAddress
GO
CREATE PROCEDURE [dbo].[Znode_GetBillingShippingAddress]
(
	@BillingaddressId INT   = 0,
    @OrderShipmentId  INT   = 0
)
AS 
/*
	 Summary :- This Procedure is used to get the display the shipping and billing address 
	 Unit Testig 
	 EXEC  GetBillingShippingAddress 1,1
	 
*/
BEGIN 
BEGIN TRY 
SET NOCOUNT ON ;

	IF OBJECT_ID('tempdb..#ZnodeAddress') IS NOT NULL 
		DROP TABLE #ZnodeAddress

	SELECT 0 as OmsOrderShipmentId,
		AddressId,
		FirstName,
		LastName,
		DisplayName,
		CompanyName,
		Address1,
		Address2,
		Address3,
		CountryName,
		StateName StateCode,
		CityName,
		PostalCode,
		PhoneNumber,
		Mobilenumber,
		AlternateMobileNumber,
		FaxNumber,
		IsDefaultBilling,
		IsDefaultShipping,
		IsActive,
		ExternalId,
		IsShipping,
		IsBilling,
		EmailAddress  
	INTO #ZnodeAddress  
	FROM ZnodeAddress 
	WHERE AddressId= @BillingaddressId 
	UNION ALL
	SELECT OmsOrderShipmentId,
		AddressId,
		ShipToFirstName AS FirstName,
		ShipToLastName AS LastName,
		ShipName AS DisplayName,
		ShipToCompanyName AS CompanyName,
		ShipToStreet1 AS Address1,
		ShipToStreet2 AS Address2,
		'' AS Address3,
		ShipToCountry AS CountryName,
		ShipToStateCode AS StateCode,
		ShipToCity AS CityName,
		ShipToPostalCode AS PostalCode,
		ShipToPhoneNumber AS PhoneNumber,
		'' AS Mobilenumber,
		'' AS AlternateMobileNumber,
		'' AS FaxNumber,
		CAST(0 AS BIT) AS IsDefaultBilling,
		CAST(0 AS BIT) AS IsDefaultShipping,
		Cast(1 AS BIT) AS IsActive,
		'' AS ExternalId,
		CAST(1 AS BIT) AS IsShipping,
		CAST(0 AS BIT) AS IsBilling,
		ShipToEmailId AS EmailAddress
	FROM ZnodeOmsOrderShipment
	WHERE OmsOrderShipmentId = @OrderShipmentId

	SELECT ZA.*,ZS.StateName 
	FROM #ZnodeAddress ZA
	Left JOIN ZnodeState ZS on ZS.StateCode = ZA.StateCode and ZS.CountryCode = ZA.CountryName
		
END TRY 
BEGIN CATCH 
	DECLARE @Status BIT ;
	SET @Status = 0;
	DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(), 
	@ErrorCall NVARCHAR(MAX)= 'EXEC Znode_GetBillingShippingAddress @BillingaddressId = '''+ISNULL(@BillingaddressId,'''''')+''',@orderShipmentId='+ISNULL(CAST(@orderShipmentId AS
	VARCHAR(50)),'''''')
              			 
	SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                    
		  
	EXEC Znode_InsertProcedureErrorLog
			@ProcedureName = 'Znode_GetBillingShippingAddress',
			@ErrorInProcedure = 'Znode_GetBillingShippingAddress',
			@ErrorMessage = @ErrorMessage,
			@ErrorLine = @ErrorLine,
			@ErrorCall = @ErrorCall;
END CATCH 
END
GO
--------------------------------------------
IF EXISTS(SELECT * FROM SYS.PROCEDURES WHERE NAME = 'Znode_InsertUpdateSaveCartLineItemBundle')
	DROP PROC Znode_InsertUpdateSaveCartLineItemBundle
GO
CREATE PROCEDURE [dbo].[Znode_InsertUpdateSaveCartLineItemBundle]
 (
	 @SaveCartLineItemType TT_SavecartLineitems READONLY  
	,@Userid  INT = 0
	,@OrderLineItemRelationshipTypeIdBundle INT
	,@OrderLineItemRelationshipTypeIdAddon INT
 )
AS 
BEGIN 
BEGIN TRY 
SET NOCOUNT ON 
    --Declared the variables
    DECLARE @GetDate datetime= dbo.Fn_GetDate(); 
   
	DECLARE @OmsInsertedData TABLE (OmsSavedCartLineItemId INT ) 	
	----To update saved cart item personalise value FROM given line item
	DECLARE @TBL_Personalise TABLE (SKU Varchar(600),OmsSavedCartLineItemId INT, ParentOmsSavedCartLineItemId int,BundleProductIds Varchar(600) ,PersonalizeCode NVARCHAr(max),PersonalizeValue NVARCHAr(max),DesignId NVARCHAr(max), ThumbnailURL NVARCHAr(max))
	INSERT INTO @TBL_Personalise
	SELECT DISTINCT a.SKU,Null, a.ParentOmsSavedCartLineItemId,a.BundleProductIds
			,Tbl.Col.value( 'PersonalizeCode[1]', 'NVARCHAR(Max)' ) AS PersonalizeCode
			,Tbl.Col.value( 'PersonalizeValue[1]', 'NVARCHAR(Max)' ) AS PersonalizeValue
			,Tbl.Col.value( 'DesignId[1]', 'NVARCHAR(Max)' ) AS DesignId
			,Tbl.Col.value( 'ThumbnailURL[1]', 'NVARCHAR(Max)' ) AS ThumbnailURL
	FROM @SaveCartLineItemType a 
	CROSS APPLY a.PersonalisedAttribute.nodes( '//PersonaliseValueModel' ) AS Tbl(Col)   

	CREATE TABLE #NewBundleSavecartLineitemDetails 
	(
		GenId INT IDENTITY(1,1),RowId	INT	,OmsSavedCartLineItemId	INT	 ,ParentOmsSavedCartLineItemId	INT,OmsSavedCartId	INT
		,SKU	NVARCHAR(MAX) ,Quantity	NUMERIC(28,6)	,OrderLineItemRelationshipTypeID	INT	,CustomText	NVARCHAR(MAX)
		,CartAddOnDetails	NVARCHAR(MAX),Sequence	INT	,AutoAddon	VARCHAR(MAX)	,OmsOrderId	INT	,ItemDetails	NVARCHAR(MAX)
		,Custom1	NVARCHAR(MAX)  ,Custom2	NVARCHAR(MAX),Custom3	NVARCHAR(MAX),Custom4	NVARCHAR(MAX),Custom5	NVARCHAR(MAX)
		,GroupId	NVARCHAR(MAX) ,ProductName	NVARCHAR(MAX),Description	NVARCHAR(MAX),Id	INT,ParentSKU NVARCHAR(MAX)
	)
	
	--Getting new save cart data(bundle product)
	INSERT INTO #NewBundleSavecartLineitemDetails
	SELECT  Min(RowId )RowId ,OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, SKU
		,Quantity, OrderLineItemRelationshipTypeID, CustomText, CartAddOnDetails, Sequence
		,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,  GroupId ,ProductName,min(Description)Description	,0 Id,NULL ParentSKU 
	FROM @SaveCartLineItemType a 
	GROUP BY  OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, SKU
		,Quantity, OrderLineItemRelationshipTypeID, CustomText, CartAddOnDetails, Sequence
		,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName
	 
	--Getting new bundle product save cart data
	INSERT INTO #NewBundleSavecartLineitemDetails 
	SELECT   Min(RowId )RowId ,OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, BundleProductIds
				,Quantity, @OrderLineItemRelationshipTypeIdBundle, CustomText, CartAddOnDetails, Sequence
				,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName,min(Description)Description	,1 Id,SKU ParentSKU  
	FROM @SaveCartLineItemType  a 
	WHERE BundleProductIds <> ''
	GROUP BY  OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, BundleProductIds
	,Quantity,  CustomText, CartAddOnDetails, Sequence ,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName,SKU
		
	--Getting new Bundle products save cart data if addon is present for any line item
	INSERT INTO #NewBundleSavecartLineitemDetails
	SELECT  Min(RowId )RowId ,OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, AddOnValueIds
	,AddOnQuantity, @OrderLineItemRelationshipTypeIdAddon, CustomText, CartAddOnDetails, Sequence
	,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName,min(Description)Description	,1 Id 
	,CASE WHEN ConfigurableProductIds <> ''  THEN ConfigurableProductIds
			WHEN  GroupProductIds <> '' THEN GroupProductIds 
			WHEN BundleProductIds <> '' THEN BundleProductIds 
			ELSE SKU END     ParentSKU 
	FROM @SaveCartLineItemType  a 
	WHERE AddOnValueIds <> ''
	GROUP BY  OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, AddOnValueIds
	,AddOnQuantity,  CustomText, CartAddOnDetails, Sequence ,ConfigurableProductIds,GroupProductIds,	BundleProductIds
	,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName,SKU

    CREATE TABLE #OldBundleSavecartLineitemDetails (OmsSavedCartId INT ,OmsSavedCartLineItemId INT,ParentOmsSavedCartLineItemId INT , SKU  NVARCHAr(2000),OrderLineItemRelationshipTypeID INT  )
	--Getting the old bundle save cart data if present for same SKU in the new save cart data for bundle product		 
	INSERT INTO #OldBundleSavecartLineitemDetails  
	SELECT  a.OmsSavedCartId,a.OmsSavedCartLineItemId,a.ParentOmsSavedCartLineItemId , a.SKU  ,a.OrderLineItemRelationshipTypeID 
	FROM ZnodeOmsSavedCartLineItem a   
	WHERE EXISTS (SELECT TOP 1 1 FROM @SaveCartLineItemType  TY WHERE TY.OmsSavedCartId = a.OmsSavedCartId AND ISNULL(a.SKU,'') = ISNULL(TY.BundleProductIds,'')   )   
    AND a.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdBundle   

	--Getting the old save cart Parent data
	INSERT INTO #OldBundleSavecartLineitemDetails 
	SELECT DISTINCT b.OmsSavedCartId,b.OmsSavedCartLineItemId,b.ParentOmsSavedCartLineItemId , b.SKU  ,b.OrderLineItemRelationshipTypeID  
	FROM ZnodeOmsSavedCartLineItem b 
	INNER JOIN #OldBundleSavecartLineitemDetails c ON (c.ParentOmsSavedCartLineItemId  = b.OmsSavedCartLineItemId AND c.OmsSavedCartId = b.OmsSavedCartId)
	WHERE EXISTS (SELECT TOP 1 1 FROM @SaveCartLineItemType  TY WHERE TY.OmsSavedCartId = b.OmsSavedCartId AND ISNULL(b.SKU,'') = ISNULL(TY.SKU,'') AND ISNULL(b.Groupid,'-') = ISNULL(TY.Groupid,'-')  )
	AND  b.OrderLineItemRelationshipTypeID IS NULL 

	------Merge Addon for same product
	SELECT * INTO #OldValueForAddon FROM #OldBundleSavecartLineitemDetails

	DELETE a FROM #OldBundleSavecartLineitemDetails a WHERE NOT EXISTS (SELECT TOP 1 1  FROM #OldBundleSavecartLineitemDetails b WHERE b.ParentOmsSavedCartLineItemId IS NULL AND b.OmsSavedCartLineItemId = a.ParentOmsSavedCartLineItemId)
	AND a.ParentOmsSavedCartLineItemId IS NOT NULL 

	--Getting the old bundle product save cart addon data for old line items if present
	INSERT INTO #OldBundleSavecartLineitemDetails 
	SELECT b.OmsSavedCartId,b.OmsSavedCartLineItemId,b.ParentOmsSavedCartLineItemId , b.SKU  ,b.OrderLineItemRelationshipTypeID  
	FROM ZnodeOmsSavedCartLineItem b 
	INNER JOIN #OldBundleSavecartLineitemDetails c ON (c.OmsSavedCartLineItemId  = b.ParentOmsSavedCartLineItemId AND c.OmsSavedCartId = b.OmsSavedCartId)
	WHERE EXISTS (SELECT TOP 1 1 FROM @SaveCartLineItemType  TY WHERE TY.OmsSavedCartId = b.OmsSavedCartId AND ISNULL(b.SKU,'') = ISNULL(TY.AddOnValueIds,'') )
	AND  b.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon	

	------Merge Addon for same product
	IF EXISTS(SELECT * FROM @SaveCartLineItemType WHERE ISNULL(AddOnValueIds,'') <> '' )
	BEGIN

		INSERT INTO #OldValueForAddon 
		SELECT b.OmsSavedCartId,b.OmsSavedCartLineItemId,b.ParentOmsSavedCartLineItemId , b.SKU  ,b.OrderLineItemRelationshipTypeID  
		FROM ZnodeOmsSavedCartLineItem b 
		INNER JOIN #OldValueForAddon c ON (c.OmsSavedCartLineItemId  = b.ParentOmsSavedCartLineItemId AND c.OmsSavedCartId = b.OmsSavedCartId)
		WHERE EXISTS (SELECT TOP 1 1 FROM @SaveCartLineItemType  TY WHERE TY.OmsSavedCartId = b.OmsSavedCartId )--AND ISNULL(b.SKU,'') = ISNULL(TY.AddOnValueIds,'') )
		AND  b.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon

		SELECT distinct SKU, STUFF(
									( SELECT  ', ' + SKU FROM    
										( SELECT DISTINCT SKU FROM     #OldValueForAddon b 
											WHERE a.ParentOmsSavedCartLineItemId=b.ParentOmsSavedCartLineItemId and OrderLineItemRelationshipTypeID = 1 ) x 
											FOR XML PATH('')
									), 1, 2, ''
									) AddOns
		INTO #AddOnsExists
		FROM #OldValueForAddon a WHERE a.ParentOmsSavedCartLineItemId is not null and OrderLineItemRelationshipTypeID<>1

		SELECT distinct a.BundleProductIds SKU, STUFF(
										( SELECT  ', ' + x.AddOnValueIds FROM    
										( SELECT DISTINCT b.AddOnValueIds FROM @SaveCartLineItemType b
											WHERE a.SKU=b.SKU ) x
											FOR XML PATH('')
										), 1, 2, ''
									) AddOns
		INTO #AddOnAdded
		FROM @SaveCartLineItemType a

		if NOT EXISTS(SELECT * FROM #AddOnsExists a INNER JOIN #AddOnAdded b on a.SKU = b.SKU and a.AddOns = b.AddOns )
		begin
			DELETE FROM #OldBundleSavecartLineitemDetails
		end

	END

	--If addon present in new and old save cart data and not matches the addon data (old and new for merge) then removing the old save cart data FROM #OldSavecartLineitemDetails
	IF NOT EXISTS (SELECT TOP 1 1  FROM @SaveCartLineItemType ty WHERE EXISTS (SELECT TOP 1 1 FROM 	#OldBundleSavecartLineitemDetails a WHERE	
	ISNULL(TY.AddOnValueIds,'') = a.SKU AND  a.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon ))
	AND EXISTS (SELECT TOP 1 1 FROM @SaveCartLineItemType WHERE ISNULL(AddOnValueIds,'')  <> '' )
	BEGIN 
		
		DELETE FROM #OldBundleSavecartLineitemDetails 
	END 
	ELSE 
	BEGIN 
		IF EXISTS (SELECT TOP 1 1 FROM @SaveCartLineItemType WHERE ISNULL(AddOnValueIds,'')  <> '' )
		BEGIN 

			 DECLARE @parenTofAddon INT  = 0 
			 SET @parenTofAddon = (SELECT TOP 1 ParentOmsSavedCartLineItemId 
			 FROM #OldBundleSavecartLineitemDetails a
			 WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon 
			 AND (SELECT COUNT (DISTINCT SKU ) FROM  ZnodeOmsSavedCartLineItem  t WHERE t.ParentOmsSavedCartLineItemId = a.ParentOmsSavedCartLineItemId AND   t.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon ) = (SELECT COUNT (DISTINCT SKU ) FROM  #NewBundleSavecartLineitemDetails  WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon  )
			  )

			 DELETE FROM #OldBundleSavecartLineitemDetails WHERE ParentOmsSavedCartLineItemId <> @parenTofAddon  AND ParentOmsSavedCartLineItemId IS NOT NULL  

			 DELETE FROM #OldBundleSavecartLineitemDetails WHERE OmsSavedCartLineItemId NOT IN (SELECT ISNULL(m.ParentOmsSavedCartLineItemId,0) FROM #OldBundleSavecartLineitemDetails m)
			 AND ParentOmsSavedCartLineItemId IS  NULL  
		 
			 IF (SELECT COUNT (DISTINCT SKU ) FROM  #OldBundleSavecartLineitemDetails  WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon ) <> (SELECT COUNT (DISTINCT SKU ) FROM  #NewBundleSavecartLineitemDetails  WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon  )
			  BEGIN 
				DELETE FROM #OldBundleSavecartLineitemDetails
			  END 
			IF (SELECT COUNT (DISTINCT SKU ) FROM  ZnodeOmsSavedCartLineItem   WHERE ParentOmsSavedCartLineItemId =@parenTofAddon AND   OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon ) <> (SELECT COUNT (DISTINCT SKU ) FROM  #NewBundleSavecartLineitemDetails  WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon  )
			BEGIN 
				DELETE FROM #OldBundleSavecartLineitemDetails
			END 

		 END 
		 ELSE IF (SELECT COUNT (OmsSavedCartLineItemId) FROM #OldBundleSavecartLineitemDetails WHERE ParentOmsSavedCartLineItemId IS NULL ) > 1 
		 BEGIN 
		    DECLARE @TBL_deleteParentOmsSavedCartLineItemId TABLE (OmsSavedCartLineItemId INT )
			INSERT INTO @TBL_deleteParentOmsSavedCartLineItemId 
			SELECT ParentOmsSavedCartLineItemId
			FROM ZnodeOmsSavedCartLineItem a 
			WHERE ParentOmsSavedCartLineItemId IN (SELECT OmsSavedCartLineItemId FROM #OldBundleSavecartLineitemDetails WHERE ParentOmsSavedCartLineItemId IS NULL )
			AND OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon 

			DELETE FROM #OldBundleSavecartLineitemDetails WHERE OmsSavedCartLineItemId IN (SELECT OmsSavedCartLineItemId FROM @TBL_deleteParentOmsSavedCartLineItemId)
			OR ParentOmsSavedCartLineItemId IN (SELECT OmsSavedCartLineItemId FROM @TBL_deleteParentOmsSavedCartLineItemId)
		 END 
		 ELSE IF (SELECT COUNT (DISTINCT SKU ) FROM  #OldBundleSavecartLineitemDetails  WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon ) <> (SELECT COUNT (DISTINCT SKU ) FROM  #NewBundleSavecartLineitemDetails  WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon  )
		  BEGIN 
		    DELETE FROM #OldBundleSavecartLineitemDetails
		  END 
		   ELSE IF  EXISTS (SELECT TOP 1 1 FROM ZnodeOmsSavedCartLineItem Wt WHERE EXISTS (SELECT TOP 1 1 FROM #OldBundleSavecartLineitemDetails ty WHERE ty.OmsSavedCartId = wt.OmsSavedCartId AND ty.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdBundle AND wt.ParentOmsSavedCartLineItemId= ty.OmsSavedCartLineItemId  ) AND wt.OrderLineItemRelationshipTypeId = @OrderLineItemRelationshipTypeIdAddon)
		      AND EXISTS (SELECT TOP 1 1 FROM @SaveCartLineItemType WHERE ISNULL(AddOnValueIds,'')  = '' )
		 BEGIN 
		   DELETE FROM #OldBundleSavecartLineitemDetails
		 END 

	END 
	
	--Getting the personalise data for old save cart if present
	DECLARE @TBL_Personaloldvalues TABLE (OmsSavedCartLineItemId INT , PersonalizeCode NVARCHAr(max), PersonalizeValue NVARCHAr(max))
	INSERT INTO @TBL_Personaloldvalues
	SELECT OmsSavedCartLineItemId , PersonalizeCode, PersonalizeValue
	FROM ZnodeOmsPersonalizeCartItem  a 
	WHERE EXISTS (SELECT TOP 1 1 FROM #OldBundleSavecartLineitemDetails TY WHERE TY.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId)
	AND EXISTS (SELECT TOP 1 1 FROM @TBL_Personalise TU WHERE TU.PersonalizeCode = a.PersonalizeCode AND TU.PersonalizeValue = a.PersonalizeValue)
		
	IF  NOT EXISTS (SELECT TOP 1 1 FROM @TBL_Personaloldvalues)
		AND EXISTS (SELECT TOP 1 1 FROM @TBL_Personalise )
	BEGIN 
		DELETE FROM #OldBundleSavecartLineitemDetails
	END 
	ELSE 
	BEGIN 
		 IF EXISTS (SELECT TOP 1 1 FROM @TBL_Personaloldvalues)
		 AND (SELECT COUNT (DISTINCT OmsSavedCartLineItemId ) FROM #OldBundleSavecartLineitemDetails WHERE ParentOmsSavedCartLineItemId IS nULL ) > 1 
		 AND (SELECT COUNT (DISTINCT OmsSavedCartLineItemId ) FROM #OldBundleSavecartLineitemDetails WHERE ParentOmsSavedCartLineItemId IS nULL ) <>
		     (SELECT COUNT (DISTINCT OmsSavedCartLineItemId ) FROM ZnodeOmsSavedCartLineItem WHERE ParentOmsSavedCartLineItemId IS nULL and OmsSavedCartLineItemId in (select OmsSavedCartLineItemId FROM #OldBundleSavecartLineitemDetails)  )
		 BEGIN 
		   
			   DELETE FROM #OldBundleSavecartLineitemDetails WHERE OmsSavedCartLineItemId IN (
			   SELECT OmsSavedCartLineItemId FROM #OldBundleSavecartLineitemDetails WHERE OmsSavedCartLineItemId NOT IN (SELECT OmsSavedCartLineItemId FROM @TBL_Personaloldvalues )
			   AND ParentOmsSavedCartLineItemId NOT IN (SELECT OmsSavedCartLineItemId FROM @TBL_Personaloldvalues ) ) 
			   OR OmsSavedCartLineItemId IN ( SELECT ParentOmsSavedCartLineItemId FROM #OldBundleSavecartLineitemDetails WHERE OmsSavedCartLineItemId NOT IN (SELECT OmsSavedCartLineItemId FROM @TBL_Personaloldvalues )
			   AND ParentOmsSavedCartLineItemId NOT IN (SELECT OmsSavedCartLineItemId FROM @TBL_Personaloldvalues ))
		 
		 END 
		 ELSE IF NOT EXISTS (SELECT TOP 1 1 FROM @TBL_Personaloldvalues)
		 AND (SELECT COUNT (DISTINCT OmsSavedCartLineItemId ) FROM #OldBundleSavecartLineitemDetails WHERE ParentOmsSavedCartLineItemId IS nULL ) > 1 
		 AND (SELECT COUNT (DISTINCT OmsSavedCartLineItemId ) FROM #OldBundleSavecartLineitemDetails WHERE ParentOmsSavedCartLineItemId IS nULL ) <>
		     (SELECT COUNT (DISTINCT OmsSavedCartLineItemId ) FROM ZnodeOmsSavedCartLineItem WHERE ParentOmsSavedCartLineItemId IS nULL and OmsSavedCartLineItemId in (select OmsSavedCartLineItemId FROM #OldBundleSavecartLineitemDetails)  )
		 BEGIN 
		   
			   DELETE n FROM #OldBundleSavecartLineitemDetails n WHERE OmsSavedCartLineItemId  IN (SELECT OmsSavedCartLineItemId FROM ZnodeOmsPersonalizeCartItem WHERE n.OmsSavedCartLineItemId = ZnodeOmsPersonalizeCartItem.OmsSavedCartLineItemId  )
			   OR ParentOmsSavedCartLineItemId  IN (SELECT OmsSavedCartLineItemId FROM ZnodeOmsPersonalizeCartItem   )
		
		 END 
		 ELSE IF NOT EXISTS (SELECT TOP 1 1  FROM @TBL_Personalise)
		        AND EXISTS (SELECT TOP 1 1 FROM ZnodeOmsPersonalizeCartItem m WHERE EXISTS (SELECT Top 1 1 FROM #OldBundleSavecartLineitemDetails YU WHERE YU.OmsSavedCartLineItemId = m.OmsSavedCartLineItemId )) 
		       AND (SELECT COUNT (DISTINCT OmsSavedCartLineItemId ) FROM #OldBundleSavecartLineitemDetails WHERE ParentOmsSavedCartLineItemId IS nULL ) = 1
		 BEGIN 
		     DELETE FROM #OldBundleSavecartLineitemDetails WHERE NOT EXISTS (SELECT TOP 1 1  FROM @TBL_Personalise)
		 END 
	END 

	IF EXISTS (SELECT TOP 1 1 FROM #OldBundleSavecartLineitemDetails )
	BEGIN 
		----DELETE old value FROM table which having personalise data in ZnodeOmsPersonalizeCartItem but same SKU not having personalise value for new cart item
		;WITH cte AS
		(
			select distinct b.*
			FROM @SaveCartLineItemType a 
			INNER JOIN #OldBundleSavecartLineitemDetails b on ( a.BundleProductIds = b.SKU or a.SKU = b.sku)
			WHERE isnull(cast(a.PersonalisedAttribute AS varchar(max)),'') = ''
		)
		,cte2 AS
		(
			select a.ParentOmsSavedCartLineItemId
			FROM #OldBundleSavecartLineitemDetails a
			INNER JOIN ZnodeOmsPersonalizeCartItem b on b.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId
		)
		DELETE a FROM #OldBundleSavecartLineitemDetails a
		INNER JOIN cte b on a.OmsSavedCartLineItemId = b.OmsSavedCartLineItemId
		INNER JOIN cte2 c on (a.OmsSavedCartLineItemId = c.ParentOmsSavedCartLineItemId or a.ParentOmsSavedCartLineItemId = c.ParentOmsSavedCartLineItemId)

		----DELETE old value FROM table which having personalise data in ZnodeOmsPersonalizeCartItem but same SKU having personalise value for new cart item
		;WITH cte AS
		(
			select distinct b.*, 
				a.PersonalizeCode
				,a.PersonalizeValue
			FROM @TBL_Personalise a 
			INNER JOIN #OldBundleSavecartLineitemDetails b on ( a.BundleProductIds = b.SKU or a.SKU = b.sku)
			WHERE isnull(a.PersonalizeValue,'') <> ''
		)
		,cte2 AS
		(
			select a.ParentOmsSavedCartLineItemId, b.PersonalizeCode, b.PersonalizeValue
			FROM #OldBundleSavecartLineitemDetails a
			INNER JOIN ZnodeOmsPersonalizeCartItem b on b.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId
			WHERE NOT EXISTS(SELECT * FROM cte c WHERE b.OmsSavedCartLineItemId = c.OmsSavedCartLineItemId and b.PersonalizeCode = c.PersonalizeCode 
								and b.PersonalizeValue = c.PersonalizeValue )
		)
		DELETE a FROM #OldBundleSavecartLineitemDetails a
		INNER JOIN cte b on a.OmsSavedCartLineItemId = b.OmsSavedCartLineItemId
		INNER JOIN cte2 c on (a.OmsSavedCartLineItemId = c.ParentOmsSavedCartLineItemId or a.ParentOmsSavedCartLineItemId = c.ParentOmsSavedCartLineItemId)

		;WITH cte AS
		(
			SELECT b.OmsSavedCartLineItemId ,b.ParentOmsSavedCartLineItemId , a.SKU AS SKU
					,a.PersonalizeCode
			  		,a.PersonalizeValue
					,a.DesignId
					,a.ThumbnailURL
			FROM @TBL_Personalise a 
			INNER JOIN #OldBundleSavecartLineitemDetails b on a.SKU = b.SKU
			INNER JOIN ZnodeOmsPersonalizeCartItem c on b.OmsSavedCartLineItemId = c.OmsSavedCartLineItemId
			WHERE a.OmsSavedCartLineItemId = 0
		)
		DELETE b1
		FROM #OldBundleSavecartLineitemDetails b1 
		WHERE NOT EXISTS(SELECT * FROM cte c WHERE (b1.OmsSavedCartLineItemId = c.ParentOmsSavedCartLineItemId or b1.ParentOmsSavedCartLineItemId = c.ParentOmsSavedCartLineItemId))
		AND EXISTS(SELECT * FROM cte)

		--------If lineitem present in ZnodeOmsPersonalizeCartItem and personalize value is different for same line item then New lineItem will generate
		--------If lineitem present in ZnodeOmsPersonalizeCartItem and personalize value is same for same line item then Quantity will added
		;WITH cte AS
		(
			SELECT b.OmsSavedCartLineItemId ,a.ParentOmsSavedCartLineItemId , a.BundleProductIds AS SKU
					,a.PersonalizeCode
			  		,a.PersonalizeValue
					,a.DesignId
					,a.ThumbnailURL
			FROM @TBL_Personalise a 
			INNER JOIN #OldBundleSavecartLineitemDetails b on a.BundleProductIds = b.SKU
			INNER JOIN ZnodeOmsPersonalizeCartItem c on b.OmsSavedCartLineItemId = c.OmsSavedCartLineItemId
			WHERE a.OmsSavedCartLineItemId = 0
		)
		DELETE c1
		FROM cte a1		  
		INNER JOIN #OldBundleSavecartLineitemDetails b1 on a1.SKU = b1.SKU
		INNER JOIN #OldBundleSavecartLineitemDetails c1 on (b1.ParentOmsSavedCartLineItemId = c1.OmsSavedCartLineItemId OR b1.OmsSavedCartLineItemId = c1.OmsSavedCartLineItemId)
		WHERE NOT EXISTS(SELECT * FROM ZnodeOmsPersonalizeCartItem c WHERE a1.OmsSavedCartLineItemId = c.OmsSavedCartLineItemId and a1.PersonalizeValue = c.PersonalizeValue)

		--Updating the cart if old and new cart data matches	
		UPDATE a
		SET a.Quantity = a.Quantity+ty.Quantity,
		a.Custom1 = ty.Custom1,
		a.Custom2 = ty.Custom2,
		a.Custom3 = ty.Custom3,
		a.Custom4 = ty.Custom4,
		a.Custom5 = ty.Custom5,
		a.ModifiedDate = @GetDate
		FROM ZnodeOmsSavedCartLineItem a
		INNER JOIN #OldBundleSavecartLineitemDetails b ON (a.OmsSavedCartLineItemId = b.OmsSavedCartLineItemId)
		INNER JOIN #NewBundleSavecartLineitemDetails ty ON (ty.SKU = b.SKU)
		WHERE a.OrderLineItemRelationshipTypeId <> @OrderLineItemRelationshipTypeIdAddon

		 UPDATE a
		 SET a.Quantity = a.Quantity+s.AddOnQuantity,
		 a.ModifiedDate = @GetDate
		 FROM ZnodeOmsSavedCartLineItem a
		 INNER JOIN #OldBundleSavecartLineitemDetails b ON (a.ParentOmsSavedCartLineItemId = b.OmsSavedCartLineItemId)
		 INNER JOIN @SaveCartLineItemType S on a.OmsSavedCartId = s.OmsSavedCartId and a.SKU = s.AddOnValueIds
		 WHERE a.OrderLineItemRelationshipTypeId = @OrderLineItemRelationshipTypeIdAddon

		 UPDATE Ab 
		 SET Ab.Quantity = Ab.Quantity+ty.Quantity,
		 Ab.Custom1 = ty.Custom1,
		 Ab.Custom2 = ty.Custom2,
		 Ab.Custom3 = ty.Custom3,
		 Ab.Custom4 = ty.Custom4,
		 Ab.Custom5 = ty.Custom5,
		 ab.ModifiedDate = @GetDate  
         FROM ZnodeOmsSavedCartLineItem a  
         INNER JOIN ZnodeOmsSavedCartLineItem ab ON (ab.OmsSavedCartLineItemId = a.ParentOmsSavedCartLineItemId)  
         INNER JOIN @SaveCartLineItemType b ON (a.OmsSavedCartId = b.OmsSavedCartId  ) 
		 INNER JOIN #NewBundleSavecartLineitemDetails ty ON (ty.SKU = b.SKU)  
		 WHERE a.OrderLineItemRelationshipTypeId = @OrderLineItemRelationshipTypeIdBundle  
		 AND EXISTS(SELECT * FROM #OldBundleSavecartLineitemDetails ov WHERE a.OmsSavedCartLineItemId = ov.OmsSavedCartLineItemId)  

	END 
	--Inserting the new save cart data if old and new cart data not match
	ELSE 
	BEGIN 
			
		UPDATE #NewBundleSavecartLineitemDetails
		SET ParentSKU = (SELECT TOP 1 SKU FROM #NewBundleSavecartLineitemDetails WHERE OrderLineItemRelationshipTypeID IS NULL )
		WHERE OrderLineItemRelationshipTypeID  = @OrderLineItemRelationshipTypeIdAddon 
		AND EXISTS (SELECT TOP 1 1 FROM #NewBundleSavecartLineitemDetails WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdBundle) 
		
		--Getting the new save cart data and generating row no. for new save cart insert
		SELECT RowId, Id ,Row_number()Over(Order BY RowId, Id,GenId) NewRowId , ParentOmsSavedCartLineItemId ,OmsSavedCartId,SKU,Quantity,OrderLineItemRelationshipTypeId  
		   ,CustomText,CartAddOnDetails,ROW_NUMBER()Over(Order BY NewId() ) Sequence ,AutoAddon  
		   ,OmsOrderId,Custom1,Custom2,Custom3 ,Custom4 ,Custom5,GroupId,ProductName ,min(Description)Description  ,ParentSKU  
		 INTO #InsertNewBundleSavecartLineitem   
		 FROM  #NewBundleSavecartLineitemDetails  
		 GROUP BY ParentOmsSavedCartLineItemId ,OmsSavedCartId,SKU,Quantity,OrderLineItemRelationshipTypeId  
		   ,CustomText,CartAddOnDetails ,AutoAddon  
		   ,OmsOrderId,Custom1,Custom2,Custom3 ,Custom4 ,Custom5,GroupId,ProductName ,RowId, Id ,GenId,ParentSKU   
		 ORDER BY RowId, Id   
   
		--Removing the line item having Quantity <=0 
		DELETE FROM #InsertNewBundleSavecartLineitem WHERE Quantity <=0  

		;WITH Add_Dup AS
		(
			SELECT  Min(NewRowId)NewRowId ,SKU ,ParentSKU ,OrderLineItemRelationshipTypeID 
			FROM  #InsertNewBundleSavecartLineitem
			GROUP BY SKU ,ParentSKU  ,OrderLineItemRelationshipTypeID
			HAVING OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon	
		)
		DELETE FROM #InsertNewBundleSavecartLineitem
		WHERE NOT EXISTS (SELECT TOP 1 1 FROM Add_Dup WHERE Add_Dup.NewRowId = #InsertNewBundleSavecartLineitem.NewRowId)
		AND OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon

		--Updating the rowid INTo new save cart line item AS new line item is merged INTo existing save cart item
		 ;WITH VTTY AS   
		(  
			SELECT m.RowId OldRowId , TY1.RowId , TY1.SKU   
			   FROM #InsertNewBundleSavecartLineitem m  
			INNER JOIN  #InsertNewBundleSavecartLineitem TY1 ON TY1.SKU = m.ParentSKU   
			WHERE m.OrderLineItemRelationshipTypeID IN ( @OrderLineItemRelationshipTypeIdAddon , @OrderLineItemRelationshipTypeIdBundle)   
		)   
		UPDATE m1   
		SET m1.RowId = TYU.RowId  
		FROM #InsertNewBundleSavecartLineitem m1   
		INNER JOIN VTTY TYU ON (TYU.OldRowId = m1.RowId)  
      
		--Deleting the new save cart line item if cart line item is merged
		;WITH VTRET AS   
		(  
			SELECT RowId,id,Min(NewRowId)NewRowId ,SKU ,ParentSKU ,OrderLineItemRelationshipTypeID   
			FROM #InsertNewBundleSavecartLineitem   
			GROUP BY RowId,id ,SKU ,ParentSKU  ,OrderLineItemRelationshipTypeID  
			Having  SKU = ParentSKU  AND OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdAddon
		)   
		DELETE FROM #InsertNewBundleSavecartLineitem WHERE NewRowId  IN (SELECT NewRowId FROM VTRET)   
     
	   --Inserting the new cart line item if not merged in existing save cart line item
       INSERT INTO  ZnodeOmsSavedCartLineItem (ParentOmsSavedCartLineItemId ,OmsSavedCartId,SKU,Quantity,OrderLineItemRelationshipTypeId  
       ,CustomText,CartAddOnDetails,Sequence,CreatedBY,CreatedDate,ModifiedBy ,ModifiedDate,AutoAddon  
       ,OmsOrderId,Custom1,Custom2,Custom3 ,Custom4 ,Custom5,GroupId,ProductName ,Description)  
       OUTPUT INSERTED.OmsSavedCartLineItemId  INTO @OmsInsertedData 
	   SELECT NULL ,OmsSavedCartId,SKU,Quantity,OrderLineItemRelationshipTypeId  
       ,CustomText,CartAddOnDetails,ROW_NUMBER()Over(Order BY NewRowId)  sequence,@UserId,@GetDate,@UserId,@GetDate,AutoAddon  
       ,OmsOrderId,Custom1,Custom2,Custom3 ,Custom4 ,Custom5,GroupId,ProductName ,Description   
       FROM  #InsertNewBundleSavecartLineitem  TH  

		SELECT  MAX(a.OmsSavedCartLineItemId ) OmsSavedCartLineItemId 
		, b.RowId ,b.GroupId ,b.SKU ,b.ParentSKU 
		INTO #Cte_newData 
		FROM ZnodeOmsSavedCartLineItem a  
		INNER JOIN #InsertNewBundleSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.ParentSKU AND ISNULL(b.GroupId,'-') = ISNULL(a.GroupId,'-')  )  
		WHERE ISNULL(a.ParentOmsSavedCartLineItemId,0) =0  
		AND EXISTS (SELECT TOP 1 1  FROM @OmsInsertedData ui WHERE ui.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId )
			AND b.OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdAddon
		 GROUP BY b.RowId ,b.GroupId ,b.SKU	,b.ParentSKU,b.OrderLineItemRelationshipTypeID			

		UPDATE a SET a.ParentOmsSavedCartLineItemId = (SELECT TOP 1 OmsSavedCartLineItemId FROM  #Cte_newData  r  
		WHERE  r.RowId = b.RowId AND ISNULL(r.GroupId,'-') = ISNULL(a.GroupId,'-')  Order by b.RowId )   
		FROM ZnodeOmsSavedCartLineItem a  
		INNER JOIN #InsertNewBundleSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.SKU AND b.id =1  )   
		WHERE a.OrderLineItemRelationshipTypeId IS NOT NULL   
		AND b.OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdAddon  
		AND a.ParentOmsSavedCartLineItemId IS nULL  
		AND  EXISTS (SELECT TOP 1 1  FROM @OmsInsertedData ui WHERE ui.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId ) 
  
		--------------------------------------------------------------------------------------------------------

		SELECT  MIN(a.OmsSavedCartLineItemId ) OmsSavedCartLineItemId 
		, b.RowId ,b.GroupId ,b.SKU ,b.ParentSKU  
		INTO #ParentOmsSavedCartId
		FROM ZnodeOmsSavedCartLineItem a  
		INNER JOIN #InsertNewBundleSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.ParentSKU AND ISNULL(b.GroupId,'-') = ISNULL(a.GroupId,'-')  )  
		WHERE ISNULL(a.ParentOmsSavedCartLineItemId,0) =0  
		AND EXISTS (SELECT TOP 1 1  FROM @OmsInsertedData ui WHERE ui.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId )
			AND b.OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdAddon
		 GROUP BY b.RowId ,b.GroupId ,b.SKU	,b.ParentSKU,b.OrderLineItemRelationshipTypeID			

		UPDATE a SET a.ParentOmsSavedCartLineItemId = (SELECT TOP 1 OmsSavedCartLineItemId FROM  #ParentOmsSavedCartId  r  
		WHERE  r.RowId = b.RowId AND ISNULL(r.GroupId,'-') = ISNULL(a.GroupId,'-')  Order by b.RowId )   
		FROM ZnodeOmsSavedCartLineItem a  
		INNER JOIN #InsertNewBundleSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.SKU AND b.id =1  )   
		WHERE a.OrderLineItemRelationshipTypeId IS NOT NULL   
		AND b.OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdAddon   
		AND  EXISTS (SELECT TOP 1 1  FROM @OmsInsertedData ui WHERE ui.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId ) 
		AND  a.sequence in (SELECT  MIN(ab.sequence) FROM ZnodeOmsSavedCartLineItem ab WHERE a.OmsSavedCartId = ab.OmsSavedCartId and 
		 a.SKU = ab.sku and ab.OrderLineItemRelationshipTypeId is not null  ) 

		-----------------------------------------------------------------------------------------------------

		SELECT a.OmsSavedCartLineItemId , b.RowId  ,b.SKU ,b.ParentSKU  ,Row_number()Over(Order BY c.OmsSavedCartLineItemId )RowIdNo
		INTO #NewBuldleProduct
		FROM ZnodeOmsSavedCartLineItem a  
		INNER JOIN #InsertNewBundleSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.ParentSKU AND ( CASE WHEN EXISTS (SELECT TOP 1 1 FROM #NewBundleSavecartLineitemDetails WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdBundle) THEN 0 ELSE 1 END = b.id OR b.Id = 1  ))  
		INNER JOIN ZnodeOmsSavedCartLineItem c on b.sku = c.sku and b.OmsSavedCartId=c.OmsSavedCartId and b.Id = 1
		WHERE ( CASE WHEN EXISTS (SELECT TOP 1 1 FROM #NewBundleSavecartLineitemDetails WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdBundle) THEN 0 ELSE 1 END = ISNULL(a.ParentOmsSavedCartLineItemId,0) OR ISNULL(a.ParentOmsSavedCartLineItemId,0) <> 0   )
		AND b.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon  AND c.ParentOmsSavedCartLineItemId IS NULL

	   ;WITH table_update AS 
	   (
		 SELECT * , ROW_NUMBER()Over(Order BY OmsSavedCartLineItemId  ) RowIdNo
		 FROM ZnodeOmsSavedCartLineItem a
		 WHERE a.OrderLineItemRelationshipTypeId IS NOT NULL   
		 AND a.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon  
		 AND a.ParentOmsSavedCartLineItemId IS NULL  
		 AND EXISTS (SELECT TOP 1 1  FROM  #InsertNewBundleSavecartLineitem ty WHERE ty.OmsSavedCartId = a.OmsSavedCartId )
		 AND EXISTS (SELECT TOP 1 1  FROM @OmsInsertedData ui WHERE ui.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId )
	   )
		UPDATE a SET a.ParentOmsSavedCartLineItemId = (SELECT TOP 1 max(OmsSavedCartLineItemId) 
		FROM #NewBuldleProduct  r  
		WHERE  r.ParentSKU = b.ParentSKU AND a.SKU = r.SKU  GROUP BY r.ParentSKU, r.SKU  )   
		FROM table_update a  
		INNER JOIN #InsertNewBundleSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.SKU AND b.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon AND  b.id =1 )   
		WHERE (SELECT TOP 1 max(OmsSavedCartLineItemId) 
			FROM #NewBuldleProduct  r  
			WHERE  r.ParentSKU = b.ParentSKU AND a.SKU = r.SKU AND a.RowIdNo = r.RowIdNo  GROUP BY r.ParentSKU, r.SKU  )    IS NOT NULL 
	 
  
		;WITH Cte_Th AS   
		(             
			  SELECT RowId    
			 FROM #InsertNewBundleSavecartLineitem a   
			 GROUP BY RowId   
			 HAVING COUNT(NewRowId) <= 1   
		)   
		UPDATE a SET a.Quantity =  NULL,
			a.ModifiedDate = @GetDate   
		FROM ZnodeOmsSavedCartLineItem a  
		INNER JOIN #InsertNewBundleSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.SKU AND b.id =0)   
		WHERE NOT EXISTS (SELECT TOP 1 1  FROM Cte_Th TY WHERE TY.RowId = b.RowId )  
		 AND a.OrderLineItemRelationshipTypeId IS NULL   
  
		UPDATE Ab SET ab.Quantity = a.Quantity,
			ab.ModifiedDate = @GetDate   
		FROM ZnodeOmsSavedCartLineItem a  
		INNER JOIN ZnodeOmsSavedCartLineItem ab ON (ab.OmsSavedCartLineItemId = a.ParentOmsSavedCartLineItemId)  
		INNER JOIN @SaveCartLineItemType b ON (a.OmsSavedCartId = b.OmsSavedCartId  )   
		WHERE a.OrderLineItemRelationshipTypeId = @OrderLineItemRelationshipTypeIdBundle  

		UPDATE  ZnodeOmsSavedCartLineItem   
		SET GROUPID = NULL   
		WHERE  EXISTS (SELECT TOP 1 1  FROM #InsertNewBundleSavecartLineitem RT WHERE RT.OmsSavedCartId = ZnodeOmsSavedCartLineItem.OmsSavedCartId )  
		AND OrderLineItemRelationshipTypeId IS NOT NULL     
       
	   ;WITH Cte_UpdateSequence AS   
		 (  
		   SELECT OmsSavedCartLineItemId ,Row_Number()Over(Order By OmsSavedCartLineItemId) RowId , Sequence   
		   FROM ZnodeOmsSavedCartLineItem   
		   WHERE EXISTS (SELECT TOP 1 1 FROM #InsertNewBundleSavecartLineitem TH WHERE TH.OmsSavedCartId = ZnodeOmsSavedCartLineItem.OmsSavedCartId )  
		 )   
		UPDATE Cte_UpdateSequence  
		SET  Sequence = RowId  
			
		UPDATE @TBL_Personalise
		SET OmsSavedCartLineItemId = b.OmsSavedCartLineItemId
		FROM @OmsInsertedData a 
		INNER JOIN ZnodeOmsSavedCartLineItem b ON (a.OmsSavedCartLineItemId = b.OmsSavedCartLineItemId and b.OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdAddon)
		WHERE b.ParentOmsSavedCartLineItemId IS not nULL
		and b.OmsSavedCartLineItemId = (select min(OmsSavedCartLineItemId) FROM @OmsInsertedData d WHERE b.OmsSavedCartLineItemId = d.OmsSavedCartLineItemId)
	 
		DELETE FROM ZnodeOmsPersonalizeCartItem	WHERE EXISTS (SELECT TOP 1 1 FROM @TBL_Personalise yu WHERE yu.OmsSavedCartLineItemId = ZnodeOmsPersonalizeCartItem.OmsSavedCartLineItemId )
			
		----Inserting saved cart item personalise value FROM given line item
		MERGE INTO ZnodeOmsPersonalizeCartItem TARGET 
		USING @TBL_Personalise SOURCE
			   ON (TARGET.OmsSavedCartLineItemId = SOURCE.OmsSavedCartLineItemId ) 
			   WHEN NOT MATCHED THEN 
				INSERT  ( OmsSavedCartLineItemId,  CreatedBy, CreatedDate, ModifiedBy, ModifiedDate
								,PersonalizeCode, PersonalizeValue,DesignId	,ThumbnailURL )
				VALUES (  SOURCE.OmsSavedCartLineItemId,  @userId, @getdate, @userId, @getdate
								,SOURCE.PersonalizeCode, SOURCE.PersonalizeValue,SOURCE.DesignId	,SOURCE.ThumbnailURL ) ;
  
		
	END 

END TRY
BEGIN CATCH 
  SELECT ERROR_MESSAGE()
END CATCH 
END
GO
--------------------------------------------
IF EXISTS(SELECT * FROM SYS.PROCEDURES WHERE NAME = 'Znode_InsertUpdateSaveCartLineItemConfigurable')
	DROP PROC Znode_InsertUpdateSaveCartLineItemConfigurable
GO
CREATE PROCEDURE [dbo].[Znode_InsertUpdateSaveCartLineItemConfigurable]
(
	@SaveCartLineItemType TT_SavecartLineitems READONLY  
	,@Userid  INT = 0 
	,@OrderLineItemRelationshipTypeIdConfigurable INT
	,@OrderLineItemRelationshipTypeIdAddon INT	
)
AS 
BEGIN 
BEGIN TRY 
 SET NOCOUNT ON 
	--Declared the variables
    DECLARE @GetDate datetime= dbo.Fn_GetDate(); 
   
	DECLARE @OmsInsertedData TABLE (OmsSavedCartLineItemId INT, OmsSavedCartId INT,SKU NVARCHAr(max),GroupId  NVARCHAr(max),ParentOmsSavedCartLineItemId INT,OrderLineItemRelationshipTypeId INT  ) 
	----To update saved cart item personalise value FROM given line item
	DECLARE @TBL_Personalise TABLE (OmsSavedCartLineItemId INT, ParentOmsSavedCartLineItemId int,ConfigurableProductIds Varchar(600) ,PersonalizeCode NVARCHAr(max),PersonalizeValue NVARCHAr(max),DesignId NVARCHAr(max), ThumbnailURL NVARCHAr(max))
	INSERT INTO @TBL_Personalise
	SELECT DISTINCT Null, a.ParentOmsSavedCartLineItemId,a.ConfigurableProductIds
			,Tbl.Col.value( 'PersonalizeCode[1]', 'NVARCHAR(Max)' ) AS PersonalizeCode
			,Tbl.Col.value( 'PersonalizeValue[1]', 'NVARCHAR(Max)' ) AS PersonalizeValue
			,Tbl.Col.value( 'DesignId[1]', 'NVARCHAR(Max)' ) AS DesignId
			,Tbl.Col.value( 'ThumbnailURL[1]', 'NVARCHAR(Max)' ) AS ThumbnailURL
	FROM @SaveCartLineItemType a 
	CROSS APPLY a.PersonalisedAttribute.nodes( '//PersonaliseValueModel' ) AS Tbl(Col) 

	 CREATE TABLE #NewConfigSavecartLineitemDetails 
	 (
		 GenId INT IDENTITY(1,1),RowId	INT	,OmsSavedCartLineItemId	INT	 ,ParentOmsSavedCartLineItemId	INT,OmsSavedCartId	INT
		,SKU	NVARCHAR(MAX) ,Quantity	NUMERIC(28,6)	,OrderLineItemRelationshipTypeID	INT	,CustomText	NVARCHAR(MAX)
		,CartAddOnDetails	NVARCHAR(MAX),Sequence	INT	,AutoAddon	VARCHAR(MAX)	,OmsOrderId	INT	,ItemDetails	NVARCHAR(MAX)
		,Custom1	NVARCHAR(MAX)  ,Custom2	NVARCHAR(MAX),Custom3	NVARCHAR(MAX),Custom4	NVARCHAR(MAX),Custom5	NVARCHAR(MAX)
		,GroupId	NVARCHAR(MAX) ,ProductName	NVARCHAR(MAX),Description	NVARCHAR(MAX),Id	INT,ParentSKU NVARCHAR(MAX)
	)
	 
	--Getting new save cart data(configurable product)
	INSERT INTO #NewConfigSavecartLineitemDetails
	SELECT  Min(RowId )RowId ,OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, SKU
		,Quantity, OrderLineItemRelationshipTypeID, CustomText, CartAddOnDetails, Sequence
		,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,  GroupId ,ProductName,min(Description)Description	,0 Id,NULL ParentSKU 
	FROM @SaveCartLineItemType a 
	GROUP BY  OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, SKU
		,Quantity, OrderLineItemRelationshipTypeID, CustomText, CartAddOnDetails, Sequence
		,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName
	 
	--Getting new configurable product save cart data
	INSERT INTO #NewConfigSavecartLineitemDetails 
	SELECT   Min(RowId )RowId ,OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, ConfigurableProductIds
				,Quantity, @OrderLineItemRelationshipTypeIdConfigurable, CustomText, CartAddOnDetails, Sequence
				,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName,min(Description)Description	,1 Id,SKU ParentSKU  
	FROM @SaveCartLineItemType  a 
	WHERE ConfigurableProductIds <> ''
	GROUP BY  OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, ConfigurableProductIds
	,Quantity,  CustomText, CartAddOnDetails, Sequence ,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName,SKU
	
	--Getting new configurable products save cart data if addon is present for any line item
	INSERT INTO #NewConfigSavecartLineitemDetails
	SELECT  Min(RowId )RowId ,OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, AddOnValueIds
	,AddOnQuantity, @OrderLineItemRelationshipTypeIdAddon, CustomText, CartAddOnDetails, Sequence
	,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName,min(Description)Description	,1 Id 
	,CASE WHEN ConfigurableProductIds <> ''  THEN ConfigurableProductIds
			WHEN  GroupProductIds <> '' THEN GroupProductIds 
			WHEN BundleProductIds <> '' THEN BundleProductIds 
			ELSE SKU END     ParentSKU 
	FROM @SaveCartLineItemType  a 
	WHERE AddOnValueIds <> ''
		GROUP BY  OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, AddOnValueIds
		,AddOnQuantity,  CustomText, CartAddOnDetails, Sequence ,ConfigurableProductIds,GroupProductIds,	BundleProductIds
		,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName,SKU
		 
	SELECT * 
	INTO #ZnodeOmsSavedCartLineItem_NT
	FROM ZnodeOmsSavedCartLineItem a with (nolock)
	WHERE EXISTS (SELECT TOP 1 1 FROM @SaveCartLineItemType  TY WHERE TY.OmsSavedCartId = a.OmsSavedCartId)

    CREATE TABLE #OldConfigSavecartLineitemDetails (OmsSavedCartId INT ,OmsSavedCartLineItemId INT,ParentOmsSavedCartLineItemId INT , SKU  NVARCHAr(2000),OrderLineItemRelationshipTypeID INT  )
	--Getting the old configurable save cart data if present for same SKU in the new save cart data for configurable product	 
	INSERT INTO #OldConfigSavecartLineitemDetails  
	SELECT  a.OmsSavedCartId,a.OmsSavedCartLineItemId,a.ParentOmsSavedCartLineItemId , a.SKU  ,a.OrderLineItemRelationshipTypeID 
	FROM #ZnodeOmsSavedCartLineItem_NT a   
	WHERE EXISTS (SELECT TOP 1 1 FROM @SaveCartLineItemType  TY WHERE TY.OmsSavedCartId = a.OmsSavedCartId AND ISNULL(a.SKU,'') = ISNULL(TY.ConfigurableProductIds,'')   )   
    AND a.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdConfigurable   

	--Getting the old save cart Parent data
	INSERT INTO #OldConfigSavecartLineitemDetails 
	SELECT DISTINCT b.OmsSavedCartId,b.OmsSavedCartLineItemId,b.ParentOmsSavedCartLineItemId , b.SKU  ,b.OrderLineItemRelationshipTypeID  
	FROM #ZnodeOmsSavedCartLineItem_NT b 
	INNER JOIN #OldConfigSavecartLineitemDetails c ON (c.ParentOmsSavedCartLineItemId  = b.OmsSavedCartLineItemId AND c.OmsSavedCartId = b.OmsSavedCartId)
	WHERE EXISTS (SELECT TOP 1 1 FROM @SaveCartLineItemType  TY WHERE TY.OmsSavedCartId = b.OmsSavedCartId AND ISNULL(b.SKU,'') = ISNULL(TY.SKU,'') AND ISNULL(b.Groupid,'-') = ISNULL(TY.Groupid,'-')  )
	AND  b.OrderLineItemRelationshipTypeID IS NULL 
		
	------Merge Addon for same product
	SELECT * INTO #OldValueForAddon FROM #OldConfigSavecartLineitemDetails
		  
	DELETE a FROM #OldConfigSavecartLineitemDetails a WHERE NOT EXISTS (SELECT TOP 1 1  FROM #OldConfigSavecartLineitemDetails b WHERE b.ParentOmsSavedCartLineItemId IS NULL AND b.OmsSavedCartLineItemId = a.ParentOmsSavedCartLineItemId)
	AND a.ParentOmsSavedCartLineItemId IS NOT NULL 

	INSERT INTO #OldConfigSavecartLineitemDetails 
	SELECT b.OmsSavedCartId,b.OmsSavedCartLineItemId,b.ParentOmsSavedCartLineItemId , b.SKU  ,b.OrderLineItemRelationshipTypeID  
	FROM #ZnodeOmsSavedCartLineItem_NT b 
	INNER JOIN #OldConfigSavecartLineitemDetails c ON (c.OmsSavedCartLineItemId  = b.ParentOmsSavedCartLineItemId AND c.OmsSavedCartId = b.OmsSavedCartId)
	WHERE EXISTS (SELECT TOP 1 1 FROM @SaveCartLineItemType  TY WHERE TY.OmsSavedCartId = b.OmsSavedCartId AND ISNULL(b.SKU,'') = ISNULL(TY.AddOnValueIds,'') )
	AND  b.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon
		
	------Merge Addon for same product
	IF EXISTS(SELECT * FROM @SaveCartLineItemType WHERE ISNULL(AddOnValueIds,'') <> '' )
	BEGIN
		
		
		INSERT INTO #OldValueForAddon 
		SELECT b.OmsSavedCartId,b.OmsSavedCartLineItemId,b.ParentOmsSavedCartLineItemId , b.SKU  ,b.OrderLineItemRelationshipTypeID  
		FROM #ZnodeOmsSavedCartLineItem_NT b 
		INNER JOIN #OldValueForAddon c ON (c.OmsSavedCartLineItemId  = b.ParentOmsSavedCartLineItemId AND c.OmsSavedCartId = b.OmsSavedCartId)
		WHERE EXISTS (SELECT TOP 1 1 FROM @SaveCartLineItemType  TY WHERE TY.OmsSavedCartId = b.OmsSavedCartId )--AND ISNULL(b.SKU,'') = ISNULL(TY.AddOnValueIds,'') )
		AND  b.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon

		SELECT distinct SKU, STUFF(
									( SELECT  ', ' + SKU FROM    
										( SELECT DISTINCT SKU FROM     #OldValueForAddon b 
											where a.OmsSavedCartLineItemId=b.ParentOmsSavedCartLineItemId AND OrderLineItemRelationshipTypeID = 1 ) x 
											FOR XML PATH('')
									), 1, 2, ''
									) AddOns
		INTO #AddOnsExists
		FROM #OldValueForAddon a where a.ParentOmsSavedCartLineItemId is not null AND OrderLineItemRelationshipTypeID<>1

		SELECT distinct a.ConfigurableProductIds SKU, STUFF(
										( SELECT  ', ' + x.AddOnValueIds FROM    
										( SELECT DISTINCT b.AddOnValueIds FROM @SaveCartLineItemType b
											where a.SKU=b.SKU ) x
											FOR XML PATH('')
										), 1, 2, ''
									) AddOns
		INTO #AddOnAdded
		FROM @SaveCartLineItemType a

			

		IF NOT EXISTS(SELECT * FROM #AddOnsExists a INNER JOIN #AddOnAdded b ON a.SKU = b.SKU AND a.AddOns = b.AddOns )
		BEGIN
			   
			DELETE FROM #OldConfigSavecartLineitemDetails 
	
		END

	END

	--If addon present in new and old save cart data and not matches the addon data (old and new for merge) then removing the old save cart data FROM #OldSavecartLineitemDetails
	IF NOT EXISTS (SELECT TOP 1 1  FROM @SaveCartLineItemType ty WHERE EXISTS (SELECT TOP 1 1 FROM 	#OldConfigSavecartLineitemDetails a WHERE	
	ISNULL(TY.AddOnValueIds,'') = a.SKU AND  a.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon ))
	AND EXISTS (SELECT TOP 1 1 FROM @SaveCartLineItemType WHERE ISNULL(AddOnValueIds,'')  <> '' )
	BEGIN 
		
		DELETE FROM #OldConfigSavecartLineitemDetails 
	END 
	ELSE 
	BEGIN 
		
		 IF EXISTS (SELECT TOP 1 1 FROM @SaveCartLineItemType WHERE ISNULL(AddOnValueIds,'')  <> '' )
		 BEGIN 
		 
			 DECLARE @parenTofAddon  TABLE( ParentOmsSavedCartLineItemId INT  )  
			 INSERT INTO  @parenTofAddon 
			 SELECT  ParentOmsSavedCartLineItemId FROM #OldConfigSavecartLineitemDetails a  WHERE a.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon  
			 AND (SELECT COUNT (DISTINCT SKU ) FROM  #ZnodeOmsSavedCartLineItem_NT  t WHERE t.ParentOmsSavedCartLineItemId = a.ParentOmsSavedCartLineItemId AND   t.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon ) = (SELECT COUNT (DISTINCT SKU ) FROM  #NewConfigSavecartLineitemDetails  WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon  )
		  

			 DELETE FROM #OldConfigSavecartLineitemDetails WHERE OmsSavedCartLineItemId NOT IN (SELECT ParentOmsSavedCartLineItemId FROM  @parenTofAddon)   
					AND ParentOmsSavedCartLineItemId IS NOT NULL  
					AND OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdAddon

			 DELETE FROM #OldConfigSavecartLineitemDetails WHERE OmsSavedCartLineItemId NOT IN (SELECT ISNULL(m.ParentOmsSavedCartLineItemId,0) FROM #OldConfigSavecartLineitemDetails m)
			 AND ParentOmsSavedCartLineItemId IS  NULL  
		 
			 IF (SELECT COUNT (DISTINCT SKU ) FROM  #OldConfigSavecartLineitemDetails  WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon ) <> (SELECT COUNT (DISTINCT SKU ) FROM  #NewConfigSavecartLineitemDetails  WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon  )
			 BEGIN 
				DELETE FROM #OldConfigSavecartLineitemDetails 
			 END 
			 IF (SELECT COUNT (DISTINCT SKU ) FROM  #ZnodeOmsSavedCartLineItem_NT   WHERE ParentOmsSavedCartLineItemId IN (SELECT ParentOmsSavedCartLineItemId FROM @parenTofAddon)AND   OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon ) <> (SELECT COUNT (DISTINCT SKU ) FROM  #NewConfigSavecartLineitemDetails  WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon  )
			 BEGIN 
				DELETE FROM #OldConfigSavecartLineitemDetails 
			 END 

		 END 
		 ELSE IF (SELECT COUNT (OmsSavedCartLineItemId) FROM #OldConfigSavecartLineitemDetails WHERE ParentOmsSavedCartLineItemId IS NULL ) > 1 
		 BEGIN 
		    DECLARE @TBL_deleteParentOmsSavedCartLineItemId TABLE (OmsSavedCartLineItemId INT )
			INSERT INTO @TBL_deleteParentOmsSavedCartLineItemId 
			SELECT ParentOmsSavedCartLineItemId
			FROM #ZnodeOmsSavedCartLineItem_NT a 
			WHERE ParentOmsSavedCartLineItemId IN (SELECT OmsSavedCartLineItemId FROM #OldConfigSavecartLineitemDetails WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdConfigurable  )
			AND OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon 

			DELETE FROM #OldConfigSavecartLineitemDetails WHERE OmsSavedCartLineItemId IN (SELECT OmsSavedCartLineItemId FROM @TBL_deleteParentOmsSavedCartLineItemId)
			OR ParentOmsSavedCartLineItemId IN (SELECT OmsSavedCartLineItemId FROM @TBL_deleteParentOmsSavedCartLineItemId)
		    
			 DELETE FROM #OldConfigSavecartLineitemDetails WHERE OmsSavedCartLineItemId NOT IN (SELECT ISNULL(m.ParentOmsSavedCartLineItemId,0) FROM #OldConfigSavecartLineitemDetails m)
		    AND ParentOmsSavedCartLineItemId IS  NULL  

		 END
		 ELSE IF (SELECT COUNT (DISTINCT SKU ) FROM  #OldConfigSavecartLineitemDetails  WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon ) <> (SELECT COUNT (DISTINCT SKU ) FROM  #NewConfigSavecartLineitemDetails  WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon  )
		  BEGIN 
		    DELETE FROM #OldConfigSavecartLineitemDetails 
		  END  
		 ELSE IF  EXISTS (SELECT TOP 1 1 FROM #ZnodeOmsSavedCartLineItem_NT Wt WHERE EXISTS (SELECT TOP 1 1 FROM #OldConfigSavecartLineitemDetails ty WHERE ty.OmsSavedCartId = wt.OmsSavedCartId AND ty.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdConfigurable AND wt.ParentOmsSavedCartLineItemId= ty.OmsSavedCartLineItemId  ) AND wt.OrderLineItemRelationshipTypeId = @OrderLineItemRelationshipTypeIdAddon)
		      AND EXISTS (SELECT TOP 1 1 FROM @SaveCartLineItemType WHERE ISNULL(AddOnValueIds,'')  = '' )
		 BEGIN 
		   DELETE FROM #OldConfigSavecartLineitemDetails 
		 END 

	END 

	DECLARE @TBL_Personaloldvalues TABLE (OmsSavedCartLineItemId INT , PersonalizeCode NVARCHAr(max), PersonalizeValue NVARCHAr(max))
	--Getting the personalise data for old save cart if present		
	INSERT INTO @TBL_Personaloldvalues
	SELECT OmsSavedCartLineItemId , PersonalizeCode, PersonalizeValue
	FROM ZnodeOmsPersonalizeCartItem  a 
	WHERE EXISTS (SELECT TOP 1 1 FROM #OldConfigSavecartLineitemDetails TY WHERE TY.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId)
	AND EXISTS (SELECT TOP 1 1 FROM @TBL_Personalise TU WHERE TU.PersonalizeCode = a.PersonalizeCode AND TU.PersonalizeValue = a.PersonalizeValue)
		
	IF  NOT EXISTS (SELECT TOP 1 1 FROM @TBL_Personaloldvalues)
		AND EXISTS (SELECT TOP 1 1 FROM @TBL_Personalise )
	BEGIN 
		 DELETE FROM #OldConfigSavecartLineitemDetails 
	END 
	ELSE 
	BEGIN 
		 IF EXISTS (SELECT TOP 1 1 FROM @TBL_Personaloldvalues)
		 AND (SELECT COUNT (DISTINCT OmsSavedCartLineItemId ) FROM #OldConfigSavecartLineitemDetails WHERE ParentOmsSavedCartLineItemId IS nULL ) > 1 
		 BEGIN 
		    DELETE FROM #OldConfigSavecartLineitemDetails WHERE OmsSavedCartLineItemId IN (
		    SELECT OmsSavedCartLineItemId FROM #OldConfigSavecartLineitemDetails WHERE OmsSavedCartLineItemId NOT IN (SELECT OmsSavedCartLineItemId FROM @TBL_Personaloldvalues )
		    AND ParentOmsSavedCartLineItemId NOT IN (SELECT OmsSavedCartLineItemId FROM @TBL_Personaloldvalues ) ) 
		    OR OmsSavedCartLineItemId IN ( SELECT ParentOmsSavedCartLineItemId FROM #OldConfigSavecartLineitemDetails WHERE OmsSavedCartLineItemId NOT IN (SELECT OmsSavedCartLineItemId FROM @TBL_Personaloldvalues )
		    AND ParentOmsSavedCartLineItemId NOT IN (SELECT OmsSavedCartLineItemId FROM @TBL_Personaloldvalues ))
		 END 
		 ELSE IF NOT EXISTS (SELECT TOP 1 1 FROM @TBL_Personaloldvalues)
		 AND (SELECT COUNT (DISTINCT OmsSavedCartLineItemId ) FROM #OldConfigSavecartLineitemDetails WHERE ParentOmsSavedCartLineItemId IS nULL ) > 1 
		 BEGIN 
		 
		   DELETE n FROM #OldConfigSavecartLineitemDetails n WHERE OmsSavedCartLineItemId  IN (SELECT OmsSavedCartLineItemId FROM ZnodeOmsPersonalizeCartItem WHERE n.OmsSavedCartLineItemId = ZnodeOmsPersonalizeCartItem.OmsSavedCartLineItemId  )
		   OR ParentOmsSavedCartLineItemId  IN (SELECT OmsSavedCartLineItemId FROM ZnodeOmsPersonalizeCartItem   )
	
		 END 
		 ELSE IF NOT EXISTS (SELECT TOP 1 1  FROM @TBL_Personalise)
		        AND EXISTS (SELECT TOP 1 1 FROM ZnodeOmsPersonalizeCartItem m WHERE EXISTS (SELECT Top 1 1 FROM #OldConfigSavecartLineitemDetails YU WHERE YU.OmsSavedCartLineItemId = m.OmsSavedCartLineItemId )) 
		       AND (SELECT COUNT (DISTINCT OmsSavedCartLineItemId ) FROM #OldConfigSavecartLineitemDetails WHERE ParentOmsSavedCartLineItemId IS nULL ) = 1
		 BEGIN 
		     DELETE FROM #OldConfigSavecartLineitemDetails WHERE NOT EXISTS (SELECT TOP 1 1  FROM @TBL_Personalise)
		 END 
	END 

	IF EXISTS (SELECT TOP 1 1 FROM #OldConfigSavecartLineitemDetails )
	BEGIN
		--------If lineitem present in ZnodeOmsPersonalizeCartItem AND personalize value is different for same line item then New lineItem will generate
		--------If lineitem present in ZnodeOmsPersonalizeCartItem AND personalize value is same for same line item then Quantity will added
		;WITH cte AS
		(
			SELECT b.OmsSavedCartLineItemId ,a.ParentOmsSavedCartLineItemId , a.ConfigurableProductIds AS SKU
					,a.PersonalizeCode
			  		,a.PersonalizeValue
					,a.DesignId
					,a.ThumbnailURL
			FROM @TBL_Personalise a 
			INNER JOIN #OldConfigSavecartLineitemDetails b ON a.ConfigurableProductIds = b.SKU
			INNER JOIN ZnodeOmsPersonalizeCartItem c ON b.OmsSavedCartLineItemId = c.OmsSavedCartLineItemId
			WHERE a.OmsSavedCartLineItemId = 0
		)
		DELETE c1
		FROM cte a1		  
		INNER JOIN #OldConfigSavecartLineitemDetails b1 ON a1.SKU = b1.SKU
		INNER JOIN #OldConfigSavecartLineitemDetails c1 ON (b1.ParentOmsSavedCartLineItemId = c1.OmsSavedCartLineItemId OR b1.OmsSavedCartLineItemId = c1.OmsSavedCartLineItemId)
		WHERE NOT EXISTS(SELECT * FROM ZnodeOmsPersonalizeCartItem c where a1.OmsSavedCartLineItemId = c.OmsSavedCartLineItemId AND a1.PersonalizeValue = c.PersonalizeValue)

		--Updating the cart if old and new cart data matches
		UPDATE a
		SET a.Quantity = a.Quantity+ty.Quantity,
		a.Custom1 = ty.Custom1,
		a.Custom2 = ty.Custom2,
		a.Custom3 = ty.Custom3,
		a.Custom4 = ty.Custom4,
		a.Custom5 = ty.Custom5,
		a.ModifiedDate = @GetDate  
		FROM ZnodeOmsSavedCartLineItem a
		INNER JOIN #OldConfigSavecartLineitemDetails b ON (a.OmsSavedCartLineItemId = b.OmsSavedCartLineItemId)
		INNER JOIN #NewConfigSavecartLineitemDetails ty ON (ty.SKU = b.SKU)
		WHERE a.OrderLineItemRelationshipTypeId <> @OrderLineItemRelationshipTypeIdAddon
		 
		 UPDATE a
		 SET a.Quantity = a.Quantity+s.AddOnQuantity,
		 a.ModifiedDate = @GetDate
		 FROM ZnodeOmsSavedCartLineItem a
		 INNER JOIN #OldConfigSavecartLineitemDetails b ON (a.ParentOmsSavedCartLineItemId = b.OmsSavedCartLineItemId)
		 INNER JOIN @SaveCartLineItemType S ON a.OmsSavedCartId = s.OmsSavedCartId AND a.SKU = s.AddOnValueIds
		 WHERE a.OrderLineItemRelationshipTypeId = @OrderLineItemRelationshipTypeIdAddon
	END 
	--Inserting the new save cart data if old and new cart data not match
	ELSE 
	BEGIN 
		--Getting the new save cart data and generating row no. for new save cart insert
		SELECT RowId, Id ,Row_number()Over(Order BY RowId, Id,GenId) NewRowId , ParentOmsSavedCartLineItemId ,OmsSavedCartId,SKU,Quantity,OrderLineItemRelationshipTypeId  
		   ,CustomText,CartAddOnDetails,ROW_NUMBER()Over(Order BY NewId() ) Sequence ,AutoAddon  
		   ,OmsOrderId,Custom1,Custom2,Custom3 ,Custom4 ,Custom5,GroupId,ProductName ,min(Description)Description  ,ParentSKU  
		 INTO #InsertNewConfigSavecartLineitem   
		 FROM  #NewConfigSavecartLineitemDetails  
		 GROUP BY ParentOmsSavedCartLineItemId ,OmsSavedCartId,SKU,Quantity,OrderLineItemRelationshipTypeId  
		   ,CustomText,CartAddOnDetails ,AutoAddon  
		   ,OmsOrderId,Custom1,Custom2,Custom3 ,Custom4 ,Custom5,GroupId,ProductName ,RowId, Id ,GenId,ParentSKU   
		 ORDER BY RowId, Id   
       	    
		--Removing the line item having Quantity <=0 
		DELETE FROM #InsertNewConfigSavecartLineitem WHERE Quantity <=0  
  
		--Updating the rowid INTo new save cart line item as new line item is merged INTo existing save cart item
		 ;WITH VTTY AS   
		(  
			SELECT m.RowId OldRowId , TY1.RowId , TY1.SKU   
			   FROM #InsertNewConfigSavecartLineitem m  
			INNER JOIN  #InsertNewConfigSavecartLineitem TY1 ON TY1.SKU = m.ParentSKU   
			WHERE m.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon   
		)   
		UPDATE m1   
		SET m1.RowId = TYU.RowId  
		FROM #InsertNewConfigSavecartLineitem m1   
		INNER JOIN VTTY TYU ON (TYU.OldRowId = m1.RowId)  
           
		;WITH VTRET AS   
		(  
			SELECT RowId,id,Min(NewRowId)NewRowId ,SKU ,ParentSKU  ,OrderLineItemRelationshipTypeID  
			FROM #InsertNewConfigSavecartLineitem   
			GROUP BY RowId,id ,SKU ,ParentSKU  ,OrderLineItemRelationshipTypeID  
			Having  SKU = ParentSKU  AND OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdAddon
		)   
		DELETE FROM #InsertNewConfigSavecartLineitem WHERE NewRowId  IN (SELECT NewRowId FROM VTRET)   
     
	   --Inserting the new cart line item if not merged in existing save cart line item
       INSERT INTO  ZnodeOmsSavedCartLineItem (ParentOmsSavedCartLineItemId ,OmsSavedCartId,SKU,Quantity,OrderLineItemRelationshipTypeId  
       ,CustomText,CartAddOnDetails,Sequence,	CreatedBY,CreatedDate,ModifiedBy ,ModifiedDate,AutoAddon  
       ,OmsOrderId,Custom1,Custom2,Custom3 ,Custom4 ,Custom5,GroupId,ProductName ,Description)  
        OUTPUT INSERTED.OmsSavedCartLineItemId,INSERTED.OmsSavedCartId,inserted.SKU,inserted.GroupId,INSERTED.ParentOmsSavedCartLineItemId,INSERTED.OrderLineItemRelationshipTypeId  INTO @OmsInsertedData 
	   SELECT NULL ,OmsSavedCartId,SKU,Quantity,OrderLineItemRelationshipTypeId  
       ,CustomText,CartAddOnDetails,ROW_NUMBER()Over(Order BY NewRowId)  sequence,@UserId,@GetDate,@UserId,@GetDate,AutoAddon  
       ,OmsOrderId,Custom1,Custom2,Custom3 ,Custom4 ,Custom5,GroupId,ProductName ,Description   
       FROM  #InsertNewConfigSavecartLineitem  TH  

		SELECT  MAX(a.OmsSavedCartLineItemId ) OmsSavedCartLineItemId 
		, b.RowId ,b.GroupId ,b.SKU ,b.ParentSKU
		INTO   #Cte_newData
		FROM @OmsInsertedData a  
		INNER JOIN #InsertNewConfigSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.ParentSKU AND ISNULL(b.GroupId,'-') = ISNULL(a.GroupId,'-')  )  
		WHERE ISNULL(a.ParentOmsSavedCartLineItemId,0) =0  
			AND b.OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdAddon
		 GROUP BY b.RowId ,b.GroupId ,b.SKU	,b.ParentSKU,b.OrderLineItemRelationshipTypeID			  
	
		UPDATE a SET a.ParentOmsSavedCartLineItemId = (SELECT TOP 1 OmsSavedCartLineItemId FROM  #Cte_newData  r  
		WHERE  r.RowId = b.RowId AND ISNULL(r.GroupId,'-') = ISNULL(a.GroupId,'-')  Order by b.RowId )   
		FROM ZnodeOmsSavedCartLineItem a  
		INNER JOIN #InsertNewConfigSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.SKU AND b.id =1  )   
		WHERE a.OrderLineItemRelationshipTypeId IS NOT NULL   
		AND b.OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdAddon  
		AND a.ParentOmsSavedCartLineItemId IS nULL   
		AND EXISTS (SELECT TOP 1 1  FROM @OmsInsertedData ui WHERE ui.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId )

	---------------------------------------------------------------------------------------------------------------------

		SELECT  MIN(a.OmsSavedCartLineItemId ) OmsSavedCartLineItemId 
		, b.RowId ,b.GroupId ,b.SKU ,b.ParentSKU 
		INTO #Cte_newData1 
		FROM @OmsInsertedData a  
		INNER JOIN #InsertNewConfigSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.ParentSKU AND ISNULL(b.GroupId,'-') = ISNULL(a.GroupId,'-')  )  
		WHERE ISNULL(a.ParentOmsSavedCartLineItemId,0) =0  
		AND EXISTS (SELECT TOP 1 1  FROM @OmsInsertedData ui WHERE ui.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId )
		AND b.OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdAddon
		 GROUP BY b.RowId ,b.GroupId ,b.SKU	,b.ParentSKU,b.OrderLineItemRelationshipTypeID			

		UPDATE a SET a.ParentOmsSavedCartLineItemId = (SELECT TOP 1 OmsSavedCartLineItemId FROM  #Cte_newData1  r  
		WHERE  r.RowId = b.RowId AND ISNULL(r.GroupId,'-') = ISNULL(a.GroupId,'-')  Order by b.RowId )   
		FROM ZnodeOmsSavedCartLineItem a  
		INNER JOIN #InsertNewConfigSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.SKU AND b.id =1  )   
		WHERE a.OrderLineItemRelationshipTypeId IS NOT NULL   
		AND b.OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdAddon     
		AND EXISTS (SELECT TOP 1 1  FROM @OmsInsertedData ui WHERE ui.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId )
		AND  a.sequence in (SELECT  MIN(ab.sequence) FROM ZnodeOmsSavedCartLineItem ab where a.OmsSavedCartId = ab.OmsSavedCartId AND 
		 a.SKU = ab.sku AND ab.OrderLineItemRelationshipTypeId is not null  ) 
 
	---------------------------------------------------------------------------------------------------------------------------

		SELECT a.OmsSavedCartLineItemId , b.RowId  ,b.SKU ,b.ParentSKU  ,Row_number()Over(Order BY c.OmsSavedCartLineItemId  )RowIdNo
		INTO #NewConfigProduct
		FROM ZnodeOmsSavedCartLineItem a with (nolock) 
		INNER JOIN #InsertNewConfigSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.ParentSKU AND ( b.Id = 1  ))  
		INNER JOIN ZnodeOmsSavedCartLineItem c with (nolock) ON b.sku = c.sku AND b.OmsSavedCartId=c.OmsSavedCartId AND b.Id = 1 
		WHERE ( ISNULL(a.ParentOmsSavedCartLineItemId,0) <> 0   )
		AND b.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon  
		AND EXISTS (SELECT TOP 1 1  FROM @OmsInsertedData ui WHERE ui.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId ) AND c.ParentOmsSavedCartLineItemId is null
  
	   ;WITH table_update AS 
	   (
		 SELECT * , ROW_NUMBER()Over(Order BY OmsSavedCartLineItemId  ) RowIdNo
		 FROM ZnodeOmsSavedCartLineItem a with (nolock)
		 WHERE a.OrderLineItemRelationshipTypeId IS NOT NULL   
		 AND a.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon  
		 AND a.ParentOmsSavedCartLineItemId IS NULL  
		 AND EXISTS (SELECT TOP 1 1  FROM  #InsertNewConfigSavecartLineitem ty WHERE ty.OmsSavedCartId = a.OmsSavedCartId )
		 AND EXISTS (SELECT TOP 1 1  FROM @OmsInsertedData ui WHERE ui.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId )
	   )
		UPDATE a SET  
		a.ParentOmsSavedCartLineItemId = (SELECT TOP 1 max(OmsSavedCartLineItemId) 
			FROM #NewConfigProduct  r  
		WHERE  r.ParentSKU = b.ParentSKU AND a.SKU = r.SKU AND a.RowIdNo = r.RowIdNo  GROUP BY r.ParentSKU, r.SKU  )   
		FROM table_update a  
		INNER JOIN #InsertNewConfigSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.SKU AND b.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon AND  b.id =1 )   
		WHERE (SELECT TOP 1 max(OmsSavedCartLineItemId) 
			FROM #NewConfigProduct  r  
		WHERE  r.ParentSKU = b.ParentSKU AND a.SKU = r.SKU AND a.RowIdNo = r.RowIdNo  GROUP BY r.ParentSKU, r.SKU  )    IS nOT NULL 

		;WITH Cte_Th AS   
		(             
			  SELECT RowId    
			 FROM #InsertNewConfigSavecartLineitem a   
			 GROUP BY RowId   
			 HAVING COUNT(NewRowId) <= 1   
		)   
		UPDATE a SET a.Quantity =  NULL,
			a.ModifiedDate = @GetDate   
		FROM ZnodeOmsSavedCartLineItem a  
		INNER JOIN #InsertNewConfigSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.SKU AND b.id =0)   
		WHERE NOT EXISTS (SELECT TOP 1 1  FROM Cte_Th TY WHERE TY.RowId = b.RowId )  
		 AND a.OrderLineItemRelationshipTypeId IS NULL   
  
		UPDATE  ZnodeOmsSavedCartLineItem   
		SET GROUPID = NULL   
		WHERE  EXISTS (SELECT TOP 1 1  FROM #InsertNewConfigSavecartLineitem RT WHERE RT.OmsSavedCartId = ZnodeOmsSavedCartLineItem.OmsSavedCartId )  
		AND OrderLineItemRelationshipTypeId IS NOT NULL     
       
	   ;WITH Cte_UpdateSequence AS   
		(  
		   SELECT OmsSavedCartLineItemId ,Row_Number()Over(Order By OmsSavedCartLineItemId) RowId , Sequence   
		   FROM ZnodeOmsSavedCartLineItem   
		   WHERE EXISTS (SELECT TOP 1 1 FROM #InsertNewConfigSavecartLineitem TH WHERE TH.OmsSavedCartId = ZnodeOmsSavedCartLineItem.OmsSavedCartId )  
		)   
		UPDATE Cte_UpdateSequence  
		SET  Sequence = RowId  

		UPDATE @TBL_Personalise
		SET OmsSavedCartLineItemId = b.OmsSavedCartLineItemId
		FROM @OmsInsertedData a 
		INNER JOIN ZnodeOmsSavedCartLineItem b with (nolock) ON (a.OmsSavedCartLineItemId = b.OmsSavedCartLineItemId  AND b.OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdAddon)
		WHERE b.ParentOmsSavedCartLineItemId IS not nULL 
	
		DELETE FROM ZnodeOmsPersonalizeCartItem WHERE EXISTS (SELECT TOP 1 1 FROM @TBL_Personalise yu WHERE yu.OmsSavedCartLineItemId = ZnodeOmsPersonalizeCartItem.OmsSavedCartLineItemId )
		
		----Inserting saved cart item personalise value FROM given line item
		MERGE INTO ZnodeOmsPersonalizeCartItem TARGET 
		USING @TBL_Personalise SOURCE
			   ON (TARGET.OmsSavedCartLineItemId = SOURCE.OmsSavedCartLineItemId ) 
			   WHEN NOT MATCHED THEN 
				INSERT  ( OmsSavedCartLineItemId,  CreatedBy, CreatedDate, ModifiedBy, ModifiedDate
								,PersonalizeCode, PersonalizeValue,DesignId	,ThumbnailURL )
				VALUES (  SOURCE.OmsSavedCartLineItemId,  @userId, @getdate, @userId, @getdate
								,SOURCE.PersonalizeCode, SOURCE.PersonalizeValue,SOURCE.DesignId	,SOURCE.ThumbnailURL ) ;
  
		
	END 

END TRY
BEGIN CATCH 
  SELECT ERROR_MESSAGE()
END CATCH 
END
GO
--------------------------------------------
IF EXISTS(SELECT * FROM SYS.PROCEDURES WHERE NAME = 'Znode_InsertUpdateSaveCartLineItemGroup')
	DROP PROC Znode_InsertUpdateSaveCartLineItemGroup
GO
CREATE PROCEDURE [dbo].[Znode_InsertUpdateSaveCartLineItemGroup]
 (
	 @SaveCartLineItemType TT_SavecartLineitems READONLY  
	,@Userid  INT = 0 
	,@OrderLineItemRelationshipTypeIdGroup INT
	,@OrderLineItemRelationshipTypeIdAddon INT
 )
AS 
BEGIN 
BEGIN TRY 
SET NOCOUNT ON 
	--Declared the variables
	DECLARE @GetDate datetime= dbo.Fn_GetDate(); 
	
	DECLARE @OmsInsertedData TABLE (SKU varchar(600),OmsSavedCartLineItemId INT ) 	
	----To update saved cart item personalise value FROM given line item
	DECLARE @TBL_Personalise TABLE (SKU varchar(600),OmsSavedCartLineItemId INT, ParentOmsSavedCartLineItemId int,GroupProductIds varchar(600),PersonalizeCode NVARCHAr(max),PersonalizeValue NVARCHAr(max),DesignId NVARCHAr(max), ThumbnailURL NVARCHAr(max))
	INSERT INTO @TBL_Personalise
	SELECT DISTINCT a.SKU,Null, a.ParentOmsSavedCartLineItemId,a.GroupProductIds
			,Tbl.Col.value( 'PersonalizeCode[1]', 'NVARCHAR(Max)' ) AS PersonalizeCode
			,Tbl.Col.value( 'PersonalizeValue[1]', 'NVARCHAR(Max)' ) AS PersonalizeValue
			,Tbl.Col.value( 'DesignId[1]', 'NVARCHAR(Max)' ) AS DesignId
			,Tbl.Col.value( 'ThumbnailURL[1]', 'NVARCHAR(Max)' ) AS ThumbnailURL
	FROM @SaveCartLineItemType a 
	CROSS APPLY a.PersonalisedAttribute.nodes( '//PersonaliseValueModel' ) AS Tbl(Col) 

	CREATE TABLE #NewGroupSavecartLineitemDetails 
	(
		GenId INT IDENTITY(1,1),RowId	INT ,OmsSavedCartLineItemId	INT,ParentOmsSavedCartLineItemId	INT,OmsSavedCartId	int
		,SKU	NVARCHAR(MAX) ,Quantity	numeric(28,6)	,OrderLineItemRelationshipTypeID	INT ,CustomText	NVARCHAR(MAX)
		,CartAddOnDetails	NVARCHAR(MAX),Sequence	INT ,AutoAddon	VARCHAR(MAX)	,OmsOrderId	INT ,ItemDetails	NVARCHAR(MAX)
		,Custom1	NVARCHAR(MAX)  ,Custom2	NVARCHAR(MAX),Custom3	NVARCHAR(MAX),Custom4	NVARCHAR(MAX),Custom5	NVARCHAR(MAX)
		,GroupId	NVARCHAR(MAX) ,ProductName	NVARCHAR(MAX),Description	NVARCHAR(MAX),Id	INT,ParentSKU NVARCHAR(MAX)
	)
	 
	--Getting new save cart data(group product)
	INSERT INTO #NewGroupSavecartLineitemDetails
	SELECT  Min(RowId )RowId ,OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, SKU
		,Quantity, OrderLineItemRelationshipTypeID, CustomText, CartAddOnDetails, Sequence
		,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,  GroupId ,ProductName,min(Description)Description	,0 Id,NULL ParentSKU 
	FROM @SaveCartLineItemType a 
	GROUP BY  OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, SKU
		,Quantity, OrderLineItemRelationshipTypeID, CustomText, CartAddOnDetails, Sequence
		,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName
	 
	--Getting new group product save cart data
	INSERT INTO #NewGroupSavecartLineitemDetails 
	SELECT   Min(RowId )RowId ,OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, GroupProductIds
				,Quantity, @OrderLineItemRelationshipTypeIdGroup, CustomText, CartAddOnDetails, Sequence
				,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName,min(Description)Description	,1 Id,SKU ParentSKU  
	FROM @SaveCartLineItemType  a 
	WHERE GroupProductIds <> ''
	GROUP BY  OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, GroupProductIds
	,Quantity,  CustomText, CartAddOnDetails, Sequence ,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName,SKU
		
	--Getting new group products save cart data if addon is present for any line item
	INSERT INTO #NewGroupSavecartLineitemDetails
	SELECT  Min(RowId )RowId ,OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, AddOnValueIds
	,AddOnQuantity, @OrderLineItemRelationshipTypeIdAddon, CustomText, CartAddOnDetails, Sequence
	,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName,min(Description)Description	,1 Id 
	,CASE WHEN ConfigurableProductIds <> ''  THEN ConfigurableProductIds
			WHEN  GroupProductIds <> '' THEN GroupProductIds 
			WHEN BundleProductIds <> '' THEN BundleProductIds 
			ELSE SKU END     ParentSKU 
	FROM @SaveCartLineItemType  a 
	WHERE AddOnValueIds <> ''
		GROUP BY  OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, AddOnValueIds
		,AddOnQuantity,  CustomText, CartAddOnDetails, Sequence ,ConfigurableProductIds,GroupProductIds,	BundleProductIds
		,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName,SKU
		 

	CREATE TABLE #OldGroupSavecartLineitemDetails (OmsSavedCartId INT ,OmsSavedCartLineItemId INT,ParentOmsSavedCartLineItemId INT , SKU  NVARCHAr(2000),OrderLineItemRelationshipTypeID INT  )
	--Getting the old group save cart data if present for same SKU in the new save cart data for group product	 	 
	INSERT INTO #OldGroupSavecartLineitemDetails  
	SELECT  a.OmsSavedCartId,a.OmsSavedCartLineItemId,a.ParentOmsSavedCartLineItemId , a.SKU  ,a.OrderLineItemRelationshipTypeID 
	FROM ZnodeOmsSavedCartLineItem a with (nolock)  
	WHERE EXISTS (SELECT TOP 1 1 FROM @SaveCartLineItemType  TY WHERE TY.OmsSavedCartId = a.OmsSavedCartId AND ISNULL(a.SKU,'') = ISNULL(TY.GroupProductIds,'')   )   
	AND a.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdGroup   

	--Getting the old save cart Parent data
	INSERT INTO #OldGroupSavecartLineitemDetails 
	SELECT DISTINCT b.OmsSavedCartId,b.OmsSavedCartLineItemId,b.ParentOmsSavedCartLineItemId , b.SKU  ,b.OrderLineItemRelationshipTypeID  
	FROM ZnodeOmsSavedCartLineItem b with (nolock)
	INNER JOIN #OldGroupSavecartLineitemDetails c ON (c.ParentOmsSavedCartLineItemId  = b.OmsSavedCartLineItemId AND c.OmsSavedCartId = b.OmsSavedCartId)
	WHERE EXISTS (SELECT TOP 1 1 FROM @SaveCartLineItemType  TY WHERE TY.OmsSavedCartId = b.OmsSavedCartId AND ISNULL(b.SKU,'') = ISNULL(TY.SKU,'') AND ISNULL(b.Groupid,'-') = ISNULL(TY.Groupid,'-')  )
	AND  b.OrderLineItemRelationshipTypeID IS NULL 

	------Merge Addon for same product
	SELECT * INTO #OldValueForAddon FROM #OldGroupSavecartLineitemDetails

	DELETE a FROM #OldGroupSavecartLineitemDetails a WHERE NOT EXISTS (SELECT TOP 1 1  FROM #OldGroupSavecartLineitemDetails b WHERE b.ParentOmsSavedCartLineItemId IS NULL AND b.OmsSavedCartLineItemId = a.ParentOmsSavedCartLineItemId)
	AND a.ParentOmsSavedCartLineItemId IS NOT NULL 

	--Getting the old config product save cart addon data for old line items if present
	INSERT INTO #OldGroupSavecartLineitemDetails 
	SELECT b.OmsSavedCartId,b.OmsSavedCartLineItemId,b.ParentOmsSavedCartLineItemId , b.SKU  ,b.OrderLineItemRelationshipTypeID  
	FROM ZnodeOmsSavedCartLineItem b with (nolock)
	INNER JOIN #OldGroupSavecartLineitemDetails c ON (c.OmsSavedCartLineItemId  = b.ParentOmsSavedCartLineItemId AND c.OmsSavedCartId = b.OmsSavedCartId)
	WHERE EXISTS (SELECT TOP 1 1 FROM @SaveCartLineItemType  TY WHERE TY.OmsSavedCartId = b.OmsSavedCartId AND ISNULL(b.SKU,'') = ISNULL(TY.AddOnValueIds,'') )
	AND  b.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon

	------Merge Addon for same product
	IF EXISTS(SELECT * FROM @SaveCartLineItemType WHERE ISNULL(AddOnValueIds,'') <> '' )
	BEGIN

		INSERT INTO #OldValueForAddon 
		SELECT b.OmsSavedCartId,b.OmsSavedCartLineItemId,b.ParentOmsSavedCartLineItemId , b.SKU  ,b.OrderLineItemRelationshipTypeID  
		FROM ZnodeOmsSavedCartLineItem b with (nolock)
		INNER JOIN #OldValueForAddon c ON (c.OmsSavedCartLineItemId  = b.ParentOmsSavedCartLineItemId AND c.OmsSavedCartId = b.OmsSavedCartId)
		WHERE EXISTS (SELECT TOP 1 1 FROM @SaveCartLineItemType  TY WHERE TY.OmsSavedCartId = b.OmsSavedCartId )--AND ISNULL(b.SKU,'') = ISNULL(TY.AddOnValueIds,'') )
		AND  b.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon

		SELECT distinct SKU, STUFF(
									( SELECT  ', ' + SKU FROM    
										( SELECT DISTINCT SKU FROM     #OldValueForAddon b 
											where a.OmsSavedCartLineItemId=b.ParentOmsSavedCartLineItemId and OrderLineItemRelationshipTypeID = 1 ) x 
											FOR XML PATH('')
									), 1, 2, ''
									) AddOns
		INTO #AddOnsExists
		FROM #OldValueForAddon a where a.ParentOmsSavedCartLineItemId is not null and OrderLineItemRelationshipTypeID<>1

		SELECT distinct a.GroupProductIds SKU, STUFF(
										( SELECT  ', ' + x.AddOnValueIds FROM    
										( SELECT DISTINCT b.AddOnValueIds FROM @SaveCartLineItemType b
											where a.SKU=b.SKU ) x
											FOR XML PATH('')
										), 1, 2, ''
									) AddOns
		INTO #AddOnAdded
		FROM @SaveCartLineItemType a

		if not exists(select * FROM #AddOnsExists a INNER JOIN #AddOnAdded b on a.SKU = b.SKU and a.AddOns = b.AddOns )
		begin
			DELETE FROM #OldGroupSavecartLineitemDetails
		end

	END

	--If addon present in new and old save cart data and not matches the addon data (old and new for merge) then removing the old save cart data FROM #OldSavecartLineitemDetails
	IF NOT EXISTS (SELECT TOP 1 1  FROM @SaveCartLineItemType ty WHERE EXISTS (SELECT TOP 1 1 FROM 	#OldGroupSavecartLineitemDetails a WHERE	
	ISNULL(TY.AddOnValueIds,'') = a.SKU AND  a.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon ))
	AND EXISTS (SELECT TOP 1 1 FROM @SaveCartLineItemType WHERE ISNULL(AddOnValueIds,'')  <> '' )
	BEGIN 
		
		DELETE FROM #OldGroupSavecartLineitemDetails 
	END 
	ELSE 
	BEGIN 
		
		 IF EXISTS (SELECT TOP 1 1 FROM @SaveCartLineItemType WHERE ISNULL(AddOnValueIds,'')  <> '' )
		 BEGIN 
		 
			 DECLARE @parenTofAddon  TABLE( ParentOmsSavedCartLineItemId INT  )  
			 INSERT INTO  @parenTofAddon 
			 SELECT  ParentOmsSavedCartLineItemId FROM #OldGroupSavecartLineitemDetails a
			 WHERE a.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon  
			 AND (SELECT COUNT (DISTINCT SKU ) FROM  ZnodeOmsSavedCartLineItem  t with (nolock) WHERE t.ParentOmsSavedCartLineItemId = a.ParentOmsSavedCartLineItemId AND   t.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon ) = (SELECT COUNT (DISTINCT SKU ) FROM  #NewGroupSavecartLineitemDetails  WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon  )
		  
			 DELETE FROM #OldGroupSavecartLineitemDetails WHERE OmsSavedCartLineItemId NOT IN (SELECT ParentOmsSavedCartLineItemId FROM  @parenTofAddon)   
						AND ParentOmsSavedCartLineItemId IS NOT NULL  
						AND OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdAddon

			 DELETE FROM #OldGroupSavecartLineitemDetails WHERE OmsSavedCartLineItemId NOT IN (SELECT ISNULL(m.ParentOmsSavedCartLineItemId,0) FROM #OldGroupSavecartLineitemDetails m)
			 AND ParentOmsSavedCartLineItemId IS  NULL  
		 

		  IF (SELECT COUNT (DISTINCT SKU ) FROM  #OldGroupSavecartLineitemDetails  WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon ) <> (SELECT COUNT (DISTINCT SKU ) FROM  #NewGroupSavecartLineitemDetails  WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon  )
		  BEGIN 
		    DELETE FROM #OldGroupSavecartLineitemDetails
		  END 
		 IF (SELECT COUNT (DISTINCT SKU ) FROM  ZnodeOmsSavedCartLineItem with (nolock)  WHERE ParentOmsSavedCartLineItemId IN (SELECT ParentOmsSavedCartLineItemId FROM @parenTofAddon)AND   OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon ) <> (SELECT COUNT (DISTINCT SKU ) FROM  #NewGroupSavecartLineitemDetails  WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon  )
		  BEGIN 
		    DELETE FROM #OldGroupSavecartLineitemDetails
		  END 

		 END 
		 ELSE IF (SELECT COUNT (OmsSavedCartLineItemId) FROM #OldGroupSavecartLineitemDetails WHERE ParentOmsSavedCartLineItemId IS NULL ) > 1 
		 BEGIN 
		 -- SELECT 3
		    DECLARE @TBL_deleteParentOmsSavedCartLineItemId TABLE (OmsSavedCartLineItemId INT )
			INSERT INTO @TBL_deleteParentOmsSavedCartLineItemId 
			SELECT ParentOmsSavedCartLineItemId
			FROM ZnodeOmsSavedCartLineItem a with (nolock)
			WHERE ParentOmsSavedCartLineItemId IN (SELECT OmsSavedCartLineItemId FROM #OldGroupSavecartLineitemDetails WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdGroup  )
			AND OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon 

			DELETE FROM #OldGroupSavecartLineitemDetails WHERE OmsSavedCartLineItemId IN (SELECT OmsSavedCartLineItemId FROM @TBL_deleteParentOmsSavedCartLineItemId)
			OR ParentOmsSavedCartLineItemId IN (SELECT OmsSavedCartLineItemId FROM @TBL_deleteParentOmsSavedCartLineItemId)
		    
			 DELETE FROM #OldGroupSavecartLineitemDetails WHERE OmsSavedCartLineItemId NOT IN (SELECT ISNULL(m.ParentOmsSavedCartLineItemId,0) FROM #OldGroupSavecartLineitemDetails m)
		     AND ParentOmsSavedCartLineItemId IS  NULL  

		 END
		 ELSE IF (SELECT COUNT (DISTINCT SKU ) FROM  #OldGroupSavecartLineitemDetails  WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon ) <> (SELECT COUNT (DISTINCT SKU ) FROM  #NewGroupSavecartLineitemDetails  WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon  )
		  BEGIN 
		    DELETE FROM #OldGroupSavecartLineitemDetails
		  END 
		   ELSE IF  EXISTS (SELECT TOP 1 1 FROM ZnodeOmsSavedCartLineItem Wt WHERE EXISTS (SELECT TOP 1 1 FROM #OldGroupSavecartLineitemDetails ty WHERE ty.OmsSavedCartId = wt.OmsSavedCartId AND ty.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdGroup AND wt.ParentOmsSavedCartLineItemId= ty.OmsSavedCartLineItemId  ) AND wt.OrderLineItemRelationshipTypeId = @OrderLineItemRelationshipTypeIdAddon)
		   AND EXISTS (SELECT TOP 1 1 FROM @SaveCartLineItemType WHERE ISNULL(AddOnValueIds,'')  = '' )
			BEGIN 
			   DELETE FROM #OldGroupSavecartLineitemDetails
			END  
	END 

	DECLARE @TBL_Personaloldvalues TABLE (OmsSavedCartLineItemId INT , PersonalizeCode NVARCHAr(max), PersonalizeValue NVARCHAr(max))
	--Getting the personalise data for old save cart if present	
	INSERT INTO @TBL_Personaloldvalues
	SELECT OmsSavedCartLineItemId , PersonalizeCode, PersonalizeValue
	FROM ZnodeOmsPersonalizeCartItem  a 
	WHERE EXISTS (SELECT TOP 1 1 FROM #OldGroupSavecartLineitemDetails TY WHERE TY.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId)
	AND EXISTS (SELECT TOP 1 1 FROM @TBL_Personalise TU WHERE TU.PersonalizeCode = a.PersonalizeCode AND TU.PersonalizeValue = a.PersonalizeValue)
		
	IF  NOT EXISTS (SELECT TOP 1 1 FROM @TBL_Personaloldvalues)
		AND EXISTS (SELECT TOP 1 1 FROM @TBL_Personalise )
	BEGIN 
		DELETE FROM #OldGroupSavecartLineitemDetails
	END 
	ELSE 
	BEGIN 
		IF EXISTS (SELECT TOP 1 1 FROM @TBL_Personaloldvalues)
		AND (SELECT COUNT (DISTINCT OmsSavedCartLineItemId ) FROM #OldGroupSavecartLineitemDetails WHERE ParentOmsSavedCartLineItemId IS nULL ) > 1 
		BEGIN 
		   
		DELETE FROM #OldGroupSavecartLineitemDetails WHERE OmsSavedCartLineItemId IN (
		SELECT OmsSavedCartLineItemId FROM #OldGroupSavecartLineitemDetails WHERE OmsSavedCartLineItemId NOT IN (SELECT OmsSavedCartLineItemId FROM @TBL_Personaloldvalues )
		AND ParentOmsSavedCartLineItemId NOT IN (SELECT OmsSavedCartLineItemId FROM @TBL_Personaloldvalues ) ) 
		OR OmsSavedCartLineItemId IN ( SELECT ParentOmsSavedCartLineItemId FROM #OldGroupSavecartLineitemDetails WHERE OmsSavedCartLineItemId NOT IN (SELECT OmsSavedCartLineItemId FROM @TBL_Personaloldvalues )
		AND ParentOmsSavedCartLineItemId NOT IN (SELECT OmsSavedCartLineItemId FROM @TBL_Personaloldvalues ))
		
		END 
		ELSE IF NOT EXISTS (SELECT TOP 1 1 FROM @TBL_Personaloldvalues)
		AND (SELECT COUNT (DISTINCT OmsSavedCartLineItemId ) FROM #OldGroupSavecartLineitemDetails WHERE ParentOmsSavedCartLineItemId IS nULL ) > 1 
		AND (SELECT COUNT (DISTINCT OmsSavedCartLineItemId ) FROM #OldGroupSavecartLineitemDetails WHERE ParentOmsSavedCartLineItemId IS nULL ) <>
			(SELECT COUNT (DISTINCT OmsSavedCartLineItemId ) FROM ZnodeOmsSavedCartLineItem WHERE ParentOmsSavedCartLineItemId IS nULL and OmsSavedCartLineItemId in (select OmsSavedCartLineItemId FROM #OldGroupSavecartLineitemDetails)  )
		BEGIN 
		 
		DELETE n FROM #OldGroupSavecartLineitemDetails n WHERE OmsSavedCartLineItemId  IN (SELECT OmsSavedCartLineItemId FROM ZnodeOmsPersonalizeCartItem WHERE n.OmsSavedCartLineItemId = ZnodeOmsPersonalizeCartItem.OmsSavedCartLineItemId  )
		OR ParentOmsSavedCartLineItemId  IN (SELECT OmsSavedCartLineItemId FROM ZnodeOmsPersonalizeCartItem   )
		
		END 
		ELSE IF NOT EXISTS (SELECT TOP 1 1  FROM @TBL_Personalise)
			AND EXISTS (SELECT TOP 1 1 FROM ZnodeOmsPersonalizeCartItem m WHERE EXISTS (SELECT Top 1 1 FROM #OldGroupSavecartLineitemDetails YU WHERE YU.OmsSavedCartLineItemId = m.OmsSavedCartLineItemId )) 
			AND (SELECT COUNT (DISTINCT OmsSavedCartLineItemId ) FROM #OldGroupSavecartLineitemDetails WHERE ParentOmsSavedCartLineItemId IS nULL ) = 1
		BEGIN 
			DELETE FROM #OldGroupSavecartLineitemDetails WHERE NOT EXISTS (SELECT TOP 1 1  FROM @TBL_Personalise)
		END 
	END 
	
	IF EXISTS (SELECT TOP 1 1 FROM #OldGroupSavecartLineitemDetails )
	BEGIN
		----DELETE old value FROM table which having personalise data in ZnodeOmsPersonalizeCartItem but same SKU not having personalise value for new cart item
		;WITH cte AS
		(
			select distinct b.*
			FROM @SaveCartLineItemType a 
					INNER JOIN #OldGroupSavecartLineitemDetails b on ( a.GroupProductIds = b.SKU or a.SKU = b.sku)
					where isnull(cast(a.PersonalisedAttribute AS varchar(max)),'') = ''
		)
		,cte2 AS
		(
			select a.ParentOmsSavedCartLineItemId
			FROM #OldGroupSavecartLineitemDetails a
			INNER JOIN ZnodeOmsPersonalizeCartItem b on b.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId
		)
		DELETE a FROM #OldGroupSavecartLineitemDetails a
		INNER JOIN cte b on a.OmsSavedCartLineItemId = b.OmsSavedCartLineItemId
		INNER JOIN cte2 c on (a.OmsSavedCartLineItemId = c.ParentOmsSavedCartLineItemId or a.ParentOmsSavedCartLineItemId = c.ParentOmsSavedCartLineItemId)

		----DELETE old value FROM table which having personalise data in ZnodeOmsPersonalizeCartItem but same SKU having personalise value for new cart item
		;WITH cte AS
		(
			select distinct b.*, 
			a.PersonalizeCode
			,a.PersonalizeValue
			FROM @TBL_Personalise a 
			INNER JOIN #OldGroupSavecartLineitemDetails b on ( a.GroupProductIds = b.SKU or a.SKU = b.sku)
			where isnull(a.PersonalizeValue,'') <> ''
		)
		,cte2 AS
		(
			select a.ParentOmsSavedCartLineItemId, b.PersonalizeCode, b.PersonalizeValue
			FROM #OldGroupSavecartLineitemDetails a
			INNER JOIN ZnodeOmsPersonalizeCartItem b on b.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId
			WHERE NOT EXISTS(SELECT * FROM cte c where b.OmsSavedCartLineItemId = c.OmsSavedCartLineItemId and b.PersonalizeCode = c.PersonalizeCode 
								and b.PersonalizeValue = c.PersonalizeValue )
		)
		DELETE a FROM #OldGroupSavecartLineitemDetails a
		INNER JOIN cte b on a.OmsSavedCartLineItemId = b.OmsSavedCartLineItemId
		INNER JOIN cte2 c on (a.OmsSavedCartLineItemId = c.ParentOmsSavedCartLineItemId or a.ParentOmsSavedCartLineItemId = c.ParentOmsSavedCartLineItemId)

		;WITH cte AS
		(
			SELECT b.OmsSavedCartLineItemId ,b.ParentOmsSavedCartLineItemId , a.SKU
					,a.PersonalizeCode
			  		,a.PersonalizeValue
					,a.DesignId
					,a.ThumbnailURL
			FROM @TBL_Personalise a 
			INNER JOIN #OldGroupSavecartLineitemDetails b on a.SKU = b.SKU
			INNER JOIN ZnodeOmsPersonalizeCartItem c on b.OmsSavedCartLineItemId = c.OmsSavedCartLineItemId
			WHERE a.OmsSavedCartLineItemId = 0
		)
		DELETE b1
		FROM #OldGroupSavecartLineitemDetails b1 
		WHERE NOT EXISTS(SELECT * FROM cte c where (b1.OmsSavedCartLineItemId = c.ParentOmsSavedCartLineItemId or b1.ParentOmsSavedCartLineItemId = c.ParentOmsSavedCartLineItemId))
		AND EXISTS(SELECT * FROM cte)

		--------If lineitem present in ZnodeOmsPersonalizeCartItem and personalize value is different for same line item then New lineItem will generate
		--------If lineitem present in ZnodeOmsPersonalizeCartItem and personalize value is same for same line item then Quantity will added
		;WITH cte AS
		(
			SELECT b.OmsSavedCartLineItemId ,a.ParentOmsSavedCartLineItemId , a.SKU AS SKU
					,a.PersonalizeCode
			  		,a.PersonalizeValue
					,a.DesignId
					,a.ThumbnailURL
			FROM @TBL_Personalise a 
			INNER JOIN #OldGroupSavecartLineitemDetails b on a.SKU = b.SKU
			INNER JOIN ZnodeOmsPersonalizeCartItem c on b.OmsSavedCartLineItemId = c.OmsSavedCartLineItemId
			WHERE a.OmsSavedCartLineItemId = 0
		)
		DELETE c1
		FROM cte a1		  
		INNER JOIN #OldGroupSavecartLineitemDetails b1 on a1.SKU = b1.SKU
		INNER JOIN #OldGroupSavecartLineitemDetails c1 on (b1.ParentOmsSavedCartLineItemId = c1.OmsSavedCartLineItemId OR b1.OmsSavedCartLineItemId = c1.OmsSavedCartLineItemId)
		WHERE NOT EXISTS(SELECT * FROM ZnodeOmsPersonalizeCartItem c where a1.OmsSavedCartLineItemId = c.OmsSavedCartLineItemId and a1.PersonalizeValue = c.PersonalizeValue)

		--Updating the cart if old and new cart data matches
		UPDATE a
		SET a.Quantity = a.Quantity+ty.Quantity,
		a.Custom1 = ty.Custom1,
		a.Custom2 = ty.Custom2,
		a.Custom3 = ty.Custom3,
		a.Custom4 = ty.Custom4,
		a.Custom5 = ty.Custom5,
		a.ModifiedDate = @GetDate  
		FROM ZnodeOmsSavedCartLineItem a
		INNER JOIN #OldGroupSavecartLineitemDetails b ON (a.OmsSavedCartLineItemId = b.OmsSavedCartLineItemId)
		INNER JOIN #NewGroupSavecartLineitemDetails ty ON (ty.SKU = b.SKU)
		WHERE a.OrderLineItemRelationshipTypeId <> @OrderLineItemRelationshipTypeIdAddon

		 UPDATE a
		 SET a.Quantity = a.Quantity+s.AddOnQuantity,
		 a.ModifiedDate = @GetDate
		 FROM ZnodeOmsSavedCartLineItem a
		 INNER JOIN #OldGroupSavecartLineitemDetails b ON (a.ParentOmsSavedCartLineItemId = b.OmsSavedCartLineItemId)
		 INNER JOIN @SaveCartLineItemType S on a.OmsSavedCartId = s.OmsSavedCartId and a.SKU = s.AddOnValueIds
		 WHERE a.OrderLineItemRelationshipTypeId = @OrderLineItemRelationshipTypeIdAddon


	END 
	--Inserting the new save cart data if old and new cart data not match
	ELSE 
	BEGIN 
	   
		SELECT RowId, Id ,Row_number()Over(ORDER BY RowId, Id,GenId) NewRowId , ParentOmsSavedCartLineItemId ,OmsSavedCartId,SKU,Quantity,OrderLineItemRelationshipTypeId  
		   ,CustomText,CartAddOnDetails,ROW_NUMBER()Over(ORDER BY NewId() ) Sequence ,AutoAddon  
		   ,OmsOrderId,Custom1,Custom2,Custom3 ,Custom4 ,Custom5,GroupId,ProductName ,min(Description)Description  ,ParentSKU  
		 INTO #InsertNewGroupSavecartLineitem   
		 FROM  #NewGroupSavecartLineitemDetails  
		 GROUP BY ParentOmsSavedCartLineItemId ,OmsSavedCartId,SKU,Quantity,OrderLineItemRelationshipTypeId  
		   ,CustomText,CartAddOnDetails ,AutoAddon  
		   ,OmsOrderId,Custom1,Custom2,Custom3 ,Custom4 ,Custom5,GroupId,ProductName ,RowId, Id ,GenId,ParentSKU   
		 ORDER BY RowId, Id   
 
		--Removing the line item having Quantity <=0 
		DELETE FROM #InsertNewGroupSavecartLineitem WHERE Quantity <=0  
  
		--Updating the rowid into new save cart line item as new line item is merged into existing save cart item
		 ;WITH VTTY AS   
		(  
			SELECT m.RowId OldRowId , TY1.RowId , TY1.SKU   
			   FROM #InsertNewGroupSavecartLineitem m  
			INNER JOIN  #InsertNewGroupSavecartLineitem TY1 ON TY1.SKU = m.ParentSKU   
			WHERE m.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon   
		)   
		UPDATE m1   
		SET m1.RowId = TYU.RowId  
		FROM #InsertNewGroupSavecartLineitem m1   
		INNER JOIN VTTY TYU ON (TYU.OldRowId = m1.RowId)  

		;WITH VTRET AS   
		(  
			SELECT RowId,id,Min(NewRowId)NewRowId ,SKU ,ParentSKU, OrderLineItemRelationshipTypeId 
			FROM #InsertNewGroupSavecartLineitem   
			GROUP BY RowId,id ,SKU ,ParentSKU  ,OrderLineItemRelationshipTypeId
			HAVING SKU = ParentSKU AND OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdAddon
		)   
		DELETE FROM #InsertNewGroupSavecartLineitem WHERE NewRowId  IN (SELECT NewRowId FROM VTRET)   
     
	   --Inserting the new cart line item if not merged in existing save cart line item
       INSERT INTO  ZnodeOmsSavedCartLineItem (ParentOmsSavedCartLineItemId ,OmsSavedCartId,SKU,Quantity,OrderLineItemRelationshipTypeId  
       ,CustomText,CartAddOnDetails,Sequence,CreatedBY,CreatedDate,ModifiedBy ,ModifiedDate,AutoAddon  
       ,OmsOrderId,Custom1,Custom2,Custom3 ,Custom4 ,Custom5,GroupId,ProductName ,Description)  
       OUTPUT INSERTED.SKU,INSERTED.OmsSavedCartLineItemId  INTO @OmsInsertedData 
	   SELECT NULL ,OmsSavedCartId,SKU,Quantity,OrderLineItemRelationshipTypeId  
       ,CustomText,CartAddOnDetails,ROW_NUMBER()Over(ORDER BY NewRowId)  sequence,@UserId,@GetDate,@UserId,@GetDate,AutoAddon  
       ,OmsOrderId,Custom1,Custom2,Custom3 ,Custom4 ,Custom5,GroupId,ProductName ,Description   
       FROM  #InsertNewGroupSavecartLineitem  TH  

		SELECT  MAX(a.OmsSavedCartLineItemId ) OmsSavedCartLineItemId 
		, b.RowId ,b.GroupId ,b.SKU ,b.ParentSKU 
		INTO  #Cte_newData
		FROM ZnodeOmsSavedCartLineItem a  with (nolock)
		INNER JOIN #InsertNewGroupSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.ParentSKU AND ISNULL(b.GroupId,'-') = ISNULL(a.GroupId,'-')  )  
		WHERE ISNULL(a.ParentOmsSavedCartLineItemId,0) =0  
		AND EXISTS (SELECT TOP 1 1  FROM @OmsInsertedData ui WHERE ui.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId )
			AND b.OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdAddon
		 GROUP BY b.RowId ,b.GroupId ,b.SKU	,b.ParentSKU,b.OrderLineItemRelationshipTypeID	  

		UPDATE a SET a.ParentOmsSavedCartLineItemId = (SELECT TOP 1 OmsSavedCartLineItemId FROM  #Cte_newData  r  
		WHERE  r.RowId = b.RowId AND ISNULL(r.GroupId,'-') = ISNULL(a.GroupId,'-')  ORDER BY b.RowId )   
		FROM ZnodeOmsSavedCartLineItem a  
		INNER JOIN #InsertNewGroupSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.SKU AND b.id =1  )   
		WHERE a.OrderLineItemRelationshipTypeId IS NOT NULL   
		AND b.OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdAddon  
		AND a.ParentOmsSavedCartLineItemId IS nULL   
		AND EXISTS (SELECT TOP 1 1  FROM @OmsInsertedData ui WHERE ui.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId )
  
		-----------------------------------------------------------------------------------------------------------------------------------

		SELECT  MIN(a.OmsSavedCartLineItemId ) OmsSavedCartLineItemId 
		, b.RowId ,b.GroupId ,b.SKU ,b.ParentSKU  
		INTO #Cte_newData1
		FROM ZnodeOmsSavedCartLineItem a with (nolock)
		INNER JOIN #InsertNewGroupSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.ParentSKU AND ISNULL(b.GroupId,'-') = ISNULL(a.GroupId,'-')  )  
		WHERE ISNULL(a.ParentOmsSavedCartLineItemId,0) =0  
		AND EXISTS (SELECT TOP 1 1  FROM @OmsInsertedData ui WHERE ui.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId )
			AND b.OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdAddon
		 GROUP BY b.RowId ,b.GroupId ,b.SKU	,b.ParentSKU,b.OrderLineItemRelationshipTypeID	  

		 UPDATE a SET a.ParentOmsSavedCartLineItemId = (SELECT TOP 1 OmsSavedCartLineItemId FROM  #Cte_newData1  r  
		WHERE  r.RowId = b.RowId AND ISNULL(r.GroupId,'-') = ISNULL(a.GroupId,'-')  ORDER BY b.RowId )   
		FROM ZnodeOmsSavedCartLineItem a  
		INNER JOIN #InsertNewGroupSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.SKU AND b.id =1  )   
		WHERE a.OrderLineItemRelationshipTypeId IS NOT NULL   
		AND b.OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdAddon   
		AND EXISTS (SELECT TOP 1 1  FROM @OmsInsertedData ui WHERE ui.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId )
		AND  a.sequence in (SELECT  MIN(ab.sequence) FROM ZnodeOmsSavedCartLineItem ab where a.OmsSavedCartId = ab.OmsSavedCartId and 
		 a.SKU = ab.sku and ab.OrderLineItemRelationshipTypeId is not null  ) 

		----------------------------------------------------------------------------------------------------------------------------

		SELECT a.OmsSavedCartLineItemId , b.RowId  ,b.SKU ,b.ParentSKU  ,Row_number()Over(ORDER BY c.OmsSavedCartLineItemId )RowIdNo
		INTO #Cte_newAddon
		FROM ZnodeOmsSavedCartLineItem a with (nolock) 
		INNER JOIN #InsertNewGroupSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.ParentSKU AND ( b.Id = 1  ))  
		INNER JOIN ZnodeOmsSavedCartLineItem c with (nolock) on b.sku = c.sku and b.OmsSavedCartId=c.OmsSavedCartId and b.Id = 1
		WHERE ( ISNULL(a.ParentOmsSavedCartLineItemId,0) <> 0   )
		AND b.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon  
		AND EXISTS (SELECT TOP 1 1  FROM @OmsInsertedData ui WHERE ui.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId ) and c.ParentOmsSavedCartLineItemId is null

	   ;WITH table_update AS 
	   (
			 SELECT * , ROW_NUMBER()Over(ORDER BY OmsSavedCartLineItemId  ) RowIdNo
			 FROM ZnodeOmsSavedCartLineItem a with (nolock)
			 WHERE a.OrderLineItemRelationshipTypeId IS NOT NULL   
			 AND a.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon  
			 AND a.ParentOmsSavedCartLineItemId IS NULL  
			 AND EXISTS (SELECT TOP 1 1  FROM  #InsertNewGroupSavecartLineitem ty WHERE ty.OmsSavedCartId = a.OmsSavedCartId )
			 AND EXISTS (SELECT TOP 1 1  FROM @OmsInsertedData ui WHERE ui.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId )
	   )
		UPDATE a SET  
			a.ParentOmsSavedCartLineItemId = (SELECT TOP 1 max(OmsSavedCartLineItemId) 
		FROM #Cte_newAddon  r  
		WHERE  r.ParentSKU = b.ParentSKU AND a.SKU = r.SKU AND a.RowIdNo = r.RowIdNo  GROUP BY r.ParentSKU, r.SKU  )   
		FROM table_update a  
		INNER JOIN #InsertNewGroupSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.SKU AND b.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon AND  b.id =1 )   
		WHERE (SELECT TOP 1 max(OmsSavedCartLineItemId)  FROM #Cte_newAddon  r  
			WHERE  r.ParentSKU = b.ParentSKU AND a.SKU = r.SKU AND a.RowIdNo = r.RowIdNo  GROUP BY r.ParentSKU, r.SKU  )    IS nOT NULL 

		;WITH Cte_Th AS   
		(             
			  SELECT RowId    
			 FROM #InsertNewGroupSavecartLineitem a   
			 GROUP BY RowId   
			 HAVING COUNT(NewRowId) <= 1   
		  )   
		UPDATE a SET a.Quantity =  NULL,
			a.ModifiedDate = @GetDate   
		FROM ZnodeOmsSavedCartLineItem a  
		INNER JOIN #InsertNewGroupSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.SKU AND b.id =0)   
		WHERE NOT EXISTS (SELECT TOP 1 1  FROM Cte_Th TY WHERE TY.RowId = b.RowId )  
		 AND a.OrderLineItemRelationshipTypeId IS NULL   
  
		UPDATE  ZnodeOmsSavedCartLineItem   
		SET GROUPID = NULL   
		WHERE  EXISTS (SELECT TOP 1 1  FROM #InsertNewGroupSavecartLineitem RT WHERE RT.OmsSavedCartId = ZnodeOmsSavedCartLineItem.OmsSavedCartId )  
		AND OrderLineItemRelationshipTypeId IS NOT NULL     
       
	   ;WITH Cte_UpdateSequence AS   
		 (  
		   SELECT OmsSavedCartLineItemId ,Row_Number()Over(ORDER BY OmsSavedCartLineItemId) RowId , Sequence   
		   FROM ZnodeOmsSavedCartLineItem with (nolock)  
		   WHERE EXISTS (SELECT TOP 1 1 FROM #InsertNewGroupSavecartLineitem TH WHERE TH.OmsSavedCartId = ZnodeOmsSavedCartLineItem.OmsSavedCartId )  
		 )   
		UPDATE Cte_UpdateSequence  
		SET  Sequence = RowId  
			
		update a set a.OmsSavedCartLineItemId = b.OmsSavedCartLineItemId
		FROM @TBL_Personalise a
		INNER JOIN @OmsInsertedData b on a.SKU = b.SKU
	
		DELETE FROM ZnodeOmsPersonalizeCartItem	WHERE EXISTS (SELECT TOP 1 1 FROM @TBL_Personalise yu WHERE yu.OmsSavedCartLineItemId = ZnodeOmsPersonalizeCartItem.OmsSavedCartLineItemId )
			
		----Inserting saved cart item personalise value FROM given line item
		MERGE INTO ZnodeOmsPersonalizeCartItem TARGET 
		USING @TBL_Personalise SOURCE
			   ON (TARGET.OmsSavedCartLineItemId = SOURCE.OmsSavedCartLineItemId ) 
			   WHEN NOT MATCHED THEN 
				INSERT  ( OmsSavedCartLineItemId,  CreatedBy, CreatedDate, ModifiedBy, ModifiedDate
								,PersonalizeCode, PersonalizeValue,DesignId	,ThumbnailURL )
				VALUES (  SOURCE.OmsSavedCartLineItemId,  @userId, @getdate, @userId, @getdate
								,SOURCE.PersonalizeCode, SOURCE.PersonalizeValue,SOURCE.DesignId	,SOURCE.ThumbnailURL ) ;
  
		
	END 

END TRY
BEGIN CATCH 
  SELECT ERROR_MESSAGE()
END CATCH 
END
GO
--------------------------------------------
IF EXISTS(SELECT * FROM SYS.PROCEDURES WHERE NAME = 'Znode_InsertUpdateSaveCartLineItemQuantityWrapper')
	DROP PROC Znode_InsertUpdateSaveCartLineItemQuantityWrapper
GO
CREATE PROCEDURE [dbo].[Znode_InsertUpdateSaveCartLineItemQuantityWrapper]
(
	@CartLineItemXML xml, 
	@UserId int,
	@PortalId Int,
	@OmsCookieMappingId INT = 0 
)
AS 
   /* 
    Summary: THis Procedure is USed to save and edit the saved cart line item      
    Unit Testing 
	begin tran  
    Exec Znode_InsertUpdateSaveCartLineItem @CartLineItemXML= '<ArrayOfSavedCartLineItemModel>
  <SavedCartLineItemModel>
    <OmsSavedCartLineItemId>0</OmsSavedCartLineItemId>
    <ParentOmsSavedCartLineItemId>0</ParentOmsSavedCartLineItemId>
    <OmsSavedCartId>1259</OmsSavedCartId>
    <SKU>BlueGreenYellow</SKU>
    <Quantity>1.000000</Quantity>
    <OrderLineItemRelationshipTypeId>0</OrderLineItemRelationshipTypeId>
    <Sequence>1</Sequence>
    <AddonProducts>YELLOW</AddonProducts>
    <BundleProducts />
    <ConfigurableProducts>GREEN</ConfigurableProducts>
  </SavedCartLineItemModel>
  <SavedCartLineItemModel>
    <OmsSavedCartLineItemId>0</OmsSavedCartLineItemId>
    <ParentOmsSavedCartLineItemId>0</ParentOmsSavedCartLineItemId>
    <OmsSavedCartId>1259</OmsSavedCartId>
    <SKU>ap1534</SKU>
    <Quantity>1.0</Quantity>
    <OrderLineItemRelationshipTypeId>0</OrderLineItemRelationshipTypeId>
    <Sequence>2</Sequence>
    <AddonProducts >PINK</AddonProducts>
    <BundleProducts />
    <ConfigurableProducts />
    <PersonaliseValuesList>Address~Hello</PersonaliseValuesList>
  </SavedCartLineItemModel>
</ArrayOfSavedCartLineItemModel>' , @UserId=1 ,@Status=0
	rollback tran
*/
BEGIN
BEGIN TRAN InsertUpdateSaveCartLineItem;
BEGIN TRY
SET NOCOUNT ON;
	--Declared the variables
	DECLARE @GetDate datetime= dbo.Fn_GetDate();
	DECLARE @AddOnQuantity numeric(28, 6)= 0;
	DECLARE @IsAddProduct   BIT = 0 
	DECLARE @OmsSavedCartLineItemId INT = 0
	
	DECLARE @TBL_SavecartLineitems TABLE
	( 
		RowId int , OmsSavedCartLineItemId int, ParentOmsSavedCartLineItemId int, OmsSavedCartId int, SKU nvarchar(600), Quantity numeric(28, 6), OrderLineItemRelationshipTypeID int, CustomText nvarchar(max), 
		CartAddOnDetails nvarchar(max), Sequence int, AddOnValueIds varchar(max), BundleProductIds varchar(max), ConfigurableProductIds varchar(max), GroupProductIds varchar(max), PersonalisedAttribute XML, 
		AutoAddon varchar(max), OmsOrderId int, ItemDetails nvarchar(max),
		Custom1	nvarchar(max),Custom2 nvarchar(max),Custom3 nvarchar(max),Custom4
		nvarchar(max),Custom5 nvarchar(max),GroupId NVARCHAR(max) ,ProductName Nvarchar(1000) , Description NVARCHAR(max),AddOnQuantity NVARCHAR(max)
	);

	--Set the OrderLineItemRelationshipTypeId for Addons product
	DECLARE @OrderLineItemRelationshipTypeIdAddon int =
	(
		SELECT TOP 1 OrderLineItemRelationshipTypeId
		FROM ZnodeOmsOrderLineItemRelationshipType
		WHERE [Name] = 'AddOns'
	);
	--Set the OrderLineItemRelationshipTypeId for simple product
	DECLARE @OrderLineItemRelationshipTypeIdSimple int =
	(
		SELECT TOP 1 OrderLineItemRelationshipTypeId
		FROM ZnodeOmsOrderLineItemRelationshipType
		WHERE [Name] = 'Simple'
	);
	--Set the OrderLineItemRelationshipTypeId for group product
	DECLARE @OrderLineItemRelationshipTypeIdGroup int=
	(
		SELECT TOP 1 OrderLineItemRelationshipTypeId
		FROM ZnodeOmsOrderLineItemRelationshipType
		WHERE [Name] = 'Group'
	);
	--Set the OrderLineItemRelationshipTypeId for Configurable product
	DECLARE @OrderLineItemRelationshipTypeIdConfigurable int=
	(
		SELECT TOP 1 OrderLineItemRelationshipTypeId
		FROM ZnodeOmsOrderLineItemRelationshipType
		WHERE [Name] = 'Configurable'
	);
	--Set the OrderLineItemRelationshipTypeId for Bundles product	
	DECLARE @OrderLineItemRelationshipTypeIdBundle int=
	(
		SELECT TOP 1 OrderLineItemRelationshipTypeId
		FROM ZnodeOmsOrderLineItemRelationshipType
		WHERE [Name] = 'Bundles'
	);

	-----Getting OmsSaveCartId FROM @OmsCookieMappingId
	DECLARE @OmsSavedCartId int
	--Getting OmsCookieMappingId on basis of @UserId and portalid if @UserId > 0 (Not a guest User)
	IF isnull(@OmsCookieMappingId,0)=0 and isnull(@UserId,0) <> 0  
	Begin
		SET @OmsCookieMappingId = (SELECT top 1 OmsCookieMappingId FROM ZnodeOmsCookieMapping with (nolock) where isnull(UserId,0) = @UserID AND isnull(PortalId,0) = @PortalId)
	END

	IF Not Exists(SELECT top 1 OmsCookieMappingId FROM ZnodeOmsCookieMapping with (nolock) Where OmsCookieMappingId = @OmsCookieMappingId)
	Begin
		INSERT INTO ZnodeOmsCookieMapping (UserId,PortalId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		SELECT case when @UserId = 0 then null else @UserId end ,@PortalId,@UserId,@GetDate,@UserId,@GetDate

		SET @OmsCookieMappingId = @@IDENTITY
	End
	----To get the oms savecartid on basis of @OmsCookieMappingId 
	SET @OmsSavedCartId = (SELECT top 1 OmsSavedCartId FROM ZnodeOmsSavedCart with (nolock) where OmsCookieMappingId = @OmsCookieMappingId)
	
	----If omssavecartid not present in ZnodeOmsSavedCart table then inserting new record to generated omssavecartid 
	IF isnull(@OmsSavedCartId,0) = 0
	BEGIN
		INSERT INTO ZnodeOmsSavedCart(OmsCookieMappingId,SalesTax,RecurringSalesTax,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		SELECT @OmsCookieMappingId,Null,Null,@UserId,@GetDate,@UserId,@GetDate

		SET @OmsSavedCartId = @@IDENTITY
	END

	--Fetching data FROM xml to table format and inserted into table @TBL_SavecartLineitems
	INSERT INTO @TBL_SavecartLineitems( RowId,OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, SKU, Quantity, OrderLineItemRelationshipTypeID, CustomText, CartAddOnDetails, Sequence, AddOnValueIds, BundleProductIds, ConfigurableProductIds, GroupProductIds, PersonalisedAttribute, AutoAddon, OmsOrderId, ItemDetails,
	Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName,Description,AddOnQuantity )
	SELECT DENSE_RANK()Over(Order BY Tbl.Col.value( 'SKU[1]', 'NVARCHAR(2000)' )) RowId ,Tbl.Col.value( 'OmsSavedCartLineItemId[1]', 'NVARCHAR(2000)' ) AS OmsSavedCartLineItemId, Tbl.Col.value( 'ParentOmsSavedCartLineItemId[1]', 'NVARCHAR(2000)' ) AS ParentOmsSavedCartLineItemId, @OmsSavedCartId AS OmsSavedCartId, Tbl.Col.value( 'SKU[1]', 'NVARCHAR(2000)' ) AS SKU, Tbl.Col.value( 'Quantity[1]', 'NVARCHAR(2000)' ) AS Quantity
	, Tbl.Col.value( 'OrderLineItemRelationshipTypeID[1]', 'NVARCHAR(2000)' ) AS OrderLineItemRelationshipTypeID, Tbl.Col.value( 'CustomText[1]', 'NVARCHAR(2000)' ) AS CustomText, Tbl.Col.value( 'CartAddOnDetails[1]', 'NVARCHAR(2000)' ) AS CartAddOnDetails, Tbl.Col.value( 'Sequence[1]', 'NVARCHAR(2000)' ) AS Sequence, Tbl.Col.value( 'AddonProducts[1]', 'NVARCHAR(2000)' ) AS AddOnValueIds, ISNULL(Tbl.Col.value( 'BundleProducts[1]', 'NVARCHAR(2000)' ),'') AS BundleProductIds, ISNULL(Tbl.Col.value( 'ConfigurableProducts[1]', 'NVARCHAR(2000)' ),'') AS ConfigurableProductIds, ISNULL(Tbl.Col.value( 'GroupProducts[1]', 'NVARCHAR(Max)' ),'') AS GroupProductIds, 
			Tbl.Col.query('(PersonaliseValuesDetail/node())') AS PersonaliseValuesDetail, Tbl.Col.value( 'AutoAddon[1]', 'NVARCHAR(Max)' ) AS AutoAddon, Tbl.Col.value( 'OmsOrderId[1]', 'NVARCHAR(Max)' ) AS OmsOrderId,
			Tbl.Col.value( 'ItemDetails[1]', 'NVARCHAR(Max)' ) AS ItemDetails,
			Tbl.Col.value( 'Custom1[1]', 'NVARCHAR(Max)' ) AS Custom1,
			Tbl.Col.value( 'Custom2[1]', 'NVARCHAR(Max)' ) AS Custom2,
			Tbl.Col.value( 'Custom3[1]', 'NVARCHAR(Max)' ) AS Custom3,
			Tbl.Col.value( 'Custom4[1]', 'NVARCHAR(Max)' ) AS Custom4,
			Tbl.Col.value( 'Custom5[1]', 'NVARCHAR(Max)' ) AS Custom5,
			Tbl.Col.value( 'GroupId[1]', 'NVARCHAR(Max)' ) AS GroupId,
			Tbl.Col.value( 'ProductName[1]', 'NVARCHAR(Max)' ) AS ProductName,
			Tbl.Col.value( 'Description[1]', 'NVARCHAR(Max)' ) AS Description, 
			Tbl.Col.value( 'AddOnQuantity[1]', 'NVARCHAR(2000)' ) AS AddOnQuantity
	FROM @CartLineItemXML.nodes( '//ArrayOfSavedCartLineItemModel/SavedCartLineItemModel' ) AS Tbl(Col);
			  
	IF OBJECT_ID('tempdb..#TBL_SavecartLineitems') is not null
		DROP TABLE #TBL_SavecartLineitems

	IF OBJECT_ID('tempdb..#OldValueForAddon') is not null
		DROP TABLE #OldValueForAddon

	SELECT * INTO #TBL_SavecartLineitems FROM @TBL_SavecartLineitems
			
	UPDATE ZnodeOmsSavedCart
	SET ModifiedDate = GETDATE()
	WHERE OmsSavedCartId = (SELECT TOP 1  OmsSavedCartId FROM @TBL_SavecartLineitems)

	UPDATE  @TBL_SavecartLineitems
	SET 	Description = ISNUll(Description,'') 

	--Save cart execution code for bundle product
	IF EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems WHERE BundleProductIds <> '' )
	BEGIN 				
		IF EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems WHERE BundleProductIds <> '' AND OmsSavedCartLineItemId <> 0  ) 
		BEGIN 
			SET @OmsSavedCartLineItemId  = (SELECT TOP 1 OmsSavedCartLineItemId FROM @TBL_SavecartLineitems WHERE BundleProductIds <> '' AND OmsSavedCartLineItemId <> 0 )

			UPDATE ZnodeOmsSavedCartLineItem 
			SET Quantity = (SELECT TOP 1 Quantity FROM @TBL_SavecartLineitems WHERE BundleProductIds <> '' AND OmsSavedCartLineItemId <> 0)
			, ModifiedDate = @GetDate
			WHERE ( OmsSavedCartLineItemId = @OmsSavedCartLineItemId  
			OR ParentOmsSavedCartLineItemId =  @OmsSavedCartLineItemId   ) 

			UPDATE ZnodeOmsSavedCartLineItem 
			SET Quantity = AddOnQuantity, ModifiedDate = @GetDate
			FROM ZnodeOmsSavedCartLineItem ZOSCLI with (nolock)
			INNER JOIN @TBL_SavecartLineitems SCLI ON ZOSCLI.ParentOmsSavedCartLineItemId = SCLI.OmsSavedCartLineItemId AND ZOSCLI.OmsSavedCartId = SCLI.OmsSavedCartId AND ZOSCLI.SKU = SCLI.AddOnValueIds
			WHERE ZOSCLI.OrderLineItemRelationshipTypeId = @OrderLineItemRelationshipTypeIdAddon
			AND SCLI.BundleProductIds <> ''

			--After update the existing cart with new save cart then deleting those records which are updated
			DELETE	FROM @TBL_SavecartLineitems WHERE BundleProductIds <> '' AND OmsSavedCartLineItemId <> 0
		END 

		--Getting bundle save cart line item entries into table to pass for bundle prodcut sp
		DECLARE @TBL_bundleProduct TT_SavecartLineitems 
		INSERT INTO @TBL_bundleProduct 
		SELECT *  
		FROM @TBL_SavecartLineitems 
		WHERE ISNULL(BundleProductIds,'') <> '' 
				
		EXEC Znode_InsertUpdateSaveCartLineItemBundle @TBL_bundleProduct,@userId,@OrderLineItemRelationshipTypeIdBundle,@OrderLineItemRelationshipTypeIdAddon
				 
		DELETE FROM  @TBL_SavecartLineitems WHERE ISNULL(BundleProductIds,'') <> '' 
		SET @OmsSavedCartLineItemId = 0 
	END 
	--Save cart execution code for configurable product
	IF EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems WHERE ConfigurableProductIds <> '' )
	BEGIN 				
		IF EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems WHERE ConfigurableProductIds <> '' AND OmsSavedCartLineItemId <> 0  ) 
		BEGIN 

			SET @OmsSavedCartLineItemId  = (SELECT TOP 1 OmsSavedCartLineItemId FROM @TBL_SavecartLineitems WHERE ConfigurableProductIds <> '' AND OmsSavedCartLineItemId <> 0 )
				 
			UPDATE ZnodeOmsSavedCartLineItem 
			SET Quantity = (SELECT TOP 1 Quantity FROM @TBL_SavecartLineitems WHERE ConfigurableProductIds <> '' AND OmsSavedCartLineItemId = @OmsSavedCartLineItemId )
			, ModifiedDate = @GetDate
			WHERE OmsSavedCartLineItemId = @OmsSavedCartLineItemId

			UPDATE ZnodeOmsSavedCartLineItem 
			SET Quantity = AddOnQuantity, ModifiedDate = @GetDate
			FROM ZnodeOmsSavedCartLineItem ZOSCLI with (nolock)
			INNER JOIN @TBL_SavecartLineitems SCLI ON ZOSCLI.ParentOmsSavedCartLineItemId = SCLI.OmsSavedCartLineItemId AND ZOSCLI.OmsSavedCartId = SCLI.OmsSavedCartId AND ZOSCLI.SKU = SCLI.AddOnValueIds
			WHERE ZOSCLI.OrderLineItemRelationshipTypeId = @OrderLineItemRelationshipTypeIdAddon
			AND SCLI.ConfigurableProductIds <> ''

			--After update the existing cart with new save cart then deleting those records which are updated
			DELETE	FROM @TBL_SavecartLineitems WHERE ConfigurableProductIds <> '' AND OmsSavedCartLineItemId <> 0
		END 
		--Getting bundle save cart line item entries into table to pass for configurable prodcut sp
		DECLARE @TBL_Configurable TT_SavecartLineitems 
		INSERT INTO @TBL_Configurable 
		SELECT *  
		FROM @TBL_SavecartLineitems 
		WHERE ISNULL(ConfigurableProductIds,'') <> '' 
		  
		EXEC Znode_InsertUpdateSaveCartLineItemConfigurable @TBL_Configurable,@userId,@OrderLineItemRelationshipTypeIdGroup,@OrderLineItemRelationshipTypeIdAddon
				  
		DELETE FROM @TBL_SavecartLineitems 
		WHERE ISNULL(ConfigurableProductIds,'') <> ''
		SET @OmsSavedCartLineItemId = 0  
	END 
	--Save cart execution code for group product
	IF EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems WHERE GroupProductIds <> '' )
	BEGIN 				
		IF EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems WHERE GroupProductIds <> '' AND OmsSavedCartLineItemId <> 0  ) 
		BEGIN 
			--Updating the existing save cart for group product
			SET @OmsSavedCartLineItemId  = (SELECT TOP 1 OmsSavedCartLineItemId FROM @TBL_SavecartLineitems WHERE GroupProductIds <> '' AND OmsSavedCartLineItemId <> 0 )
			UPDATE ZnodeOmsSavedCartLineItem 
			SET Quantity = (SELECT TOP 1 Quantity FROM @TBL_SavecartLineitems WHERE GroupProductIds <> '' AND OmsSavedCartLineItemId = @OmsSavedCartLineItemId )
			WHERE OmsSavedCartLineItemId = @OmsSavedCartLineItemId

			UPDATE ZnodeOmsSavedCartLineItem 
			SET Quantity = AddOnQuantity, ModifiedDate = @GetDate
			FROM ZnodeOmsSavedCartLineItem ZOSCLI with (nolock)
			INNER JOIN @TBL_SavecartLineitems SCLI ON ZOSCLI.ParentOmsSavedCartLineItemId = SCLI.OmsSavedCartLineItemId AND ZOSCLI.OmsSavedCartId = SCLI.OmsSavedCartId AND ZOSCLI.SKU = SCLI.AddOnValueIds
			WHERE ZOSCLI.OrderLineItemRelationshipTypeId = @OrderLineItemRelationshipTypeIdAddon
			AND SCLI.GroupProductIds <> ''

			--After update the existing cart with new save cart then deleting those records which are updated
			DELETE	FROM @TBL_SavecartLineitems WHERE GroupProductIds <> '' AND OmsSavedCartLineItemId <> 0
		END 
		--Getting bundle save cart line item entries into table to pass for group prodcut sp
		DECLARE @TBL_Group TT_SavecartLineitems 
		INSERT INTO @TBL_Group 
		SELECT *  
		FROM @TBL_SavecartLineitems 
		WHERE ISNULL(GroupProductIds,'') <> '' 
		
		EXEC Znode_InsertUpdateSaveCartLineItemGroup @TBL_Group,@userId,@OrderLineItemRelationshipTypeIdGroup,@OrderLineItemRelationshipTypeIdAddon
		
		--After update the existing cart with new save cart then deleting those records which are updated
		DELETE FROM @TBL_SavecartLineitems WHERE ISNULL(GroupProductIds,'') <> ''
		SET @OmsSavedCartLineItemId = 0  
	END 
	
	--This part is for updating the cart for existing line items for simple product
	IF EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems WHERE  OmsSavedCartLineItemId <> 0  ) 
	BEGIN 
				 
		SET @OmsSavedCartLineItemId  = (SELECT TOP 1 OmsSavedCartLineItemId FROM @TBL_SavecartLineitems WHERE  OmsSavedCartLineItemId <> 0 )
		UPDATE ZnodeOmsSavedCartLineItem 
		SET Quantity = (SELECT TOP 1 Quantity FROM @TBL_SavecartLineitems WHERE  OmsSavedCartLineItemId = @OmsSavedCartLineItemId )
		, ModifiedDate = @GetDate
		WHERE OmsSavedCartLineItemId = @OmsSavedCartLineItemId

		UPDATE ZnodeOmsSavedCartLineItem 
		SET Quantity = AddOnQuantity, ModifiedDate = @GetDate
		FROM ZnodeOmsSavedCartLineItem ZOSCLI with (nolock)
		INNER JOIN @TBL_SavecartLineitems SCLI ON ZOSCLI.ParentOmsSavedCartLineItemId = @OmsSavedCartLineItemId AND ZOSCLI.OmsSavedCartId = SCLI.OmsSavedCartId AND ZOSCLI.SKU = SCLI.AddOnValueIds
		WHERE ZOSCLI.OrderLineItemRelationshipTypeId = @OrderLineItemRelationshipTypeIdAddon
					
		DELETE	FROM @TBL_SavecartLineitems WHERE OmsSavedCartLineItemId <> 0
	END 
	--Save cart execution code for Simple product
	DECLARE @OmsInsertedData TABLE (OmsSavedCartLineItemId INT )
	--Inserting the personalise data into variable table @TBL_Personalise for inserting the personalise data for new products
	DECLARE @TBL_Personalise TABLE (OmsSavedCartLineItemId INT, ParentOmsSavedCartLineItemId int,SKU Varchar(600) ,PersonalizeCode NVARCHAr(max),PersonalizeValue NVARCHAr(max),DesignId NVARCHAr(max), ThumbnailURL NVARCHAr(max))
	INSERT INTO @TBL_Personalise
	SELECT DISTINCT Null, a.ParentOmsSavedCartLineItemId,a.SKU
			,Tbl.Col.value( 'PersonalizeCode[1]', 'NVARCHAR(Max)' ) AS PersonalizeCode
			,Tbl.Col.value( 'PersonalizeValue[1]', 'NVARCHAR(Max)' ) AS PersonalizeValue
			,Tbl.Col.value( 'DesignId[1]', 'NVARCHAR(Max)' ) AS DesignId
			,Tbl.Col.value( 'ThumbnailURL[1]', 'NVARCHAR(Max)' ) AS ThumbnailURL
	FROM @TBL_SavecartLineitems a 
	CROSS APPLY a.PersonalisedAttribute.nodes( '//PersonaliseValueModel' ) AS Tbl(Col) 
			  
	----To update saved cart item personalise value FROM given line item
	DECLARE @TBL_Personalise1 TABLE (OmsSavedCartLineItemId INT ,PersonalizeCode NVARCHAr(max),PersonalizeValue NVARCHAr(max),DesignId NVARCHAr(max), ThumbnailURL NVARCHAr(max))
	INSERT INTO @TBL_Personalise1
	SELECT DISTINCT a.OmsSavedCartLineItemId 
			,Tbl.Col.value( 'PersonalizeCode[1]', 'NVARCHAR(Max)' ) AS PersonalizeCode
			,Tbl.Col.value( 'PersonalizeValue[1]', 'NVARCHAR(Max)' ) AS PersonalizeValue
			,Tbl.Col.value( 'DesignId[1]', 'NVARCHAR(Max)' ) AS DesignId
			,Tbl.Col.value( 'ThumbnailURL[1]', 'NVARCHAR(Max)' ) AS ThumbnailURL
	FROM (SELECT TOP 1 OmsSavedCartLineItemId,PersonalisedAttribute Valuex FROM  #TBL_SavecartLineitems TRTR ) a 
	CROSS APPLY	a.Valuex.nodes( '//PersonaliseValueModel' ) AS Tbl(Col)  
		    
			
	CREATE TABLE #NewSavecartLineitemDetails 
	(
		GenId INT IDENTITY(1,1),RowId	INT	,OmsSavedCartLineItemId	INT	 ,ParentOmsSavedCartLineItemId	INT,OmsSavedCartId	INT
		,SKU	NVARCHAR(MAX) ,Quantity	NUMERIC(28,6)	,OrderLineItemRelationshipTypeID	INT	,CustomText	NVARCHAR(MAX)
		,CartAddOnDetails	NVARCHAR(MAX),Sequence	int	,AutoAddon	varchar(MAX)	,OmsOrderId	INT	,ItemDetails	NVARCHAR(MAX)
		,Custom1	NVARCHAR(MAX)  ,Custom2	NVARCHAR(MAX),Custom3	NVARCHAR(MAX),Custom4	NVARCHAR(MAX),Custom5	NVARCHAR(MAX)
		,GroupId	NVARCHAR(MAX) ,ProductName	NVARCHAR(MAX),Description	NVARCHAR(MAX),Id	INT,ParentSKU NVARCHAR(MAX)
	)
	
	--Getting new save cart data
	INSERT INTO #NewSavecartLineitemDetails
	SELECT  Min(RowId )RowId ,OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, SKU
		,Quantity, OrderLineItemRelationshipTypeID, CustomText, CartAddOnDetails, Sequence
		,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,  GroupId ,ProductName,min(Description)Description	,0 Id,NULL ParentSKU 
	FROM @TBL_SavecartLineitems a 
	GROUP BY  OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, SKU
		,Quantity, OrderLineItemRelationshipTypeID, CustomText, CartAddOnDetails, Sequence
		,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName
	
	--Getting new simple product save cart data
	INSERT INTO #NewSavecartLineitemDetails
	SELECT  Min(RowId )RowId ,OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, SKU
		,Quantity, @OrderLineItemRelationshipTypeIdSimple, CustomText, CartAddOnDetails, Sequence
		,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName,min(Description)Description	,1 Id,SKU ParentSKU 
	FROM @TBL_SavecartLineitems  a 
	WHERE ISNULL(BundleProductIds,'') =  '' 
	AND  ISNULL(GroupProductIds,'') = ''	AND ISNULL(	ConfigurableProductIds,'') = ''
		GROUP BY  OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, SKU
		,Quantity,  CustomText, CartAddOnDetails, Sequence
		,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName
			
	--Getting new Group,Bundle and Configurable products save cart data if addon is present for any line item
	INSERT INTO #NewSavecartLineitemDetails
	SELECT  Min(RowId )RowId ,OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, AddOnValueIds
		,AddOnQuantity, @OrderLineItemRelationshipTypeIdAddon, CustomText, CartAddOnDetails, Sequence
		,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName,min(Description)Description	,1 Id 
		,CASE WHEN ConfigurableProductIds <> ''  THEN ConfigurableProductIds
		WHEN  GroupProductIds <> '' THEN GroupProductIds 
		WHEN BundleProductIds <> '' THEN BundleProductIds 
			ELSE SKU END     ParentSKU 
	FROM @TBL_SavecartLineitems  a 
	WHERE AddOnValueIds <> ''
	GROUP BY  OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, AddOnValueIds
	,AddOnQuantity,  CustomText, CartAddOnDetails, Sequence ,ConfigurableProductIds,GroupProductIds,	BundleProductIds
	,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName,SKU
	
	CREATE TABLE #OldSavecartLineitemDetails (OmsSavedCartId INT ,OmsSavedCartLineItemId INT,ParentOmsSavedCartLineItemId INT , SKU  NVARCHAr(2000),OrderLineItemRelationshipTypeID INT  )
	--Getting the old save cart data if present for same SKU in the new save cart data for simple product	 
	INSERT INTO #OldSavecartLineitemDetails  
	SELECT  a.OmsSavedCartId,a.OmsSavedCartLineItemId,a.ParentOmsSavedCartLineItemId , a.SKU  ,a.OrderLineItemRelationshipTypeID 
	FROM ZnodeOmsSavedCartLineItem a with (nolock)  
	WHERE EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems  TY WHERE TY.OmsSavedCartId = a.OmsSavedCartId AND ISNULL(a.SKU,'') = ISNULL(TY.SKU,'')   )   
	AND a.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdSimple   

	--Getting the old save cart Parent data 
	INSERT INTO #OldSavecartLineitemDetails 
	SELECT DISTINCT b.OmsSavedCartId,b.OmsSavedCartLineItemId,b.ParentOmsSavedCartLineItemId , b.SKU  ,b.OrderLineItemRelationshipTypeID  
	FROM ZnodeOmsSavedCartLineItem b with (nolock)
	INNER JOIN #OldSavecartLineitemDetails c ON (c.ParentOmsSavedCartLineItemId  = b.OmsSavedCartLineItemId AND c.OmsSavedCartId = b.OmsSavedCartId)
	WHERE EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems  TY WHERE TY.OmsSavedCartId = b.OmsSavedCartId AND ISNULL(b.SKU,'') = ISNULL(TY.SKU,'') AND ISNULL(b.Groupid,'-') = ISNULL(TY.Groupid,'-')  )
	AND  b.OrderLineItemRelationshipTypeID IS NULL 
		 
	DELETE a FROM #OldSavecartLineitemDetails a WHERE NOT EXISTS (SELECT TOP 1 1  FROM #OldSavecartLineitemDetails b WHERE b.ParentOmsSavedCartLineItemId IS NULL AND b.OmsSavedCartLineItemId = a.ParentOmsSavedCartLineItemId)
	AND a.ParentOmsSavedCartLineItemId IS NOT NULL 
		
	------Merge Addon for same product
	SELECT * INTO #OldValueForAddon FROM #OldSavecartLineitemDetails
		
	--Getting the old save cart addon data for old line items if present
	INSERT INTO #OldSavecartLineitemDetails 
	SELECT b.OmsSavedCartId,b.OmsSavedCartLineItemId,b.ParentOmsSavedCartLineItemId , b.SKU  ,b.OrderLineItemRelationshipTypeID  
	FROM ZnodeOmsSavedCartLineItem b with (nolock)
	INNER JOIN #OldSavecartLineitemDetails c ON (c.OmsSavedCartLineItemId  = b.ParentOmsSavedCartLineItemId AND c.OmsSavedCartId = b.OmsSavedCartId)
	WHERE EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems  TY WHERE TY.OmsSavedCartId = b.OmsSavedCartId AND ISNULL(b.SKU,'') = ISNULL(TY.AddOnValueIds,'') )
	AND  b.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon

	------Merge Addon for same product
	IF EXISTS(SELECT * FROM @TBL_SavecartLineitems WHERE ISNULL(AddOnValueIds,'') <> '' )
	BEGIN

		INSERT INTO #OldValueForAddon 
		SELECT b.OmsSavedCartId,b.OmsSavedCartLineItemId,b.ParentOmsSavedCartLineItemId , b.SKU  ,b.OrderLineItemRelationshipTypeID  
		FROM ZnodeOmsSavedCartLineItem b with (nolock)
		INNER JOIN #OldValueForAddon c ON (c.OmsSavedCartLineItemId  = b.ParentOmsSavedCartLineItemId AND c.OmsSavedCartId = b.OmsSavedCartId)
		WHERE EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems  TY WHERE TY.OmsSavedCartId = b.OmsSavedCartId )--AND ISNULL(b.SKU,'') = ISNULL(TY.AddOnValueIds,'') )
		AND  b.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon

		SELECT distinct SKU, STUFF(
								( SELECT  ', ' + SKU FROM    
									( SELECT DISTINCT SKU FROM     #OldValueForAddon b 
										where a.OmsSavedCartLineItemId=b.ParentOmsSavedCartLineItemId and OrderLineItemRelationshipTypeID = 1 ) x 
										FOR XML PATH('')
								), 1, 2, ''
								) AddOns
		INTO #AddOnsExists
		FROM #OldValueForAddon a where a.ParentOmsSavedCartLineItemId is not null and OrderLineItemRelationshipTypeID<>1

		SELECT distinct a.SKU, STUFF(
									( SELECT  ', ' + x.AddOnValueIds FROM    
									( SELECT DISTINCT b.AddOnValueIds FROM @TBL_SavecartLineitems b
										where a.SKU=b.SKU ) x
										FOR XML PATH('')
									), 1, 2, ''
								) AddOns
		INTO #AddOnAdded
		FROM @TBL_SavecartLineitems a

		IF NOT EXISTS(SELECT * FROM #AddOnsExists a INNER JOIN #AddOnAdded b on a.SKU = b.SKU and a.AddOns = b.AddOns )
		BEGIN
			DELETE FROM #OldSavecartLineitemDetails
		END

	END

	--If addon present in new and old save cart data and not matches the addon data (old and new for merge) then removing the old save cart data FROM #OldSavecartLineitemDetails
	IF NOT EXISTS (SELECT TOP 1 1  FROM @TBL_SavecartLineitems ty WHERE EXISTS (SELECT TOP 1 1 FROM 	#OldSavecartLineitemDetails a WHERE	
	ISNULL(TY.AddOnValueIds,'') = a.SKU AND  a.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon ))
	AND EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems WHERE ISNULL(AddOnValueIds,'')  <> '' )
	BEGIN 
		
		DELETE FROM #OldSavecartLineitemDetails 
	END 
	ELSE 
	BEGIN 
	    
		IF EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems WHERE ISNULL(AddOnValueIds,'')  <> '' )
		BEGIN 
		 
			DECLARE @parenTofAddon  TABLE( ParentOmsSavedCartLineItemId INT  )  
			INSERT INTO  @parenTofAddon 
			SELECT  ParentOmsSavedCartLineItemId FROM #OldSavecartLineitemDetails WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon  

			DELETE FROM #OldSavecartLineitemDetails WHERE OmsSavedCartLineItemId NOT IN (SELECT ParentOmsSavedCartLineItemId FROM  @parenTofAddon)   
				AND ParentOmsSavedCartLineItemId IS NOT NULL  
				AND OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdAddon

			DELETE FROM #OldSavecartLineitemDetails WHERE OmsSavedCartLineItemId NOT IN (SELECT ISNULL(m.ParentOmsSavedCartLineItemId,0) FROM #OldSavecartLineitemDetails m)
			AND ParentOmsSavedCartLineItemId IS  NULL  
		 
		END 
		ELSE IF (SELECT COUNT (OmsSavedCartLineItemId) FROM #OldSavecartLineitemDetails WHERE ParentOmsSavedCartLineItemId IS NULL ) > 1 
		BEGIN 

			DECLARE @TBL_deleteParentOmsSavedCartLineItemId TABLE (OmsSavedCartLineItemId INT )
			INSERT INTO @TBL_deleteParentOmsSavedCartLineItemId 
			SELECT ParentOmsSavedCartLineItemId
			FROM ZnodeOmsSavedCartLineItem a with (nolock)
			WHERE ParentOmsSavedCartLineItemId IN (SELECT OmsSavedCartLineItemId FROM #OldSavecartLineitemDetails WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdSimple  )
			AND OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon 

			DELETE FROM #OldSavecartLineitemDetails WHERE OmsSavedCartLineItemId IN (SELECT OmsSavedCartLineItemId FROM @TBL_deleteParentOmsSavedCartLineItemId)
			OR ParentOmsSavedCartLineItemId IN (SELECT OmsSavedCartLineItemId FROM @TBL_deleteParentOmsSavedCartLineItemId)
		    
			DELETE FROM #OldSavecartLineitemDetails WHERE OmsSavedCartLineItemId NOT IN (SELECT ISNULL(m.ParentOmsSavedCartLineItemId,0) FROM #OldSavecartLineitemDetails m)
			AND ParentOmsSavedCartLineItemId IS  NULL  

		END
		ELSE IF  EXISTS (SELECT TOP 1 1 FROM ZnodeOmsSavedCartLineItem Wt WHERE EXISTS (SELECT TOP 1 1 FROM #OldSavecartLineitemDetails ty WHERE ty.OmsSavedCartId = wt.OmsSavedCartId AND ty.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdSimple AND wt.ParentOmsSavedCartLineItemId= ty.OmsSavedCartLineItemId  ) AND wt.OrderLineItemRelationshipTypeId = @OrderLineItemRelationshipTypeIdAddon)
		AND EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems WHERE ISNULL(AddOnValueIds,'')  = '' )
		BEGIN 

			DELETE FROM #OldSavecartLineitemDetails
		END 
	END 

	--Getting the personalise data for old save cart if present
	DECLARE @TBL_Personaloldvalues TABLE (OmsSavedCartLineItemId INT , PersonalizeCode NVARCHAr(max), PersonalizeValue NVARCHAr(max))
	INSERT INTO @TBL_Personaloldvalues
	SELECT OmsSavedCartLineItemId , PersonalizeCode, PersonalizeValue
	FROM ZnodeOmsPersonalizeCartItem  a 
	WHERE EXISTS (SELECT TOP 1 1 FROM #OldSavecartLineitemDetails TY WHERE TY.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId)
	AND EXISTS (SELECT TOP 1 1 FROM @TBL_Personalise TU WHERE TU.PersonalizeCode = a.PersonalizeCode AND TU.PersonalizeValue = a.PersonalizeValue)

	IF  NOT EXISTS (SELECT TOP 1 1 FROM @TBL_Personaloldvalues)
	AND EXISTS (SELECT TOP 1 1 FROM @TBL_Personalise )
	BEGIN 
		DELETE FROM #OldSavecartLineitemDetails
	END 
	ELSE 
	BEGIN 
		IF EXISTS (SELECT TOP 1 1 FROM @TBL_Personaloldvalues)
		AND (SELECT COUNT (DISTINCT OmsSavedCartLineItemId ) FROM #OldSavecartLineitemDetails WHERE ParentOmsSavedCartLineItemId IS nULL ) > 1 
		BEGIN 
		   
			DELETE FROM #OldSavecartLineitemDetails WHERE OmsSavedCartLineItemId IN (
			SELECT OmsSavedCartLineItemId FROM #OldSavecartLineitemDetails WHERE OmsSavedCartLineItemId NOT IN (SELECT OmsSavedCartLineItemId FROM @TBL_Personaloldvalues )
			AND ParentOmsSavedCartLineItemId NOT IN (SELECT OmsSavedCartLineItemId FROM @TBL_Personaloldvalues ) ) 
			OR OmsSavedCartLineItemId IN ( SELECT ParentOmsSavedCartLineItemId FROM #OldSavecartLineitemDetails WHERE OmsSavedCartLineItemId NOT IN (SELECT OmsSavedCartLineItemId FROM @TBL_Personaloldvalues )
			AND ParentOmsSavedCartLineItemId NOT IN (SELECT OmsSavedCartLineItemId FROM @TBL_Personaloldvalues ))
		      
		END 
		ELSE IF NOT EXISTS (SELECT TOP 1 1 FROM @TBL_Personaloldvalues)
		AND (SELECT COUNT (DISTINCT OmsSavedCartLineItemId ) FROM #OldSavecartLineitemDetails WHERE ParentOmsSavedCartLineItemId IS nULL ) > 1 
		BEGIN 

			DELETE n FROM #OldSavecartLineitemDetails n WHERE OmsSavedCartLineItemId  IN (SELECT OmsSavedCartLineItemId FROM ZnodeOmsPersonalizeCartItem WHERE n.OmsSavedCartLineItemId = ZnodeOmsPersonalizeCartItem.OmsSavedCartLineItemId  )
			OR ParentOmsSavedCartLineItemId  IN (SELECT OmsSavedCartLineItemId FROM ZnodeOmsPersonalizeCartItem   )
	   
		END 
		ELSE IF NOT EXISTS (SELECT TOP 1 1  FROM @TBL_Personalise)
		AND EXISTS (SELECT TOP 1 1 FROM ZnodeOmsPersonalizeCartItem m WHERE EXISTS (SELECT Top 1 1 FROM #OldSavecartLineitemDetails YU WHERE YU.OmsSavedCartLineItemId = m.OmsSavedCartLineItemId )) 
		AND (SELECT COUNT (DISTINCT OmsSavedCartLineItemId ) FROM #OldSavecartLineitemDetails WHERE ParentOmsSavedCartLineItemId IS nULL ) = 1
		BEGIN 
			DELETE FROM #OldSavecartLineitemDetails WHERE NOT EXISTS (SELECT TOP 1 1  FROM @TBL_Personalise)
		END 
	  
	END 

	--If already exists cart 
	IF EXISTS (SELECT TOP 1 1 FROM #OldSavecartLineitemDetails )
	BEGIN
		----DELETE old value FROM table which having personalise data in ZnodeOmsPersonalizeCartItem but same SKU not having personalise value for new cart item
		;WITH cte AS
		(
			SELECT distinct b.*
			FROM @TBL_SavecartLineitems a 
			INNER JOIN #OldSavecartLineitemDetails b on ( a.SKU = b.sku)
			where isnull(cast(a.PersonalisedAttribute AS varchar(max)),'') = ''
		)
		,cte2 AS
		(
			SELECT c.ParentOmsSavedCartLineItemId
			FROM #OldSavecartLineitemDetails a
			INNER JOIN ZnodeOmsSavedCartLineItem c on a.OmsSavedCartLineItemId = c.ParentOmsSavedCartLineItemId
			INNER JOIN ZnodeOmsPersonalizeCartItem b on b.OmsSavedCartLineItemId = c.OmsSavedCartLineItemId
		)
		DELETE a FROM #OldSavecartLineitemDetails a
		INNER JOIN cte b on a.OmsSavedCartLineItemId = b.OmsSavedCartLineItemId
		INNER JOIN cte2 c on (a.OmsSavedCartLineItemId = c.ParentOmsSavedCartLineItemId or a.ParentOmsSavedCartLineItemId = c.ParentOmsSavedCartLineItemId)

		----DELETE old value FROM table which having personalise data in ZnodeOmsPersonalizeCartItem but same SKU having personalise value for new cart item
		;WITH cte AS
		(
			SELECT distinct b.*, 
				a.PersonalizeCode
				,a.PersonalizeValue
			FROM @TBL_Personalise a 
			INNER JOIN #OldSavecartLineitemDetails b on ( a.SKU = b.sku)
			where a.PersonalizeValue <> ''
		)
		,cte2 AS
		(
			SELECT a.ParentOmsSavedCartLineItemId, b.PersonalizeCode, b.PersonalizeValue
			FROM #OldSavecartLineitemDetails a
			INNER JOIN ZnodeOmsPersonalizeCartItem b on b.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId
			WHERE NOT EXISTS(SELECT * FROM cte c where b.OmsSavedCartLineItemId = c.OmsSavedCartLineItemId and b.PersonalizeCode = c.PersonalizeCode 
						and b.PersonalizeValue = c.PersonalizeValue )
		)
		DELETE a FROM #OldSavecartLineitemDetails a
		INNER JOIN cte b on a.OmsSavedCartLineItemId = b.OmsSavedCartLineItemId
		INNER JOIN cte2 c on (a.OmsSavedCartLineItemId = c.ParentOmsSavedCartLineItemId or a.ParentOmsSavedCartLineItemId = c.ParentOmsSavedCartLineItemId)

		;WITH cte AS
		(
			SELECT b.OmsSavedCartLineItemId ,b.ParentOmsSavedCartLineItemId , a.SKU AS SKU
				,a.PersonalizeCode
				,a.PersonalizeValue
				,a.DesignId
				,a.ThumbnailURL
			FROM @TBL_Personalise a 
			INNER JOIN #OldSavecartLineitemDetails b on a.SKU = b.SKU
			INNER JOIN ZnodeOmsPersonalizeCartItem c on b.OmsSavedCartLineItemId = c.OmsSavedCartLineItemId
			WHERE a.OmsSavedCartLineItemId = 0
		)
		DELETE b1
		FROM #OldSavecartLineitemDetails b1 
		WHERE NOT EXISTS(SELECT * FROM cte c where (b1.OmsSavedCartLineItemId = c.ParentOmsSavedCartLineItemId or b1.ParentOmsSavedCartLineItemId = c.ParentOmsSavedCartLineItemId))
		AND EXISTS(SELECT * FROM cte)

		--------If lineitem present in ZnodeOmsPersonalizeCartItem and personalize value is different for same line item then New lineItem will generate
		--------If lineitem present in ZnodeOmsPersonalizeCartItem and personalize value is same for same line item then Quantity will added
	
		;WITH cte AS
		(
			SELECT b.OmsSavedCartLineItemId ,a.ParentOmsSavedCartLineItemId , a.SKU
						,d.PersonalizeCode
				,d.PersonalizeValue
				,d.DesignId
				,d.ThumbnailURL
				FROM @TBL_SavecartLineitems a 
				INNER JOIN #OldSavecartLineitemDetails b on a.SKU = b.SKU
				INNER JOIN @TBL_Personalise d on d.SKU = a.SKU
				INNER JOIN ZnodeOmsPersonalizeCartItem  c  with (nolock)  on b.OmsSavedCartLineItemId = c.OmsSavedCartLineItemId
				WHERE a.OmsSavedCartLineItemId = 0
		)
		DELETE b1
		FROM cte a1		  
		INNER JOIN #OldSavecartLineitemDetails b1 on a1.sku = b1.SKU
		WHERE NOT EXISTS(SELECT * FROM ZnodeOmsPersonalizeCartItem c where a1.OmsSavedCartLineItemId = c.OmsSavedCartLineItemId and a1.PersonalizeValue = c.PersonalizeValue)

		--Updating the cart if old and new cart data matches 
		UPDATE a
		SET a.Quantity = a.Quantity+ty.Quantity,
		a.Custom1 = ty.Custom1,
		a.Custom2 = ty.Custom2,
		a.Custom3 = ty.Custom3,
		a.Custom4 = ty.Custom4,
		a.Custom5 = ty.Custom5, 
		a.ModifiedDate = @GetDate
		FROM ZnodeOmsSavedCartLineItem a
		INNER JOIN #OldSavecartLineitemDetails b ON (a.OmsSavedCartLineItemId = b.OmsSavedCartLineItemId)
		INNER JOIN #NewSavecartLineitemDetails ty ON (ty.SKU = b.SKU)

	END 
	--Inserting the new save cart data if old and new cart data not match
	ELSE 
	BEGIN 
	    --Getting the new save cart data and generating row no. for new save cart insert
		SELECT RowId, Id ,Row_number()Over(Order BY RowId, Id,GenId) NewRowId , ParentOmsSavedCartLineItemId ,OmsSavedCartId,SKU,Quantity,OrderLineItemRelationshipTypeId  
		,CustomText,CartAddOnDetails,ROW_NUMBER()Over(Order BY NewId() ) Sequence ,AutoAddon  
		,OmsOrderId,Custom1,Custom2,Custom3 ,Custom4 ,Custom5,GroupId,ProductName ,min(Description)Description  ,ParentSKU  
		INTO #InsertNewSavecartLineitem   
		FROM  #NewSavecartLineitemDetails  
		GROUP BY ParentOmsSavedCartLineItemId ,OmsSavedCartId,SKU,Quantity,OrderLineItemRelationshipTypeId  
		,CustomText,CartAddOnDetails ,AutoAddon  
		,OmsOrderId,Custom1,Custom2,Custom3 ,Custom4 ,Custom5,GroupId,ProductName ,RowId, Id ,GenId,ParentSKU   
		ORDER BY RowId, Id   
       	
		--Removing the line item having Quantity <=0	     
		DELETE FROM #InsertNewSavecartLineitem WHERE Quantity <=0  
  
		--Updating the rowid into new save cart line item as new line item is merged into existing save cart item
		;WITH VTTY AS   
		(  
			SELECT m.RowId OldRowId , TY1.RowId , TY1.SKU   
			FROM #InsertNewSavecartLineitem m  
			INNER JOIN  #InsertNewSavecartLineitem TY1 ON TY1.SKU = m.ParentSKU   
			WHERE m.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon   
		)   
		UPDATE m1   
		SET m1.RowId = TYU.RowId  
		FROM #InsertNewSavecartLineitem m1   
		INNER JOIN VTTY TYU ON (TYU.OldRowId = m1.RowId)  
        
		--Deleting the new save cart line item if cart line item is merged
		;WITH VTRET AS   
		(  
			SELECT RowId,id,Min(NewRowId) NewRowId ,SKU ,ParentSKU ,OrderLineItemRelationshipTypeID  
			FROM #InsertNewSavecartLineitem   
			GROUP BY RowId,id ,SKU ,ParentSKU ,OrderLineItemRelationshipTypeID
			Having  SKU = ParentSKU  AND OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdSimple
		)   
		DELETE FROM #InsertNewSavecartLineitem WHERE NewRowId IN (SELECT NewRowId FROM VTRET)  

		--Inserting the new cart line item if not merged in existing save cart line item
		INSERT INTO  ZnodeOmsSavedCartLineItem (ParentOmsSavedCartLineItemId ,OmsSavedCartId,SKU,Quantity,OrderLineItemRelationshipTypeId  
		,CustomText,CartAddOnDetails,Sequence,CreatedBY,CreatedDate,ModifiedBy ,ModifiedDate,AutoAddon  
		,OmsOrderId,Custom1,Custom2,Custom3 ,Custom4 ,Custom5,GroupId,ProductName ,Description)  
		OUTPUT INSERTED.OmsSavedCartLineItemId  INTO @OmsInsertedData 
		SELECT NULL ,OmsSavedCartId,SKU,Quantity,OrderLineItemRelationshipTypeId  
		,CustomText,CartAddOnDetails,ROW_NUMBER()Over(Order BY NewRowId)  sequence,@UserId,@GetDate,@UserId,@GetDate,AutoAddon  
		,OmsOrderId,Custom1,Custom2,Custom3 ,Custom4 ,Custom5,GroupId,ProductName ,Description   
		FROM  #InsertNewSavecartLineitem  TH  

		SELECT  MAX(a.OmsSavedCartLineItemId ) OmsSavedCartLineItemId 
		, b.RowId ,b.GroupId ,b.SKU ,b.ParentSKU  
		INTO #ParentOmsSavedCartId
		FROM ZnodeOmsSavedCartLineItem a with (nolock) 
		INNER JOIN #InsertNewSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.ParentSKU AND ISNULL(b.GroupId,'-') = ISNULL(a.GroupId,'-')  )  
		WHERE ISNULL(a.ParentOmsSavedCartLineItemId,0) =0  
		AND b.OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdAddon
		AND CASE WHEN EXISTS (SELECT TOP 1 1 FROM #InsertNewSavecartLineitem TU WHERE TU.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdSimple)  THEN ISNULL(a.OrderLineItemRelationshipTypeID,0) ELSE 0 END = 0 
		GROUP BY b.RowId ,b.GroupId ,b.SKU	,b.ParentSKU,b.OrderLineItemRelationshipTypeID

		UPDATE a SET a.ParentOmsSavedCartLineItemId = (SELECT TOP 1 OmsSavedCartLineItemId FROM  #ParentOmsSavedCartId  r  
		WHERE  r.RowId = b.RowId AND ISNULL(r.GroupId,'-') = ISNULL(a.GroupId,'-')  Order by b.RowId )   
		FROM ZnodeOmsSavedCartLineItem a  
		INNER JOIN #InsertNewSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.SKU AND b.id =1  )   
		WHERE a.OrderLineItemRelationshipTypeId IS NOT NULL   
		AND b.OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdAddon  
		AND a.ParentOmsSavedCartLineItemId IS nULL   

		SELECT a.OmsSavedCartLineItemId , b.RowId  ,b.SKU ,b.ParentSKU  ,Row_number()Over(Order BY c.OmsSavedCartLineItemId )RowIdNo
		INTO #NewSimpleProduct
		FROM ZnodeOmsSavedCartLineItem a with (nolock) 
		INNER JOIN #InsertNewSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.ParentSKU AND ( b.Id = 1  ))  
		INNER JOIN ZnodeOmsSavedCartLineItem c on b.sku = c.sku and b.OmsSavedCartId=c.OmsSavedCartId and b.Id = 1 
		WHERE ( ISNULL(a.ParentOmsSavedCartLineItemId,0) <> 0   )
		AND b.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon  and c.ParentOmsSavedCartLineItemId is null

		--Updating ParentOmsSavedCartLineItemId for newly added save cart line item
		;WITH table_update AS 
		(
			SELECT * , ROW_NUMBER()Over(Order BY OmsSavedCartLineItemId  ) RowIdNo
			FROM ZnodeOmsSavedCartLineItem a with (nolock)
			WHERE a.OrderLineItemRelationshipTypeId IS NOT NULL   
			AND a.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon  
			AND a.ParentOmsSavedCartLineItemId IS NULL  
			AND EXISTS (SELECT TOP 1 1  FROM  #InsertNewSavecartLineitem ty WHERE ty.OmsSavedCartId = a.OmsSavedCartId )
			AND EXISTS (SELECT TOP 1 1 FROM #NewSimpleProduct TI WHERE TI.SKU = a.SKU)
		)
		UPDATE a SET  
		a.ParentOmsSavedCartLineItemId =(SELECT TOP 1 max(OmsSavedCartLineItemId) 
		FROM #NewSimpleProduct  r  
		WHERE  r.ParentSKU = b.ParentSKU AND a.SKU = r.SKU  GROUP BY r.ParentSKU, r.SKU  )   
		FROM table_update a  
		INNER JOIN #InsertNewSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.SKU AND b.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon AND  b.id =1 )   
		WHERE (SELECT TOP 1 max(OmsSavedCartLineItemId) 
		FROM #NewSimpleProduct  r  
		WHERE  r.ParentSKU = b.ParentSKU AND a.SKU = r.SKU   GROUP BY r.ParentSKU, r.SKU  )    IS NOT NULL 
	 
		;WITH Cte_Th AS   
		(             
			SELECT RowId    
			FROM #InsertNewSavecartLineitem a   
			GROUP BY RowId   
			HAVING COUNT(NewRowId) <= 1   
		)   
		UPDATE a SET a.Quantity =  NULL , a.ModifiedDate = @GetDate  
		FROM ZnodeOmsSavedCartLineItem a  
		INNER JOIN #InsertNewSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.SKU AND b.id =0)   
		WHERE NOT EXISTS (SELECT TOP 1 1  FROM Cte_Th TY WHERE TY.RowId = b.RowId )  
		AND a.OrderLineItemRelationshipTypeId IS NULL   
  
		UPDATE  ZnodeOmsSavedCartLineItem   
		SET GROUPID = NULL   
		WHERE  EXISTS (SELECT TOP 1 1  FROM #InsertNewSavecartLineitem RT WHERE RT.OmsSavedCartId = ZnodeOmsSavedCartLineItem.OmsSavedCartId )  
		AND OrderLineItemRelationshipTypeId IS NOT NULL     

		;WITH Cte_UpdateSequence AS   
		(  
			SELECT OmsSavedCartLineItemId ,Row_Number()Over(Order By OmsSavedCartLineItemId) RowId , Sequence   
			FROM ZnodeOmsSavedCartLineItem with (nolock)  
			WHERE EXISTS (SELECT TOP 1 1 FROM #InsertNewSavecartLineitem TH WHERE TH.OmsSavedCartId = ZnodeOmsSavedCartLineItem.OmsSavedCartId )  
		)   
		UPDATE Cte_UpdateSequence  
		SET  Sequence = RowId  
	
		----To update saved cart item personalise value FROM given line item	
		IF EXISTS(SELECT * FROM @TBL_Personalise1 where isnull(PersonalizeValue,'') <> '' and isnull(OmsSavedCartLineItemId,0) <> 0)
		BEGIN
			DELETE FROM ZnodeOmsPersonalizeCartItem 
			WHERE EXISTS (SELECT TOP 1 1 FROM @TBL_Personalise1 yu WHERE yu.OmsSavedCartLineItemId = ZnodeOmsPersonalizeCartItem.OmsSavedCartLineItemId )

			MERGE INTO ZnodeOmsPersonalizeCartItem TARGET 
			USING @TBL_Personalise1 SOURCE
			ON (TARGET.OmsSavedCartLineItemId = SOURCE.OmsSavedCartLineItemId ) 
			WHEN NOT MATCHED THEN 
			INSERT  ( OmsSavedCartLineItemId,  CreatedBy, CreatedDate, ModifiedBy, ModifiedDate
							,PersonalizeCode, PersonalizeValue,DesignId	,ThumbnailURL )
			VALUES (  SOURCE.OmsSavedCartLineItemId,  @userId, @getdate, @userId, @getdate
							,SOURCE.PersonalizeCode, SOURCE.PersonalizeValue,SOURCE.DesignId	,SOURCE.ThumbnailURL ) ;
		END		
	
		UPDATE @TBL_Personalise
		SET OmsSavedCartLineItemId = b.OmsSavedCartLineItemId
		FROM @OmsInsertedData a 
		INNER JOIN ZnodeOmsSavedCartLineItem b ON (a.OmsSavedCartLineItemId = b.OmsSavedCartLineItemId and b.OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdAddon)
		WHERE b.ParentOmsSavedCartLineItemId IS NOT NULL 
	
		DELETE FROM ZnodeOmsPersonalizeCartItem 
		WHERE EXISTS (SELECT TOP 1 1 FROM @TBL_Personalise yu WHERE yu.OmsSavedCartLineItemId = ZnodeOmsPersonalizeCartItem.OmsSavedCartLineItemId )
						
		MERGE INTO ZnodeOmsPersonalizeCartItem TARGET 
		USING @TBL_Personalise SOURCE
		ON (TARGET.OmsSavedCartLineItemId = SOURCE.OmsSavedCartLineItemId ) 
		WHEN NOT MATCHED THEN 
		INSERT  ( OmsSavedCartLineItemId,  CreatedBy, CreatedDate, ModifiedBy, ModifiedDate
					,PersonalizeCode, PersonalizeValue,DesignId	,ThumbnailURL )
		VALUES (  SOURCE.OmsSavedCartLineItemId,  @userId, @getdate, @userId, @getdate
					,SOURCE.PersonalizeCode, SOURCE.PersonalizeValue,SOURCE.DesignId	,SOURCE.ThumbnailURL ) ;
  
		
		 
	END 

	Declare @OutputTable Table (CartCount numeric(28,6))

	INSERT INTO @OutputTable
	EXEC [Znode_GetOmsSavedCartLineItemCount] @OmsCookieMappingId = @OmsCookieMappingId,@UserId=@UserId,@PortalId=@UserId
	

	SELECT CAST(1 AS bit) AS Status,@OmsSavedCartId AS SavedCartId,@OmsCookieMappingId AS CookieMappingId,CartCount
	FROM @OutputTable

COMMIT TRAN InsertUpdateSaveCartLineItem;
END TRY
BEGIN CATCH
	SELECT ERROR_MESSAGE()	
	DECLARE @Error_procedure varchar(1000)= ERROR_PROCEDURE(), @ErrorMessage nvarchar(max)= ERROR_MESSAGE(), @ErrorLine varchar(100)= ERROR_LINE(), @ErrorCall nvarchar(max)= 'EXEC Znode_InsertUpdateSaveCartLineItem @CartLineItemXML = '+CAST(@CartLineItemXML
	AS varchar(max))+',@UserId = '+CAST(@UserId AS varchar(50))+',@PortalId='+CAST(@PortalId AS varchar(10))+',@OmsCookieMappingId='+CAST(@OmsCookieMappingId AS varchar(10));

	SELECT 0 AS ID, CAST(0 AS bit) AS Status,ERROR_MESSAGE();
	ROLLBACK TRAN InsertUpdateSaveCartLineItem;
	EXEC Znode_InsertProcedureErrorLog @ProcedureName = 'Znode_InsertUpdateSaveCartLineItem', @ErrorInProcedure = @Error_procedure, @ErrorMessage = @ErrorMessage, @ErrorLine = @ErrorLine, @ErrorCall = @ErrorCall;
END CATCH;
END
GO
------------------------------------------------

----ZPD-14892
update  ZnodeEmailTemplateLocale 
set Content = '<!DOCTYPE html><html><body><div style="font-family: Arial, Helvetica; text-align: left; color: black; border: solid 1px black;">  <div style="background-color: #005286; font-size: 1.5em; font-weight: bold; padding-top: .5em; padding-bottom: .5em; padding-left: 1em; border-bottom: solid 1px #edffff; color: white;">#StoreLogo# New Contact Request</div>  <p style="padding: 0 1rem; margin: 10px 0px 0px; font-size: 14px; line-height: 1.714; letter-spacing: -0.005em; color: #172b4d; font-family: -apple-system, BlinkMacSystemFont, ''Segoe UI'', Roboto, Oxygen, Ubuntu, ''Fira Sans'', ''Droid Sans'', ''Helvetica Neue'', sans-serif; white-space: pre-wrap;">Team,</p>  <p style="padding: 0 1rem; margin: 10px 0px 0px; font-size: 14px; line-height: 1.714; letter-spacing: -0.005em; color: #172b4d; font-family: -apple-system, BlinkMacSystemFont, ''Segoe UI'', Roboto, Oxygen, Ubuntu, ''Fira Sans'', ''Droid Sans'', ''Helvetica Neue'', sans-serif; white-space: pre-wrap;">You have a new contact request. Please take a look at the submission received:-</p>  <div style="padding-top: .5em; padding-bottom: .5em; padding-left: 1rem;"><span style="color: #333333; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; letter-spacing: normal; white-space: normal;">DyanamicHtml</span></div>  <p style="padding: 0 1rem; margin: 10px 0px 0px; font-size: 14px; line-height: 1.714; letter-spacing: -0.005em; color: #172b4d; font-family: -apple-system, BlinkMacSystemFont, ''Segoe UI'', Roboto, Oxygen, Ubuntu, ''Fira Sans'', ''Droid Sans'', ''Helvetica Neue'', sans-serif; white-space: pre-wrap;">Thank you,<br />Admin</p>  </div></body></html>'
,Subject ='Contact Acknowlegement'
,Descriptions ='Contact Acknowlegement'
where EmailTemplateId in (select EmailTemplateId from ZnodeEmailTemplate where TemplateName = 'ContactAcknowledgment') and LocaleId =1


UPDATE  ZnodeEmailTemplateLocale 
set Content = '<div style="font-family: Arial, Helvetica; text-align: left; color: black; border: solid 1px black;">
<div style="background-color: #005286; font-size: 1.5em; font-weight: bold; padding-top: .5em; padding-bottom: .5em; padding-left: 1em; border-bottom: solid 1px #edffff; color: white;">#StoreLogo# New Contact Request</div>
<p style="padding: 0 1rem; margin: 10px 0px 0px; font-size: 14px; line-height: 1.714; letter-spacing: -0.005em; color: #172b4d; font-family: -apple-system, BlinkMacSystemFont, ''Segoe UI'', Roboto, Oxygen, Ubuntu, ''Fira Sans'', ''Droid Sans'', ''Helvetica Neue'', sans-serif; white-space: pre-wrap;">Team,</p>
<p style="padding: 0 1rem; margin: 10px 0px 0px; font-size: 14px; line-height: 1.714; letter-spacing: -0.005em; color: #172b4d; font-family: -apple-system, BlinkMacSystemFont, ''Segoe UI'', Roboto, Oxygen, Ubuntu, ''Fira Sans'', ''Droid Sans'', ''Helvetica Neue'', sans-serif; white-space: pre-wrap;">You have a new contact request. Please take a look at the submission received:-</p>
<div style="padding-top: .5em; padding-left: 1rem; font-size: 14px; line-height: 1.714; letter-spacing: -0.005em; color: #172b4d; font-family: -apple-system, BlinkMacSystemFont, ''Segoe UI'', Roboto, Oxygen, Ubuntu, ''Fira Sans'', ''Droid Sans'', ''Helvetica Neue'', sans-serif; white-space: pre-wrap;"><span style="color: #333333; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; letter-spacing: normal; white-space: normal;">DyanamicHtml</span></div>
<p style="padding: 0 1rem; margin: 10px 0px 0px; font-size: 14px; line-height: 1.714; letter-spacing: -0.005em; color: #172b4d; font-family: -apple-system, BlinkMacSystemFont, ''Segoe UI'', Roboto, Oxygen, Ubuntu, ''Fira Sans'', ''Droid Sans'', ''Helvetica Neue'', sans-serif; white-space: pre-wrap;">Thank you,<br />Admin</p>
</div>'
WHERE EmailTemplateId = (SELECT TOP 1 EmailTemplateId FROM ZnodeEmailTemplate WHERE TemplateName = 'NewContactRequest') AND LocaleId =1

update  ZnodeEmailTemplateLocale 
set Content = '<!DOCTYPE html><html><body><div style="font-family: Arial, Helvetica; text-align: left; color: black; border: solid 1px black;">  <div style="background-color: #005286; font-size: 1.5em; font-weight: bold; padding-top: .5em; padding-bottom: .5em; padding-left: 1em; border-bottom: solid 1px #edffff; color: white;">#StoreLogo# New Contact Request</div>  <p style="padding: 0 1rem; margin: 10px 0px 0px; font-size: 14px; line-height: 1.714; letter-spacing: -0.005em; color: #172b4d; font-family: -apple-system, BlinkMacSystemFont, ''Segoe UI'', Roboto, Oxygen, Ubuntu, ''Fira Sans'', ''Droid Sans'', ''Helvetica Neue'', sans-serif; white-space: pre-wrap;">Team,</p>  <p style="padding: 0 1rem; margin: 10px 0px 0px; font-size: 14px; line-height: 1.714; letter-spacing: -0.005em; color: #172b4d; font-family: -apple-system, BlinkMacSystemFont, ''Segoe UI'', Roboto, Oxygen, Ubuntu, ''Fira Sans'', ''Droid Sans'', ''Helvetica Neue'', sans-serif; white-space: pre-wrap;">You have a new contact request. Please take a look at the submission received:-</p>  <div style="padding-top: .5em; padding-bottom: .5em; padding-left: 1rem;"><span style="color: #333333; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; letter-spacing: normal; white-space: normal;">DyanamicHtml</span></div>  <p style="padding: 0 1rem; margin: 10px 0px 0px; font-size: 14px; line-height: 1.714; letter-spacing: -0.005em; color: #172b4d; font-family: -apple-system, BlinkMacSystemFont, ''Segoe UI'', Roboto, Oxygen, Ubuntu, ''Fira Sans'', ''Droid Sans'', ''Helvetica Neue'', sans-serif; white-space: pre-wrap;">Thank you,<br />Admin</p>  </div></body></html>'
,Subject ='Contact Acknowlegement'
,Descriptions ='Contact Acknowlegement'
where EmailTemplateId in (select EmailTemplateId from ZnodeEmailTemplate where TemplateName = 'ContactAcknowledgment') and LocaleId =1



------ZPD-14898
update ZnodeApplicationSetting set Setting='<?xml version="1.0" encoding="utf-16"?><columns><column><id>1</id><name>PimProductTypeAssociationId</name><headertext>Checkbox</headertext><width>20</width><datatype>Int32</datatype><columntype>Int32</columntype><allowsorting>false</allowsorting><allowpaging>true</allowpaging><format></format><isvisible>y</isvisible><mustshow>y</mustshow><musthide>n</musthide><maxlength>0</maxlength><isallowsearch>n</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>y</ischeckbox><checkboxparamfield>PimProductTypeAssociationId</checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext></displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>2</id><name>RelatedProductId</name><headertext>RelatedProductId</headertext><width>20</width><datatype>Int32</datatype><columntype>Int32</columntype><allowsorting>false</allowsorting><allowpaging>true</allowpaging><format></format><isvisible>n</isvisible><mustshow>n</mustshow><musthide>y</musthide><maxlength>0</maxlength><isallowsearch>n</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield>PimProductTypeAssociationId</checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext></displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>3</id><name>PimProductId</name><headertext>ID</headertext><width>30</width><datatype>Int32</datatype><columntype>Int32</columntype><allowsorting>false</allowsorting><allowpaging>true</allowpaging><format></format><isvisible>y</isvisible><mustshow>y</mustshow><musthide>n</musthide><maxlength>0</maxlength><isallowsearch>n</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext></displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>4</id><name>Image</name><headertext>Image</headertext><width>20</width><datatype>String</datatype><columntype>String</columntype><allowsorting>false</allowsorting><allowpaging>false</allowpaging><format></format><isvisible>y</isvisible><mustshow>n</mustshow><musthide>n</musthide><maxlength>0</maxlength><isallowsearch>n</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext></displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield>ProductImage,ProductName</imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class>imageicon</Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>5</id><name>ProductName</name><headertext>Product Name</headertext><width>60</width><datatype>String</datatype><columntype>String</columntype><allowsorting>true</allowsorting><allowpaging>true</allowpaging><format></format><isvisible>y</isvisible><mustshow>y</mustshow><musthide>n</musthide><maxlength>0</maxlength><isallowsearch>y</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext></displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>6</id><name>ProductType</name><headertext>Product Type</headertext><width>30</width><datatype>String</datatype><columntype>String</columntype><allowsorting>true</allowsorting><allowpaging>true</allowpaging><format></format><isvisible>y</isvisible><mustshow>n</mustshow><musthide>n</musthide><maxlength>0</maxlength><isallowsearch>y</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext></displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>7</id><name>SKU</name><headertext>SKU</headertext><width>30</width><datatype>String</datatype><columntype>String</columntype><allowsorting>true</allowsorting><allowpaging>true</allowpaging><format></format><isvisible>y</isvisible><mustshow>y</mustshow><musthide>n</musthide><maxlength>0</maxlength><isallowsearch>y</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext>SKU</displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>8</id><name>AvailableInventory</name><headertext>Available Inventory</headertext><width>30</width><datatype>String</datatype><columntype>String</columntype><allowsorting>false</allowsorting><allowpaging>false</allowpaging><format></format><isvisible>y</isvisible><mustshow>y</mustshow><musthide>n</musthide><maxlength>0</maxlength><isallowsearch>y</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext>SKU</displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>9</id><name>Assortment</name><headertext>Assortment</headertext><width>0</width><datatype>String</datatype><columntype>String</columntype><allowsorting>false</allowsorting><allowpaging>false</allowpaging><format></format><isvisible>n</isvisible><mustshow>n</mustshow><musthide>y</musthide><maxlength>0</maxlength><isallowsearch>n</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext></displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>10</id><name>DisplayOrder</name><headertext>Display Order</headertext><width>10</width><datatype>Int32</datatype><columntype>Int32</columntype><allowsorting>true</allowsorting><allowpaging>true</allowpaging><format></format><isvisible>y</isvisible><mustshow>n</mustshow><musthide>n</musthide><maxlength>5</maxlength><isallowsearch>n</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>y</iscontrol><controltype>Text</controltype><controlparamfield></controlparamfield><displaytext></displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>Text</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>11</id><name>BundleQuantity</name><headertext>Quantity</headertext><width>10</width><datatype>Int32</datatype><columntype>Int32</columntype><allowsorting>false</allowsorting><allowpaging>false</allowpaging><format></format><isvisible>y</isvisible><mustshow>y</mustshow><musthide>n</musthide><maxlength>5</maxlength><isallowsearch>n</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>y</iscontrol><controltype>Text</controltype><controlparamfield></controlparamfield><displaytext></displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>Text</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>12</id><name>Manage</name><headertext>Action</headertext><width>30</width><datatype>String</datatype><columntype>String</columntype><allowsorting>false</allowsorting><allowpaging>true</allowpaging><format>Edit|Delete</format><isvisible>y</isvisible><mustshow>y</mustshow><musthide>n</musthide><maxlength>0</maxlength><isallowsearch>n</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext>Edit|Delete</displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl>/PIM/Products/UpdateAssociatedProducts|/PIM/Products/UnassociateProducts</manageactionurl><manageparamfield>PimProductTypeAssociationId,PimProductId,RelatedProductId|PimProductTypeAssociationId,PimProductId</manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column></columns>'
where ItemName = 'View_ManageProductTypeList_BundleProduct'



UPDATE ZnodeState SET StateName = dbo.Fn_CamelCase(ltrim(rtrim(replace(StateName,' ','')))) 
WHERE StateName like ' %'

UPDATE ZnodeState SET StateName = dbo.Fn_CamelCase(ltrim(rtrim(StateName))) 

UPDATE ZnodeState SET StateName = replace(dbo.Fn_CamelCase(ltrim(rtrim(replace(StateName,'-',' - ')))),' - ','-') 
where StateName like '%-%'

UPDATE ZnodeState SET StateName = replace(dbo.Fn_CamelCase(ltrim(rtrim(replace(StateName,'(',' ( ')))),' ( ','(') 
where StateName like '%(%'

UPDATE ZnodeState SET StateName = replace(dbo.Fn_CamelCase(ltrim(rtrim(replace(StateName,'[',' [ ')))),' [ ','[') 
where StateName like '%[%'

UPDATE ZnodeState SET StateName = replace(dbo.Fn_CamelCase(ltrim(rtrim(replace(StateName,'(','( ')))),'( ','(')
where StateName like '%(%'UPDATE ZnodeState SET StateName = dbo.Fn_CamelCase(ltrim(rtrim(replace(StateName,substring(StateName,1,1),''))))
WHERE CountryCode = 'MX'

---------------------------------

UPDATE ZnodeState SET StateName = dbo.Fn_CamelCase(ltrim(rtrim(StateName))) 

UPDATE ZnodeState SET StateName = replace(dbo.Fn_CamelCase(ltrim(rtrim(replace(StateName,'-',' - ')))),' - ','-') 
where StateName like '%-%'

UPDATE ZnodeState SET StateName = replace(dbo.Fn_CamelCase(ltrim(rtrim(replace(StateName,'(',' ( ')))),' ( ','(') 
where StateName like '%(%'

UPDATE ZnodeState SET StateName = replace(dbo.Fn_CamelCase(ltrim(rtrim(replace(StateName,'[',' [ ')))),' [ ','[') 
where StateName like '%[%'


UPDATE ZnodeState SET StateName = dbo.Fn_CamelCase(ltrim(rtrim(replace(StateName,substring(StateName,1,1),''))))
WHERE CountryCode = 'MX' and substring(StateName,1,1) not like '%[a-Z]%'


UPDATE ZnodeState SET StateName = replace(dbo.Fn_CamelCase(ltrim(rtrim(replace(StateName,'(','( ')))),'( ','(')
where StateName like '%(%'


GO
--------------------------------------------
IF EXISTS(SELECT * FROM SYS.PROCEDURES WHERE NAME = 'Znode_InsertUpdateQuoteLineItem')
	DROP PROC Znode_InsertUpdateQuoteLineItem
GO


CREATE PROCEDURE [dbo].[Znode_InsertUpdateQuoteLineItem]
(   
	@OmsQuoteId      INT ,
    @UserId          INT = 0,
	@OmsSavedCartId  INT = 0,
    @Status          BIT OUT ,
	@SKUPriceForQuote [dbo].[SKUPriceForQuote] ReadOnly
)
AS 
   /* 
    Summary: This Procedure is used to save and edit the quote line item      
    Unit Testing   
    Exec Znode_InsertUpdateQuoteLineItem 1071,1527,0
   
	*/
     BEGIN
         BEGIN TRAN A;
         BEGIN TRY
             SET NOCOUNT ON;
			 DECLARE @GetDate DATETIME = dbo.Fn_GetDate();
			 DECLARE  @OmsQuoteLineItemId INT;
			 DECLARE @TempOmsQuoteLineItem TABLE (OmsQuoteLineItemId INT , OmsSavedCartLineItemId INT , ParentOmsSavedCartLineItemId INT  )
			 DECLARE @OrderLineItemRelationshipTypeIdBundle INT = (SELECT TOP 1 OrderLineItemRelationshipTypeId FROM ZnodeOmsOrderLineItemRelationshipType WHERE Name = 'Bundles')

			CREATE TABLE #ZnodeOmsSavedCartLineItem
			(OmsSavedCartLineItemId	int,
			ParentOmsSavedCartLineItemId	int,
			OmsSavedCartId	int,
			SKU	nvarchar(200),
			Quantity	numeric(28,	13) ,
			OrderLineItemRelationshipTypeId	int,
			CustomText	nvarchar(MAX),
			CartAddOnDetails	nvarchar(MAX),
			Sequence	int,
			CreatedBy	int,
			CreatedDate	datetime,
			ModifiedBy	int,
			ModifiedDate	datetime,
			AutoAddon	nvarchar(MAX),
			OmsOrderId	int,
			Custom1	nvarchar(MAX),
			Custom2	nvarchar(MAX),
			Custom3	nvarchar(MAX),
			Custom4	nvarchar(MAX),
			Custom5	nvarchar(MAX),
			GroupId	nvarchar(MAX),
			ProductName	nvarchar(2000),
			Description	nvarchar(MAX),
			Price numeric(28,6),
			ShippingCost numeric(28,6))

			IF (isnull(@OmsSavedCartId,0) <> 0)
			BEGIN
				INSERT INTO #ZnodeOmsSavedCartLineItem(OmsSavedCartLineItemId,ParentOmsSavedCartLineItemId,OmsSavedCartId,SKU,Quantity,OrderLineItemRelationshipTypeId,CustomText,CartAddOnDetails,
				                                       Sequence,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,AutoAddon,OmsOrderId,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName,Description)
				SELECT a.OmsSavedCartLineItemId,a.ParentOmsSavedCartLineItemId,a.OmsSavedCartId,a.SKU,a.Quantity,a.OrderLineItemRelationshipTypeId,a.CustomText,a.CartAddOnDetails,
				       a.Sequence,a.CreatedBy,a.CreatedDate,a.ModifiedBy,a.ModifiedDate,a.AutoAddon,a.OmsOrderId,a.Custom1,a.Custom2,a.Custom3,a.Custom4,a.Custom5,a.GroupId,a.ProductName,a.Description 				
				FROM ZnodeOmsSavedCartLineItem  a 
				WHERE NOT EXISTS (SELECT TOP 1 1 FROM ZnodeOmsQuoteLineItem ty WHERE ty.OmsQuoteId = @OmsQuoteId) and a.OmsSavedCartId=@OmsSavedCartId
				
			END
			ELSE
			BEGIN
				INSERT INTO #ZnodeOmsSavedCartLineItem(OmsSavedCartLineItemId,ParentOmsSavedCartLineItemId,OmsSavedCartId,SKU,Quantity,OrderLineItemRelationshipTypeId,CustomText,CartAddOnDetails,
				                                       Sequence,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,AutoAddon,OmsOrderId,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName,Description)
				SELECT a.OmsSavedCartLineItemId,a.ParentOmsSavedCartLineItemId,a.OmsSavedCartId,a.SKU,a.Quantity,a.OrderLineItemRelationshipTypeId,a.CustomText,a.CartAddOnDetails,
				       a.Sequence,a.CreatedBy,a.CreatedDate,a.ModifiedBy,a.ModifiedDate,a.AutoAddon,a.OmsOrderId,a.Custom1,a.Custom2,a.Custom3,a.Custom4,a.Custom5,a.GroupId,a.ProductName,a.Description 
				FROM ZnodeOmsSavedCartLineItem  a 
				INNER JOIN ZnodeOmsSavedCart b ON (b.OmsSavedCartId = a.OmsSavedCartId)
				INNER JOIN ZnodeOmsCookieMapping c ON (c.OmsCookieMappingId = b.OmsCookieMappingId)
				WHERE NOT EXISTS (SELECT TOP 1 1 FROM ZnodeOmsQuoteLineItem ty WHERE ty.OmsQuoteId = @OmsQuoteId) 
				AND c.UserId = @UserId
			END

			UPDATE ZOSLI set ZOSLI.Price = SPQ.Price, ZOSLI.ShippingCost = SPQ.ShippingCost
			FROM #ZnodeOmsSavedCartLineItem ZOSLI
			INNER JOIN @SKUPriceForQuote SPQ ON ZOSLI.OmsSavedCartLineItemId = SPQ.OmsSavedCartLineItemId

			--To update price and ShippingCost for child entries of bundle product
			UPDATE ZOSLI set ZOSLI.Price = SPQ.Price, ZOSLI.ShippingCost = SPQ.ShippingCost
			FROM #ZnodeOmsSavedCartLineItem ZOSLI
			INNER JOIN @SKUPriceForQuote SPQ ON ZOSLI.ParentOmsSavedCartLineItemId = SPQ.OmsSavedCartLineItemId
			WHERE ZOSLI.OrderLineItemRelationshipTypeId = @OrderLineItemRelationshipTypeIdBundle


			IF OBJECT_ID('Tempdb..#desupdate1') IS NOT NULL  
			DROP TABLE Tempdb..#desupdate1 
			select ZnodeProductId,des into #desupdate1 from (
									   SELECT  ZnodeProductId, CONCAT('QTY: ',cast(cast(AssociatedProductBundleQuantity as int) as varchar(5)),'| ',sku,' - ', productname, char(10) ) des  FROM ZnodePublishBundleProductEntity  ZPBPE1
											inner join ZnodePublishProductDetail ZPP  on  ZPBPE1.AssociatedZnodeProductId = ZPP.PublishProductId
											)asd group by ZnodeProductId, des


			IF OBJECT_ID('Tempdb..#desupdate') IS NOT NULL  
			DROP TABLE Tempdb..#desupdate 

			select ZPBPE.des, asd.* into #desupdate from (
				select VLMP.PimProductId PPI , ZOSCL.* from #ZnodeOmsSavedCartLineItem ZOSCL 
							inner join [dbo].[View_LoadManageProduct] VLMP on VLMP.AttributeCode ='SKU' and VLMP.AttributeValue= ZOSCL.sku
							inner join (select * from ZnodepimAttributevalue where PimAttributeId = 10 and  PimAttributeValueId in (
											select PimAttributeValueId from ZnodePimProductAttributeDefaultValue where PimAttributeDefaultValueId in (
											select PimAttributeDefaultValueId from ZnodePimAttributeDefaultValue where AttributeDefaultValueCode = 'BundleProduct' ))) BP
												on BP.PimProductId =  VLMP.PimProductId ) asd
						cross apply
						 (
						SELECT   *
						FROM    (SELECT ZnodeProductId, '<p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 12px;">' +
							(SELECT  des +' </br> '  
											FROM #desupdate1 AS p 
											WHERE p.ZnodeProductId = a.ZnodeProductId
											FOR XML PATH(''))
							
						+ '</span></p>' AS des
									FROM #desupdate1 a
									GROUP BY ZnodeProductId)  ZPBPE1
						inner join ZnodePublishProduct ZPP on  ZPBPE1.ZnodeProductId = ZPP.PublishProductId

						WHERE   Zpp.PimProductId = asd.PPI
      
						) 
				   ZPBPE

			UPDATE ZOSLI set ZOSLI.Description = SPQ.des
			FROM #ZnodeOmsSavedCartLineItem ZOSLI
			INNER JOIN #desupdate SPQ ON ZOSLI.OmsSavedCartLineItemId = SPQ.OmsSavedCartLineItemId or ZOSLI.ParentOmsSavedCartLineItemId = SPQ.OmsSavedCartLineItemId


			
			MERGE INTO ZnodeOmsQuoteLineItem TARGET 
			USING #ZnodeOmsSavedCartLineItem  SOURCE 
			ON (1=0 )
			WHEN NOT MATCHED THEN 
			INSERT   (ParentOmsQuoteLineItemId,OmsQuoteId,SKU,Quantity,OrderLineItemRelationshipTypeId
						,CustomText,CartAddOnDetails,Sequence,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,GroupId,ProductName,Description, Price, ShippingCost)
			
			 
			VALUES ( NULL,@OmsQuoteId,SOURCE.SKU,SOURCE.Quantity,SOURCE.OrderLineItemRelationshipTypeId
						,SOURCE.CustomText,SOURCE.CartAddOnDetails,SOURCE.Sequence,SOURCE.CreatedBy,SOURCE.CreatedDate,SOURCE.ModifiedBy,SOURCE.ModifiedDate,SOURCE.GroupId,SOURCE.ProductName,SOURCE.Description, SOURCE.Price, SOURCE.ShippingCost ) 
			
			OUTPUT Inserted.OmsQuoteLineItemId , SOURCE.OmsSavedCartLineItemId,Source.ParentOmsSavedCartLineItemId  INTO @TempOmsQuoteLineItem ;


			UPDATE a
			SET a.ParentOmsQuoteLineItemId =( SELECT TOP 1 b1.OmsQuoteLineItemId FROM  @TempOmsQuoteLineItem b1 WHERE b.ParentOmsSavedCartLineItemId  = b1.OmsSavedCartLineItemId)  
			FROM ZnodeOmsQuoteLineItem  a 
			INNER JOIN @TempOmsQuoteLineItem b ON (b.OmsQuoteLineItemId = a.OmsQuoteLineItemId)
			WHERE b.ParentOmsSavedCartLineItemId IS NOT NULL 



			INSERT INTO ZnodeOmsQuotePersonalizeItem (OmsQuoteLineItemId,PersonalizeCode,PersonalizeValue,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate
														,DesignId,ThumbnailURL)
			SELECT b.OmsQuoteLineItemId,PersonalizeCode,PersonalizeValue,a.CreatedBy,a.CreatedDate,a.ModifiedBy,a.ModifiedDate
														,DesignId,ThumbnailURL
			FROM ZnodeOmsPersonalizeCartItem a  
			INNER JOIN @TempOmsQuoteLineItem b ON (b.OmsSavedCartLineItemId =  a.OmsSavedCartLineItemId )         


             SET @Status = 1;
             COMMIT TRAN A;
         END TRY
         BEGIN CATCH
        
		     SET @Status = 0;
		     DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(),
			 @ErrorCall NVARCHAR(MAX)= 'EXEC Znode_InsertUpdateQuoteLineItem @CartLineItemXML = '+CAST('' AS VARCHAR(max))+',@UserId = '+CAST(@UserId AS VARCHAR(50))+',@Status='+CAST(@Status AS VARCHAR(10));
              			 
             SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                    
			 ROLLBACK TRAN A;
             EXEC Znode_InsertProcedureErrorLog
				@ProcedureName = 'Znode_InsertUpdateQuoteLineItem',
				@ErrorInProcedure = @Error_procedure,
				@ErrorMessage = @ErrorMessage,
				@ErrorLine = @ErrorLine,
				@ErrorCall = @ErrorCall;
         END CATCH;
     END

Go
----------------
INSERT INTO ZnodeApplicationSetting (GroupName,	ItemName,	Setting,	ViewOptions,	FrontPageName,	FrontObjectName,	IsCompressed,	OrderByFields,	ItemNameWithoutCurrency,	CreatedByName,	ModifiedByName,	CreatedBy,	CreatedDate,	ModifiedBy,	ModifiedDate)
SELECT null,'View_ManageProductTypeList_BundleProduct',
'<?xml version="1.0" encoding="utf-16"?>  <columns>    <column>      <id>1</id>      <name>PimProductTypeAssociationId</name>      <headertext>Checkbox</headertext>      <width>20</width>      <datatype>Int32</datatype>      <columntype>Int32</columntype>      <allowsorting>false</allowsorting>      <allowpaging>true</allowpaging>      <format>      </format>      <isvisible>y</isvisible>      <mustshow>y</mustshow>      <musthide>n</musthide>      <maxlength>0</maxlength>      <isallowsearch>n</isallowsearch>      <isconditional>n</isconditional>      <isallowlink>n</isallowlink>      <islinkactionurl>      </islinkactionurl>      <islinkparamfield>      </islinkparamfield>      <ischeckbox>y</ischeckbox>      <checkboxparamfield>PimProductTypeAssociationId</checkboxparamfield>      <iscontrol>n</iscontrol>      <controltype>      </controltype>      <controlparamfield>      </controlparamfield>      <displaytext>      </displaytext>      <editactionurl>      </editactionurl>      <editparamfield>      </editparamfield>      <deleteactionurl>      </deleteactionurl>      <deleteparamfield>      </deleteparamfield>      <viewactionurl>      </viewactionurl>      <viewparamfield>      </viewparamfield>      <imageactionurl>      </imageactionurl>      <imageparamfield>      </imageparamfield>      <manageactionurl>      </manageactionurl>      <manageparamfield>      </manageparamfield>      <copyactionurl>      </copyactionurl>      <copyparamfield>      </copyparamfield>      <xaxis>n</xaxis>      <yaxis>n</yaxis>      <isadvancesearch>y</isadvancesearch>      <Class>      </Class>      <SearchControlType>--Select--</SearchControlType>      <SearchControlParameters>      </SearchControlParameters>      <DbParamField>      </DbParamField>      <useMode>DataBase</useMode>      <IsGraph>n</IsGraph>      <allowdetailview>n</allowdetailview>    </column>    <column>      <id>2</id>      <name>RelatedProductId</name>      <headertext>RelatedProductId</headertext>      <width>20</width>      <datatype>Int32</datatype>      <columntype>Int32</columntype>      <allowsorting>false</allowsorting>      <allowpaging>true</allowpaging>      <format>      </format>      <isvisible>n</isvisible>      <mustshow>n</mustshow>      <musthide>y</musthide>      <maxlength>0</maxlength>      <isallowsearch>n</isallowsearch>      <isconditional>n</isconditional>      <isallowlink>n</isallowlink>      <islinkactionurl>      </islinkactionurl>      <islinkparamfield>      </islinkparamfield>      <ischeckbox>n</ischeckbox>      <checkboxparamfield>PimProductTypeAssociationId</checkboxparamfield>      <iscontrol>n</iscontrol>      <controltype>      </controltype>      <controlparamfield>      </controlparamfield>      <displaytext>      </displaytext>      <editactionurl>      </editactionurl>      <editparamfield>      </editparamfield>      <deleteactionurl>      </deleteactionurl>      <deleteparamfield>      </deleteparamfield>      <viewactionurl>      </viewactionurl>      <viewparamfield>      </viewparamfield>      <imageactionurl>      </imageactionurl>      <imageparamfield>      </imageparamfield>      <manageactionurl>      </manageactionurl>      <manageparamfield>      </manageparamfield>      <copyactionurl>      </copyactionurl>      <copyparamfield>      </copyparamfield>      <xaxis>n</xaxis>      <yaxis>n</yaxis>      <isadvancesearch>y</isadvancesearch>      <Class>      </Class>      <SearchControlType>--Select--</SearchControlType>      <SearchControlParameters>      </SearchControlParameters>      <DbParamField>      </DbParamField>      <useMode>DataBase</useMode>      <IsGraph>n</IsGraph>      <allowdetailview>n</allowdetailview>    </column>    <column>      <id>3</id>      <name>PimProductId</name>      <headertext>ID</headertext>      <width>30</width>      <datatype>Int32</datatype>      <columntype>Int32</columntype>      <allowsorting>false</allowsorting>      <allowpaging>true</allowpaging>      <format>      </format>      <isvisible>y</isvisible>      <mustshow>y</mustshow>      <musthide>n</musthide>      <maxlength>0</maxlength>      <isallowsearch>n</isallowsearch>      <isconditional>n</isconditional>      <isallowlink>n</isallowlink>      <islinkactionurl>      </islinkactionurl>      <islinkparamfield>      </islinkparamfield>      <ischeckbox>n</ischeckbox>      <checkboxparamfield>      </checkboxparamfield>      <iscontrol>n</iscontrol>      <controltype>      </controltype>      <controlparamfield>      </controlparamfield>      <displaytext>      </displaytext>      <editactionurl>      </editactionurl>      <editparamfield>      </editparamfield>      <deleteactionurl>      </deleteactionurl>      <deleteparamfield>      </deleteparamfield>      <viewactionurl>      </viewactionurl>      <viewparamfield>      </viewparamfield>      <imageactionurl>      </imageactionurl>      <imageparamfield>      </imageparamfield>      <manageactionurl>      </manageactionurl>      <manageparamfield>      </manageparamfield>      <copyactionurl>      </copyactionurl>      <copyparamfield>      </copyparamfield>      <xaxis>n</xaxis>      <yaxis>n</yaxis>      <isadvancesearch>y</isadvancesearch>      <Class>      </Class>      <SearchControlType>--Select--</SearchControlType>      <SearchControlParameters>      </SearchControlParameters>      <DbParamField>      </DbParamField>      <useMode>DataBase</useMode>      <IsGraph>n</IsGraph>      <allowdetailview>n</allowdetailview>    </column>    <column>      <id>4</id>      <name>Image</name>      <headertext>Image</headertext>      <width>20</width>      <datatype>String</datatype>      <columntype>String</columntype>      <allowsorting>false</allowsorting>      <allowpaging>false</allowpaging>      <format>      </format>      <isvisible>y</isvisible>      <mustshow>n</mustshow>      <musthide>n</musthide>      <maxlength>0</maxlength>      <isallowsearch>n</isallowsearch>      <isconditional>n</isconditional>      <isallowlink>n</isallowlink>      <islinkactionurl>      </islinkactionurl>      <islinkparamfield>      </islinkparamfield>      <ischeckbox>n</ischeckbox>      <checkboxparamfield>      </checkboxparamfield>      <iscontrol>n</iscontrol>      <controltype>      </controltype>      <controlparamfield>      </controlparamfield>      <displaytext>      </displaytext>      <editactionurl>      </editactionurl>      <editparamfield>      </editparamfield>      <deleteactionurl>      </deleteactionurl>      <deleteparamfield>      </deleteparamfield>      <viewactionurl>      </viewactionurl>      <viewparamfield>      </viewparamfield>      <imageactionurl>      </imageactionurl>      <imageparamfield>ProductImage,ProductName</imageparamfield>      <manageactionurl>      </manageactionurl>      <manageparamfield>      </manageparamfield>      <copyactionurl>      </copyactionurl>      <copyparamfield>      </copyparamfield>      <xaxis>n</xaxis>      <yaxis>n</yaxis>      <isadvancesearch>y</isadvancesearch>      <Class>imageicon</Class>      <SearchControlType>--Select--</SearchControlType>      <SearchControlParameters>      </SearchControlParameters>      <DbParamField>      </DbParamField>      <useMode>DataBase</useMode>      <IsGraph>n</IsGraph>      <allowdetailview>n</allowdetailview>    </column>    <column>      <id>5</id>      <name>ProductName</name>      <headertext>Product Name</headertext>      <width>60</width>      <datatype>String</datatype>      <columntype>String</columntype>      <allowsorting>true</allowsorting>      <allowpaging>true</allowpaging>      <format>      </format>      <isvisible>y</isvisible>      <mustshow>y</mustshow>      <musthide>n</musthide>      <maxlength>0</maxlength>      <isallowsearch>y</isallowsearch>      <isconditional>n</isconditional>      <isallowlink>n</isallowlink>      <islinkactionurl>      </islinkactionurl>      <islinkparamfield>      </islinkparamfield>      <ischeckbox>n</ischeckbox>      <checkboxparamfield>      </checkboxparamfield>      <iscontrol>n</iscontrol>      <controltype>      </controltype>      <controlparamfield>      </controlparamfield>      <displaytext>      </displaytext>      <editactionurl>      </editactionurl>      <editparamfield>      </editparamfield>      <deleteactionurl>      </deleteactionurl>      <deleteparamfield>      </deleteparamfield>      <viewactionurl>      </viewactionurl>      <viewparamfield>      </viewparamfield>      <imageactionurl>      </imageactionurl>      <imageparamfield>      </imageparamfield>      <manageactionurl>      </manageactionurl>      <manageparamfield>      </manageparamfield>      <copyactionurl>      </copyactionurl>      <copyparamfield>      </copyparamfield>      <xaxis>n</xaxis>      <yaxis>n</yaxis>      <isadvancesearch>y</isadvancesearch>      <Class>      </Class>      <SearchControlType>--Select--</SearchControlType>      <SearchControlParameters>      </SearchControlParameters>      <DbParamField>      </DbParamField>      <useMode>DataBase</useMode>      <IsGraph>n</IsGraph>      <allowdetailview>n</allowdetailview>    </column>    <column>      <id>6</id>      <name>ProductType</name>      <headertext>Product Type</headertext>      <width>30</width>      <datatype>String</datatype>      <columntype>String</columntype>      <allowsorting>true</allowsorting>      <allowpaging>true</allowpaging>      <format>      </format>      <isvisible>y</isvisible>      <mustshow>n</mustshow>      <musthide>n</musthide>      <maxlength>0</maxlength>      <isallowsearch>y</isallowsearch>      <isconditional>n</isconditional>      <isallowlink>n</isallowlink>      <islinkactionurl>      </islinkactionurl>      <islinkparamfield>      </islinkparamfield>      <ischeckbox>n</ischeckbox>      <checkboxparamfield>      </checkboxparamfield>      <iscontrol>n</iscontrol>      <controltype>      </controltype>      <controlparamfield>      </controlparamfield>      <displaytext>      </displaytext>      <editactionurl>      </editactionurl>      <editparamfield>      </editparamfield>      <deleteactionurl>      </deleteactionurl>      <deleteparamfield>      </deleteparamfield>      <viewactionurl>      </viewactionurl>      <viewparamfield>      </viewparamfield>      <imageactionurl>      </imageactionurl>      <imageparamfield>      </imageparamfield>      <manageactionurl>      </manageactionurl>      <manageparamfield>      </manageparamfield>      <copyactionurl>      </copyactionurl>      <copyparamfield>      </copyparamfield>      <xaxis>n</xaxis>      <yaxis>n</yaxis>      <isadvancesearch>y</isadvancesearch>      <Class>      </Class>      <SearchControlType>--Select--</SearchControlType>      <SearchControlParameters>      </SearchControlParameters>      <DbParamField>      </DbParamField>      <useMode>DataBase</useMode>      <IsGraph>n</IsGraph>      <allowdetailview>n</allowdetailview>    </column>    <column>      <id>7</id>      <name>SKU</name>      <headertext>SKU</headertext>      <width>30</width>      <datatype>String</datatype>      <columntype>String</columntype>      <allowsorting>true</allowsorting>      <allowpaging>true</allowpaging>      <format>      </format>      <isvisible>y</isvisible>      <mustshow>y</mustshow>      <musthide>n</musthide>      <maxlength>0</maxlength>      <isallowsearch>y</isallowsearch>      <isconditional>n</isconditional>      <isallowlink>n</isallowlink>      <islinkactionurl>      </islinkactionurl>      <islinkparamfield>      </islinkparamfield>      <ischeckbox>n</ischeckbox>      <checkboxparamfield>      </checkboxparamfield>      <iscontrol>n</iscontrol>      <controltype>      </controltype>      <controlparamfield>      </controlparamfield>      <displaytext>SKU</displaytext>      <editactionurl>      </editactionurl>      <editparamfield>      </editparamfield>      <deleteactionurl>      </deleteactionurl>      <deleteparamfield>      </deleteparamfield>      <viewactionurl>      </viewactionurl>      <viewparamfield>      </viewparamfield>      <imageactionurl>      </imageactionurl>      <imageparamfield>      </imageparamfield>      <manageactionurl>      </manageactionurl>      <manageparamfield>      </manageparamfield>      <copyactionurl>      </copyactionurl>      <copyparamfield>      </copyparamfield>      <xaxis>n</xaxis>      <yaxis>n</yaxis>      <isadvancesearch>y</isadvancesearch>      <Class>      </Class>      <SearchControlType>--Select--</SearchControlType>      <SearchControlParameters>      </SearchControlParameters>      <DbParamField>      </DbParamField>      <useMode>DataBase</useMode>      <IsGraph>n</IsGraph>      <allowdetailview>n</allowdetailview>    </column>    <column>      <id>8</id>      <name>AvailableInventory</name>      <headertext>Available Inventory</headertext>      <width>30</width>      <datatype>String</datatype>      <columntype>String</columntype>      <allowsorting>false</allowsorting>      <allowpaging>false</allowpaging>      <format>      </format>      <isvisible>y</isvisible>      <mustshow>y</mustshow>      <musthide>n</musthide>      <maxlength>0</maxlength>      <isallowsearch>y</isallowsearch>      <isconditional>n</isconditional>      <isallowlink>n</isallowlink>      <islinkactionurl>      </islinkactionurl>      <islinkparamfield>      </islinkparamfield>      <ischeckbox>n</ischeckbox>      <checkboxparamfield>      </checkboxparamfield>      <iscontrol>n</iscontrol>      <controltype>      </controltype>      <controlparamfield>      </controlparamfield>      <displaytext>SKU</displaytext>      <editactionurl>      </editactionurl>      <editparamfield>      </editparamfield>      <deleteactionurl>      </deleteactionurl>      <deleteparamfield>      </deleteparamfield>      <viewactionurl>      </viewactionurl>      <viewparamfield>      </viewparamfield>      <imageactionurl>      </imageactionurl>      <imageparamfield>      </imageparamfield>      <manageactionurl>      </manageactionurl>      <manageparamfield>      </manageparamfield>      <copyactionurl>      </copyactionurl>      <copyparamfield>      </copyparamfield>      <xaxis>n</xaxis>      <yaxis>n</yaxis>      <isadvancesearch>y</isadvancesearch>      <Class>      </Class>      <SearchControlType>--Select--</SearchControlType>      <SearchControlParameters>      </SearchControlParameters>      <DbParamField>      </DbParamField>      <useMode>DataBase</useMode>      <IsGraph>n</IsGraph>      <allowdetailview>n</allowdetailview>    </column>    <column>      <id>9</id>      <name>Assortment</name>      <headertext>Assortment</headertext>      <width>0</width>      <datatype>String</datatype>      <columntype>String</columntype>      <allowsorting>false</allowsorting>      <allowpaging>false</allowpaging>      <format>      </format>      <isvisible>n</isvisible>      <mustshow>n</mustshow>      <musthide>y</musthide>      <maxlength>0</maxlength>      <isallowsearch>y</isallowsearch>      <isconditional>n</isconditional>      <isallowlink>n</isallowlink>      <islinkactionurl>      </islinkactionurl>      <islinkparamfield>      </islinkparamfield>      <ischeckbox>n</ischeckbox>      <checkboxparamfield>      </checkboxparamfield>      <iscontrol>n</iscontrol>      <controltype>      </controltype>      <controlparamfield>      </controlparamfield>      <displaytext>      </displaytext>      <editactionurl>      </editactionurl>      <editparamfield>      </editparamfield>      <deleteactionurl>      </deleteactionurl>      <deleteparamfield>      </deleteparamfield>      <viewactionurl>      </viewactionurl>      <viewparamfield>      </viewparamfield>      <imageactionurl>      </imageactionurl>      <imageparamfield>      </imageparamfield>      <manageactionurl>      </manageactionurl>      <manageparamfield>      </manageparamfield>      <copyactionurl>      </copyactionurl>      <copyparamfield>      </copyparamfield>      <xaxis>n</xaxis>      <yaxis>n</yaxis>      <isadvancesearch>y</isadvancesearch>      <Class>      </Class>      <SearchControlType>--Select--</SearchControlType>      <SearchControlParameters>      </SearchControlParameters>      <DbParamField>      </DbParamField>      <useMode>DataBase</useMode>      <IsGraph>n</IsGraph>      <allowdetailview>n</allowdetailview>    </column>    <column>      <id>10</id>      <name>DisplayOrder</name>      <headertext>Display Order</headertext>      <width>10</width>      <datatype>Int32</datatype>      <columntype>Int32</columntype>      <allowsorting>true</allowsorting>      <allowpaging>true</allowpaging>      <format>      </format>      <isvisible>y</isvisible>      <mustshow>n</mustshow>      <musthide>n</musthide>      <maxlength>5</maxlength>      <isallowsearch>n</isallowsearch>      <isconditional>n</isconditional>      <isallowlink>n</isallowlink>      <islinkactionurl>      </islinkactionurl>      <islinkparamfield>      </islinkparamfield>      <ischeckbox>n</ischeckbox>      <checkboxparamfield>      </checkboxparamfield>      <iscontrol>y</iscontrol>      <controltype>Text</controltype>      <controlparamfield>      </controlparamfield>      <displaytext>      </displaytext>      <editactionurl>      </editactionurl>      <editparamfield>      </editparamfield>      <deleteactionurl>      </deleteactionurl>      <deleteparamfield>      </deleteparamfield>      <viewactionurl>      </viewactionurl>      <viewparamfield>      </viewparamfield>      <imageactionurl>      </imageactionurl>      <imageparamfield>      </imageparamfield>      <manageactionurl>      </manageactionurl>      <manageparamfield>      </manageparamfield>      <copyactionurl>      </copyactionurl>      <copyparamfield>      </copyparamfield>      <xaxis>n</xaxis>      <yaxis>n</yaxis>      <isadvancesearch>y</isadvancesearch>      <Class>      </Class>      <SearchControlType>Text</SearchControlType>      <SearchControlParameters>      </SearchControlParameters>      <DbParamField>      </DbParamField>      <useMode>DataBase</useMode>      <IsGraph>n</IsGraph>      <allowdetailview>n</allowdetailview>    </column>    <column>      <id>11</id>      <name>BundleQuantity</name>      <headertext>Quantity</headertext>      <width>10</width>      <datatype>Int32</datatype>      <columntype>Int32</columntype>      <allowsorting>false</allowsorting>      <allowpaging>true</allowpaging>      <format>      </format>      <isvisible>y</isvisible>      <mustshow>n</mustshow>      <musthide>n</musthide>      <maxlength>5</maxlength>      <isallowsearch>n</isallowsearch>      <isconditional>n</isconditional>      <isallowlink>n</isallowlink>      <islinkactionurl>      </islinkactionurl>      <islinkparamfield>      </islinkparamfield>      <ischeckbox>n</ischeckbox>      <checkboxparamfield>      </checkboxparamfield>      <iscontrol>y</iscontrol>      <controltype>Text</controltype>      <controlparamfield>      </controlparamfield>      <displaytext>      </displaytext>      <editactionurl>      </editactionurl>      <editparamfield>      </editparamfield>      <deleteactionurl>      </deleteactionurl>      <deleteparamfield>      </deleteparamfield>      <viewactionurl>      </viewactionurl>      <viewparamfield>      </viewparamfield>      <imageactionurl>      </imageactionurl>      <imageparamfield>      </imageparamfield>      <manageactionurl>      </manageactionurl>      <manageparamfield>      </manageparamfield>      <copyactionurl>      </copyactionurl>      <copyparamfield>      </copyparamfield>      <xaxis>n</xaxis>      <yaxis>n</yaxis>      <isadvancesearch>y</isadvancesearch>      <Class>      </Class>      <SearchControlType>Text</SearchControlType>      <SearchControlParameters>      </SearchControlParameters>      <DbParamField>      </DbParamField>      <useMode>DataBase</useMode>      <IsGraph>n</IsGraph>      <allowdetailview>n</allowdetailview>    </column>    <column>      <id>12</id>      <name>Manage</name>      <headertext>Action</headertext>      <width>30</width>      <datatype>String</datatype>      <columntype>String</columntype>      <allowsorting>false</allowsorting>      <allowpaging>true</allowpaging>      <format>Edit|Delete</format>      <isvisible>y</isvisible>      <mustshow>y</mustshow>      <musthide>n</musthide>      <maxlength>0</maxlength>      <isallowsearch>n</isallowsearch>      <isconditional>n</isconditional>      <isallowlink>n</isallowlink>      <islinkactionurl>      </islinkactionurl>      <islinkparamfield>      </islinkparamfield>      <ischeckbox>n</ischeckbox>      <checkboxparamfield>      </checkboxparamfield>      <iscontrol>n</iscontrol>      <controltype>      </controltype>      <controlparamfield>      </controlparamfield>      <displaytext>Edit|Delete</displaytext>      <editactionurl>      </editactionurl>      <editparamfield>      </editparamfield>      <deleteactionurl>      </deleteactionurl>      <deleteparamfield>      </deleteparamfield>      <viewactionurl>      </viewactionurl>      <viewparamfield>      </viewparamfield>      <imageactionurl>      </imageactionurl>      <imageparamfield>      </imageparamfield>      <manageactionurl>/PIM/Products/UpdateAssociatedProducts|/PIM/Products/UnassociateProducts</manageactionurl>      <manageparamfield>PimProductTypeAssociationId,PimProductId,RelatedProductId|PimProductTypeAssociationId,PimProductId</manageparamfield>      <copyactionurl>      </copyactionurl>      <copyparamfield>      </copyparamfield>      <xaxis>n</xaxis>      <yaxis>n</yaxis>      <isadvancesearch>y</isadvancesearch>      <Class>      </Class>      <SearchControlType>--Select--</SearchControlType>      <SearchControlParameters>      </SearchControlParameters>      <DbParamField>      </DbParamField>      <useMode>DataBase</useMode>      <IsGraph>n</IsGraph>      <allowdetailview>n</allowdetailview>    </column>  </columns>',
'AssociatedBundleProducts','AssociatedBundleProducts','AssociatedBundleProducts',0,null,null,null,null,2,GETDATE(),2,GETDATE()
WHERE NOT EXISTS(SELECT * FROM ZnodeApplicationSetting WHERE ItemName = 'View_ManageProductTypeList_BundleProduct')

UPDATE ZnodeApplicationSetting SET Setting='<?xml version="1.0" encoding="utf-16"?><columns><column><id>1</id><name>PimProductTypeAssociationId</name><headertext>Checkbox</headertext><width>20</width><datatype>Int32</datatype><columntype>Int32</columntype><allowsorting>false</allowsorting><allowpaging>true</allowpaging><format></format><isvisible>y</isvisible><mustshow>y</mustshow><musthide>n</musthide><maxlength>0</maxlength><isallowsearch>n</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>y</ischeckbox><checkboxparamfield>PimProductTypeAssociationId</checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext></displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>2</id><name>RelatedProductId</name><headertext>RelatedProductId</headertext><width>20</width><datatype>Int32</datatype><columntype>Int32</columntype><allowsorting>false</allowsorting><allowpaging>true</allowpaging><format></format><isvisible>n</isvisible><mustshow>n</mustshow><musthide>y</musthide><maxlength>0</maxlength><isallowsearch>n</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield>PimProductTypeAssociationId</checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext></displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>3</id><name>PimProductId</name><headertext>ID</headertext><width>30</width><datatype>Int32</datatype><columntype>Int32</columntype><allowsorting>false</allowsorting><allowpaging>true</allowpaging><format></format><isvisible>y</isvisible><mustshow>y</mustshow><musthide>n</musthide><maxlength>0</maxlength><isallowsearch>n</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext></displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>4</id><name>Image</name><headertext>Image</headertext><width>20</width><datatype>String</datatype><columntype>String</columntype><allowsorting>false</allowsorting><allowpaging>false</allowpaging><format></format><isvisible>y</isvisible><mustshow>n</mustshow><musthide>n</musthide><maxlength>0</maxlength><isallowsearch>n</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext></displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield>ProductImage,ProductName</imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class>imageicon</Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>5</id><name>ProductName</name><headertext>Product Name</headertext><width>60</width><datatype>String</datatype><columntype>String</columntype><allowsorting>true</allowsorting><allowpaging>true</allowpaging><format></format><isvisible>y</isvisible><mustshow>y</mustshow><musthide>n</musthide><maxlength>0</maxlength><isallowsearch>y</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext></displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>6</id><name>ProductType</name><headertext>Product Type</headertext><width>30</width><datatype>String</datatype><columntype>String</columntype><allowsorting>true</allowsorting><allowpaging>true</allowpaging><format></format><isvisible>y</isvisible><mustshow>n</mustshow><musthide>n</musthide><maxlength>0</maxlength><isallowsearch>y</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext></displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>7</id><name>SKU</name><headertext>SKU</headertext><width>30</width><datatype>String</datatype><columntype>String</columntype><allowsorting>true</allowsorting><allowpaging>true</allowpaging><format></format><isvisible>y</isvisible><mustshow>y</mustshow><musthide>n</musthide><maxlength>0</maxlength><isallowsearch>y</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext>SKU</displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>8</id><name>AvailableInventory</name><headertext>Available Inventory</headertext><width>30</width><datatype>String</datatype><columntype>String</columntype><allowsorting>false</allowsorting><allowpaging>false</allowpaging><format></format><isvisible>y</isvisible><mustshow>y</mustshow><musthide>n</musthide><maxlength>0</maxlength><isallowsearch>y</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext>SKU</displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>9</id><name>Assortment</name><headertext>Assortment</headertext><width>0</width><datatype>String</datatype><columntype>String</columntype><allowsorting>false</allowsorting><allowpaging>false</allowpaging><format></format><isvisible>n</isvisible><mustshow>n</mustshow><musthide>y</musthide><maxlength>0</maxlength><isallowsearch>y</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext></displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>10</id><name>DisplayOrder</name><headertext>Display Order</headertext><width>10</width><datatype>Int32</datatype><columntype>Int32</columntype><allowsorting>true</allowsorting><allowpaging>true</allowpaging><format></format><isvisible>y</isvisible><mustshow>n</mustshow><musthide>n</musthide><maxlength>5</maxlength><isallowsearch>n</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>y</iscontrol><controltype>Text</controltype><controlparamfield></controlparamfield><displaytext></displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>Text</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>12</id><name>Manage</name><headertext>Action</headertext><width>30</width><datatype>String</datatype><columntype>String</columntype><allowsorting>false</allowsorting><allowpaging>true</allowpaging><format>Edit|Delete</format><isvisible>y</isvisible><mustshow>y</mustshow><musthide>n</musthide><maxlength>0</maxlength><isallowsearch>n</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext>Edit|Delete</displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl>/PIM/Products/UpdateAssociatedProducts|/PIM/Products/UnassociateProducts</manageactionurl><manageparamfield>PimProductTypeAssociationId,PimProductId,RelatedProductId|PimProductTypeAssociationId,PimProductId</manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column></columns>'
WHERE ItemName = 'View_ManageProductTypeList_GroupProduct'

GO
--------------------------------------------
IF EXISTS(SELECT * FROM SYS.PROCEDURES WHERE NAME = 'Znode_GetBundleProductAssociatedChildQuantity')
	DROP PROC Znode_GetBundleProductAssociatedChildQuantity
GO
CREATE PROCEDURE [dbo].[Znode_GetBundleProductAssociatedChildQuantity]      
(       
	@SKU VARCHAR(max) = NULL,    
	@versionid INT =0,    
	@localeId INT =0,    
	@publishCatalogId INT =0,    
	@PortalId INT =1    
)      
AS      
/*      
 Sample execute query    
 Exec Znode_GetBundleProductAssociatedChildQuantity @SKU='E4-48221', @versionid=12, @localeId=1, @publishCatalogId=3 ,@portalid=1  
      
*/      
 BEGIN      
BEGIN TRY      
SET NOCOUNT ON;           
       
       
	-- This sp is used to provide parent sku's child sku and bundlle quantity and remaining quantity    
	IF OBJECT_ID('tempdb..#SKU') IS NOT NULL 
		DROP TABLE #SKU    
        
	SELECT * INTO #SKU FROM DBO.Split(@SKU, ',') AS Sp    
    
	IF OBJECT_ID('tempdb..#ParentData') IS NOT NULL 
		DROP TABLE #ParentData    
        
	SELECT * INTO #ParentData from ZnodePublishProductEntity 
	WHERE SKU IN (SELECT item FROM #SKU)    
    
	IF OBJECT_ID('tempdb..#Parentchilddate') IS NOT NULL 
		DROP TABLE #Parentchilddate    
       
	SELECT p.ZnodeProductId,SKU Parentsku,AssociatedZnodeProductId,AssociatedProductBundleQuantity      
	INTO #Parentchilddate    
	FROM ZnodePublishBundleProductEntity  ZPBPE    
	INNER JOIN #ParentData p on p.ZnodeProductId=  ZPBPE.ZnodeProductId    
	WHERE ZPBPE.VersionId=@versionid AND ZPBPE.ZnodeCatalogId = @publishCatalogId    
    
	SELECT DISTINCT PCD.ZnodeProductId ParentPublishProductId,PCD.Parentsku ParentBundleSKU, PCD.AssociatedZnodeProductId PublishProductId, 
		ZPPE.SKU SKU, COALESCE(CAST(PCD.AssociatedProductBundleQuantity as DECIMAL(9,2)),0) AssociatedQuantity, ZPPE.[Name], 
        ZPPE.Attributes Attribute , ZI1.Quantity Quantity    
    FROM #Parentchilddate PCD    
    INNER JOIN ZnodePublishProductEntity ZPPE ON ZPPE.ZnodeProductId = PCD.AssociatedZnodeProductId    
    LEFT JOIN (SELECT ZI.* FROM  ZnodePortalWarehouse ZPW INNER JOIN ZnodeInventory ZI on ZPW.PortalId =@PortalId  and ZI.WarehouseId =ZPW.WarehouseId ) ZI1  
     ON ZI1.SKU = ZPPE.SKU    
    WHERE ZPPE.VersionId=@versionid AND ZPPE.LocaleId=@localeId AND ZPPE.ZnodeCatalogId=@publishCatalogId    
       
END TRY      
BEGIN CATCH      
	DECLARE @Status BIT ;      
	SET @Status = 0;      
	DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(), @ErrorCall NVARCHAR(MAX)= 'EXEC Znode_GetBundleProductAssociatedChildQuantity @SKU = '+ @SKU +' ';      
                        
		SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                          
          
		EXEC Znode_InsertProcedureErrorLog      
	@ProcedureName = 'Znode_GetBundleProductAssociatedChildQuantity',      
	@ErrorInProcedure = @Error_procedure,      
	@ErrorMessage = @ErrorMessage,      
	@ErrorLine = @ErrorLine,      
	@ErrorCall = @ErrorCall;      
END CATCH      
END

GO
--------------------------------------------
IF EXISTS(SELECT * FROM SYS.PROCEDURES WHERE NAME = 'Znode_ManageProductList_XML')
	DROP PROC Znode_ManageProductList_XML
GO
CREATE  PROCEDURE [dbo].[Znode_ManageProductList_XML]
(   @WhereClause						 XML,
    @Rows								 INT           = 100,
    @PageNo								 INT           = 1,
    @Order_BY			 VARCHAR(1000) = '',
    @LocaleId			 INT           = 1,
    @PimProductId		 VARCHAR(2000) = 0,
    @IsProductNotIn	 BIT           = 0,
	@IsCallForAttribute BIT		   = 0,
	@AttributeCode      VARCHAR(max ) = '' ,
	@PimCatalogId   INT = 0,
	@IsCatalogFilter   BIT            = 0,
	@IsDebug            Bit		   = 0 )
AS
    
/*
		  Summary:-   This Procedure is used for get product List  
				    Procedure will pivot verticle table(ZnodePimattributeValues) into horizontal table with columns 
				    ProductId,ProductName,ProductType,AttributeFamily,SKU,Price,Quantity,IsActive,ImagePath,Assortment,LocaleId,DisplayOrder
        
		  Unit Testing
		  
exec Znode_ManageProductList_XML @WhereClause=N'',@Rows=50,@PageNo=1,@Order_By=N'',@LocaleId=1,@PimProductId=N'0,1',@IsProductNotIn=1,@IsCallForAttribute=0,@AttributeCode=''
          select * from ZnodeAttributeType  WHERE AttributeValue LIKE '%&%'
		  UPDATE VieW_lOADMANAGEpRODUCT SET  AttributeValue = 'A & B'  WHERE AttributeValue LIKE '% and %' AND PimProductId = 158
    */

     BEGIN
         SET NOCOUNT ON;
         BEGIN TRY
		 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 
             DECLARE @PimProductIds TransferId, --VARCHAR(MAX), 
					 @FirstWhereClause NVARCHAR(MAX)= '', 
					 @SQL NVARCHAR(MAX)= '' ,
					 @OutPimProductIds VARCHAR(max),
					 @ProductXML NVARCHAR(max) ;

             DECLARE @DefaultLocaleId INT= dbo.Fn_GetDefaultLocaleId()
					 ,@RowsCount INT =0 ;
             DECLARE @TransferPimProductId TransferId 
			 DECLARE @TBL_AttributeDefaultValue TABLE
             (PimAttributeId            INT,
              AttributeDefaultValueCode VARCHAR(100),
              IsEditable                BIT,
              AttributeDefaultValue     NVARCHAR(MAX)
			  ,DisplayOrder INT 
			  ,PimAttributedefaultValueId INT 
             );
             DECLARE @TBL_AttributeDetails AS TABLE
             (PimProductId   INT,
              AttributeValue NVARCHAR(MAX),
              AttributeCode  VARCHAR(600),
              PimAttributeId INT,
			  AttributeDefaultValue NVARCHAR(MAX)
             );
			 Create table #TBL_AttributeDetailsLocale
             (PimProductId   INT,
              AttributeValue NVARCHAR(MAX),
              AttributeCode  VARCHAR(600),
              PimAttributeId INT
             );
			 DECLARE @TBL_MultiSelectAttribute TABLE (PimAttributeId INT , AttributeCode VARCHAR(600))
			
			 DECLARE @TBL_MediaAttribute TABLE (Id INT ,PimAttributeId INT ,AttributeCode VARCHAR(600) )
			 
			 DECLARE @TBL_ProductIds TABLE 
			 (
			  PimProductId INT,
			  ModifiedDate DATETIME  
			 )

			 DECLARE @FamilyDetails TABLE
             (
			  PimProductId         INT,
              PimAttributeFamilyId INT,
              FamilyName           NVARCHAR(Max)
             );
             DECLARE @DefaultAttributeFamily INT= dbo.Fn_GetDefaultPimProductFamilyId();
             DECLARE @ProductIdTable TABLE
             (PimProductId INT,
              CountId      INT,
              RowId        INT IDENTITY(1,1)
             );
          		
             IF EXISTS ( SELECT TOP 1 1 FROM @WhereClause.nodes ( '//ArrayOfWhereClauseModel/WhereClauseModel'  ) AS Tbl(Col)
			 WHERE LTRIM(RTRIM((REPLACE(REPLACE(Tbl.Col.value ( 'attributecode[1]' , 'NVARCHAR(max)'),' = ',''),'''',''))))  =  'Brand'
                OR LTRIM(RTRIM((REPLACE(REPLACE(Tbl.Col.value ( 'attributecode[1]' , 'NVARCHAR(max)'),' = ',''),'''','')))) = 'Vendor'
                OR LTRIM(RTRIM((REPLACE(REPLACE(Tbl.Col.value ( 'attributecode[1]' , 'NVARCHAR(max)'),' = ',''),'''',''))))  =  'ShippingCostRules'
                OR LTRIM(RTRIM((REPLACE(REPLACE(Tbl.Col.value ( 'attributecode[1]' , 'NVARCHAR(max)'),' = ',''),'''','')))) =  'Highlights') and @IsCallForAttribute=1
                 BEGIN
                DECLARE @AttributeCodeValue TABLE (AttributeValue NVARCHAr(max),AttributeCode NVARCHAR(max))

				INSERT INTO @AttributeCodeValue(AttributeValue,AttributeCode)
				SELECT  Tbl.Col.value ( 'attributevalue[1]' , 'NVARCHAR(max)') AS AttributeValue
						 ,Tbl.Col.value ( 'attributecode[1]' , 'NVARCHAR(max)') AS AttributeCode
				FROM @WhereClause.nodes ( '//ArrayOfWhereClauseModel/WhereClauseModel'  ) AS Tbl(Col)
				WHERE LTRIM(RTRIM((REPLACE(REPLACE(Tbl.Col.value ( 'attributecode[1]' , 'NVARCHAR(max)'),' = ',''),'''',''))))  =  'Brand'
                OR LTRIM(RTRIM((REPLACE(REPLACE(Tbl.Col.value ( 'attributecode[1]' , 'NVARCHAR(max)'),' = ',''),'''',''))))  = 'Vendor'
                OR LTRIM(RTRIM((REPLACE(REPLACE(Tbl.Col.value ( 'attributecode[1]' , 'NVARCHAR(max)'),' = ',''),'''',''))))  =  'ShippingCostRules'
                OR LTRIM(RTRIM((REPLACE(REPLACE(Tbl.Col.value ( 'attributecode[1]' , 'NVARCHAR(max)'),' = ',''),'''',''))))  =  'Highlights'
		
				SET @SQL =   
				           ';WIth Cte_DefaultValue AS (
										  SELECT AttributeDefaultValueCode , ZPDF.PimAttributeId ,FNPA.AttributeCode
										  FROM ZnodePImAttributeDefaultValue ZPDF
										  INNER JOIN [dbo].[Fn_GetProductDefaultFilterAttributes] () FNPA ON ( FNPA.PimAttributeId = ZPDF.PimAttributeId) 
										)
										, Cte_productIds AS 
										(
										  SELECT a.PimProductId, c.AttributeCode , CTDV.AttributeDefaultValueCode AttributeValue,b.ModifiedDate 
										  FROM  ZnodePimAttributeValue a
										  LEFT JOIN ZnodePimAttribute c ON(c.PimAttributeId = a.PimAttributeId)
										  LEFT JOIN ZnodePimAttributeValueLocale b ON(b.PimAttributeValueId = a.PimAttributeValueId)  
										  INNER JOIN Cte_DefaultValue CTDV ON (CTDV.AttributeCode = c.AttributeCode 
										  AND EXISTS (SELECT TOP 1 1 FROM dbo.split(b.AttributeValue,'','') SP WHERE SP.Item = CTDV.AttributeDefaultValueCode) )
										  Union all 
										  
											SELECT a.PimProductId,c.AttributeCode,ZPADV.AttributeDefaultValueCode AttributeValue ,a.ModifiedDate 
											FROM ZnodePimProductAttributeDefaultValue ZPPADV
											INNER JOIN ZnodePimAttributeDefaultValue ZPADV ON (ZPPADV.PimAttributeDefaultValueId = ZPADV.PimAttributeDefaultValueId)
											LEFT JOIN ZnodePimAttributeValue a ON (a.PimAttributeValueId = ZPPADV.PimAttributeValueId )
											LEFT JOIN ZnodePimAttribute c ON ( c.PimAttributeId=a.PimAttributeId )
											INNER JOIN Cte_DefaultValue CTDV ON (CTDV.AttributeCode = c.AttributeCode )
										)										
										SELECT PimProductId ,ModifiedDate
										FROM Cte_productIds WHERE  AttributeCode '+(SELECT TOP 1 AttributeCode  FROM @AttributeCodeValue )+' AND 
										AttributeValue '+(SELECT TOP 1 AttributeValue  FROM @AttributeCodeValue )+' 
										GROUP BY PimProductId,ModifiedDate Order By ModifiedDate DESC ';


					 SET @Order_BY = CASE WHEN @Order_BY = '' THEN 'ModifiedDate DESC' ELSE @Order_BY END 
					 	
					 SET @WhereClause = CAST(REPLACE(CAST(@WhereClause AS NVARCHAR(max)),'<WhereClauseModel><attributecode>'+(SELECT TOP 1 AttributeCode  FROM @AttributeCodeValue )+'</attributecode><attributevalue>'+(SELECT TOP 1 AttributeValue   FROM @AttributeCodeValue )+'</attributevalue></WhereClauseModel>','') AS XML )
					
				     INSERT INTO @TBL_ProductIds ( PimProductId, ModifiedDate )
					 EXEC (@SQL);
			  					 
					
						 INSERT INTO @ProductIdTable( PimProductId )
						 SELECT PimProductId 
						 FROM @TBL_ProductIds
					

                     INSERT INTO @TransferPimProductId
					 SELECT PimProductId
                     FROM @ProductIdTable
                   
				   			  
     DELETE FROM @ProductIdTable;
   --  SET @WhereClause = CAST(REPLACE(CAST(@WhereClause AS NVARCHAR(MAX)), @FirstWhereClause, ' 1 = 1') AS XML);
                 END
	            ELSE IF @PimProductId <> ''
			    BEGIN 
		
				 INSERT INTO @TransferPimProductId(id)
				 SELECT Item 
				 FROM dbo.split(@PimProductId,',')
			    END 
		
			
	 DECLARE  @ProductListIdRTR TransferId
	 DECLARE @TAb Transferid 
	 --DECLARE @tBL_mainList TABLE (Id INT,RowId INT)
	 Create table #TBL_ProductMainList (Id INT,RowId INT)

	 	IF @PimProductId <> ''  OR   @IsCallForAttribute=1 --OR (CAST(@WhereClause AS NVARCHAR(max))= N'' AND @Order_by <> N'' AND @AttributeCode = N'')
		BEGIN 
	 SET @IsProductNotIn = CASE WHEN @IsProductNotIn = 0 THEN 1  
					 WHEN @IsProductNotIn = 1 THEN 0 END 
		END 
	
	 INSERT INTO @ProductListIdRTR
	 EXEC Znode_GetProductList  @IsProductNotIn,@TransferPimProductId, @PimCatalogId,@IsCatalogFilter
 
	 IF CAST(@WhereClause AS NVARCHAR(max))<> N''
	 BEGIN 
	 
	  SET @SQL = 'SELECT Distinct PimProductId FROM ##Temp_PimProductId'+CAST(@@SPID AS VARCHAR(500))

	  EXEC Znode_GetFilterPimProductId @WhereClause,@ProductListIdRTR,@localeId
	  
      INSERT INTO @TAB 
	  EXEC (@SQL)
	 
	 END 
	 
	

	 IF EXISTS (SELECT Top 1 1 FROM @TAb ) OR CAST(@WhereClause AS NVARCHAR(max)) <> N''
	 BEGIN 
	 
	 SET @AttributeCode = REPLACE(dbo.FN_TRIM(REPLACE(REPLACE(@order_by,' DESC',''),' ASC','')),'DisplayOrder','ProductName')
	 SET @order_by = REPLACE(@order_by,'DisplayOrder','ProductName')
	 INSERT INTO #TBL_ProductMainList(id,RowId)
	 EXEC Znode_GetOrderByPagingProduct @order_by,@rows,@PageNo, @TAb ,@AttributeCode,@localeId
	 
	 END 
	 ELSE 
	 BEGIN
	      
	 SET @AttributeCode = REPLACE(dbo.FN_TRIM(REPLACE(REPLACE(@order_by,' DESC',''),' ASC','')),'DisplayOrder','ProductName')
	 SET @order_by = REPLACE(@order_by,'DisplayOrder','ProductName')
	 INSERT INTO #TBL_ProductMainList(id,RowId)
	 EXEC Znode_GetOrderByPagingProduct @order_by,@rows,@PageNo, @ProductListIdRTR ,@AttributeCode,@localeId 
	 END 
          

  			 INSERT INTO @PimProductIds ( Id  )
			 SELECT DISTINCT id FROM #TBL_ProductMainList

			 DECLARE @TBL_PimProductIds transferId 
			 INSERT INTO @TBL_PimProductIds
			 SELECT id 
             FROM @PimProductIds
			 			 	
			 DECLARE @PimAttributeIds TransferId  
			 INSERT INTO @PimAttributeIds
			 SELECT PimAttributeId  
			 FROM [dbo].[Fn_GetProductGridAttributes]()
			 
			

			 INSERT INTO @TBL_AttributeDetails
             (PimProductId,
              AttributeValue,
              AttributeCode,
              PimAttributeId,
			  AttributeDefaultValue
             )
             EXEC Znode_GetProductsAttributeValue_newTesting
                  @TBL_PimProductIds,
                  @PimAttributeIds,
                  @localeId;
			
			
			UPDATE @TBL_AttributeDetails
			SET AttributeValue = ISNULL(AttributeValue,'')
			WHERE AttributeValue IS NULL 

----------------------------------------------------------------------------------------------------

			

		    declare @SKU SelectColumnList
			declare @TBL_Inventorydetails table (Quantity NVARCHAR(MAx),PimProductId INT)

			INSERT INTO @SKU
			SELECT AttributeValue 
			FROM @TBL_AttributeDetails
			WHERE AttributeCode = 'SKU'
 
 			INSERT INTO @TBL_Inventorydetails(Quantity,PimProductId)
			EXEC Znode_GetPimProductAttributeInventory @SKU--vishal

			 INSERT INTO @FamilyDetails
             (PimAttributeFamilyId,
              PimProductId
             )
             EXEC [dbo].[Znode_GetPimProductAttributeFamilyId]
                  @PimProductIds,
                  1;
             
		 UPDATE a
               SET
                   FamilyName = b.AttributeFamilyName
             FROM @FamilyDetails a
                  INNER JOIN ZnodePimFamilyLocale b ON(a.PimAttributeFamilyId = b.PimAttributeFamilyId
                                                       AND LocaleId = @LocaleId);
             UPDATE a
               SET
                   FamilyName = b.AttributeFamilyName
             FROM @FamilyDetails a
                  INNER JOIN ZnodePimFamilyLocale b ON(a.PimAttributeFamilyId = b.PimAttributeFamilyId
                                                       AND LocaleId = @DefaultLocaleId)
             WHERE a.FamilyName IS NULL
                   OR a.FamilyName = '';
           	
			INSERT INTO @TBL_AttributeDetails             (PimProductId,              AttributeValue,              AttributeCode,              PimAttributeId             )
			SELECT PimProductId ,FamilyName, 'AttributeFamily',NULL
			FROM @FamilyDetails 
			
			INSERT INTO @TBL_AttributeDetails             (PimProductId,              AttributeValue,              AttributeCode,              PimAttributeId             )
			SELECT a.ID PimProductId ,th.DisplayName, 'PublishStatus',NULL
			FROM @PimProductIds a 
			INNER JOIN ZnodePimProduct b ON (b.PimProductId = a.ID)
			LEFT JOIN ZnodePublishState th ON (th.PublishStateId = b.PublishStateId)

	  INSERT INTO #TBL_AttributeDetailsLocale (PimProductId ,PimAttributeId,AttributeCode )
			SELECT  TBLAD.PimProductId ,TBLAD.PimAttributeId,TBLAD.AttributeCode 
			FROM @TBL_AttributeDetails TBLAD 
			GROUP BY  TBLAD.PimProductId ,TBLAD.PimAttributeId,TBLAD.AttributeCode 
       					

	    UPDATE TBLPP 
		SET AttributeValue = CTDD.AttributeValue 
		FROM  @TBL_AttributeDetails CTDD 
		INNER JOIN #TBL_AttributeDetailsLocale TBLPP ON (TBLPP.PimProductId = CTDD.PimProductId AND TBLPP.AttributeCode  = CTDD.AttributeCode)
		WHERE TBLPP.AttributeValue IS NULL 

    	SET @ProductXML =  '<MainProduct>'+ STUFF( (  SELECT '<Product>'+'<PimProductId>'+CAST(TBAD.PimProductId AS VARCHAR(50))+'</PimProductId>'
																		+'<AvailableInventory>'+CAST(ISNULL(IDD.[Quantity],'') AS VARCHAR(50))+'</AvailableInventory>'
																		+'<DisplayOrder>99</DisplayOrder>'
																		+'<BundleQuantity>1</BundleQuantity>'
		+ STUFF(    (  SELECT '<'+TBADI.AttributeCode+'>'+CAST( (SELECT  ''+TBADI.AttributeValue FOR XML PATH('')) AS NVARCHAR(max))+'</'+TBADI.AttributeCode+'>'   
															FROM #TBL_AttributeDetailsLocale TBADI      
															 WHERE TBADI.PimProductId = TBAD.PimProductId 
															 ORDER BY TBADI.PimProductId DESC
															 FOR XML PATH (''), TYPE
																).value('.', ' Nvarchar(max)'), 1, 0, '')+'</Product>'	   

		FROM #TBL_AttributeDetailsLocale TBAD
		INNER JOIN #TBL_ProductMainList TBPI ON (TBAD.PimProductid = TBPI.id )
		LEFT JOIN @TBL_ProductIds TPT ON TBAD.PimProductId = TPT.PimProductId
		LEFT JOIN @TBL_InventoryDetails IDD ON (TBPI.id = IDD.PimProductId)
		GROUP BY TBAD.pimProductid, TPT.ModifiedDate,TBPI.RowId,IDD.Quantity
		ORDER BY TBPI.RowId 
		FOR XML PATH (''),TYPE).value('.', ' Nvarchar(max)'), 1, 0, '')+'</MainProduct>'
			--FOR XML PATH ('MainProduct'))
 

			SELECT  CAST(@ProductXML AS XML ) ProductXMl
		   
		     SELECT AttributeCode ,  ZPAL.AttributeName
			 FROM ZnodePimAttribute ZPA 
			 LEFT JOIN ZnodePiMAttributeLOcale ZPAL ON (ZPAL.PimAttributeId = ZPA.PimAttributeId )
             WHERE LocaleId = 1  
			 AND  IsCategory = 0 
			 AND ZPA.IsShowOnGrid = 1  
			 UNION ALL 
			 SELECT 'PublishStatus','Publish Status'

     IF EXISTS (SELECT Top 1 1 FROM @TAb )
	 BEGIN 

		  SELECT (SELECT COUNT(1) FROM @TAb) AS RowsCount   
	 END 
	 ELSE 
	 BEGIN
	 		  SELECT (SELECT COUNT(1) FROM @ProductListIdRTR) AS RowsCount   
	 END 
		;

             -- find the all locale values 
         END TRY
         BEGIN CATCH
		    SELECT ERROR_MESSAGE()
                DECLARE @Status BIT ;
		     SET @Status = 0;
		     DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(),
			 @ErrorCall NVARCHAR(MAX)= 'EXEC Znode_ManageProductList_XML @WhereClause = '+CAST(@WhereClause AS VARCHAR(max))+',@Rows='+CAST(@Rows AS VARCHAR(50))+',@PageNo='+CAST(@PageNo AS VARCHAR(50))+',@Order_BY='+@Order_BY+',@LocaleId = '+CAST(@LocaleId AS VARCHAR(50))+',@PimProductId='+@PimProductId+',@IsProductNotIn='+CAST(@IsProductNotIn AS VARCHAR(50))+',@IsCallForAttribute='+CAST(@IsCallForAttribute AS VARCHAR(50))+',@Status='+CAST(@Status AS VARCHAR(10));
              			 
             SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                    
		  
             EXEC Znode_InsertProcedureErrorLog
				@ProcedureName = 'Znode_ManageProductList_XML',
				@ErrorInProcedure = @Error_procedure,
				@ErrorMessage = @ErrorMessage,
				@ErrorLine = @ErrorLine,
				@ErrorCall = @ErrorCall;

         END CATCH;

     END
GO
------------ZPD-14702
update ZnodeApplicationSetting set Setting='<?xml version="1.0" encoding="utf-16"?><columns><column><id>1</id><name>PimProductTypeAssociationId</name><headertext>Checkbox</headertext><width>20</width><datatype>Int32</datatype><columntype>Int32</columntype><allowsorting>false</allowsorting><allowpaging>true</allowpaging><format></format><isvisible>y</isvisible><mustshow>y</mustshow><musthide>n</musthide><maxlength>0</maxlength><isallowsearch>n</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>y</ischeckbox><checkboxparamfield>PimProductTypeAssociationId</checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext></displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>2</id><name>RelatedProductId</name><headertext>RelatedProductId</headertext><width>20</width><datatype>Int32</datatype><columntype>Int32</columntype><allowsorting>false</allowsorting><allowpaging>true</allowpaging><format></format><isvisible>n</isvisible><mustshow>n</mustshow><musthide>y</musthide><maxlength>0</maxlength><isallowsearch>n</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield>PimProductTypeAssociationId</checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext></displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>3</id><name>PimProductId</name><headertext>ID</headertext><width>30</width><datatype>Int32</datatype><columntype>Int32</columntype><allowsorting>false</allowsorting><allowpaging>true</allowpaging><format></format><isvisible>y</isvisible><mustshow>y</mustshow><musthide>n</musthide><maxlength>0</maxlength><isallowsearch>n</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext></displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>4</id><name>Image</name><headertext>Image</headertext><width>20</width><datatype>String</datatype><columntype>String</columntype><allowsorting>false</allowsorting><allowpaging>false</allowpaging><format></format><isvisible>y</isvisible><mustshow>n</mustshow><musthide>n</musthide><maxlength>0</maxlength><isallowsearch>n</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext></displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield>ProductImage,ProductName</imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class>imageicon</Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>5</id><name>ProductName</name><headertext>Product Name</headertext><width>60</width><datatype>String</datatype><columntype>String</columntype><allowsorting>true</allowsorting><allowpaging>true</allowpaging><format></format><isvisible>y</isvisible><mustshow>y</mustshow><musthide>n</musthide><maxlength>0</maxlength><isallowsearch>y</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext></displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>6</id><name>ProductType</name><headertext>Product Type</headertext><width>30</width><datatype>String</datatype><columntype>String</columntype><allowsorting>true</allowsorting><allowpaging>true</allowpaging><format></format><isvisible>y</isvisible><mustshow>n</mustshow><musthide>n</musthide><maxlength>0</maxlength><isallowsearch>y</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext></displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>7</id><name>SKU</name><headertext>SKU</headertext><width>30</width><datatype>String</datatype><columntype>String</columntype><allowsorting>true</allowsorting><allowpaging>true</allowpaging><format></format><isvisible>y</isvisible><mustshow>y</mustshow><musthide>n</musthide><maxlength>0</maxlength><isallowsearch>y</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext>SKU</displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>8</id><name>AvailableInventory</name><headertext>Available Inventory</headertext><width>30</width><datatype>String</datatype><columntype>String</columntype><allowsorting>false</allowsorting><allowpaging>false</allowpaging><format></format><isvisible>y</isvisible><mustshow>y</mustshow><musthide>n</musthide><maxlength>0</maxlength><isallowsearch>y</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext>SKU</displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>9</id><name>Assortment</name><headertext>Assortment</headertext><width>0</width><datatype>String</datatype><columntype>String</columntype><allowsorting>false</allowsorting><allowpaging>false</allowpaging><format></format><isvisible>n</isvisible><mustshow>n</mustshow><musthide>y</musthide><maxlength>0</maxlength><isallowsearch>y</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext></displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>10</id><name>DisplayOrder</name><headertext>Display Order</headertext><width>10</width><datatype>Int32</datatype><columntype>Int32</columntype><allowsorting>true</allowsorting><allowpaging>true</allowpaging><format></format><isvisible>y</isvisible><mustshow>n</mustshow><musthide>n</musthide><maxlength>5</maxlength><isallowsearch>n</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>y</iscontrol><controltype>Text</controltype><controlparamfield></controlparamfield><displaytext></displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>Text</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>11</id><name>BundleQuantity</name><headertext>Quantity</headertext><width>10</width><datatype>Int32</datatype><columntype>Int32</columntype><allowsorting>false</allowsorting><allowpaging>false</allowpaging><format></format><isvisible>y</isvisible><mustshow>y</mustshow><musthide>n</musthide><maxlength>5</maxlength><isallowsearch>n</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>y</iscontrol><controltype>Text</controltype><controlparamfield></controlparamfield><displaytext></displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl></manageactionurl><manageparamfield></manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>Text</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column><column><id>12</id><name>Manage</name><headertext>Action</headertext><width>30</width><datatype>String</datatype><columntype>String</columntype><allowsorting>false</allowsorting><allowpaging>true</allowpaging><format>Edit|Delete</format><isvisible>y</isvisible><mustshow>y</mustshow><musthide>n</musthide><maxlength>0</maxlength><isallowsearch>n</isallowsearch><isconditional>n</isconditional><isallowlink>n</isallowlink><islinkactionurl></islinkactionurl><islinkparamfield></islinkparamfield><ischeckbox>n</ischeckbox><checkboxparamfield></checkboxparamfield><iscontrol>n</iscontrol><controltype></controltype><controlparamfield></controlparamfield><displaytext>Edit|Delete</displaytext><editactionurl></editactionurl><editparamfield></editparamfield><deleteactionurl></deleteactionurl><deleteparamfield></deleteparamfield><viewactionurl></viewactionurl><viewparamfield></viewparamfield><imageactionurl></imageactionurl><imageparamfield></imageparamfield><manageactionurl>/PIM/Products/UpdateAssociatedProducts|/PIM/Products/UnassociateProducts</manageactionurl><manageparamfield>PimProductTypeAssociationId,PimProductId,RelatedProductId|PimProductTypeAssociationId,PimProductId</manageparamfield><copyactionurl></copyactionurl><copyparamfield></copyparamfield><xaxis>n</xaxis><yaxis>n</yaxis><isadvancesearch>y</isadvancesearch><Class></Class><SearchControlType>--Select--</SearchControlType><SearchControlParameters></SearchControlParameters><DbParamField></DbParamField><useMode>DataBase</useMode><IsGraph>n</IsGraph><allowdetailview>n</allowdetailview></column></columns>'
where ItemName = 'View_ManageProductTypeList_BundleProduct'

----------ZPD-14893
Insert  INTO ZnodeActions (AreaName,ControllerName,ActionName,IsGlobalAccess,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select NULL ,'Products','GetAssociatedBundleProducts',0,2,Getdate(),2,Getdate() where not exists
(select * from ZnodeActions where ControllerName = 'Products' and ActionName = 'GetAssociatedBundleProducts')

 

INSERT INTO ZnodeMenuActionsPermission (MenuId,ActionId,AccessPermissionId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select TOP 1 MenuId from ZnodeMenu where MenuName='PIM'), 
(select TOP 1 ActionId from ZnodeActions where ControllerName = 'Products' and ActionName= 'GetAssociatedBundleProducts'),1,2,getdate(),2,getdate()
where exists(select TOP 1 MenuId from ZnodeMenu where MenuName='PIM') and
exists(select TOP 1 ActionId from ZnodeActions where ControllerName = 'Products' and ActionName= 'GetAssociatedBundleProducts')
and not exists(select * from ZnodeMenuActionsPermission where MenuId = (select TOP 1 MenuId from ZnodeMenu where MenuName='PIM')
and ActionId =(select TOP 1 ActionId from ZnodeActions where ControllerName = 'Products' and ActionName= 'GetAssociatedBundleProducts'))



insert into ZnodeMenuActionsPermission ( MenuId, ActionId, AccessPermissionId, CreatedBy ,CreatedDate, ModifiedBy, ModifiedDate )
SELECT (select TOP 1 MenuId from ZnodeMenu where MenuName='Products'), 
(select TOP 1 ActionId from ZnodeActions where ControllerName = 'Products' and ActionName= 'GetAssociatedBundleProducts'),1,2,getdate(),2,getdate()
where exists(select TOP 1 MenuId from ZnodeMenu where MenuName='Products') and
exists(select TOP 1 ActionId from ZnodeActions where ControllerName = 'Products' and ActionName= 'GetAssociatedBundleProducts')
and not exists(select * from ZnodeMenuActionsPermission where MenuId = (select TOP 1 MenuId from ZnodeMenu where MenuName='Products')
and ActionId =(select TOP 1 ActionId from ZnodeActions where ControllerName = 'Products' and ActionName= 'GetAssociatedBundleProducts'))

GO

--------------------------------------------
IF EXISTS(SELECT * FROM SYS.PROCEDURES WHERE NAME = 'Znode_GetAccountGlobalAttributeValue')
	DROP PROC Znode_GetAccountGlobalAttributeValue
GO
CREATE PROCEDURE [dbo].[Znode_GetAccountGlobalAttributeValue]
(
	@EntityName NVARCHAR(200) = 0,
	@GlobalEntityValueId   INT = 0,
	@LocaleCode VARCHAR(100) = '',
	@GroupCode NVARCHAR(200) = NULL,
	@SelectedValue BIT = 0
)
AS
/*
	 Summary :- This procedure is used to get the Attribute and EntityValue attribute value as per filter pass 
	 Unit Testing 
	 BEGIN TRAN
	 EXEC [Znode_GetGlobalEntityAttributeValue] 'Store',1
	 EXEC [Znode_GetAccountGlobalAttributeValue_r] @EntityName=N'Account',@GlobalEntityValueId=1
	 ROLLBACK TRAN

*/	 
BEGIN
BEGIN TRY
SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @EntityValue NVARCHAR(200), @LocaleId INT

 	DECLARE @GlobalFamilyId INT
	SET  @GlobalFamilyId = (SELECT FM.GlobalAttributeFamilyId FROM ZnodeGlobalEntity GE 
		INNER JOIN ZnodeGlobalEntityFamilyMapper FM ON GE.GlobalEntityId = FM.GlobalEntityId
		WHERE GE.EntityName =  @EntityName AND  (FM.GlobalEntityValueId = @GlobalEntityValueId OR FM.GlobalEntityValueId IS NULL))
  

	DECLARE @V_MediaServerThumbnailPath VARCHAR(4000);
    SET @V_MediaServerThumbnailPath =
    (
        SELECT ISNULL(CASE WHEN CDNURL = '' THEN NULL ELSE CDNURL END,URL)+ZMSM.ThumbnailFolderName+'/'  
        FROM ZnodeMediaConfiguration ZMC 
		INNER JOIN ZnodeMediaServerMaster ZMSM ON (ZMSM.MediaServerMasterId = ZMC.MediaServerMasterId)
		WHERE IsActive = 1 
    );

	 --Getting account name
	 SELECT @EntityValue=Name FROM ZnodeAccount
	 WHERE AccountId=@GlobalEntityValueId

	IF ISNULL(@EntityValue,'')  <> ''
	BEGIN
		--Getting GlobalEntityId from @EntityName
		DECLARE @GlobalEntityId INT
		SET @GlobalEntityId =( SELECT GlobalEntityId FROM dbo.ZnodeGlobalEntity WHERE EntityName = @EntityName )

		CREATE TABLE #EntityAttributeList   
		(
			GlobalEntityId INT,EntityName NVARCHAR(300),EntityValue NVARCHAR(MAX),GlobalAttributeGroupId INT,
			GlobalAttributeId INT,AttributeTypeId INT,AttributeTypeName NVARCHAR(300),AttributeCode NVARCHAR(300) ,
			IsRequired BIT,IsLocalizable BIT,AttributeName  NVARCHAR(300) , HelpDescription NVARCHAR(MAX),DisplayOrder INT
		) 
			 
		CREATE TABLE #EntityAttributeValidationList 
		( 
			GlobalAttributeId INT, ControlName NVARCHAR(300), ValidationName NVARCHAR(300),
			SubValidationName NVARCHAR(300),RegExp NVARCHAR(300), ValidationValue NVARCHAR(300),IsRegExp Bit
		)

		CREATE TABLE #EntityAttributeValueList 
		(
			GlobalAttributeId INT,AttributeValue NVARCHAR(MAX),GlobalAttributeValueId INT,GlobalAttributeDefaultValueId INT,
			AttributeDefaultValueCode NVARCHAR(300),AttributeDefaultValue NVARCHAR(300),
			MediaId INT,MediaPath NVARCHAR(300),IsEditable bit,DisplayOrder INT 
		)

		CREATE TABLE #EntityAttributeDefaultValueList
		(
			GlobalAttributeDefaultValueId INT,GlobalAttributeId INT,AttributeDefaultValueCode NVARCHAR(300),
			AttributeDefaultValue NVARCHAR(300),RowId INT,IsEditable bit,DisplayOrder INT 
		)
		
		SET @LocaleId = (SELECT TOP 1 LocaleId FROM ZnodeLocale WHERE Code = @LocaleCode)

        --Getting account global attribute list
		INSERT INTO #EntityAttributeList
		(	
			GlobalEntityId ,EntityName ,EntityValue ,GlobalAttributeGroupId ,GlobalAttributeId ,AttributeTypeId ,
			AttributeTypeName,AttributeCode  ,IsRequired ,IsLocalizable ,AttributeName,HelpDescription,DisplayOrder  
		) 
		SELECT @GlobalEntityId,@EntityName,@EntityValue EntityValue,ww.GlobalAttributeGroupId,
			c.GlobalAttributeId,c.AttributeTypeId,q.AttributeTypeName,c.AttributeCode,c.IsRequired,
			c.IsLocalizable,f.AttributeName,c.HelpDescription,c.DisplayOrder
		FROM dbo.ZnodeGlobalAttributeGroupMapper AS ww 
		INNER JOIN dbo.ZnodeGlobalAttribute AS c ON ww.GlobalAttributeId = c.GlobalAttributeId
		INNER JOIN dbo.ZnodeAttributeType AS q ON c.AttributeTypeId = q.AttributeTypeId
		INNER JOIN dbo.ZnodeGlobalAttributeLocale AS f ON c.GlobalAttributeId = f.GlobalAttributeId
		WHERE ( f.LocaleId = isnull(@LocaleId, 0 ) or isnull(@LocaleId,0) = 0 )
		AND EXISTS(SELECT * FROM dbo.ZnodeGlobalAttributeFamily AS GAF 
			INNER JOIN dbo.ZnodeGlobalEntity W ON GAF.GlobalEntityId = w.GlobalEntityId  
			INNER JOIN dbo.ZnodeGlobalFamilyGroupMapper AS FGM ON FGM.GlobalAttributeFamilyId = GAF.GlobalAttributeFamilyId
			WHERE GAF.GlobalAttributeFamilyId = @GlobalFamilyId AND w.GlobalEntityId = @GlobalEntityId AND ww.GlobalAttributeGroupId = FGM.GlobalAttributeGroupId)
		AND EXISTS( SELECT 1 FROM ZnodeGlobalAttributeGroup g WHERE ww.GlobalAttributeGroupId = g.GlobalAttributeGroupId 
					AND (g.GroupCode = ISNULL(@GroupCode,'') OR ISNULL(@GroupCode,'') = '' ))

		--Getting globat attribute validation data
		IF EXISTS(SELECT * FROM #EntityAttributeList)
		BEGIN
			INSERT INTO #EntityAttributeValidationList(GlobalAttributeId,ControlName,ValidationName,SubValidationName,RegExp,ValidationValue,IsRegExp)
			SELECT aa.GlobalAttributeId,i.ControlName,i.Name AS ValidationName,j.ValidationName AS SubValidationName,
				j.RegExp,k.Name AS ValidationValue,CAST(CASE WHEN j.RegExp IS NULL THEN 0 ELSE 1 END AS BIT) AS IsRegExp
			FROM #EntityAttributeList aa
			INNER JOIN dbo.ZnodeGlobalAttributeValidation AS k ON k.GlobalAttributeId = aa.GlobalAttributeId
			INNER JOIN dbo.ZnodeAttributeInputValidation AS i ON k.InputValidationId = i.InputValidationId
			LEFT JOIN dbo.ZnodeAttributeInputValidationRule AS j ON k.InputValidationRuleId = j.InputValidationRuleId
		END

		----Getting globat attribute values
		INSERT INTO #EntityAttributeValueList(GlobalAttributeId,GlobalAttributeValueId,GlobalAttributeDefaultValueId,AttributeValue ,MediaId,MediaPath)
		SELECT DISTINCT GlobalAttributeId,aa.AccountGlobalAttributeValueId,bb.GlobalAttributeDefaultValueId,
			CASE WHEN bb.MediaPath IS NOT NULL THEN @V_MediaServerThumbnailPath+bb.MediaPath--+'~'+convert(NVARCHAR(10),bb.MediaId) 
			ELSE bb.AttributeValue END AS AttributeValue,	bb.MediaId,bb.MediaPath
		FROM dbo.ZnodeAccountGlobalAttributeValue aa
		INNER JOIN ZnodeAccountGlobalAttributeValueLocale bb ON bb.AccountGlobalAttributeValueId = aa.AccountGlobalAttributeValueId 
		WHERE aa.AccountId = @GlobalEntityValueId
		  
		--Updating default globat attribute data into table #EntityAttributeValueList
		IF EXISTS(SELECT * FROM #EntityAttributeValueList)
		BEGIN
			UPDATE aa
			SET AttributeDefaultValueCode= h.AttributeDefaultValueCode,
				AttributeDefaultValue=g.AttributeDefaultValue,
				GlobalAttributeDefaultValueId=g.GlobalAttributeDefaultValueId,
				AttributeValue= CASE WHEN aa.AttributeValue IS NULL THEN h.AttributeDefaultValueCode ELSE aa.AttributeValue END,
				IsEditable = ISNULL(h.IsEditable, 1),DisplayOrder = h.DisplayOrder
			FROM #EntityAttributeValueList aa
			INNER JOIN dbo.ZnodeGlobalAttributeDefaultValue h ON h.GlobalAttributeDefaultValueId = aa.GlobalAttributeDefaultValueId                                       
			INNER JOIN dbo.ZnodeGlobalAttributeDefaultValueLocale g ON h.GlobalAttributeDefaultValueId = g.GlobalAttributeDefaultValueId
         END

		--Getting globat attribute default values
		IF EXISTS(SELECT * FROM #EntityAttributeList)
		BEGIN
			INSERT INTO #EntityAttributeDefaultValueList(GlobalAttributeDefaultValueId,GlobalAttributeId,AttributeDefaultValueCode,AttributeDefaultValue ,RowId ,IsEditable ,DisplayOrder )
			SELECT h.GlobalAttributeDefaultValueId, aa.GlobalAttributeId,h.AttributeDefaultValueCode,g.AttributeDefaultValue,0,ISNULL(h.IsEditable, 1),h.DisplayOrder
			FROM #EntityAttributeList aa
			INNER JOIN dbo.ZnodeGlobalAttributeDefaultValue h ON h.GlobalAttributeId = aa.GlobalAttributeId
			INNER JOIN dbo.ZnodeGlobalAttributeDefaultValueLocale g ON h.GlobalAttributeDefaultValueId = g.GlobalAttributeDefaultValueId
		END
		 
		IF NOT EXISTS (SELECT 1 FROM #EntityAttributeList )
		BEGIN
			INSERT INTO #EntityAttributeList
			(	
				GlobalEntityId ,EntityName ,EntityValue ,GlobalAttributeGroupId ,GlobalAttributeId ,AttributeTypeId ,
				AttributeTypeName ,	AttributeCode  ,IsRequired ,IsLocalizable ,AttributeName,HelpDescription  
			) 
			SELECT qq.GlobalEntityId,qq.EntityName,@EntityValue EntityValue,0 GlobalAttributeGroupId,
				0 GlobalAttributeId,0 AttributeTypeId,''AttributeTypeName,''AttributeCode,0 IsRequired,
				0 IsLocalizable,'' AttributeName,'' HelpDescription
			FROM dbo.ZnodeGlobalEntity AS qq
			WHERE qq.EntityName=@EntityName 
		END
				

		SELECT GlobalEntityId,EntityName,EntityValue,GlobalAttributeGroupId, AA.GlobalAttributeId,AttributeTypeId,
			AttributeTypeName,AttributeCode,IsRequired,IsLocalizable,AttributeName,ControlName,ValidationName,
			SubValidationName,RegExp,ValidationValue,CAST(ISNULL(IsRegExp,0) as bit)  IsRegExp,HelpDescription,
			AttributeValue,GlobalAttributeValueId,bb.GlobalAttributeDefaultValueId,aab.AttributeDefaultValueCode,
			aab.AttributeDefaultValue,ISNULL(aab.RowId,0) AS RowId,CAST(ISNULL(aab.IsEditable,0) AS BIT) AS IsEditable
			,bb.MediaId,AA.DisplayOrder
		fROM #EntityAttributeList AA				
		LEFT JOIN #EntityAttributeDefaultValueList aab on aab.GlobalAttributeId=AA.GlobalAttributeId	
		LEFT JOIN #EntityAttributeValidationList vl on vl.GlobalAttributeId=aa.GlobalAttributeId			
		LEFT JOIN #EntityAttributeValueList BB ON BB.GlobalAttributeId=AA.GlobalAttributeId		 
			AND ( (aab.GlobalAttributeDefaultValueId=bb.GlobalAttributeDefaultValueId	)
					OR  ( bb.MediaId IS NOT NULL AND isnull(vl.ValidationName,'')='IsAllowMultiUpload' AND bb.GlobalAttributeDefaultValueId IS NULL )
					OR  ( bb.MediaId IS NULL AND bb.GlobalAttributeDefaultValueId IS NULL )
				)
		ORDER BY AA.DisplayOrder, aab.DisplayOrder
	END
	ELSE
	BEGIN
		 --Showing blank output if Account is not present
		 CREATE TABLE #TempOutput
		 ( 
			GlobalEntityId INT,EntityName VARCHAR(100),EntityValue VARCHAR(100),GlobalAttributeGroupId INT, 
			GlobalAttributeId INT,AttributeTypeId INT, AttributeTypeName VARCHAR(100),AttributeCode VARCHAR(100),
			IsRequired BIT,IsLocalizable BIT,AttributeName VARCHAR(100),ControlName VARCHAR(100),ValidationName VARCHAR(100),
			SubValidationName VARCHAR(100),RegExp VARCHAR(100),ValidationValue VARCHAR(100), IsRegExp BIT,
			HelpDescription VARCHAR(100),AttributeValue VARCHAR(100),GlobalAttributeValueId INT,
			GlobalAttributeDefaultValueId INT,AttributeDefaultValueCode VARCHAR(100),AttributeDefaultValue VARCHAR(100), 
			RowId INT, IsEditable BIT, MediaId INT,DisplayOrder INT
		)
		SELECT * FROM #TempOutput
	END

	SELECT 1 AS ID,CAST(1 AS BIT) AS Status;       
END TRY
BEGIN CATCH
	SELECT ERROR_MESSAGE()
	DECLARE @Status BIT ;
	SET @Status = 0;
	DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(),
	@ErrorLine VARCHAR(100)= ERROR_LINE(),
	@ErrorCall NVARCHAR(MAX)= null       			 
	SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                    
		 
	EXEC Znode_InsertProcedureErrorLog
	@ProcedureName = 'Znode_GetGlobalEntityValueAttributeValues',
	@ErrorInProcedure = @Error_procedure,
	@ErrorMessage = @ErrorMessage,
	@ErrorLine = @ErrorLine,
	@ErrorCall = @ErrorCall;
END CATCH;
END

GO
--------------------------------------------
IF EXISTS(SELECT * FROM SYS.PROCEDURES WHERE NAME = 'Znode_GetUserGlobalAttributeValue')
	DROP PROC Znode_GetUserGlobalAttributeValue
GO
CREATE PROCEDURE [dbo].[Znode_GetUserGlobalAttributeValue]
(
    @EntityName NVARCHAR(200) = 0,
    @GlobalEntityValueId INT = 0,
	@LocaleCode VARCHAR(100) = '',
    @GroupCode NVARCHAR(200) = NULL,
	@SELECTedValue BIT = 0
)
AS
/*
	 Summary :- This procedure is used to get the Attribute and EntityValue attribute value as per filter pass 
	 Unit Testing 
	 BEGIN TRAN
	 EXEC [Znode_GetGlobalEntityAttributeValue] 'USER',1
	 ROLLBACK TRAN

*/	 
BEGIN
BEGIN TRY
SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @EntityValue NVARCHAR(200), @LocaleId INT

	DECLARE @GlobalFamilyId int
	SET @GlobalFamilyId = (SELECT FM.GlobalAttributeFamilyId FROM ZnodeGlobalEntity GE 
		INNER JOIN  ZnodeGlobalEntityFamilyMapper FM ON GE.GlobalEntityId = FM.GlobalEntityId
		WHERE GE.EntityName =  @EntityName and  (FM.GlobalEntityValueId = @GlobalEntityValueId or FM.GlobalEntityValueId IS NULL))
  
	DECLARE @V_MediaServerThumbnailPath VARCHAR(4000);
	--Set the MediaServerThumbnailPath
	SET @V_MediaServerThumbnailPath =
	(
		SELECT ISNULL(CASE WHEN CDNURL = '' THEN NULL ELSE CDNURL END,URL)+ZMSM.ThumbnailFolderName+'/'  
		FROM ZnodeMediaConfiguration ZMC 
		INNER JOIN ZnodeMediaServerMaster ZMSM ON (ZMSM.MediaServerMasterId = ZMC.MediaServerMasterId)
		WHERE IsActive = 1 
	);

	--Getting user name
	SELECT @EntityValue=Isnull(FirstName,'')+' '+Isnull(LastName,'')
	FROM ZnodeUser
	WHERE UserId=@GlobalEntityValueId

	--Getting GlobalEntityId from @EntityName
	DECLARE @GlobalEntityId INT
	SET @GlobalEntityId =( SELECT GlobalEntityId FROM dbo.ZnodeGlobalEntity WHERE EntityName = @EntityName )

	CREATE TABLE #EntityAttributeList
	(
		GlobalEntityId INT,EntityName NVARCHAR(300),EntityValue NVARCHAR(MAX),GlobalAttributeGroupId INT,
		GlobalAttributeId INT,AttributeTypeId INT,AttributeTypeName NVARCHAR(300), AttributeCode NVARCHAR(300) ,
		IsRequired BIT,IsLocalizable BIT,AttributeName  NVARCHAR(300) , HelpDescription NVARCHAR(MAX),DisplayOrder INT
	) 
			 
	CREATE TABLE #EntityAttributeValidationList
	( 
		GlobalAttributeId INT, ControlName NVARCHAR(300), ValidationName NVARCHAR(300),SubValidationName NVARCHAR(300),
		RegExp NVARCHAR(300), ValidationValue NVARCHAR(300),IsRegExp Bit
	)

	CREATE TABLE #EntityAttributeValueList 
	(
		GlobalAttributeId INT,AttributeValue NVARCHAR(MAX),GlobalAttributeValueId INT,GlobalAttributeDefaultValueId INT,
		AttributeDefaultValueCode NVARCHAR(300),AttributeDefaultValue NVARCHAR(300),
		MediaId INT,MediaPath NVARCHAR(300),IsEditable BIT,DisplayOrder INT 
	)

	CREATE TABLE #EntityAttributeDefaultValueList 
	(	
		GlobalAttributeDefaultValueId INT,GlobalAttributeId INT,AttributeDefaultValueCode NVARCHAR(300),
		AttributeDefaultValue NVARCHAR(300),RowId INT,IsEditable BIT,DisplayOrder INT 
	)

	SET @LocaleId = (SELECT top 1 LocaleId FROM ZnodeLocale WHERE Code = @LocaleCode)
	--Getting user global attribute list
	INSERT INTO #EntityAttributeList
	(   
		GlobalEntityId ,EntityName ,EntityValue ,GlobalAttributeGroupId ,GlobalAttributeId ,AttributeTypeId ,
		AttributeTypeName ,AttributeCode  ,IsRequired ,IsLocalizable ,AttributeName,HelpDescription,DisplayOrder 
	) 
	SELECT @GlobalEntityId,@EntityName,@EntityValue EntityValue,ww.GlobalAttributeGroupId,
		c.GlobalAttributeId,c.AttributeTypeId,q.AttributeTypeName,c.AttributeCode,c.IsRequired,
		c.IsLocalizable,f.AttributeName,c.HelpDescription,c.DisplayOrder
	FROM dbo.ZnodeGlobalAttributeGroupMapper AS ww 
	INNER JOIN dbo.ZnodeGlobalAttribute AS c ON ww.GlobalAttributeId = c.GlobalAttributeId
	INNER JOIN dbo.ZnodeAttributeType AS q ON c.AttributeTypeId = q.AttributeTypeId
	INNER JOIN dbo.ZnodeGlobalAttributeLocale AS f ON c.GlobalAttributeId = f.GlobalAttributeId
	WHERE ( f.LocaleId = isnull(@LocaleId, 0 ) or isnull(@LocaleId,0) = 0 )
	AND EXISTS(SELECT * FROM dbo.ZnodeGlobalAttributeFamily AS GAF 
			INNER JOIN dbo.ZnodeGlobalEntity W ON GAF.GlobalEntityId = w.GlobalEntityId  
			INNER JOIN dbo.ZnodeGlobalFamilyGroupMapper AS FGM ON FGM.GlobalAttributeFamilyId = GAF.GlobalAttributeFamilyId
			WHERE GAF.GlobalAttributeFamilyId = @GlobalFamilyId AND w.GlobalEntityId = @GlobalEntityId AND ww.GlobalAttributeGroupId = FGM.GlobalAttributeGroupId)
	AND EXISTS( SELECT 1 FROM ZnodeGlobalAttributeGroup g WHERE ww.GlobalAttributeGroupId = g.GlobalAttributeGroupId 
					AND (g.GroupCode = ISNULL(@GroupCode,'') OR ISNULL(@GroupCode,'') = '' ))

	--Getting globat attribute validation data
	IF EXISTS(SELECT * FROM #EntityAttributeList)
	BEGIN
		INSERT INTO #EntityAttributeValidationList(GlobalAttributeId,ControlName , ValidationName ,SubValidationName ,RegExp, ValidationValue,IsRegExp)
		SELECT aa.GlobalAttributeId,i.ControlName,i.Name AS ValidationName,j.ValidationName AS SubValidationName,
			j.RegExp,k.Name AS ValidationValue,CAST(CASE WHEN j.RegExp IS NULL THEN 0 ELSE 1 END AS BIT) AS IsRegExp	
		FROM #EntityAttributeList aa
		INNER JOIN dbo.ZnodeGlobalAttributeValidation AS k ON k.GlobalAttributeId = aa.GlobalAttributeId
		INNER JOIN dbo.ZnodeAttributeInputValidation AS i ON k.InputValidationId = i.InputValidationId
		LEFT JOIN dbo.ZnodeAttributeInputValidationRule AS j ON k.InputValidationRuleId = j.InputValidationRuleId
	END

	---Getting globat attribute values
	INSERT INTO #EntityAttributeValueList(GlobalAttributeId,GlobalAttributeValueId,GlobalAttributeDefaultValueId,AttributeValue ,MediaId,MediaPath)
	SELECT GlobalAttributeId,aa.UserGlobalAttributeValueId,bb.GlobalAttributeDefaultValueId,
		CASE WHEN bb.MediaPath IS NOT NULL THEN @V_MediaServerThumbnailPath+bb.MediaPath--+'~'+convert(NVARCHAR(10),bb.MediaId) 
		ELSE bb.AttributeValue END AS AttributeValue,bb.MediaId,bb.MediaPath
	FROM  dbo.ZnodeUserGlobalAttributeValue aa
	INNER JOIN ZnodeUserGlobalAttributeValueLocale bb ON bb.UserGlobalAttributeValueId = aa.UserGlobalAttributeValueId 
	WHERE aa.UserId=@GlobalEntityValueId

	--Updating default globat attribute data into table #EntityAttributeValueList
	IF EXISTS(SELECT * FROM #EntityAttributeValueList)
	BEGIN
		UPDATE aa
		SET AttributeDefaultValueCode= h.AttributeDefaultValueCode,
			AttributeDefaultValue=g.AttributeDefaultValue,
			GlobalAttributeDefaultValueId=g.GlobalAttributeDefaultValueId,
			AttributeValue= CASE WHEN aa.AttributeValue IS NULL THEN h.AttributeDefaultValueCode ELSE aa.AttributeValue END, 
			IsEditable = ISNULL(h.IsEditable, 1),DisplayOrder = h.DisplayOrder
		FROM  #EntityAttributeValueList aa
		INNER JOIN dbo.ZnodeGlobalAttributeDefaultValue h ON h.GlobalAttributeDefaultValueId = aa.GlobalAttributeDefaultValueId                                       
		INNER JOIN dbo.ZnodeGlobalAttributeDefaultValueLocale g ON h.GlobalAttributeDefaultValueId = g.GlobalAttributeDefaultValueId
    END

	--Getting globat attribute default values
	IF EXISTS(SELECT * FROM #EntityAttributeList)
	BEGIN
		INSERT INTO #EntityAttributeDefaultValueList(GlobalAttributeDefaultValueId,GlobalAttributeId,AttributeDefaultValueCode,AttributeDefaultValue ,RowId ,IsEditable ,DisplayOrder )
		SELECT  h.GlobalAttributeDefaultValueId, aa.GlobalAttributeId,h.AttributeDefaultValueCode,g.AttributeDefaultValue,0,ISNULL(h.IsEditable, 1),h.DisplayOrder
		FROM  #EntityAttributeList aa
		INNER JOIN dbo.ZnodeGlobalAttributeDefaultValue h ON h.GlobalAttributeId = aa.GlobalAttributeId
		INNER JOIN dbo.ZnodeGlobalAttributeDefaultValueLocale g ON h.GlobalAttributeDefaultValueId = g.GlobalAttributeDefaultValueId
	END  

	IF NOT EXISTS (SELECT 1 FROM #EntityAttributeList )
	BEGIN
		INSERT INTO #EntityAttributeList
		(
			GlobalEntityId ,EntityName ,EntityValue ,GlobalAttributeGroupId ,GlobalAttributeId ,AttributeTypeId ,
			AttributeTypeName ,	AttributeCode  ,IsRequired ,IsLocalizable ,AttributeName,HelpDescription  
		) 
		SELECT qq.GlobalEntityId,qq.EntityName,@EntityValue EntityValue,0 GlobalAttributeGroupId,
		0 GlobalAttributeId,0 AttributeTypeId,''AttributeTypeName,''AttributeCode,0 IsRequired,
		0 IsLocalizable,'' AttributeName,'' HelpDescription
		FROM dbo.ZnodeGlobalEntity AS qq
		WHERE qq.EntityName=@EntityName 
	END
				

	SELECT GlobalEntityId,EntityName,EntityValue,GlobalAttributeGroupId,AA.GlobalAttributeId,AttributeTypeId,AttributeTypeName,
		AttributeCode,IsRequired,IsLocalizable,AttributeName,ControlName,ValidationName,SubValidationName,RegExp,
		ValidationValue,cast(isnull(IsRegExp,0) as bit)  IsRegExp,HelpDescription,AttributeValue,GlobalAttributeValueId,
		bb.GlobalAttributeDefaultValueId,aab.AttributeDefaultValueCode,aab.AttributeDefaultValue,isnull(aab.RowId,0)   
		RowId,cast(isnull(aab.IsEditable,0) as bit)   IsEditable,bb.MediaId,AA.DisplayOrder
	FROM #EntityAttributeList AA				
	LEFT JOIN #EntityAttributeDefaultValueList aab on aab.GlobalAttributeId=AA.GlobalAttributeId	
	LEFT JOIN #EntityAttributeValidationList vl on vl.GlobalAttributeId=aa.GlobalAttributeId			
	LEFT JOIN #EntityAttributeValueList BB ON BB.GlobalAttributeId=AA.GlobalAttributeId		 
	AND ( (aab.GlobalAttributeDefaultValueId=bb.GlobalAttributeDefaultValueId	)
		OR  ( bb.MediaId IS NOT NULL AND ISNULL(vl.ValidationName,'')='IsAllowMultiUpload'  and bb.GlobalAttributeDefaultValueId IS NULL )
		OR  ( bb.MediaId IS NULL AND bb.GlobalAttributeDefaultValueId is null ))
	ORDER BY AA.DisplayOrder,aab.DisplayOrder

	SELECT 1 AS ID,CAST(1 AS BIT) AS Status;       
END TRY
BEGIN CATCH
	SELECT ERROR_MESSAGE()
	DECLARE @Status BIT ;
	SET @Status = 0;
	DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(),
	@ErrorLine VARCHAR(100)= ERROR_LINE(),
	@ErrorCall NVARCHAR(MAX)= null       			 
	SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                    
		 
	EXEC Znode_InsertProcedureErrorLog
	@ProcedureName = 'Znode_GetGlobalEntityValueAttributeValues',
	@ErrorInProcedure = @Error_procedure,
	@ErrorMessage = @ErrorMessage,
	@ErrorLine = @ErrorLine,
	@ErrorCall = @ErrorCall;
END CATCH;
END;
GO
--------------------------------------------
IF EXISTS(SELECT * FROM SYS.PROCEDURES WHERE NAME = 'Znode_GetPublishAssociatedAddonsJson')
	DROP PROC Znode_GetPublishAssociatedAddonsJson
GO


CREATE PROCEDURE [dbo].[Znode_GetPublishAssociatedAddonsJson]
(
	@PublishCatalogId NVARCHAR(MAX) = 0,
	@PimProductId TransferId Readonly,
	@VersionId INT    = 0,
	@UserId    INT,														 
	@PimCategoryHierarchyId INT = 0, 
	@LocaleId       TransferId READONLY,
	@PublishStateId INT = 0, 
	@VersionIdString  VARCHAR(100)= '',
	@Status		   Bit  OutPut,
	@RevisionType VARCHAR(50)
)
AS 
   
/*
    Summary : If PimcatalogId is provided get all products with Addons and provide above mentioned data
       If PimProductId is provided get all Addons if associated with given product id and provide above mentioned data
    			Input: @PublishCatalogId or @PimProductId
    		    output should be in XML format
       sample xml5
    Begin transaction  
	 declare  @PimProductId TransferId 
	 Declare @Status bit 
	 INSERT INTO @PimProductId  values (768)

			Exec [_Znode_GetPublishAssociatedAddonsJson]
				@PublishCatalogId = 0 ,
				@PimProductId   = @PimProductId,
				@UserId =2,														 
				@VersionIdString = '',--'1662,1664,1663,1665',
				@Status	 =@Status Out,
				@RevisionType= 'Production'
				rollback transaction
				--SELECT * from ZnodePublishVersionEntity 
   
	*/

BEGIN
BEGIN TRY
SET NOCOUNT ON 
	DECLARE @GetDate DATETIME= dbo.Fn_GetDate();
	DECLARE @LocaleIdIn INT, @DefaultLocaleId INT= dbo.Fn_GetDefaultLocaleId()
	, @Counter INT= 1
	, @MaxRowId INT= 0;

	-- DECLARE @PimAddOnGroupId VARCHAR(MAX);

	CREATE TABLE #TBL_PublisshIds  (PublishProductId INT , PimProductId INT , PublishCatalogId INT)
	DECLARE @TBL_LocaleId TABLE (RowId    INT IDENTITY(1, 1),LocaleId INT);
	
	IF OBJECT_ID('tempdb..#VesionIds') IS NOT NULL
		DROP TABLE #VesionIds
	
	CREATE TABLE #VesionIds(ZnodeCatalogId INT , VersionId INT , LocaleId INT , RevisionType VARCHAR(50) )

	IF @VersionIdString <> ''		 
		INSERT INTO #VesionIds (ZnodeCatalogId, VersionId, LocaleId, RevisionType)	
		SELECT PV.ZnodeCatalogId, PV.VersionId, PV.LocaleId, PV.RevisionType FROM ZnodePublishVersionEntity PV Inner join Split(@VersionIdString,',') S ON PV.VersionId = S.Item
	ELSE 
	BEGIN
		IF  (@RevisionType like '%Preview%'  OR @RevisionType like '%Production%' )
			INSERT INTO #VesionIds (ZnodeCatalogId, VersionId, LocaleId, RevisionType)	
			SELECT PV.ZnodeCatalogId, PV.VersionId, PV.LocaleId, PV.RevisionType FROM ZnodePublishVersionEntity PV  where PV.IsPublishSuccess =1 
			AND PV.RevisionType ='Preview'

		IF  (@RevisionType like '%Production%' OR @RevisionType = 'None')
			INSERT INTO #VesionIds (ZnodeCatalogId, VersionId, LocaleId, RevisionType)	
			SELECT PV.ZnodeCatalogId, PV.VersionId, PV.LocaleId, PV.RevisionType FROM ZnodePublishVersionEntity PV  where PV.IsPublishSuccess =1 
			AND PV.RevisionType ='Production'
										
	END 
			 
	IF  @PublishCatalogId IS NULL  OR @PublishCatalogId = 0 
	BEGIN 
		INSERT INTO #TBL_PublisshIds 
		EXEC [dbo].[Znode_InsertPublishProductIds] @PublishCatalogId,@userid,@PimProductId,1
	END 
	IF  ISNULL(@PimCategoryHierarchyId,0) <> 0 
	BEGIN 
		INSERT INTO #TBL_PublisshIds 
		EXEC [dbo].[Znode_InsertPublishProductIds] @PublishCatalogId,@userid,@PimProductId,1,@PimCategoryHierarchyId 
	END 

	CREATE TABLE #TBL_PublishCatalogId (PublishCatalogId INT,PublishProductId INT,PimProductId  INT , VersionId INT ,LocaleId INT  );

	IF  ISNULL(@PimCategoryHierarchyId,0) <> 0 
	BEGIN 
		INSERT INTO #TBL_PublishCatalogId 
		SELECT ZPP.PublishCatalogId , ZPP.PublishProductId,PimProductId, MAX(ZPCP.PublishCatalogLogId)  ,LocaleId 
		FROM ZnodePublishProduct ZPP 
		INNER JOIN ZnodePublishCatalogLog ZPCP ON (ZPCP.PublishCatalogId  = ZPP.PublishCatalogId)
		WHERE EXISTS (SELECT TOP 1 1 FROM #TBL_PublisshIds SP WHERE SP.PublishProductId = ZPP.PublishProductId   ) 
		AND ZPCP.Publishstateid = @PublishStateId
		GROUP BY ZPP.PublishCatalogId,ZPP.PublishProductId,PimProductId,LocaleId 
	END 
	ELSE 
	BEGIN
		INSERT INTO #TBL_PublishCatalogId  
		SELECT ZPP.PublishCatalogId , ZPP.PublishProductId,PimProductId,0  VersionId  ,0 LocaleId 
		FROM ZnodePublishProduct ZPP  
		WHERE EXISTS  (SELECT TOP 1 1 FROM #VesionIds ZPCP WHERE (ZPCP.ZnodeCatalogId  = ZPP.PublishCatalogId)) AND
		(EXISTS (SELECT TOP 1 1 FROM #TBL_PublisshIds SP WHERE SP.PublishProductId = ZPP.PublishProductId  
		AND  @PublishCatalogId = '0' )  OR (ZPP.PublishCatalogId =  @PublishCatalogId ))
	END

	DECLARE @TBL_AddonGroupLocale TABLE( PimAddonGroupId INT, DisplayType NVARCHAR(400),AddonGroupName NVARCHAR(MAX),LocaleId INT );
	INSERT INTO @TBL_LocaleId(LocaleId)
	SELECT LocaleId FROM ZnodeLocale MT  WHERE IsActive = 1 AND (EXISTS (SELECT TOP 1 1  FROM @LocaleId RT WHERE RT.Id = MT.LocaleId )
	OR NOT EXISTS (SELECT TOP 1 1 FROM @LocaleId ));
	SET @MaxRowId = ISNULL(( SELECT MAX(RowId) FROM @TBL_LocaleId ), 0);
    
	WHILE @Counter <= @MaxRowId
	BEGIN
		SET @LocaleIdIn =( SELECT LocaleId FROM @TBL_LocaleId WHERE RowId = @Counter );
		INSERT INTO @TBL_AddonGroupLocale (PimAddonGroupId, DisplayType, AddonGroupName)
		EXEC Znode_GetAddOnGroupLocale '', @LocaleIdIn;
		UPDATE @TBL_AddonGroupLocale SET LocaleId = @LocaleIdIn WHERE LocaleId IS NULL 
		SET @Counter = @Counter + 1;
	END;
		
	IF OBJECT_ID('tempdb..#AddonProductPublishedXML') IS NOT NULL
		DROP TABLE #AddonProductPublishedXML
						
	SELECT  ZPPP.PublishProductId,ZPPP.PublishCatalogId,V.LocaleId,v.VersionId,
		ZPPP.[PublishProductId]  ZnodeProductId,
		ZPPP.[PublishCatalogId] ZnodeCatalogId,
		ZPP.PublishProductId  AssociatedZnodeProductId,
		ZPAOP.DisplayOrder DisplayOrder,
		ZPOPD.DisplayOrder AssociatedProductDisplayOrder,
		RequiredType ,
		DisplayType, 
		ISNULL((SELECT ''+AddonGroupName for xml path('')),'') GroupName,
		ISNULL(IsDefault,0) IsDefault
	INTO #AddonProductPublishedXML
	FROM [ZnodePimAddOnProductDetail] AS ZPOPD
	INNER JOIN [ZnodePimAddOnProduct] AS ZPAOP ON ZPOPD.[PimAddOnProductId] = ZPAOP.[PimAddOnProductId]
	INNER JOIN #TBL_PublishCatalogId ZPPP ON (ZPPP.PimProductId = ZPAOP.PimProductId )
	INNER JOIN #TBL_PublishCatalogId ZPP ON(ZPP.PimProductId = ZPOPD.[PimChildProductId] AND ZPP.PublishCatalogId = ZPPP.PublishCatalogId  )
	INNER JOIN #VesionIds V ON ZPPP.PublishCatalogId  = V.ZnodeCataLogId
	INNER JOIN @TBL_AddonGroupLocale TBAG ON (TBAG.PimAddonGroupId   = ZPAOP.PimAddonGroupId AND TBAG.LocaleId = V.LocaleId )
					
	IF (ISNULL(@PublishCatalogId ,'') = '' and @VersionIdString = '') OR 
		(ISNULL(@PublishCatalogId ,0) = 0 and @VersionIdString = '')
	BEGIN 
		DELETE ZPE 
		FROM ZnodePublishAddonEntity ZPE 
		WHERE EXISTS(SELECT * FROM #TBL_PublishCatalogId z
		INNER JOIN ZnodePublishProduct ZPP ON Z.PimProductId = ZPP.PimProductId
		WHERE ZPE.ZnodeProductId = ZPP.PublishProductId )
	END 
					
	INSERT INTO  ZnodePublishAddonEntity
	(VersionId,ZnodeProductId,ZnodeCatalogId,AssociatedZnodeProductId,AssociatedProductDisplayOrder,
	LocaleId,GroupName,DisplayType,DisplayOrder,IsRequired,RequiredType,IsDefault)
	SELECT VersionId,ZnodeProductId,ZnodeCatalogId,AssociatedZnodeProductId,ISNULL(AssociatedProductDisplayOrder,0),
		LocaleId,GroupName,DisplayType,DisplayOrder,1 IsRequired,RequiredType,IsDefault 
	FROM #AddonProductPublishedXML

	SET @Status = 1;
     
END TRY
BEGIN CATCH
	SELECT ERROR_MESSAGE(),ERROR_PROCEDURE()
     
	SET @Status = 0;
	DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(), @ErrorCall NVARCHAR(MAX)= 'EXEC Znode_GetPublishAssociatedAddonsJson @PublishCatalogId = '+@PublishCatalogId+',@VersionId='+CAST(@VersionId AS VARCHAR(50))+',@UserId='+CAST(@UserId AS VARCHAR(50))+',@Status='+CAST(@Status AS VARCHAR(10));
	SELECT 0 AS ID,
	CAST(0 AS BIT) AS Status;
	EXEC Znode_InsertProcedureErrorLog
	@ProcedureName = 'Znode_GetPublishAssociatedAddonsJson',
	@ErrorInProcedure = @Error_procedure,
	@ErrorMessage = @ErrorMessage,
	@ErrorLine = @ErrorLine,
	@ErrorCall = @ErrorCall;
END CATCH;
END;
GO

----------------------------------------------------------
----------ZPD-14025

insert into ZnodeGlobalAttribute
(AttributeTypeId, AttributeCode, IsRequired, IsLocalizable, IsActive, DisplayOrder,HelpDescription,CreatedBy, CreatedDate, ModifiedBy,ModifiedDate, IsSystemDefined, GlobalEntityId)
select 
(select top 1  AttributeTypeId from ZnodeAttributeType where AttributeTypeName ='Number' ),N'SuccessBufferTime',1,1,0,14,'Success Buffer Time?',2,getdate(),2,getdate(),0 , (select top 1 GlobalEntityId from znodeGlobalEntity where EntityName ='Store')
where not exists (select AttributeCode from  ZnodeGlobalAttribute where AttributeCode ='SuccessBufferTime' )

insert into ZnodeGlobalAttribute
(AttributeTypeId, AttributeCode, IsRequired, IsLocalizable, IsActive, DisplayOrder,HelpDescription,CreatedBy, CreatedDate, ModifiedBy,ModifiedDate, IsSystemDefined, GlobalEntityId)
select 
(select top 1  AttributeTypeId from ZnodeAttributeType where AttributeTypeName ='Number' ),N'SuccessRandomTime',1,1,0,14,'Success Random Time?',2,getdate(),2,getdate(),0, (select top 1 GlobalEntityId from znodeGlobalEntity where EntityName ='Store')
where not exists  (select AttributeCode from  ZnodeGlobalAttribute where AttributeCode ='SuccessRandomTime')

insert into ZnodeGlobalAttribute
(AttributeTypeId, AttributeCode, IsRequired, IsLocalizable, IsActive, DisplayOrder,HelpDescription,CreatedBy, CreatedDate, ModifiedBy,ModifiedDate, IsSystemDefined, GlobalEntityId)
select 
(select top 1  AttributeTypeId from ZnodeAttributeType where AttributeTypeName ='Number' ),N'ErrorBufferTime',1,1,0,14,'Error Buffer Time?',2,getdate(),2,getdate(),0, (select top 1 GlobalEntityId from znodeGlobalEntity where EntityName ='Store')
where not exists  (select AttributeCode from  ZnodeGlobalAttribute where AttributeCode ='ErrorBufferTime')

insert into ZnodeGlobalAttribute
(AttributeTypeId, AttributeCode, IsRequired, IsLocalizable, IsActive, DisplayOrder,HelpDescription,CreatedBy, CreatedDate, ModifiedBy,ModifiedDate, IsSystemDefined, GlobalEntityId)
select 
(select top 1  AttributeTypeId from ZnodeAttributeType where AttributeTypeName ='Number' ),N'ErrorRandomTime',1,1,0,14,'Error Random Time?',2,getdate(),2,getdate(),0, (select top 1 GlobalEntityId from znodeGlobalEntity where EntityName ='Store')
where not exists  (select AttributeCode from  ZnodeGlobalAttribute where AttributeCode ='ErrorRandomTime')

insert into ZnodeGlobalAttribute
(AttributeTypeId, AttributeCode, IsRequired, IsLocalizable, IsActive, DisplayOrder,HelpDescription,CreatedBy, CreatedDate, ModifiedBy,ModifiedDate, IsSystemDefined, GlobalEntityId)
select 
(select top 1  AttributeTypeId from ZnodeAttributeType where AttributeTypeName ='Number' ),N'SuccessSignUpBufferTime',1,1,0,14,'Success SignUp Buffer Time?',2,getdate(),2,getdate(),0, (select top 1 GlobalEntityId from znodeGlobalEntity where EntityName ='Store')
where not exists  (select AttributeCode from  ZnodeGlobalAttribute where AttributeCode ='ErrorRandomTime')

insert into ZnodeGlobalAttribute
(AttributeTypeId, AttributeCode, IsRequired, IsLocalizable, IsActive, DisplayOrder,HelpDescription,CreatedBy, CreatedDate, ModifiedBy,ModifiedDate, IsSystemDefined, GlobalEntityId)
select 
(select top 1  AttributeTypeId from ZnodeAttributeType where AttributeTypeName ='Number' ),N'SuccessSignUpRandomTime',1,1,0,14,'Success SignUp Random Time?',2,getdate(),2,getdate(),0, (select top 1 GlobalEntityId from znodeGlobalEntity where EntityName ='Store')
where not exists  (select AttributeCode from  ZnodeGlobalAttribute where AttributeCode ='ErrorRandomTime')

insert into ZnodeGlobalAttribute
(AttributeTypeId, AttributeCode, IsRequired, IsLocalizable, IsActive, DisplayOrder,HelpDescription,CreatedBy, CreatedDate, ModifiedBy,ModifiedDate, IsSystemDefined, GlobalEntityId)
select 
(select top 1  AttributeTypeId from ZnodeAttributeType where AttributeTypeName ='Number' ),N'ErrorSignUpBufferTime',1,1,0,14,'Error SignUp Buffer Time?',2,getdate(),2,getdate(),0, (select top 1 GlobalEntityId from znodeGlobalEntity where EntityName ='Store')
where not exists  (select AttributeCode from  ZnodeGlobalAttribute where AttributeCode ='ErrorRandomTime')

insert into ZnodeGlobalAttribute
(AttributeTypeId, AttributeCode, IsRequired, IsLocalizable, IsActive, DisplayOrder,HelpDescription,CreatedBy, CreatedDate, ModifiedBy,ModifiedDate, IsSystemDefined, GlobalEntityId)
select 
(select top 1 AttributeTypeId from ZnodeAttributeType where AttributeTypeName ='Number' ),N'ErrorSignUpRandomTime',1,1,0,14,'Error SignUp Random Time?',2,getdate(),2,getdate(),0, (select top 1 GlobalEntityId from znodeGlobalEntity where EntityName ='Store')
where not exists  (select AttributeCode from  ZnodeGlobalAttribute where AttributeCode ='ErrorRandomTime')

insert into ZnodeGlobalAttributeGroup
(GroupCode,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsSystemDefined,GlobalEntityId)
select N'DelayTimeConfiguration', NULL, 2, GETDATE(), 2, GETDATE(), 0, (select top 1 GlobalEntityId from znodeGlobalEntity where EntityName ='Store')
where  not exists   (select top 1 GroupCode from ZnodeGlobalAttributeGroup where GroupCode =N'DelayTimeConfiguration') 


Insert into ZnodeGlobalAttributeDefaultValue
(GlobalAttributeId,AttributeDefaultValueCode,IsEditable,DisplayOrder,MediaId,SwatchText,IsDefault,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select top 1  GlobalAttributeId, N'0',NULL,NULL,NULL,NULL,NULL,2,Getdate(),2,Getdate()
from ZnodeGlobalAttribute where AttributeCode ='SuccessBufferTime' and  GlobalAttributeId not in (select GlobalAttributeId from ZnodeGlobalAttributeDefaultValue)

Insert into ZnodeGlobalAttributeDefaultValue
(GlobalAttributeId,AttributeDefaultValueCode,IsEditable,DisplayOrder,MediaId,SwatchText,IsDefault,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select top 1  GlobalAttributeId, N'0',NULL,NULL,NULL,NULL,NULL,2,Getdate(),2,Getdate()
from ZnodeGlobalAttribute where AttributeCode ='SuccessRandomTime' and  GlobalAttributeId not in (select GlobalAttributeId from ZnodeGlobalAttributeDefaultValue)

Insert into ZnodeGlobalAttributeDefaultValue
(GlobalAttributeId,AttributeDefaultValueCode,IsEditable,DisplayOrder,MediaId,SwatchText,IsDefault,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select top 1  GlobalAttributeId, N'0',NULL,NULL,NULL,NULL,NULL,2,Getdate(),2,Getdate()
from ZnodeGlobalAttribute where AttributeCode ='ErrorBufferTime' and  GlobalAttributeId not in (select GlobalAttributeId from ZnodeGlobalAttributeDefaultValue)

Insert into ZnodeGlobalAttributeDefaultValue
(GlobalAttributeId,AttributeDefaultValueCode,IsEditable,DisplayOrder,MediaId,SwatchText,IsDefault,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select top 1  GlobalAttributeId, N'0',NULL,NULL,NULL,NULL,NULL,2,Getdate(),2,Getdate()
from ZnodeGlobalAttribute where AttributeCode ='ErrorRandomTime' and  GlobalAttributeId not in (select GlobalAttributeId from ZnodeGlobalAttributeDefaultValue)
--
Insert into ZnodeGlobalAttributeDefaultValue
(GlobalAttributeId,AttributeDefaultValueCode,IsEditable,DisplayOrder,MediaId,SwatchText,IsDefault,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select top 1  GlobalAttributeId, N'0',NULL,NULL,NULL,NULL,NULL,2,Getdate(),2,Getdate()
from ZnodeGlobalAttribute where AttributeCode ='SuccessSignUpBufferTime' and  GlobalAttributeId not in (select GlobalAttributeId from ZnodeGlobalAttributeDefaultValue)

Insert into ZnodeGlobalAttributeDefaultValue
(GlobalAttributeId,AttributeDefaultValueCode,IsEditable,DisplayOrder,MediaId,SwatchText,IsDefault,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select top 1  GlobalAttributeId, N'0',NULL,NULL,NULL,NULL,NULL,2,Getdate(),2,Getdate()
from ZnodeGlobalAttribute where AttributeCode ='SuccessSignUpRandomTime' and  GlobalAttributeId not in (select GlobalAttributeId from ZnodeGlobalAttributeDefaultValue)

Insert into ZnodeGlobalAttributeDefaultValue
(GlobalAttributeId,AttributeDefaultValueCode,IsEditable,DisplayOrder,MediaId,SwatchText,IsDefault,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select top 1  GlobalAttributeId, N'0',NULL,NULL,NULL,NULL,NULL,2,Getdate(),2,Getdate()
from ZnodeGlobalAttribute where AttributeCode ='ErrorSignUpBufferTime' and  GlobalAttributeId not in (select GlobalAttributeId from ZnodeGlobalAttributeDefaultValue)

Insert into ZnodeGlobalAttributeDefaultValue
(GlobalAttributeId,AttributeDefaultValueCode,IsEditable,DisplayOrder,MediaId,SwatchText,IsDefault,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select top 1  GlobalAttributeId, N'0',NULL,NULL,NULL,NULL,NULL,2,Getdate(),2,Getdate()
from ZnodeGlobalAttribute where AttributeCode ='ErrorSignUpRandomTime' and  GlobalAttributeId not in (select GlobalAttributeId from ZnodeGlobalAttributeDefaultValue)



insert into ZnodeGlobalAttributeGroup
(GroupCode,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsSystemDefined,GlobalEntityId)
select N'DelayTimeConfiguration', NULL, 2, GETDATE(), 2, GETDATE(), 0, (select top 1 GlobalEntityId from znodeGlobalEntity where EntityName ='Store')
where  not exists   (select top 1 GroupCode from ZnodeGlobalAttributeGroup where GroupCode =N'DelayTimeConfiguration') 

GO
--------------------------------------------
IF EXISTS(SELECT * FROM SYS.PROCEDURES WHERE NAME = 'Znode_GetFilterPimProductId')
	DROP PROC Znode_GetFilterPimProductId
GO


CREATE PROCEDURE [dbo].[Znode_GetFilterPimProductId]
(
  @WhereClause XML 
 ,@PimProductId TransferId READONLY 
 ,@LocaleId   INT 
)
AS 
BEGIN 
SET NOCOUNT ON 

		DECLARE  @DefaultLocaleId INT = dbo.Fn_GetDefaultLocaleID()
		DECLARE @SQL NVARCHAR(MAX)
		DECLARE @InternalProductWhereClause NVARCHAR(MAX)

		DECLARE @WorkingProcess INT = 0 

		DECLARE @TBL_FilterClause TABLE (ID INT IDENTITY(1,1),AttributeValue NVARCHAR(MAX),AttributeCode NVARCHAr(MAX),PimAttributeId INT ,AttributeTypeName VARCHAR(300),AttributeCodeOrg VARCHAR(600))

		DECLARE @WhereClauseXML XML = @WhereClause 

		SET @SQL  = ''

		IF EXISTS (SELECT TOP 1 1 FROM @WhereClauseXml.nodes ( '//ArrayOfWhereClauseModel/WhereClauseModel'  ) AS Tbl(Col) 
		WHERE Tbl.Col.value ( 'attributecode[1]' , 'NVARCHAR(MAX)')  LIKE  '% in (%')
		BEGIN 
			SET @WorkingProcess = 1

				INSERT INTO @TBL_FilterClause (AttributeValue,AttributeCode,AttributeTypeName,PimAttributeId,AttributeCodeOrg)
			SELECT  Tbl.Col.value ( 'attributevalue[1]' , 'NVARCHAR(MAX)') AS AttributeValue
			,Tbl.Col.value ( 'attributecode[1]' , 'NVARCHAR(MAX)') AS AttributeValue,ZTY.AttributeTypeName,ZPA.PimAttributeId,AttributeCode AttributeCodeOrg
			FROM @WhereClauseXml.nodes ( '//ArrayOfWhereClauseModel/WhereClauseModel'  ) AS Tbl(Col)
			LEFT JOIN  ZnodePimAttribute ZPA  ON ((Tbl.Col.value ( 'attributecode[1]' , 'NVARCHAR(MAX)')  LIKE '%in (%' OR dbo.Fn_Trim(REPLACE(REPLACE(Tbl.Col.value ( 'attributecode[1]' , 'NVARCHAR(MAX)'),' = ',''),'''','')) 
												= ZPA.AttributeCode ) AND IsCategory = 0 
			AND ( ZPA.IsShowOnGrid = 1 OR ZPA.IsConfigurable =1  )  )
			LEFT JOIN ZnodeAttributeType ZTY ON (ZTY.AttributeTypeId = ZPA.AttributeTypeId)

		END 
		ELSE 
		BEGIN 

			INSERT INTO @TBL_FilterClause (AttributeValue,AttributeCode,AttributeTypeName,PimAttributeId,AttributeCodeOrg)
			SELECT  Tbl.Col.value ( 'attributevalue[1]' , 'NVARCHAR(MAX)') AS AttributeValue
			,Tbl.Col.value ( 'attributecode[1]' , 'NVARCHAR(MAX)') AS AttributeValue,ZTY.AttributeTypeName,ZPA.PimAttributeId,AttributeCode AttributeCodeOrg
			FROM @WhereClauseXml.nodes ( '//ArrayOfWhereClauseModel/WhereClauseModel'  ) AS Tbl(Col)
			LEFT JOIN ZnodePimAttribute ZPA  ON (dbo.Fn_Trim(REPLACE(REPLACE(Tbl.Col.value ( 'attributecode[1]' , 'NVARCHAR(MAX)'),' = ',''),'''','')) 
												= ZPA.AttributeCode AND ZPA.IsCategory = 0 
			AND ( ZPA.IsShowOnGrid = 1 OR ZPA.IsConfigurable =1  )  )
			LEFT JOIN ZnodeAttributeType ZTY ON (ZTY.AttributeTypeId = ZPA.AttributeTypeId)

		END 

		CREATE TABLE #TBL_PimProductId (PimProductId INT)

		CREATE TABLE #TBL_PimProductIdDelete (PimProductId INT )

		INSERT INTO #TBL_PimProductId (PimProductId )
		SELECT Id 
		FROM @PimProductId

		SELECT ZPAV.PimProductId ,PimAttributeValueId ,ZPAV.CreatedDate,ZPAV.ModifiedDate,TBLA.AttributeCodeOrg AttributeCode
		INTO #TBL_AttributeValueId 
		FROM  ZnodePimAttributeValue ZPAV 
		INNER JOIN @TBL_FilterClause TBLA ON (TBLA.PimAttributeId = ZPAV.PimAttributeId)
		INNER JOIN #TBL_PimProductId YT ON (YT.PimProductId = ZPAV.PimProductId OR NOT EXISTS (SELECT TOP 1 1 #TBL_PimProductId))

		DELETE FROM #TBL_PimProductId

		INSERT INTO #TBL_PimProductId (PimProductId )
		SELECT DISTINCT PimProductId 
		FROM #TBL_AttributeValueId

		IF @WorkingProcess =1 
		BEGIN 
				DECLARE @PimAttributeId_in TransferId 

				INSERT INTO @PimAttributeId_in 
				SELECT PimAttributeId
				FROM  @TBL_FilterClause 
				WHERE AttributeTypeName IN ('Simple Select','Multi Select') 
				AND AttributeCode LIKE '%in (%'

				CREATE TABLE #TBL_AttributeDefaultValue_in ( PimAttributeId INT ,
							AttributeDefaultValueCode VARCHAR(MAX),IsEditable INT,AttributeDefaultValue NVARCHAR(MAX),DisplayOrder INT,PimAttributeDefaultValueId INT,IsDefault Bit  )    
				INSERT INTO #TBL_AttributeDefaultValue_in(PimAttributeId,AttributeDefaultValueCode,IsEditable,AttributeDefaultValue,DisplayOrder,PimAttributeDefaultValueId,IsDefault)
				EXEC Znode_GetAttributeDefaultValueLocaleNew_TansferId @PimAttributeId_in, @LocaleId;
 
				DECLARE @WhereClauseInCom NVARCHAR(MAX) = (SELECT TOP 1 AttributeValue FROM @TBL_FilterClause WHERE AttributeCode LIKE '%in (%') 


			SET @SQL = '
			;With Cte_AttributeValue AS 
			(
			SELECT PimAttributeValueId 
			FROM ZnodePimAttributeValueLocale 
			WHERE AttributeValue '+@WhereClauseInCom+'
			UNION ALL 
			SELECT ZPADV.PimAttributeValueID 
			FROM ZnodePimProductAttributeDefaultValue ZPADV 
			INNER JOIN #TBL_AttributeDefaultValue_in TBL ON (TBL.PimAttributeDefaultValueId = ZPADV.PimAttributeDefaultValueId)
			WHERE TBL.AttributeDefaultValue '+@WhereClauseInCom+'
			)
   
			SELECT PimProductId 
			FROM #TBL_AttributeValueId ZPAV 
			INNER JOIN  Cte_AttributeValue CTAC ON (CTAC.PimAttributeValueId = ZPAV.PimAttributeVaLueId )
			GROUP BY PimProductId
			UNION ALL  
			SELECT PimProductId 
			FROM ZnodePimProduct a
			INNER JOIN ZnodePimFamilyLocale b ON (b.PimAttributeFamilyId = a.PimAttributeFamilyId) 
			WHERE b.AttributeFamilyName '+@WhereClauseInCom+'
			GROUP BY PimProductId
			UNION ALL 
			SELECT  TBLAV.PimProductId 
			FROM ZnodePimProduct TBLAV
			WHERE CASE WHEN TBLAV.IsProductPublish  IS NULL THEN ''Not Published'' 
					WHEN TBLAV.IsProductPublish = 0 THEN ''Draft''
					ELSE  ''Published'' END '+@WhereClauseInCom+'
			GROUP BY TBLAV.PimProductId 
			'
   
			DELETE FROM #TBL_PimProductIdDelete 
			INSERT INTO #TBL_PimProductIdDelete  (PimProductId)
			EXEC (@SQL)
			DELETE FROM #TBL_PimProductId
			INSERT INTO #TBL_PimProductId
			SELECT PimProductId FROM #TBL_PimProductIdDelete
			INSERT INTO #TBL_PimProductId
			SELECT -1 
			WHERE NOT EXISTS (SELECT TOP 1 1  FROM #TBL_PimProductId)

			DELETE  FROM @TBL_FilterClause WHERE AttributeCode LIKE '% in (%'

			DROP TABLE #TBL_AttributeDefaultValue_in
			SET @WorkingProcess  = 0 
   
		END 

		IF EXISTS (SELECT TOP 1 1 FROM @TBL_FilterClause WHERE AttributeCode <> '' AND ISNULL(AttributeValue,'') = '')
		BEGIN 
  
				SET  @InternalProductWhereClause = STUFF( (  SELECT ' INNER JOIN #TBL_AttributeValueId AS ZPAVL'+CAST(ID AS VARCHAR(200))+
												' ON ( TBLAV.PimProductId = ZPAVL'+CAST(ID AS VARCHAR(200))+'.PimProductId AND ZPAVL'+CAST(ID AS VARCHAR(200))+'.AttributeCode '+AttributeCode+
												' )'				
										FROM @TBL_FilterClause
										WHERE ISNULL(AttributeValue,'') = ''
										FOR XML PATH (''), TYPE).value('.', ' Nvarchar(MAX)'), 1, 0, '')
				----Change for configurable product varient page seach
				SET @SQL = ' 
							SELECT  TBLAV.PimProductId 
							FROM #TBL_AttributeValueId TBLAV '+@InternalProductWhereClause+' 
							WHERE EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId TBLP WHERE TBLP.PimProductId = TBLAV.PimProductId )
							GROUP BY TBLAV.PimProductId 
						'
  

				DELETE FROM #TBL_PimProductIdDelete 
				INSERT INTO #TBL_PimProductIdDelete  (PimProductId)
				EXEC (@SQL)
				DELETE FROM #TBL_PimProductId
				INSERT INTO #TBL_PimProductId
				SELECT PimProductId FROM #TBL_PimProductIdDelete
				INSERT INTO #TBL_PimProductId
				SELECT -1 
				WHERE NOT EXISTS (SELECT TOP 1 1  FROM #TBL_PimProductId)
				DELETE FROM @TBL_FilterClause WHERE ISNULL(AttributeValue,'') = ''
				IF NOT EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId ) AND EXISTS (SELECT TOP 1 1  FROM @PimProductId Having Max (ID) = 0 )
				BEGIN
				INSERT INTO  #TBL_PimProductId (PimProductId)
				SELECT 0 
			END
  
		END 

		IF EXISTS (SELECT TOP 1 1 FROM @TBL_FilterClause WHERE AttributeTypeName IN ('Simple Select','Multi Select') )
		BEGIN
			DECLARE @PimAttributeId TransferId 

			INSERT INTO @PimAttributeId 
			SELECT DISTINCT PimAttributeId
			FROM  @TBL_FilterClause WHERE AttributeTypeName IN ('Simple Select','Multi Select') 

			CREATE TABLE #TBL_AttributeDefaultValue ( PimAttributeId INT ,
						AttributeDefaultValueCode VARCHAR(MAX),IsEditable INT,AttributeDefaultValue NVARCHAR(MAX),DisplayOrder INT,PimAttributeDefaultValueId INT,IsDefault BIT  )    
			INSERT INTO #TBL_AttributeDefaultValue(PimAttributeId,AttributeDefaultValueCode,IsEditable,AttributeDefaultValue,DisplayOrder,PimAttributeDefaultValueId,IsDefault)
			EXEC Znode_GetAttributeDefaultValueLocaleNew_TansferId @PimAttributeId, @LocaleId;
 
			IF @DefaultLocaleId = @LocaleID AND  @WorkingProcess = 0 
			BEGIN 


				SET  @InternalProductWhereClause = STUFF( (  SELECT ' INNER JOIN Cte_AttributeValue AS ZPAVL'+CAST(ID AS VARCHAR(200))+
												' ON ( TBLAV.PimProductId = ZPAVL'+CAST(ID AS VARCHAR(200))+'.PimProductId AND ZPAVL'+CAST(ID AS VARCHAR(200))+'.AttributeCode '+AttributeCode+
												+' AND ZPAVL'+CAST(ID AS VARCHAR(200))+'.AttributeValue '+AttributeValue+' AND ZPAVL'+CAST(ID AS VARCHAR(200))+
												'.LocaleId='+CAST(@LocaleID AS VARCHAR(200))+' )'				
										FROM @TBL_FilterClause
										WHERE AttributeTypeName IN ('Simple Select','Multi Select')
										AND AttributeValue <> ''
										AND AttributeValue IS NOT NULL
										FOR XML PATH (''), TYPE).value('.', ' Nvarchar(MAX)'), 1, 0, '')

				SET @SQL = ' ;With Cte_AttributeValue AS 
							(
							SELECT TBLAV.PimAttributeValueId ,SUBSTRING((SELECT '',''+AttributeDefaultValue FROM #TBL_AttributeDefaultValue TTR 
							INNER JOIN ZnodePimProductAttributeDefaultValue ZPAVL ON (TTR.PimAttributeDefaultValueId = ZPAVL.PimAttributeDefaultValueId )
							WHERE ZPAVL.PimAttributeValueId = TBLAV.PimAttributeValueId  
							AND ZPAVL.LocaleId = '+Cast(@localeId AS VARCHAR(200))+'
							FOR XML PATH('''') ),2,4000) AttributeValue
								,  '+Cast(@localeId AS VARCHAR(200))+' LocaleId,TBLAV.AttributeCode,TBLAV.PimProductId
							FROM #TBL_AttributeValueId TBLAV
							'+CASE WHEN NOT EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId ) THEN '' 
										ELSE ' WHERE EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId TBLP WHERE TBLP.PimProductId = TBLAV.PimProductId ) ' END+'
							GROUP BY TBLAV.PimAttributeValueId,TBLAV.AttributeCode,TBLAV.PimProductId
							)
  
						SELECT  TBLAV.PimProductId
						FROM #TBL_AttributeValueId TBLAV
						'+@InternalProductWhereClause+'	GROUP BY TBLAV.PimProductId '
			
				
			END 
			ELSE IF  @WorkingProcess = 0 
			BEGIN 

				SET  @InternalProductWhereClause = 
										STUFF( (  SELECT ' INNER JOIN Cte_AttributeValue AS ZPAVL'+CAST(ID AS VARCHAR(200))+
												' ON ( TBLAV.PimProductId = ZPAVL'+CAST(ID AS VARCHAR(200))+'.PimProductId AND ZPAVL'+CAST(ID AS VARCHAR(200))+'.AttributeCode '+AttributeCode+
												' AND ZPAVL'+CAST(ID AS VARCHAR(200))+'.AttributeValue '+AttributeValue+'  )'				
										FROM @TBL_FilterClause
										WHERE AttributeTypeName IN ('Simple Select','Multi Select')
										AND AttributeValue <> ''
										AND AttributeValue IS NOT NULL
										FOR XML PATH (''), TYPE).value('.', ' Nvarchar(MAX)'), 1, 0, '')
				SET @SQL = '  			 
							SELECT TBLAV.PimAttributeValueId,ZPAVL.PimAttributeDefaultValueId , ZPAVL.LocaleId ,COUNT(*)Over(Partition By TBLAV.PimAttributeValueId ,TBLAV.PimProductId ORDER BY TBLAV.PimAttributeValueId ,TBLAV.PimProductId  ) RowId
							INTO #temp_Table 
							FROM #TBL_AttributeValueId TBLAV 
							INNER JOIN ZnodePimProductAttributeDefaultValue ZPAVL ON (ZPAVL.PimAttributeValueId = TBLAV.PimAttributeValueId)
							WHERE (ZPAVL.LocaleId = '+Cast(@localeId AS VARCHAR(200))+' OR ZPAVL.LocaleId = '+Cast(@DefaultlocaleId AS VARCHAR(200))+')
				
							;with Cte_AttributeValue AS 
							(
							SELECT TBLAV.PimAttributeValueId ,SUBSTRING((SELECT '',''+AttributeDefaultValue FROM #TBL_AttributeDefaultValue TTR 
							INNER JOIN #temp_Table  ZPAVL ON (TTR.PimAttributeDefaultValueId = ZPAVL.PimAttributeDefaultValueId )
							WHERE ZPAVL.PimAttributeValueId = TBLAV.PimAttributeValueId  
							AND ZPAVL.LocaleId = CASE WHEN ZPAVL.RowId = 2 THEN '+CAST(@LocaleId AS Varchar(300))+' ELSE '+Cast(@DefaultLocaleId AS Varchar(300))+' END  
							FOR XML PATH('''') ),2,4000) AttributeValue,TBLAV.AttributeCode ,TBLAV.PimProductId 
							FROM #TBL_AttributeValueId TBLAV
							'+CASE WHEN NOT EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId ) THEN '' 
										ELSE ' WHERE EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId TBLP WHERE TBLP.PimProductId = TBLAV.PimProductId ) ' END+'
							GROUP BY TBLAV.PimAttributeValueId,TBLAV.AttributeCode ,TBLAV.PimProductId 
							)
  
							SELECT   TBLAV.PimProductId
							FROM  #TBL_AttributeValueId TBLAV
							'+@InternalProductWhereClause+'	GROUP BY TBLAV.PimProductId '

			END 

			DELETE FROM #TBL_PimProductIdDelete 
			INSERT INTO #TBL_PimProductIdDelete  (PimProductId)
			EXEC (@SQL)
			DELETE FROM #TBL_PimProductId
			INSERT INTO #TBL_PimProductId
			SELECT PimProductId FROM #TBL_PimProductIdDelete
 
			DROP TABLE #TBL_AttributeDefaultValue

		END 

		IF EXISTS (SELECT TOP 1 1 FROM @TBL_FilterClause WHERE AttributeTypeName IN ('Text','Number','Datetime','Yes/No') )
		BEGIN  
   
				IF @DefaultLocaleId = @LocaleID AND @WorkingProcess = 0 
				BEGIN 
					SET  @InternalProductWhereClause = 
											STUFF( (  SELECT ' INNER JOIN View_PimProducttextValue AS ZPAVL'+CAST(ID AS VARCHAR(200))+
													' ON ( TBLAV.PimProductId = ZPAVL'+CAST(ID AS VARCHAR(200))+'.PimProductId AND ZPAVL'+CAST(ID AS VARCHAR(200))+'.AttributeCode '+AttributeCode+
													' AND ZPAVL'+CAST(ID AS VARCHAR(200))+'.AttributeValue '+AttributeValue+' AND ZPAVL'+CAST(ID AS VARCHAR(200))+
													'.LocaleId='+CAST(@LocaleID AS VARCHAR(200))+' )'				
											FROM @TBL_FilterClause
											WHERE AttributeTypeName IN ('Text','Number','Datetime','Yes/No')
											FOR XML PATH (''), TYPE).value('.', ' Nvarchar(MAX)'), 1, 0, '')
 
					SET @SQL = '	SELECT  TBLAV.PimProductId 
								FROM #TBL_AttributeValueId TBLAV
								'+@InternalProductWhereClause+'
								'+CASE WHEN NOT EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId ) THEN '' 
											ELSE ' WHERE EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId TBLP WHERE TBLP.PimProductId = TBLAV.PimProductId   ) ' END 
								+' GROUP BY TBLAV.PimProductId '

			END 
			ELSE IF @WorkingProcess = 0 
			BEGIN 
				SET  @InternalProductWhereClause = 
										STUFF( (  SELECT ' INNER JOIN Cte_AttributeDetails AS ZPAVL'+CAST(ID AS VARCHAR(200))+
												' ON ( TBLAV.PimProductId = ZPAVL'+CAST(ID AS VARCHAR(200))+'.PimProductId AND ZPAVL'+CAST(ID AS VARCHAR(200))+'.AttributeCode '+AttributeCode+
												' AND ZPAVL'+CAST(ID AS VARCHAR(200))+'.AttributeValue '+AttributeValue+' AND ZPAVL'+CAST(ID AS VARCHAR(200))+
												'.LocaleId = CASE WHEN ZPAVL'+CAST(ID AS VARCHAR(200))+'.RowId = 2 THEN  '+CAST(@LocaleId AS Varchar(300))+' ELSE '+Cast(@DefaultLocaleId AS Varchar(300))+' END  )'				
										FROM @TBL_FilterClause
										WHERE AttributeTypeName IN ('Text','Number','Datetime','Yes/No')
										FOR XML PATH (''), TYPE).value('.', ' Nvarchar(MAX)'), 1, 0, '')
				SET @SQL = ' 
					;With Cte_AttributeDetails AS 
					(
					SELECT TBLAV.PimProductId,ZPAVL.AttributeValue,TBLAV.AttributeCode,ZPAVL.LocaleId ,COUNT(*)Over(Partition By TBLAV.PimProductId,TBLAV.AttributeCode ORDER BY TBLAV.PimProductId,TBLAV.AttributeCode  ) RowId
					FROM #TBL_AttributeValueId TBLAV 
					INNER JOIN ZnodePimAttributeValueLocale ZPAVL ON (ZPAVL.PimAttributeValueId = TBLAV.PimAttributeValueId )
					WHERE (LocaleId = '+Cast(@DefaultLocaleId AS Varchar(300))+' OR LocaleId = '+CAST(@LocaleId AS Varchar(300))+' )'+CASE WHEN NOT EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId ) THEN '' 
										ELSE ' AND EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId TBLP WHERE TBLP.PimProductId = TBLAV.PimProductId ) ' END +'
					) 
					SELECT  TBLAV.PimProductId 
  					FROM #TBL_AttributeValueId TBLAV
					'+@InternalProductWhereClause+'
					GROUP BY TBLAV.PimProductId 
					'
			END 
			DELETE FROM #TBL_PimProductIdDelete 
			INSERT INTO #TBL_PimProductIdDelete  (PimProductId)
			EXEC (@SQL)
			DELETE FROM #TBL_PimProductId
			INSERT INTO #TBL_PimProductId
			SELECT PimProductId FROM #TBL_PimProductIdDelete

		END 

		IF EXISTS (SELECT TOP 1 1 FROM @TBL_FilterClause WHERE AttributeTypeName IN ('Text Area') )
		BEGIN    
			IF @DefaultLocaleId = @LocaleID AND @WorkingProcess = 0 
			BEGIN 
				SET  @InternalProductWhereClause = 
											STUFF( (  SELECT ' INNER JOIN View_PimProductTextAreaValue AS ZPAVL'+CAST(ID AS VARCHAR(200))+
												' ON ( TBLAV.PimProductId = ZPAVL'+CAST(ID AS VARCHAR(200))+'.PimProductId AND ZPAVL'+CAST(ID AS VARCHAR(200))+'.AttributeCode '+AttributeCode+
												' AND ZPAVL'+CAST(ID AS VARCHAR(200))+'.AttributeValue '+AttributeValue+' AND ZPAVL'+CAST(ID AS VARCHAR(200))+
												'.LocaleId='+CAST(@LocaleID AS VARCHAR(200))+' )'				
										FROM @TBL_FilterClause
										WHERE AttributeTypeName IN ('Text Area')
										FOR XML PATH (''), TYPE).value('.', ' Nvarchar(MAX)'), 1, 0, '')
 
				SET @SQL = '
							SELECT  TBLAV.PimProductId 
							FROM #TBL_AttributeValueId TBLAV
							'+@InternalProductWhereClause+CASE WHEN NOT EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId ) THEN '' 
										ELSE ' WHERE EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId TBLP WHERE TBLP.PimProductId = TBLAV.PimProductId ) ' END 
										+' GROUP BY TBLAV.PimProductId '
			END 
			ELSE IF @WorkingProcess = 0 
			BEGIN 
				SET  @InternalProductWhereClause = 
										STUFF( (  SELECT ' INNER JOIN Cte_AttributeDetails AS ZPAVL'+CAST(ID AS VARCHAR(200))+
												' ON ( TBLAV.PimProductId = ZPAVL'+CAST(ID AS VARCHAR(200))+'.PimProductId AND ZPAVL'+CAST(ID AS VARCHAR(200))+'.AttributeCode '+AttributeCode+
												' AND ZPAVL'+CAST(ID AS VARCHAR(200))+'.AttributeValue '+AttributeValue+' AND ZPAVL'+CAST(ID AS VARCHAR(200))+
												'.LocaleId = CASE WHEN ZPAVL'+CAST(ID AS VARCHAR(200))+'.RowId = 2 THEN  '+CAST(@LocaleId AS Varchar(300))+' ELSE '+Cast(@DefaultLocaleId AS Varchar(300))+' END  )'				
										FROM @TBL_FilterClause
										WHERE AttributeTypeName IN ('Text Area')
										FOR XML PATH (''), TYPE).value('.', ' Nvarchar(MAX)'), 1, 0, '')
				SET @SQL = ' 
					;With Cte_AttributeDetails AS 
					(
					SELECT TBLAV.PimProductId,TBLAV.AttributeCode,ZPAVL.AttributeValue,ZPAVL.LocaleId ,COUNT(*)Over(Partition By TBLAV.PimProductId,TBLAV.AttributeCode ORDER BY TBLAV.PimProductId,TBLAV.AttributeCode  ) RowId
					FROM #TBL_AttributeValueId TBLAV 
					INNER JOIN ZnodePimProductAttributeTextAreaValue ZPAVL ON (ZPAVL.PimAttributeValueId = TBLAV.PimAttributeValueId )
					WHERE (LocaleId = '+Cast(@DefaultLocaleId AS Varchar(300))+' OR LocaleId = '+CAST(@LocaleId AS Varchar(300))+' )
	 
					) 
					SELECT  TBLAV.PimProductId 
  					FROM #TBL_AttributeValueId TBLAV
					'+@InternalProductWhereClause+CASE WHEN NOT EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId ) THEN '' 
										ELSE ' WHERE EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId TBLP WHERE TBLP.PimProductId = TBLAV.PimProductId ) ' END 
										+' 
										GROUP BY TBLAV.PimProductId 	
										'
				END 
			DELETE FROM #TBL_PimProductIdDelete 
			INSERT INTO #TBL_PimProductIdDelete  (PimProductId)
			EXEC (@SQL)
			DELETE FROM #TBL_PimProductId
			INSERT INTO #TBL_PimProductId
			SELECT PimProductId FROM #TBL_PimProductIdDelete

		END 
		IF EXISTS (SELECT TOP 1 1 FROM @TBL_FilterClause WHERE AttributeCode  LIKE '%PublishStatus%' )
		BEGIN    
 
				SET @SQL = '
							SELECT  TBLAV.PimProductId 
							FROM ZnodePimProduct TBLAV
							WHERE CASE WHEN TBLAV.IsProductPublish  IS NULL THEN ''Not Published'' 
							WHEN TBLAV.IsProductPublish = 0 THEN ''Draft''
							ELSE  ''Published'' END '+(SELECT TOP 1 AttributeValue FROM @TBL_FilterClause WHERE AttributeCode LIKE '%PublishStatus%')+CASE WHEN NOT EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId ) THEN '' 
										ELSE ' AND EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId TBLP WHERE TBLP.PimProductId = TBLAV.PimProductId ) ' END 
										+' GROUP BY TBLAV.PimProductId '
  
				DELETE FROM #TBL_PimProductIdDelete 
				INSERT INTO #TBL_PimProductIdDelete  (PimProductId)
				EXEC (@SQL)
				DELETE FROM #TBL_PimProductId
				INSERT INTO #TBL_PimProductId
				SELECT PimProductId FROM #TBL_PimProductIdDelete

		END 
		IF EXISTS (SELECT TOP 1 1 FROM @TBL_FilterClause WHERE AttributeCode  LIKE '%AttributeFamily%' )
		BEGIN 

				;With Cte_attributeValue AS 
				(
					SELECT ZPAF.PimAttributeFamilyId,FamilyCode,AttributeFamilyName ,ZPFL.LocaleId
					FROM ZnodePimAttributeFamily ZPAF
					INNER JOIN ZnodePimFamilyLocale ZPFL ON (ZPFL.PimAttributeFamilyId = ZPAF.PimAttributeFamilyId) 
					WHERE ZPFL.LocaleId IN (@DefaultLocaleId,@LocaleId)
				) 
				, Cte_AttributeValueAttribute AS 
				(
					SELECT PimAttributeFamilyId,FamilyCode,AttributeFamilyName
					FROM Cte_attributeValue RTY 
					WHERE LocaleId = @LocaleId
				)
				, Cte_AttributeValueTht AS 
				(
					SELECT PimAttributeFamilyId,FamilyCode,AttributeFamilyName
					FROM Cte_AttributeValueAttribute
					UNION ALL 
					SELECT PimAttributeFamilyId,FamilyCode,AttributeFamilyName
					FROM Cte_attributeValue TYY  
					WHERE NOT EXISTS (SELECT TOP 1 1 FROM Cte_AttributeValueAttribute THE WHERE THE.PimAttributeFamilyId = TYY.PimAttributeFamilyId )
					AND TYY.LocaleId = @DefaultLocaleId
				)
				SELECT PimAttributeFamilyId,FamilyCode,AttributeFamilyName
				INTO #TBL_FamilyLocale
				FROM Cte_AttributeValueTht 


				SET @SQL = '
							SELECT  TBLAV.PimProductId 
							FROM ZnodePimProduct TBLAV 
							INNER JOIN #TBL_FamilyLocale THY ON (THY.PimAttributeFamilyId = TBLAV.PimAttributeFamilyId )
							WHERE AttributeFamilyName '+(SELECT TOP 1 AttributeValue FROM @TBL_FilterClause WHERE AttributeCode LIKE '%AttributeFamily%')+CASE WHEN NOT EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId ) THEN '' 
										ELSE ' AND EXISTS (SELECT TOP 1 1 FROM #TBL_PimProductId TBLP WHERE TBLP.PimProductId = TBLAV.PimProductId ) ' END 
										+' GROUP BY TBLAV.PimProductId '
  

			DELETE FROM #TBL_PimProductIdDelete 
			INSERT INTO #TBL_PimProductIdDelete  (PimProductId)
			EXEC (@SQL)
			DELETE FROM #TBL_PimProductId
			INSERT INTO #TBL_PimProductId
			SELECT PimProductId FROM #TBL_PimProductIdDelete

		END 
	
		SET @SQL = '
		IF EXISTS ( SELECT TOP 1 1 FROM tempdb..sysobjects WHERE name = ''##Temp_PimProductId'+CAST(@@SPID AS VARCHAR(500))+''' )
		BEGIN 
			DROP TABLE ##Temp_PimProductId'+CAST(@@SPID AS VARCHAR(500))+'
		END 
		CREATE TABLE ##Temp_PimProductId'+CAST(@@SPID AS VARCHAR(500))+' (PimProductId INT )
		INSERT INTO  ##Temp_PimProductId'+CAST(@@SPID AS VARCHAR(500))+'
		SELECT PimProductId 
		FROM #TBL_PimProductId
		'
		EXEC (@SQL)
		DROP TABLE #TBL_PimProductId
		DROP TABLE #TBL_AttributeValueId
		DROP TABLE #TBL_PimProductIdDelete
 END
 GO
 IF EXISTS(SELECT * FROM SYS.PROCEDURES WHERE NAME = 'Znode_GetOmsOrderDetail')
	DROP PROC Znode_GetOmsOrderDetail
GO
CREATE PROCEDURE [dbo].[Znode_GetOmsOrderDetail]
( @WhereClause NVARCHAR(MAX),
  @Rows        INT            = 100,
  @PageNo      INT            = 1,
  @Order_BY    VARCHAR(1000)  = '',
  @RowsCount   INT OUT			,
  @UserId	   INT = 0 ,
  @IsFromAdmin int=0,
  @SalesRepUserId int = 0 
 )
AS
    /*
     Summary : This procedure is used to get the oms order detils
			   Records are fetched for those users who placed the order i.e UserId is Present in ZnodeUser and  ZnodeOmsOrderDetails tables
	 Unit Testing:

    EXEC [Znode_GetOmsOrderDetail_SCT] 'PortalId =1',@Order_BY = '',@RowsCount= 0, @UserId = 0 ,@Rows = 50, @PageNo = 1

	declare @p7 int
	set @p7=4
	exec sp_executesql N'Znode_GetOmsOrderDetail_SCT @WhereClause, @Rows,@PageNo,@Order_By,@RowCount OUT,@UserId,@IsFromAdmin',N'@WhereClause nvarchar(30),@Rows int,@PageNo int,@Order_By nvarchar(14),@RowCount int output,@UserId int,@IsFromAdmin int',@WhereClause=N'(PortalId in(''1'',''4'',''5'',''6''))',@Rows=50,@PageNo=1,@Order_By=N'orderdate desc',@RowCount=@p7 output,@UserId=0,@IsFromAdmin=1
	select @p7



   */
BEGIN
    BEGIN TRY
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
		DECLARE @SQL NVARCHAR(MAX), @ProcessType  varchar(50)='Order'
		DECLARE @OrderLineItemRelationshipTypeId INT
		SET @OrderLineItemRelationshipTypeId = ( SELECT top 1 OrderLineItemRelationshipTypeId  FROM ZnodeOmsOrderLineItemRelationshipType where Name = 'AddOns' )

		----Verifying that the @SalesRepUserId is having 'Sales Rep' role
		IF NOT EXISTS
		(
			SELECT * FROM ZnodeUser ZU
			INNER JOIN AspNetZnodeUser ANZU ON ZU.UserName = ANZU.UserName
			INNER JOIN AspNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName
			INNER JOIN AspNetUserRoles ANUR ON ANU.Id = ANUR.UserId
			Where Exists(select * from AspNetRoles ANR Where Name = 'Sales Rep' AND ANUR.RoleId = ANR.Id) 
			AND ZU.UserId = @SalesRepUserId
		)   
		Begin
			SET @SalesRepUserId = 0
		End

		DECLARE @Fn_GetPaginationWhereClause VARCHAR(500) = dbo.Fn_GetPaginationWhereClause(@PageNo,@Rows),
		@Fn_GetFilterWhereClause NVARCHAR(MAX) = ''
		set @Fn_GetFilterWhereClause=dbo.Fn_GetFilterWhereClause(@WhereClause)
			
		IF @Order_BY = ''
			set @Order_BY = 'OrderDate desc'

			set @Order_BY = replace(@Order_BY,'PortalId','ZODD.PortalId')
			set @Order_BY = replace(@Order_BY,'UserName','ISNULL(RTRIM(LTRIM(ZODD.FirstName)),'''')+'' ''+ISNULL(RTRIM(LTRIM(ZODD.LastName)),'''')')
			set @Order_BY = replace(@Order_BY,'email','ZODD.Email')
			set @Order_BY = replace(@Order_BY,'OrderState','case when ZOS.IsShowToCustomer=0 and '+cast( @IsFromAdmin as varchar(50))+' = 0 then ZOSC.Description else  ZOS.Description end')
			set @Order_BY = replace(@Order_BY,'PaymentStatus','ZOPS.Name')
			set @Order_BY = replace(@Order_BY,'PublishState','ZODPS.DisplayName')
			set @Order_BY = replace(@Order_BY,'StoreName','ZP.StoreName')

			Declare @Fn_GetPagingRowId NVARCHAR(MAX) = ' DENSE_RANK()Over('+ ' Order By '+CASE WHEN Isnull(@Order_BY,'') = '' THEN 'OmsOrderId DESC' ELSE @Order_BY + ',OmsOrderId DESC' END  + ') RowId '
						
			IF OBJECT_ID('tempdb..#TBL_RowCount') is not null
				DROP TABLE #TBL_RowCount

			IF OBJECT_ID('tempdb..#Portal') is not null
				DROP TABLE #Portal
			
			Create table #TBL_RowCount(RowsCount int )
			CREATE TABLE #tbl_GetRecurciveUserId  (ID INT IDENTITY(1,1) Primary key,UserId INT,ParentUserId INT)
			INSERT INTO #tbl_GetRecurciveUserId
			SELECT UserId,ParentUserId FROM dbo.Fn_GetRecurciveUserId (CAST(@UserId AS VARCHAR(50)),@ProcessType ) FNRU
			 
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'PortalId','ZODD.PortalId')
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'UserName','ISNULL(RTRIM(LTRIM(ZODD.FirstName)),'''')+'' ''+ISNULL(RTRIM(LTRIM(ZODD.LastName)),'''')')
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'email','ZODD.Email')
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'OrderState','case when ZOS.IsShowToCustomer=0 and '+cast( @IsFromAdmin as varchar(50))+' = 0 then ZOSC.Description else  ZOS.Description end')
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'PaymentStatus','ZOPS.Name')
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'PublishState','ZODPS.DisplayName')
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'StoreName','ZP.StoreName')

			set @Order_BY = replace(@Order_BY,'ShippingPostalCode','BillingPostalCode')

			set @Fn_GetPagingRowId = replace(@Fn_GetPagingRowId,'OmsOrderId','Zoo.OmsOrderId')

			set @Rows = @PageNo * @Rows

			CREATE TABLE #Portal (PortalId int,StoreName varchar(200))
			insert into #Portal
			select PortalId,StoreName
			from ZnodePortal

		SET @SQL = '
		SELECT Distinct top '+cast(@Rows as varchar(10))+' Zoo.OmsOrderId,Zoo.OrderNumber, ZODD.PortalId,ZP.StoreName ,ZODD.CurrencyCode,
		case when ZOS.IsShowToCustomer=0 and '+cast( @IsFromAdmin as varchar(50))+' = 0 then ZOSC.Description else  ZOS.Description end  OrderState,ZODD.ShippingId,ZODD.PaymentTypeId,ZODD.PaymentSettingId
		,ZOPS.Name PaymentStatus,ZPS.Name PaymentType,CAST(1 AS BIT) ShippingStatus ,ZODD.OrderDate,ZODD.UserId,ISNULL(RTRIM(LTRIM(ZODD.FirstName)),'''')
		+'' ''+ISNULL(RTRIM(LTRIM(ZODD.LastName)),'''') UserName ,ZODD.PaymentTransactionToken ,ZODD.Total ,ZODD.OmsOrderDetailsId,ZODD.PoDocument,
		ZODD.Email ,ZODD.PhoneNumber ,ZODD.SubTotal ,ZODD.TaxCost ,ZODD.ShippingCost,ZODD.BillingPostalCode,
		ZODD.ModifiedDate AS OrderModifiedDate,  ZODD.PaymentDisplayName  ,isnull(Zoo.ExternalId,0) ExternalId,ZODD.CreditCardExpMonth,ZODD.CultureCode--,ZODD.TotalAdditionalCost
		,ZODD.CreditCardExpYear,ZODD.CardType,ZODD.CreditCardNumber,ZODD.PaymentExternalId,ZODPS.DisplayName as PublishState,
		ZOOLI.ProductName ProductName, 0 CountId, CAST (0 as bit) IsInRMA, '+@Fn_GetPagingRowId+' 
		INTO #Cte_OrderLineDescribe
		FROM ZnodeOmsOrder (nolock) ZOO 
		INNER JOIN ZnodeOmsOrderDetails (nolock) ZODD ON (ZODD.OmsOrderId = ZOO.OmsOrderId AND  ZODD.IsActive = 1)
		INNER JOIN ZnodePublishState ZODPS ON (ZODPS.PublishStateId = ZOO.PublishStateId)
		INNER JOIN #Portal ZP (nolock) ON ZODD.PortalId = ZP.PortalId
		LEFT JOIN ZnodePaymentType (nolock) ZPS ON (ZPS.PaymentTypeId = ZODD.PaymentTypeId )
		LEFT JOIN ZnodeOmsOrderStateShowToCustomer (nolock) ZOSC ON (ZOSC.OmsOrderStateId = ZODD.OmsOrderStateId)
		LEFT JOIN ZnodeOmsOrderState (nolock) ZOS ON (ZOS.OmsOrderStateId = ZODD.OmsOrderStateId)
		LEFT JOIN ZnodeOmsPaymentState (nolock) ZOPS ON (ZOPS.OmsPaymentStateId = ZODD.OmsPaymentStateId)
		LEFT JOIN ZnodeOmsOrderLineItems ZOOLI on ZOOLI.OmsOrderDetailsId = ZODD.OmsOrderDetailsId and ParentOmsOrderLineItemsId is null
		WHERE (exists(select * from ZnodeSalesRepCustomerUserPortal SalRep where SalRep.SalesRepUserId = '+cast(@SalesRepUserId as varchar(10))+' and ZODD.UserId = SalRep.CustomerUserid) or '+cast(@SalesRepUserId as varchar(10))+' = 0 )  
		AND (EXISTS (SELECT TOP 1 1 FROM #tbl_GetRecurciveUserId FNRU WHERE FNRU.UserId = ZODD.UserId ) OR '+cast(@UserId as varchar(10))+'  =0 )'
		+ @Fn_GetFilterWhereClause+' 

		Insert Into #TBL_RowCount 
		SELECT count(*)
		FROM ZnodeOmsOrder (nolock) ZOO 
		INNER JOIN ZnodeOmsOrderDetails (nolock) ZODD ON (ZODD.OmsOrderId = ZOO.OmsOrderId AND  ZODD.IsActive = 1)
		INNER JOIN ZnodePublishState ZODPS ON (ZODPS.PublishStateId = ZOO.PublishStateId)
		INNER JOIN #Portal ZP (nolock) ON ZODD.PortalId = ZP.PortalId
		LEFT JOIN ZnodePaymentType (nolock) ZPS ON (ZPS.PaymentTypeId = ZODD.PaymentTypeId )
		LEFT JOIN ZnodeOmsOrderStateShowToCustomer (nolock) ZOSC ON (ZOSC.OmsOrderStateId = ZODD.OmsOrderStateId)
		LEFT JOIN ZnodeOmsOrderState (nolock) ZOS ON (ZOS.OmsOrderStateId = ZODD.OmsOrderStateId)
		LEFT JOIN ZnodeOmsPaymentState (nolock) ZOPS ON (ZOPS.OmsPaymentStateId = ZODD.OmsPaymentStateId)
		WHERE (EXISTS(select * from ZnodeSalesRepCustomerUserPortal SalRep where SalRep.SalesRepUserId = '+cast(@SalesRepUserId as varchar(10))+' and ZODD.UserId = SalRep.CustomerUserid) or '+cast(@SalesRepUserId as varchar(10))+' = 0 )  
		AND (EXISTS (SELECT TOP 1 1 FROM #tbl_GetRecurciveUserId FNRU WHERE FNRU.UserId = ZODD.UserId ) OR '+cast(@UserId as varchar(10))+'  =0 )'
		+ @Fn_GetFilterWhereClause+' 
			
		Create index Ind_OrderLineDescribe_RowId on #Cte_OrderLineDescribe(RowId )

		SELECT OmsOrderId,OrderNumber,PortalId,StoreName,CurrencyCode,OrderState,ShippingId,
		PaymentTypeId,PaymentSettingId,PaymentStatus,PaymentType,ShippingStatus,OrderDate,UserId,UserName,PaymentTransactionToken,Total,
		ProductName OrderItem,OmsOrderDetailsId,CountId ItemCount, PoDocument AS PODocumentPath,IsInRMA ,
		Email,PhoneNumber,SubTotal,TaxCost,ShippingCost,BillingPostalCode
		,OrderModifiedDate,PaymentDisplayName, 
		ExternalId,CreditCardExpMonth,CreditCardExpYear,CardType,CreditCardNumber,PaymentExternalId,CultureCode,PublishState --TotalAdditionalCost
		FROM #Cte_OrderLineDescribe
		' + @Fn_GetPaginationWhereClause +' order by RowId '

		print @SQL
		EXEC(@SQL)
		Select @RowsCount= isnull(RowsCount  ,0) from #TBL_RowCount
		
		IF OBJECT_ID('tempdb..#TBL_RowCount') is not null
				DROP TABLE #TBL_RowCount

		IF OBJECT_ID('tempdb..#Portal') is not null
			DROP TABLE #Portal
		
    END TRY
    BEGIN CATCH
        DECLARE @Status BIT ;
		SET @Status = 0;
		DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(),
		@ErrorCall NVARCHAR(MAX)= 'EXEC Znode_GetOmsOrderDetail @WhereClause = '''+ISNULL(CAST(@WhereClause AS VARCHAR(max)),'''''')+''',@Rows='''+ISNULL(CAST(@Rows AS VARCHAR(50)),'''''')+''',@PageNo='+ISNULL(CAST(@PageNo AS VARCHAR(50)),'''')+',
		@Order_BY='+ISNULL(@Order_BY,'''''')+',@UserId = '+ISNULL(CAST(@UserId AS VARCHAR(50)),'''')+',@RowsCount='+ISNULL(CAST(@RowsCount AS VARCHAR(50)),'''')+',@IsFromAdmin='+ISNULL(CAST(@IsFromAdmin AS VARCHAR(10)),'''');
              			 
        SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                    
		  
        EXEC Znode_InsertProcedureErrorLog
		@ProcedureName = 'Znode_GetOmsOrderDetail',
		@ErrorInProcedure = 'Znode_GetOmsOrderDetail',
		@ErrorMessage = @ErrorMessage,
		@ErrorLine = @ErrorLine,
		@ErrorCall = @ErrorCall;
    END CATCH;
END;
GO
IF EXISTS(SELECT * FROM SYS.PROCEDURES WHERE NAME = 'Znode_ImportAttributeDefaultValue')
	DROP PROC Znode_ImportAttributeDefaultValue
GO
CREATE PROCEDURE [dbo].[Znode_ImportAttributeDefaultValue](
	  @TableName nvarchar(100), @Status bit OUT, @UserId int, @ImportProcessLogId int, @NewGUId nvarchar(200))
AS
	--------------------------------------------------------------------------------------
	-- Summary :  Import Attribute Code Name and their default input validation rule other 
	--			  flag will be inserted as default we need to modify front end
	
	-- Unit Testing: 

	--------------------------------------------------------------------------------------
BEGIN
	BEGIN TRAN A;
	BEGIN TRY
		DECLARE @MessageDisplay nvarchar(100), @SSQL nvarchar(max);
		DECLARE @GetDate datetime= dbo.Fn_GetDate(), @LocaleId int  ;
		SELECT @LocaleId = DBO.Fn_GetDefaultLocaleId();
		-- Retrive RoundOff Value from global setting 
		DECLARE @InsertPimAtrribute TABLE
		( 
			RowId int IDENTITY(1, 1) PRIMARY KEY,
			-- RowNumber int, AttributeName varchar(300), AttributeCode varchar(300), AttributeType varchar(300), DisplayOrder int, GUID nvarchar(400)
			RowNumber int, AttributeCode varchar(300),AttributeDefaultValueCode varchar(300),AttributeDefaultValue varchar(1000),IsEditable varchar(10),DisplayOrder int, IsDefault varchar(10), SwatchText varchar(1000),SwatchImage varchar(500), SwatchImagePath varchar(500), GUID nvarchar(400)
		
		);
		DECLARE @InsertedPimAttributeIds TABLE (PimAttributeId int ,PimAttributeDefaultValueId int,AttributeDefaultValueCode nvarchar(300))
		
		SET @SSQL = 'Select RowNumber,AttributeCode,AttributeDefaultValueCode,AttributeDefaultValue,IsEditable,DisplayOrder,IsDefault,SwatchText,SwatchImage, SwatchImagePath ,GUID FROM '+@TableName;
		INSERT INTO @InsertPimAtrribute( RowNumber,AttributeCode,AttributeDefaultValueCode,AttributeDefaultValue,IsEditable,DisplayOrder,IsDefault,SwatchText,SwatchImage, SwatchImagePath ,GUID)
		EXEC sys.sp_sqlexec @SSQL;

		--@MessageDisplay will use to display validate message for input inventory value  
		DECLARE @AttributeCode TABLE
		( 
		   AttributeCode nvarchar(300)
		);
		INSERT INTO @AttributeCode
			   SELECT AttributeCode
			   FROM ZnodePimAttribute 

		-- Start Functional Validation 
		-----------------------------------------------
		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '9', 'AttributeCode', AttributeCode, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM @InsertPimAtrribute AS ii
			   WHERE ii.AttributeCode not in 
			   (
				   SELECT AttributeCode FROM @AttributeCode
			   );

		WITH CTE_AttributeDefaultValueCode AS(
		SELECT AttributeDefaultValueCode , AttributeCode, RowNumber ,ROW_NUMBER ()OVER (PARTITION BY AttributeDefaultValueCode , AttributeCode ORDER BY AttributeDefaultValueCode , AttributeCode )  Row_Id
		FROM @InsertPimAtrribute Z
		
		
		)
		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '53', 'AttributeDefaultValueCode', AttributeDefaultValueCode, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM @InsertPimAtrribute AS ii
			   WHERE EXISTS
			  ( SELECT TOP 1 1 FROM CTE_AttributeDefaultValueCode Z WHERE Z.RowNumber=ii.RowNumber AND Z.Row_Id >1
			   )
			  

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '50', 'AttributeDefaultValueCode', AttributeDefaultValueCode, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM @InsertPimAtrribute AS ii
			   WHERE ltrim(rtrim(isnull(ii.AttributeDefaultValueCode,''))) like '%[^0-9A-Za-z]%'

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '35', 'AttributeDefaultValueCode', AttributeDefaultValueCode, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM @InsertPimAtrribute AS ii
			   WHERE ltrim(rtrim(isnull(ii.AttributeDefaultValueCode,''))) like '% %' -----space not allowed

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )  
			SELECT '35', 'IsEditable', IsEditable, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId  
			FROM @InsertPimAtrribute AS ii  
			WHERE ii.IsEditable not in ('True','1','Yes','FALSE','0','No','')

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )  
			SELECT '35', 'IsDefault', IsDefault, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId  
			FROM @InsertPimAtrribute AS ii  
			WHERE ii.IsDefault not in ('True','1','Yes','FALSE','0','No','')

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			SELECT '17', 'DisplayOrder', DisplayOrder, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
			FROM @InsertPimAtrribute AS ii
			WHERE (ii.DisplayOrder <> '' OR ii.DisplayOrder IS NOT NULL ) AND  ii.DisplayOrder = 0

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			SELECT '64', 'DisplayOrder', DisplayOrder, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
			FROM @InsertPimAtrribute AS ii
			WHERE (ii.DisplayOrder <> '' OR ii.DisplayOrder IS NOT NULL )AND  ii.DisplayOrder > 99999

		UPDATE ZIL
			SET ZIL.ColumnName =   ZIL.ColumnName + ' [ AttributeDefaultValueCode - ' + ISNULL(AttributeDefaultValueCode,'') + ' ] '
			FROM ZnodeImportLog ZIL 
			INNER JOIN @InsertPimAtrribute IPA ON (ZIL.RowNumber = IPA.RowNumber)
			WHERE  ZIL.ImportProcessLogId = @ImportProcessLogId AND ZIL.RowNumber IS NOT NULL


		-- End Function Validation 	
		-----------------------------------------------
		-- Delete Invalid Data after functional validatin  
		DELETE FROM @InsertPimAtrribute
		WHERE RowNumber IN
		(
			SELECT DISTINCT 
				   RowNumber
			FROM ZnodeImportLog
			WHERE ImportProcessLogId = @ImportProcessLogId  and RowNumber is not null 
		);
		
		-- Update Record count in log 
        DECLARE @FailedRecordCount BIGINT
		DECLARE @SuccessRecordCount BIGINT
		SELECT @FailedRecordCount = COUNT(DISTINCT RowNumber) FROM ZnodeImportLog WHERE RowNumber IS NOT NULL AND  ImportProcessLogId = @ImportProcessLogId;
		Select @SuccessRecordCount = count(DISTINCT RowNumber) FROM @InsertPimAtrribute
		UPDATE ZnodeImportProcessLog SET FailedRecordcount = @FailedRecordCount , SuccessRecordCount = @SuccessRecordCount
		,	TotalProcessedRecords = (ISNULL(@FailedRecordCount,0) + ISNULL(@SuccessRecordCount,0)) 
		WHERE ImportProcessLogId = @ImportProcessLogId;

		DECLARE @MediaId INT
		SET @MediaId = (SELECT TOP 1 MediaId from @InsertPimAtrribute IPA INNER JOIN ZnodeMedia ZM ON IPA.SwatchImage = ZM.FileName and IPA.SwatchImagePath = ZM.Path)

		if (isnull(@MediaId,0)=0)
			SET @MediaId = (SELECT max(MediaId) from @InsertPimAtrribute IPA INNER JOIN ZnodeMedia ZM ON IPA.SwatchImage = ZM.FileName)

        update ZPADV set ZPADV.IsEditable = case when IPA.IsEditable in ('True','1','Yes') then 1 else 0 end,ZPADV.DisplayOrder = Case when Isnull(IPA.DisplayOrder,0) <> 0 then  IPA.DisplayOrder else ZPADV.DisplayOrder end ,
		                 ZPADV.IsDefault = case when IPA.IsDefault in ('True','1','Yes') then 1 else 0 end ,ZPADV.SwatchText = IPA.SwatchText ,
		                 ZPADV.MediaId = case when isnull(@MediaId,0)= 0 then ZPADV.MediaId else @MediaId end, ZPADV.ModifiedBy = @UserId, ZPADV.ModifiedDate = @GetDate 
		from @InsertPimAtrribute IPA 
		INNER JOIN ZnodePimAttribute ZPA ON IPA.AttributeCode = ZPA.AttributeCode 
		inner join ZnodePimAttributeDefaultValue ZPADV on ZPA.PimAttributeId = ZPADV.PimAttributeId and IPA.AttributeDefaultValueCode = ZPADV.AttributeDefaultValueCode

		update ZPADVL set ZPADVL.AttributeDefaultValue = IPA.AttributeDefaultValue, ZPADVL.ModifiedBy = @UserId, ZPADVL.ModifiedDate = @GetDate 
		from @InsertPimAtrribute IPA 
		INNER JOIN ZnodePimAttribute ZPA ON IPA.AttributeCode = ZPA.AttributeCode 
		inner join ZnodePimAttributeDefaultValue ZPADV on ZPA.PimAttributeId = ZPADV.PimAttributeId and IPA.AttributeDefaultValueCode = ZPADV.AttributeDefaultValueCode
		inner join ZnodePimAttributeDefaultValueLocale ZPADVL ON ( ZPADVL.PimAttributeDefaultValueId = ZPADV.PimAttributeDefaultValueId)

		--- Insert data into base table ZnodePimatrribute with their validation 

		INSERT INTO ZnodePimAttributeDefaultValue (PimAttributeId,AttributeDefaultValueCode,IsEditable,DisplayOrder,IsDefault,SwatchText,MediaId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)		
		OUTPUT Inserted.PimAttributeId,Inserted.PimAttributeDefaultValueId,Inserted.AttributeDefaultValueCode INTO @InsertedPimAttributeIds  		
		SELECT ZPA.PimAttributeId,IPA.AttributeDefaultValueCode, case when IPA.IsEditable in ('True','1','Yes') then 1 else 0 end , Case when Isnull(IPA.DisplayOrder,0) = 0 then  99999 else IPA.DisplayOrder end  , 
		       case when IPA.IsDefault in ('True','1','Yes') then 1 else 0 end , IPA.SwatchText, @MediaId,@UserId , @GetDate ,@UserId , @GetDate 
		from @InsertPimAtrribute IPA 
		INNER JOIN ZnodePimAttribute ZPA ON IPA.AttributeCode = ZPA.AttributeCode  
		where not exists(select * from ZnodePimAttributeDefaultValue ZPADV where ZPA.PimAttributeId = ZPADV.PimAttributeId and IPA.AttributeDefaultValueCode = ZPADV.AttributeDefaultValueCode)
		
		INSERT INTO ZnodePimAttributeDefaultValueLocale(LocaleId,PimAttributeDefaultValueId,AttributeDefaultValue,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		Select @LocaleId ,IPAS.PimAttributeDefaultValueId, IPA.AttributeDefaultValue, '', @UserId , @GetDate ,@UserId , @GetDate   
		FROM @InsertedPimAttributeIds IPAS 
		INNER JOIN ZnodePimAttribute ZPA ON IPAS.PimAttributeId = ZPA.PimAttributeId  
		INNER JOIN @InsertPimAtrribute IPA ON ZPA.AttributeCode= IPA.AttributeCode and IPAS.AttributeDefaultValueCode = IPA.AttributeDefaultValueCode


		UPDATE ZnodeImportProcessLog
		  SET STATUS = dbo.Fn_GetImportStatus( 2 ), ProcessCompletedDate = @GetDate
		WHERE ImportProcessLogId = @ImportProcessLogId;
		COMMIT TRAN A;
	END TRY
	BEGIN CATCH
	ROLLBACK TRAN A;

		SET @Status = 0;
		SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE();
	
		DECLARE @Error_procedure VARCHAR(8000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(), @ErrorCall NVARCHAR(MAX)= 'EXEC Znode_ImportAttributeDefaultValue @TableName = '+CAST(@TableName AS VARCHAR(max)) +',@Status='+ CAST(@Status AS VARCHAR(10))+',@UserId = '+CAST(@UserId AS VARCHAR(50))+',@ImportProcessLogId='+CAST(@ImportProcessLogId AS VARCHAR(10))+',@NewGUId='+CAST(@NewGUId AS VARCHAR(200));

		---Import process updating fail due to database error
		UPDATE ZnodeImportProcessLog
		SET Status = dbo.Fn_GetImportStatus( 3 ), ProcessCompletedDate = @GetDate
		WHERE ImportProcessLogId = @ImportProcessLogId;

		---Loging error for Import process due to database error
		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		SELECT '93', '', '', @NewGUId,  @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId

		--Updating total and fail record count
		UPDATE ZnodeImportProcessLog SET FailedRecordcount = (SELECT TOP 1 RowsCount FROM Znode_ImportCsvRowCount with (nolock) WHERE ImportProcessLogId = @ImportProcessLogId) , SuccessRecordCount = 0 ,
		TotalProcessedRecords = (SELECT TOP 1 RowsCount FROM Znode_ImportCsvRowCount with (nolock) Where ImportProcessLogId = @ImportProcessLogId)
		WHERE ImportProcessLogId = @ImportProcessLogId;

		EXEC Znode_InsertProcedureErrorLog
		@ProcedureName = 'Znode_ImportAttributeDefaultValue',
		@ErrorInProcedure = @Error_procedure,
		@ErrorMessage = @ErrorMessage,
		@ErrorLine = @ErrorLine,
		@ErrorCall = @ErrorCall;
		

	END CATCH;
END;
GO
UPDATE  ZnodeEmailTemplateLocale SET Content = '<!DOCTYPE html><html><body><div>  <div style="font-family: Arial, Helvetica; font-size: 10px; text-align: left; color: #292a2a; border: solid 1px #c3c3c3; margin-top: 10px;">  <div>#StoreLogo# #SiteName#&nbsp;Customer Receipt</div>  <div style="padding: 10px;">  <div style="font-family: Verdana; color: #333333; font-size: 11px;">#ReceiptText#</div>  <table style="font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" border="0" width="100%" cellspacing="3" cellpadding="0">  <tbody>  <tr>  <td colspan="5"><hr /></td>  </tr>  <tr>  <td colspan="2" align="left" nowrap="nowrap" width="25%">  <div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">Order Information</div>  </td>  <td colspan="2" align="left" nowrap="nowrap">  <div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">Customer Service</div>  </td>  </tr>  <tr>  <td align="left" nowrap="nowrap" width="10%"><strong>Order Number:</strong></td>  <td align="left" nowrap="nowrap" width="30%">#OrderId#</td>  <td align="left" nowrap="nowrap" width="4%"><strong>E-Mail:</strong></td>  <td align="left" nowrap="nowrap">#CustomerServiceEmail#</td>  </tr>  <tr>  <td align="left" nowrap="nowrap"><strong>Order Date:</strong></td>  <td align="left" nowrap="nowrap">#OrderDate#</td>  <td align="left" nowrap="nowrap"><strong>Phone:</strong></td>  <td align="left" nowrap="nowrap">#CustomerServicePhoneNumber#</td>  </tr>  <tr>  <td style="vertical-align: top;" align="left" nowrap="nowrap"><strong>Promotion Code:</strong></td>  <td align="left" nowrap="nowrap">#PromotionCode#</td>  <td>&nbsp;</td>  <td>&nbsp;</td>  </tr>  <tr>  <td align="left" nowrap="nowrap"><strong>Payment Method:</strong></td>  <td align="left" nowrap="nowrap">#PaymentName#</td>  <td>&nbsp;</td>  <td>&nbsp;</td>  </tr>  <tr>  <td align="left" nowrap="nowrap"><strong>#CardTransactionLabel#</strong></td>  <td align="left" nowrap="nowrap">#CardTransactionID#</td>  <td>&nbsp;</td>  <td>&nbsp;</td>  </tr>  <tr>  <td align="left" nowrap="nowrap"><strong>#PurchaseNumberLabel#</strong></td>  <td align="left" nowrap="nowrap">#PONumber#</td>  <td>&nbsp;</td>  <td>&nbsp;</td>  </tr>  <tr>  <td colspan="4"><hr /></td>  </tr>  <tr>  <td style="font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2" align="left" nowrap="nowrap" width="45%">  <div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">Billing Address</div>  </td>  <td colspan="2" align="left">  <div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">Shipping Address</div>  </td>  </tr>  <tr>  <td colspan="2" align="Left" nowrap="nowrap">#BillingAddress#</td>  <td colspan="3" valign="top">#ShippingAddress#</td>  </tr>  <tr>  <td colspan="7">&nbsp;</td>  </tr>  </tbody>  </table>  <div data-info="#AddressItems#">  <table width="100%" cellspacing="0" cellpadding="3">  <tbody>  <tr>  <td style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;" colspan="6">Order Receipt</td>  </tr>  <tr>  <td style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;" colspan="6">#ShipmentNo#</td>  </tr>  <tr>  <td style="color: black; font-size: 10px;" colspan="6">#ShipTo#</td>  </tr>  <tr style="background-color: #eff3fb;">  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>SKU</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Item</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Description</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" align="center"><strong>Qty</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Price</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" colspan="2"><strong>Total</strong></td>  </tr>  <tr data-info="#LineItems#OmsOrderShipmentID##">  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="10%">#SKU#&nbsp;</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="25%">#Name#</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="25%">#Description#&nbsp;  <p>#ShortDescription#</p>  #UOMDescription#<br /><br />#Column1#</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="center" width="5%">#Quantity#</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="20%">#Price#</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; min-width: 170px; padding: 0 5px;" align="right" width="20%" colspan="2">#ExtendedPrice#</td>  </tr>  <tr data-info="#AmountLineItems#OmsOrderShipmentID##">  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" colspan="5" align="right"><strong>#Title#</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; min-width: 170px; padding: 0 5px;" colspan="2" align="right" width="20%">#Amount#</td>  </tr>  <tr data-info="#GrandAmountLineItems#">  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" colspan="5" align="right"><strong>#Title#</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; min-width: 170px; padding: 0 5px;" align="right" colspan="2" width="20%">#Amount#</td>  </tr>  <tr data-info="#TaxSummaryHead#">  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 9px; padding: 0 5px;" colspan="5" align="right"><strong>#TaxName#</strong></td>  <td style="border: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 9px; padding: 0 5px;" align="center" width="8%"><strong>#Rates#</strong></td>  <td style="border: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 9px; padding: 0 5px;" align="right"><strong>#Amount#</strong></td>  </tr>  <tr data-info="#TaxSummary#">  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 9px; padding: 0 5px;" colspan="5" align="right">#TaxName#</td>  <td style="border: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 9px; padding: 0 5px;" align="center" width="8%">#Rates#</td>  <td style="border: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 9px; padding: 0 5px;" align="right">#Amount#</td>  </tr>  <tr>  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #292a2a; font-size: 11px; padding: 0 5px 5px 5px;" colspan="5" align="right"><strong>Total</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; min-width: 170px; padding: 0 5px;" align="right" width="20%" colspan="2"><strong>#TotalCost#</strong></td>  </tr>  </tbody>  </table>  <table id="RturnedItemTable" width="100%" cellspacing="0" cellpadding="3">  <tbody>  <tr>  <td style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;" colspan="6">Returned Item</td>  </tr>  <tr>  <td style="color: black; font-size: 10px;" colspan="6">&nbsp;</td>  </tr>  <tr style="background-color: #eff3fb;">  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>SKU</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Item</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Description</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" align="center"><strong>Qty</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Price</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Total</strong></td>  </tr>  <tr data-info="#ReturnLineItems#">  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="10%">#ReturnedSKU#&nbsp;</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="25%">#ReturnedName#</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="25%">#ReturnedDescription#&nbsp;  <p>#ReturnedShortDescription#</p>  #ReturnedUOMDescription#</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="center" width="5%">#ReturnedQuantity#</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="20%">#ReturnedPrice#</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; min-width: 170px; padding: 0 5px;" align="right" width="20%">#ReturnedExtendedPrice#</td>  </tr>  <tr data-info="#AmountLineItems#OmsOrderShipmentID##">  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" colspan="5" align="right"><strong>#ReturnedTitle#</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; min-width: 170px; padding: 0 5px;" align="right" width="20%">#ReturnedAmount#</td>  </tr>  <tr data-info="#ReturnedGrandAmountLineItems#">  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" colspan="5" align="right"><strong>#ReturnedTitle#</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; min-width: 170px; padding: 0 5px;" align="right" width="20%">#ReturnedAmount#</td>  </tr>  <tr>  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #292a2a; font-size: 11px; padding: 0 5px 5px 5px;" colspan="5" align="right"><strong>Total</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; min-width: 170px; padding: 0 5px;" align="right" width="20%"><strong>#ReturnedTotalCost#</strong></td>  </tr>  </tbody>  </table>  </div>  <table style="width: 100%; padding: 10px; display: inline-block;" cellspacing="0" cellpadding="3">  <tbody>  <tr>  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2" align="left" nowrap="nowrap">  <div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">#AdditionalInstructLabel#</div>  </td>  </tr>  <tr>  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2">  <div style="width: 675px;" align="justify">#AdditionalInstructions#</div>  </td>  </tr>  <tr>  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2">  <div style="margin-bottom: 5px;">&nbsp;</div>  </td>  </tr>  <tr>  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2" align="left" nowrap="nowrap">#FeedBack#</td>  </tr>  </tbody>  </table>  </div>  </div>  </div></body></html>'
WHERE EmailTemplateId = (SELECT top 1 EmailTemplateId FROM ZnodeEmailTemplate WHERE TemplateName = 'ResendOrderReceipt')
AND LocaleId = 1

UPDATE  ZnodeEmailTemplateLocale SET Content = '<!DOCTYPE html><html><body><p></p>  <div>  <div style="font-family: Arial, Helvetica; font-size: 10px; text-align: left; color: #292a2a; border: solid 1px #c3c3c3; margin-top: 10px;">  <div style="background-color: #eff3fb; color: #292a2a; font-size: 1.5em; font-weight: bold; padding: .5em; border-bottom: solid 1px #c3c3c3;">#StoreLogo# #SiteName# Customer Receipt</div>  <div style="padding: 10px;">  <div style="font-family: Verdana; color: #333333; font-size: 11px;">#ReceiptText#</div>  <table style="font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" border="0" width="100%" cellspacing="3" cellpadding="0">  <tbody>  <tr>  <td colspan="5"><hr /></td>  </tr>  <tr>  <td colspan="2" align="left" nowrap="nowrap" width="25%">  <div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">Order Information</div>  </td>  <td colspan="2" align="left" nowrap="nowrap">  <div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">Customer Service</div>  </td>  </tr>  <tr>  <td align="left" nowrap="nowrap" width="10%"><strong>Order Number:</strong></td>  <td align="left" nowrap="nowrap" width="30%">#OrderId#</td>  <td align="left" nowrap="nowrap" width="4%"><strong>E-Mail:</strong></td>  <td align="left" nowrap="nowrap">#CustomerServiceEmail#</td>  </tr>  <tr>  <td align="left" nowrap="nowrap"><strong>Order Date:</strong></td>  <td align="left" nowrap="nowrap">#OrderDate#</td>  <td align="left" nowrap="nowrap"><strong>Phone:</strong></td>  <td align="left" nowrap="nowrap">#CustomerServicePhoneNumber#</td>  </tr>  <tr>  <td style="vertical-align: top;" align="left" nowrap="nowrap"><strong>Promotion Code:</strong></td>  <td align="left" nowrap="nowrap">#PromotionCode#</td>  <td></td>  <td></td>  </tr>  <tr>  <td align="left" nowrap="nowrap"><strong>Payment Method:</strong></td>  <td align="left" nowrap="nowrap">#PaymentName#</td>  <td></td>  <td></td>  </tr>  <tr>  <td align="left" nowrap="nowrap"><strong>#CardTransactionLabel#</strong></td>  <td align="left" nowrap="nowrap">#CardTransactionID#</td>  <td></td>  <td></td>  </tr>  <tr>  <td align="left" nowrap="nowrap"><strong>#PurchaseNumberLabel#</strong></td>  <td align="left" nowrap="nowrap">#PONumber#</td>  <td></td>  <td></td>  </tr>  <tr>  <td align="left" nowrap="nowrap"><strong>AccountNumber:</strong></td>  <td align="left" nowrap="nowrap">#AccountNumber#</td>  <td></td>  <td></td>  </tr>  <tr>  <td align="left" nowrap="nowrap"><strong>ShippingMethod:</strong></td>  <td align="left" nowrap="nowrap">#ShippingName#</td>  <td></td>  <td></td>  </tr>  <tr>  <td align="left" nowrap="nowrap"><strong>Job / Project Name:</strong></td>  <td align="left" nowrap="nowrap">#JobName#</td>  <td></td>  <td></td>  </tr>  <tr>  <td colspan="4"><hr /></td>  </tr>  <tr>  <td style="font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2" align="left" nowrap="nowrap" width="45%">  <div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">Billing Address</div>  </td>  <td colspan="2" align="left">  <div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">Shipping Address</div>  </td>  </tr>  <tr>  <td colspan="2" align="Left" nowrap="nowrap">#BillingAddress#</td>  <td colspan="3" valign="top">#ShippingAddress#</td>  </tr>  <tr>  <td align="Left" nowrap="nowrap" colspan="2">&nbsp;</td>  <td valign="top" colspan="3"><hr /><span style="width: 130px; display: inline-block; font-weight: bold;">#LabelInHandDate#: </span>#InHandDate#<br /><span style="width: 130px; display: inline-block; font-weight: bold;">#LabelShippingConstraintsCode#: </span>#ShippingConstraintCode#<br /><br /></td>  </tr>  <tr>  <td colspan="7"></td>  </tr>  </tbody>  </table>  <div data-info="#AddressItems#">  <table width="100%" cellspacing="0" cellpadding="3">  <tbody>  <tr>  <td style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;" colspan="6">#ShipmentNo#</td>  </tr>  <tr>  <td style="color: black; font-size: 10px;" colspan="6">#ShipTo#</td>  </tr>  <tr style="background-color: #eff3fb;">  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>SKU</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Item</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Description</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" align="center"><strong>Qty</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Price</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" colspan="2"><strong>Total</strong></td>  </tr>  <tr data-info="#LineItems#OmsOrderShipmentID##">  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="10%">#SKU#</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="25%">#Name#</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="25%">#Description#  <p>#ShortDescription#</p>  #UOMDescription# <br /><br />#Column1#</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="center" width="5%">#Quantity#</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="20%">#Price#</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; min-width: 170px; padding: 0 5px;" align="right" width="20%" colspan="2">#ExtendedPrice#</td>  </tr>  <tr data-info="#AmountLineItems#OmsOrderShipmentID##">  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" colspan="5" align="right"><strong>#Title#</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; min-width: 170px; padding: 0 5px;" align="right" width="20%" colspan="2">#Amount#</td>  </tr>  <tr data-info="#GrandAmountLineItems#">  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" colspan="5" align="right"><strong>#Title#</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; min-width: 170px; padding: 0 5px;" align="right" width="20%" colspan="2">#Amount#</td>  </tr>  <tr data-info="#TaxSummaryHead#">  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 9px; padding: 0 5px;" colspan="5" align="right"><strong>#TaxName#</strong></td>  <td style="border: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 9px; padding: 0 5px;" align="center" width="8%"><strong>#Rates#</strong></td>  <td style="border: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 9px; padding: 0 5px;" align="right"><strong>#Amount#</strong></td>  </tr>  <tr data-info="#TaxSummary#">  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 9px; padding: 0 5px;" colspan="5" align="right">#TaxName#</td>  <td style="border: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 9px; padding: 0 5px;" align="center" width="8%">#Rates#</td>  <td style="border: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 9px; padding: 0 5px;" align="right">#Amount#</td>  </tr>  <tr>  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #292a2a; font-size: 11px; padding: 0 5px 5px 5px;" colspan="5" align="right"><strong>Total</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; min-width: 170px; padding: 0 5px;" align="right" width="20%" colspan="2"><strong>#TotalCost#</strong></td>  </tr>  </tbody>  </table>  </div>  <table style="width: 100%; padding: 10px; display: inline-block;" cellspacing="0" cellpadding="3">  <tbody>  <tr>  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2" align="left" nowrap="nowrap">  <div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">#AdditionalInstructLabel#</div>  </td>  </tr>  <tr>  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2">  <div style="width: 675px;" align="justify">#AdditionalInstructions#</div>  </td>  </tr>  <tr>  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2">  <div style="margin-bottom: 5px;"></div>  </td>  </tr>  <tr>  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2" align="left" nowrap="nowrap">#FeedBack#</td>  </tr>  </tbody>  </table>  </div>  </div>  </div></body></html>'
WHERE EmailTemplateId = (SELECT top 1 EmailTemplateId FROM ZnodeEmailTemplate WHERE TemplateName = 'OrderReceipt')
AND LocaleId = 1

UPDATE  ZnodeEmailTemplateLocale SET Content = '<p> </p><p>Customer Receipt -FR</p><div><div style="font-family: Arial, Helvetica; font-size: 10px; text-align: left; color: #292a2a; border: solid 1px #c3c3c3; margin-top: 10px;"><div style="background-color: #eff3fb; color: #292a2a; font-size: 1.5em; font-weight: bold; padding: .5em; border-bottom: solid 1px #c3c3c3;">#SiteName# Customer Receipt - FR</div><div style="padding: 10px;"><div style="font-family: Verdana; color: #333333; font-size: 11px;">#ReceiptText#</div><table style="font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" border="0" width="100%" cellspacing="3" cellpadding="0"><tbody><tr><td colspan="5"><hr /></td></tr><tr><td colspan="2" align="left" nowrap="nowrap" width="25%"><div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">Order Information</div></td><td colspan="2" align="left" nowrap="nowrap"><div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">Customer Service</div></td></tr><tr><td align="left" nowrap="nowrap" width="10%"><strong>Order Number:</strong></td><td align="left" nowrap="nowrap" width="30%">#OrderId#</td><td align="left" nowrap="nowrap" width="4%"><strong>E-Mail:</strong></td><td align="left" nowrap="nowrap">#CustomerServiceEmail#</td></tr><tr><td align="left" nowrap="nowrap"><strong>Order Date:</strong></td><td align="left" nowrap="nowrap">#OrderDate#</td><td align="left" nowrap="nowrap"><strong>Phone:</strong></td><td align="left" nowrap="nowrap">#CustomerServicePhoneNumber#</td></tr><tr><td style="vertical-align: top;" align="left" nowrap="nowrap"><strong>Promotion Code:</strong></td><td align="left" nowrap="nowrap">#PromotionCode#</td><td> </td><td> </td></tr><tr><td align="left" nowrap="nowrap"><strong>Payment Method:</strong></td><td align="left" nowrap="nowrap">#PaymentName#</td><td> </td><td> </td></tr><tr><td align="left" nowrap="nowrap"><strong>#CardTransactionLabel#</strong></td><td align="left" nowrap="nowrap">#CardTransactionID#</td><td> </td><td> </td></tr><tr><td align="left" nowrap="nowrap"><strong>#PurchaseNumberLabel#</strong></td><td align="left" nowrap="nowrap">#PONumber#</td><td> </td><td> </td></tr><tr><td align="left" nowrap="nowrap"><strong>AccountNumber:</strong></td><td align="left" nowrap="nowrap">#AccountNumber#</td><td> </td><td> </td></tr><tr><td align="left" nowrap="nowrap"><strong>ShippingMethod:</strong></td><td align="left" nowrap="nowrap">#ShippingName#</td><td> </td><td> </td></tr><tr><td colspan="4"><hr /></td></tr><tr><td style="font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2" align="left" nowrap="nowrap" width="45%"><div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">Billing Address</div></td><td colspan="2" align="left"><div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">Shipping Address</div></td></tr><tr><td colspan="2" align="Left" nowrap="nowrap">#BillingAddress#</td><td colspan="3" valign="top">#ShippingAddress#</td></tr><tr><td colspan="7"> </td></tr></tbody></table><div data-info="#AddressItems#"><table width="100%" cellspacing="0" cellpadding="3"><tbody><tr><td style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;" colspan="6">#ShipmentNo#</td></tr><tr><td style="color: black; font-size: 10px;" colspan="6">#ShipTo#</td></tr><tr style="background-color: #eff3fb;"><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>SKU</strong></td><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Item</strong></td><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Description</strong></td><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" align="center"><strong>Qty</strong></td><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Price</strong></td><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Total</strong></td></tr><tr data-info="#LineItems#OmsOrderShipmentID##"><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="10%">#SKU# </td><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="25%">#Name#</td><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="25%">#Description#            <p>#ShortDescription#</p>  #UOMDescription#        </td><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="center" width="5%">#Quantity#</td><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="20%">#Price#</td><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; min-width: 170px; padding: 0 5px;" align="right" width="20%">#ExtendedPrice#</td></tr><tr data-info="#AmountLineItems#OmsOrderShipmentID##"><td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" colspan="5" align="right"><strong>#Title#</strong></td><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; min-width: 170px; padding: 0 5px;" align="right" width="20%">#Amount#</td></tr><tr data-info="#GrandAmountLineItems#"><td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" colspan="5" align="right"><strong>#Title#</strong></td><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; min-width: 170px; padding: 0 5px;" align="right" width="20%">#Amount#</td></tr><tr><td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #292a2a; font-size: 11px; padding: 0 5px 5px 5px;" colspan="5" align="right"><strong>Total</strong></td><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; min-width: 170px; padding: 0 5px;" align="right" width="20%"><strong>#TotalCost#</strong></td></tr></tbody></table></div><table style="width: 100%; padding: 10px; display: inline-block;" cellspacing="0" cellpadding="3"><tbody><tr><td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2" align="left" nowrap="nowrap"><div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">#AdditionalInstructLabel#</div></td></tr><tr><td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2"><div style="width: 675px;" align="justify">#AdditionalInstructions#</div></td></tr><tr><td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2"><div style="margin-bottom: 5px;"> </div></td></tr><tr><td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2" align="left" nowrap="nowrap">#FeedBack#</td></tr></tbody></table></div></div></div>'
WHERE EmailTemplateId = (SELECT top 1 EmailTemplateId FROM ZnodeEmailTemplate WHERE TemplateName = 'OrderReceipt')
AND LocaleId = 2
GO
UPDATE  ZnodeEmailTemplateLocale SET Content = '<!DOCTYPE html><html><body><div>  <div style="font-family: Arial, Helvetica; font-size: 10px; text-align: left; color: #292a2a; border: solid 1px #c3c3c3; margin-top: 10px;">  <div>#StoreLogo# #SiteName#&nbsp;Customer Receipt</div>  <div style="padding: 10px;">  <div style="font-family: Verdana; color: #333333; font-size: 11px;">#ReceiptText#</div>  <table style="font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" border="0" width="100%" cellspacing="3" cellpadding="0">  <tbody>  <tr>  <td colspan="5"><hr /></td>  </tr>  <tr>  <td colspan="2" align="left" nowrap="nowrap" width="25%">  <div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">Order Information</div>  </td>  <td colspan="2" align="left" nowrap="nowrap">  <div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">Customer Service</div>  </td>  </tr>  <tr>  <td align="left" nowrap="nowrap" width="10%"><strong>Order Number:</strong></td>  <td align="left" nowrap="nowrap" width="30%">#OrderId#</td>  <td align="left" nowrap="nowrap" width="4%"><strong>E-Mail:</strong></td>  <td align="left" nowrap="nowrap">#CustomerServiceEmail#</td>  </tr>  <tr>  <td align="left" nowrap="nowrap"><strong>Order Date:</strong></td>  <td align="left" nowrap="nowrap">#OrderDate#</td>  <td align="left" nowrap="nowrap"><strong>Phone:</strong></td>  <td align="left" nowrap="nowrap">#CustomerServicePhoneNumber#</td>  </tr>  <tr>  <td style="vertical-align: top;" align="left" nowrap="nowrap"><strong>Promotion Code:</strong></td>  <td align="left" nowrap="nowrap">#PromotionCode#</td>  <td>&nbsp;</td>  <td>&nbsp;</td>  </tr>  <tr>  <td align="left" nowrap="nowrap"><strong>Payment Method:</strong></td>  <td align="left" nowrap="nowrap">#PaymentName#</td>  <td>&nbsp;</td>  <td>&nbsp;</td>  </tr>  <tr>  <td align="left" nowrap="nowrap"><strong>#CardTransactionLabel#</strong></td>  <td align="left" nowrap="nowrap">#CardTransactionID#</td>  <td>&nbsp;</td>  <td>&nbsp;</td>  </tr>  <tr>  <td align="left" nowrap="nowrap"><strong>#PurchaseNumberLabel#</strong></td>  <td align="left" nowrap="nowrap">#PONumber#</td>  <td>&nbsp;</td>  <td>&nbsp;</td>  </tr>  <tr>  <td colspan="4"><hr /></td>  </tr>  <tr>  <td style="font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2" align="left" nowrap="nowrap" width="45%">  <div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">Billing Address</div>  </td>  <td colspan="2" align="left">  <div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">Shipping Address</div>  </td>  </tr>  <tr>  <td colspan="2" align="Left" nowrap="nowrap">#BillingAddress#</td>  <td colspan="3" valign="top">#ShippingAddress#</td>  </tr>  <tr>  <td colspan="7">&nbsp;</td>  </tr>  </tbody>  </table>  <div data-info="#AddressItems#">  <table width="100%" cellspacing="0" cellpadding="3">  <tbody>  <tr>  <td style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;" colspan="6">Order Receipt</td>  </tr>  <tr>  <td style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;" colspan="6">#ShipmentNo#</td>  </tr>  <tr>  <td style="color: black; font-size: 10px;" colspan="6">#ShipTo#</td>  </tr>  <tr style="background-color: #eff3fb;">  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>SKU</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Item</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Description</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" align="center"><strong>Qty</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Price</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Total</strong></td>  </tr>  <tr data-info="#LineItems#OmsOrderShipmentID##">  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="10%">#SKU#&nbsp;</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="25%">#Name#</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="25%">#Description#&nbsp;  <p>#ShortDescription#</p>  #UOMDescription#<br /><br />#Column1#</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="center" width="15%">#Quantity#</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="15%">#Price#</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="right" width="15%">#ExtendedPrice#</td>  </tr>  <tr data-info="#AmountLineItems#OmsOrderShipmentID##">  <td colspan="3"></td>  <td style="border-left: 1px solid silver; border-bottom: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 3px 5px 3px 10px;" colspan="2" align="left"><strong>#Title#</strong></td>  <td style="border-right: silver 1px solid; border-bottom: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 3px 10px;" align="right">#Amount#</td>  </tr>  <tr data-info="#GrandAmountLineItems#">  <td colspan="3"></td>  <td style="border-left: 1px solid silver; border-bottom: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 3px 5px 3px 10px;" colspan="2" align="left"><strong>#Title#</strong></td>  <td style="border-right: silver 1px solid; border-bottom: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 3px 10px;" align="right">#Amount#</td>  </tr>  <tr data-info="#TaxSummaryHead#">  <td colspan="3"></td>  <td style="border-left: 1px solid silver; border-bottom: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; background-color: #eff3fb; font-size: 9px; padding: 3px 5px 3px 10px;" align="left"><strong>#TaxName#</strong></td>  <td style="border-bottom: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; background-color: #eff3fb; font-size: 9px; padding: 3px 5px;" align="center"><strong>#Rates#</strong></td>  <td style="border-right: 1px solid silver; border-bottom: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; background-color: #eff3fb; font-size: 9px; padding: 3px 10px;" align="right"><strong>#Amount#</strong></td>  </tr>  <tr data-info="#TaxSummary#">  <td colspan="3"></td>  <td style="border-left: 1px solid silver; border-bottom: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 9px; padding: 3px 5px 3px 10px;" align="left">#TaxName#</td>  <td style="border-bottom: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 9px; padding: 0 5px;" align="center">#Rates#</td>  <td style="border-right: 1px solid silver; border-bottom: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 9px; padding: 0 10px;" align="right">#Amount#</td>  </tr>  <tr>  <td colspan="3"></td>  <td style="border-left: 1px solid silver; border-bottom: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #292a2a; font-size: 11px; padding: 5px 10px;" colspan="2" align="left"><strong>Total</strong></td>  <td style="border-right: 1px solid silver; border-bottom: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 11px; padding: 5px 10px;" align="right"><strong>#TotalCost#</strong></td>  </tr>  </tbody>  </table>  <table id="RturnedItemTable" width="100%" cellspacing="0" cellpadding="3">  <tbody>  <tr>  <td style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;" colspan="6">Returned Item</td>  </tr>  <tr>  <td style="color: black; font-size: 10px;" colspan="6">&nbsp;</td>  </tr>  <tr style="background-color: #eff3fb;">  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>SKU</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Item</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Description</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" align="center"><strong>Qty</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Price</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Total</strong></td>  </tr>  <tr data-info="#ReturnLineItems#">  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="10%">#ReturnedSKU#&nbsp;</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="25%">#ReturnedName#</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="25%">#ReturnedDescription#&nbsp;  <p>#ReturnedShortDescription#</p>  #ReturnedUOMDescription#</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="center" width="5%">#ReturnedQuantity#</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="20%">#ReturnedPrice#</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; min-width: 170px; padding: 0 5px;" align="right" width="20%">#ReturnedExtendedPrice#</td>  </tr>  <tr data-info="#AmountLineItems#OmsOrderShipmentID##">  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" colspan="5" align="right"><strong>#ReturnedTitle#</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; min-width: 170px; padding: 0 5px;" align="right" width="20%">#ReturnedAmount#</td>  </tr>  <tr data-info="#ReturnedGrandAmountLineItems#">  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" colspan="5" align="right"><strong>#ReturnedTitle#</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; min-width: 170px; padding: 0 5px;" align="right" width="20%">#ReturnedAmount#</td>  </tr>  <tr>  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #292a2a; font-size: 11px; padding: 0 5px 5px 5px;" colspan="5" align="right"><strong>Total</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; min-width: 170px; padding: 0 5px;" align="right" width="20%"><strong>#ReturnedTotalCost#</strong></td>  </tr>  </tbody>  </table>  </div>  <table style="width: 100%; padding: 10px; display: inline-block;" cellspacing="0" cellpadding="3">  <tbody>  <tr>  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2" align="left" nowrap="nowrap">  <div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">#AdditionalInstructLabel#</div>  </td>  </tr>  <tr>  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2">  <div style="width: 675px;" align="justify">#AdditionalInstructions#</div>  </td>  </tr>  <tr>  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2">  <div style="margin-bottom: 5px;">&nbsp;</div>  </td>  </tr>  <tr>  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2" align="left" nowrap="nowrap">#FeedBack#</td>  </tr>  </tbody>  </table>  </div>  </div>  </div></body></html>'
WHERE EmailTemplateId = (SELECT top 1 EmailTemplateId FROM ZnodeEmailTemplate WHERE TemplateName = 'ResendOrderReceipt')
AND LocaleId = 1

UPDATE  ZnodeEmailTemplateLocale SET Content = '<!DOCTYPE html><html><body><p></p>  <div>  <div style="font-family: Arial, Helvetica; font-size: 10px; text-align: left; color: #292a2a; border: solid 1px #c3c3c3; margin-top: 10px;">  <div style="background-color: #eff3fb; color: #292a2a; font-size: 1.5em; font-weight: bold; padding: .5em; border-bottom: solid 1px #c3c3c3;">#StoreLogo# #SiteName# Customer Receipt</div>  <div style="padding: 10px;">  <div style="font-family: Verdana; color: #333333; font-size: 11px;">#ReceiptText#</div>  <table style="font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" border="0" width="100%" cellspacing="3" cellpadding="0">  <tbody>  <tr>  <td colspan="5"><hr /></td>  </tr>  <tr>  <td colspan="2" align="left" nowrap="nowrap" width="25%">  <div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">Order Information</div>  </td>  <td colspan="2" align="left" nowrap="nowrap">  <div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">Customer Service</div>  </td>  </tr>  <tr>  <td align="left" nowrap="nowrap" width="10%"><strong>Order Number:</strong></td>  <td align="left" nowrap="nowrap" width="30%">#OrderId#</td>  <td align="left" nowrap="nowrap" width="4%"><strong>E-Mail:</strong></td>  <td align="left" nowrap="nowrap">#CustomerServiceEmail#</td>  </tr>  <tr>  <td align="left" nowrap="nowrap"><strong>Order Date:</strong></td>  <td align="left" nowrap="nowrap">#OrderDate#</td>  <td align="left" nowrap="nowrap"><strong>Phone:</strong></td>  <td align="left" nowrap="nowrap">#CustomerServicePhoneNumber#</td>  </tr>  <tr>  <td style="vertical-align: top;" align="left" nowrap="nowrap"><strong>Promotion Code:</strong></td>  <td align="left" nowrap="nowrap">#PromotionCode#</td>  <td></td>  <td></td>  </tr>  <tr>  <td align="left" nowrap="nowrap"><strong>Payment Method:</strong></td>  <td align="left" nowrap="nowrap">#PaymentName#</td>  <td></td>  <td></td>  </tr>  <tr>  <td align="left" nowrap="nowrap"><strong>#CardTransactionLabel#</strong></td>  <td align="left" nowrap="nowrap">#CardTransactionID#</td>  <td></td>  <td></td>  </tr>  <tr>  <td align="left" nowrap="nowrap"><strong>#PurchaseNumberLabel#</strong></td>  <td align="left" nowrap="nowrap">#PONumber#</td>  <td></td>  <td></td>  </tr>  <tr>  <td align="left" nowrap="nowrap"><strong>AccountNumber:</strong></td>  <td align="left" nowrap="nowrap">#AccountNumber#</td>  <td></td>  <td></td>  </tr>  <tr>  <td align="left" nowrap="nowrap"><strong>ShippingMethod:</strong></td>  <td align="left" nowrap="nowrap">#ShippingName#</td>  <td></td>  <td></td>  </tr>  <tr>  <td align="left" nowrap="nowrap"><strong>Job / Project Name:</strong></td>  <td align="left" nowrap="nowrap">#JobName#</td>  <td></td>  <td></td>  </tr>  <tr>  <td colspan="4"><hr /></td>  </tr>  <tr>  <td style="font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2" align="left" nowrap="nowrap" width="45%">  <div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">Billing Address</div>  </td>  <td colspan="2" align="left">  <div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">Shipping Address</div>  </td>  </tr>  <tr>  <td colspan="2" align="Left" nowrap="nowrap">#BillingAddress#</td>  <td colspan="3" valign="top">#ShippingAddress#</td>  </tr>  <tr>  <td align="Left" nowrap="nowrap" colspan="2">&nbsp;</td>  <td valign="top" colspan="3"><hr /><span style="width: 130px; display: inline-block; font-weight: bold;">#LabelInHandDate#: </span>#InHandDate#<br /><span style="width: 130px; display: inline-block; font-weight: bold;">#LabelShippingConstraintsCode#: </span>#ShippingConstraintCode#<br /><br /></td>  </tr>  <tr>  <td colspan="7"></td>  </tr>  </tbody>  </table>  <div data-info="#AddressItems#">  <table width="100%" cellspacing="0" cellpadding="3">  <tbody>  <tr>  <td style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;" colspan="6">#ShipmentNo#</td>  </tr>  <tr>  <td style="color: black; font-size: 10px;" colspan="6">#ShipTo#</td>  </tr>  <tr style="background-color: #eff3fb;">  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>SKU</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Item</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Description</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" align="center"><strong>Qty</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Price</strong></td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 10px;" align="right"><strong>Total</strong></td>  </tr>  <tr data-info="#LineItems#OmsOrderShipmentID##">  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="10%">#SKU#</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="25%">#Name#</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="25%">#Description#  <p>#ShortDescription#</p>  #UOMDescription# <br /><br />#Column1#</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="center" width="15%">#Quantity#</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="15%">#Price#</td>  <td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 10px;" align="right" width="15%">#ExtendedPrice#</td>  </tr>  <tr data-info="#AmountLineItems#OmsOrderShipmentID##">  <td colspan="3"></td>  <td style="border-left: 1px solid silver; border-bottom: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 3px 5px 3px 10px;" colspan="2" align="left"><strong>#Title#</strong></td>  <td style="border-right: silver 1px solid; border-bottom: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 3px 10px;" align="right">#Amount#</td>  </tr>  <tr data-info="#GrandAmountLineItems#">  <td colspan="3"></td>  <td style="border-left: 1px solid silver; border-bottom: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 3px 5px 3px 10px;" colspan="2" align="left"><strong>#Title#</strong></td>  <td style="border-right: silver 1px solid; border-bottom: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 3px 10px;" align="right">#Amount#</td>  </tr>  <tr data-info="#TaxSummaryHead#">  <td colspan="3"></td>  <td style="border-left: 1px solid silver; border-bottom: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; background-color: #eff3fb; font-size: 9px; padding: 3px 5px 3px 10px;" align="left"><strong>#TaxName#</strong></td>  <td style="border-bottom: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; background-color: #eff3fb; font-size: 9px; padding: 3px 5px;" align="center"><strong>#Rates#</strong></td>  <td style="border-right: 1px solid silver; border-bottom: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; background-color: #eff3fb; font-size: 9px; padding: 3px 10px;" align="right"><strong>#Amount#</strong></td>  </tr>  <tr data-info="#TaxSummary#">  <td colspan="3"></td>  <td style="border-left: 1px solid silver; border-bottom: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 9px; padding: 3px 5px 3px 10px;" align="left">#TaxName#</td>  <td style="border-bottom: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 9px; padding: 0 5px;" align="center">#Rates#</td>  <td style="border-right: 1px solid silver; border-bottom: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 9px; padding: 0 10px;" align="right">#Amount#</td>  </tr>  <tr>  <td colspan="3"></td>  <td style="border-left: 1px solid silver; border-bottom: 1px solid silver; font-family: Verdana, Helvetica, sans-serif; color: #292a2a; font-size: 11px; padding: 5px 10px;" colspan="2" align="left"><strong>Total</strong></td>  <td style="border-right: 1px solid silver; border-bottom: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 11px; padding: 5px 10px;" align="right"><strong>#TotalCost#</strong></td>  </tr>  </tbody>  </table>  </div>  <table style="width: 100%; padding: 10px; display: inline-block;" cellspacing="0" cellpadding="3">  <tbody>  <tr>  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2" align="left" nowrap="nowrap">  <div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">#AdditionalInstructLabel#</div>  </td>  </tr>  <tr>  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2">  <div style="width: 675px;" align="justify">#AdditionalInstructions#</div>  </td>  </tr>  <tr>  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2">  <div style="margin-bottom: 5px;"></div>  </td>  </tr>  <tr>  <td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2" align="left" nowrap="nowrap">#FeedBack#</td>  </tr>  </tbody>  </table>  </div>  </div>  </div></body></html>'
WHERE EmailTemplateId = (SELECT top 1 EmailTemplateId FROM ZnodeEmailTemplate WHERE TemplateName = 'OrderReceipt')
AND LocaleId = 1

UPDATE  ZnodeEmailTemplateLocale SET Content = '<p> </p><p>Customer Receipt -FR</p><div><div style="font-family: Arial, Helvetica; font-size: 10px; text-align: left; color: #292a2a; border: solid 1px #c3c3c3; margin-top: 10px;"><div style="background-color: #eff3fb; color: #292a2a; font-size: 1.5em; font-weight: bold; padding: .5em; border-bottom: solid 1px #c3c3c3;">#SiteName# Customer Receipt - FR</div><div style="padding: 10px;"><div style="font-family: Verdana; color: #333333; font-size: 11px;">#ReceiptText#</div><table style="font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" border="0" width="100%" cellspacing="3" cellpadding="0"><tbody><tr><td colspan="5"><hr /></td></tr><tr><td colspan="2" align="left" nowrap="nowrap" width="25%"><div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">Order Information</div></td><td colspan="2" align="left" nowrap="nowrap"><div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">Customer Service</div></td></tr><tr><td align="left" nowrap="nowrap" width="10%"><strong>Order Number:</strong></td><td align="left" nowrap="nowrap" width="30%">#OrderId#</td><td align="left" nowrap="nowrap" width="4%"><strong>E-Mail:</strong></td><td align="left" nowrap="nowrap">#CustomerServiceEmail#</td></tr><tr><td align="left" nowrap="nowrap"><strong>Order Date:</strong></td><td align="left" nowrap="nowrap">#OrderDate#</td><td align="left" nowrap="nowrap"><strong>Phone:</strong></td><td align="left" nowrap="nowrap">#CustomerServicePhoneNumber#</td></tr><tr><td style="vertical-align: top;" align="left" nowrap="nowrap"><strong>Promotion Code:</strong></td><td align="left" nowrap="nowrap">#PromotionCode#</td><td> </td><td> </td></tr><tr><td align="left" nowrap="nowrap"><strong>Payment Method:</strong></td><td align="left" nowrap="nowrap">#PaymentName#</td><td> </td><td> </td></tr><tr><td align="left" nowrap="nowrap"><strong>#CardTransactionLabel#</strong></td><td align="left" nowrap="nowrap">#CardTransactionID#</td><td> </td><td> </td></tr><tr><td align="left" nowrap="nowrap"><strong>#PurchaseNumberLabel#</strong></td><td align="left" nowrap="nowrap">#PONumber#</td><td> </td><td> </td></tr><tr><td align="left" nowrap="nowrap"><strong>AccountNumber:</strong></td><td align="left" nowrap="nowrap">#AccountNumber#</td><td> </td><td> </td></tr><tr><td align="left" nowrap="nowrap"><strong>ShippingMethod:</strong></td><td align="left" nowrap="nowrap">#ShippingName#</td><td> </td><td> </td></tr><tr><td colspan="4"><hr /></td></tr><tr><td style="font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2" align="left" nowrap="nowrap" width="45%"><div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">Billing Address</div></td><td colspan="2" align="left"><div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">Shipping Address</div></td></tr><tr><td colspan="2" align="Left" nowrap="nowrap">#BillingAddress#</td><td colspan="3" valign="top">#ShippingAddress#</td></tr><tr><td colspan="7"> </td></tr></tbody></table><div data-info="#AddressItems#"><table width="100%" cellspacing="0" cellpadding="3"><tbody><tr><td style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;" colspan="6">#ShipmentNo#</td></tr><tr><td style="color: black; font-size: 10px;" colspan="6">#ShipTo#</td></tr><tr style="background-color: #eff3fb;"><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>SKU</strong></td><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Item</strong></td><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Description</strong></td><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" align="center"><strong>Qty</strong></td><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Price</strong></td><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;"><strong>Total</strong></td></tr><tr data-info="#LineItems#OmsOrderShipmentID##"><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="10%">#SKU# </td><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="25%">#Name#</td><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="25%">#Description#            <p>#ShortDescription#</p>  #UOMDescription#        </td><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="center" width="5%">#Quantity#</td><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" align="left" width="20%">#Price#</td><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; min-width: 170px; padding: 0 5px;" align="right" width="20%">#ExtendedPrice#</td></tr><tr data-info="#AmountLineItems#OmsOrderShipmentID##"><td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" colspan="5" align="right"><strong>#Title#</strong></td><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; min-width: 170px; padding: 0 5px;" align="right" width="20%">#Amount#</td></tr><tr data-info="#GrandAmountLineItems#"><td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; padding: 0 5px;" colspan="5" align="right"><strong>#Title#</strong></td><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; min-width: 170px; padding: 0 5px;" align="right" width="20%">#Amount#</td></tr><tr><td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #292a2a; font-size: 11px; padding: 0 5px 5px 5px;" colspan="5" align="right"><strong>Total</strong></td><td style="border: silver 1px solid; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px; min-width: 170px; padding: 0 5px;" align="right" width="20%"><strong>#TotalCost#</strong></td></tr></tbody></table></div><table style="width: 100%; padding: 10px; display: inline-block;" cellspacing="0" cellpadding="3"><tbody><tr><td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2" align="left" nowrap="nowrap"><div style="color: #292a2a; font-weight: bold; font-size: 11px; padding-bottom: 5px;">#AdditionalInstructLabel#</div></td></tr><tr><td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2"><div style="width: 675px;" align="justify">#AdditionalInstructions#</div></td></tr><tr><td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2"><div style="margin-bottom: 5px;"> </div></td></tr><tr><td style="border: none; font-family: Verdana, Helvetica, sans-serif; color: #333333; font-size: 10px;" colspan="2" align="left" nowrap="nowrap">#FeedBack#</td></tr></tbody></table></div></div></div>'
WHERE EmailTemplateId = (SELECT top 1 EmailTemplateId FROM ZnodeEmailTemplate WHERE TemplateName = 'OrderReceipt')
AND LocaleId = 2
GO

INSERT INTO ZnodeGlobalAttribute(AttributeTypeId,AttributeCode,IsRequired,IsLocalizable,IsActive,DisplayOrder, HelpDescription,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsSystemDefined,GlobalEntityId)
SELECT (SELECT TOP 1 AttributeTypeId FROM ZnodeAttributeType WHERE AttributeTypeName = 'Text'),'BusinessIdentificationNumber',0,1,0,500,
NULL,2,GETDATE(),2,GETDATE(),0,(SELECT TOP 1 GlobalEntityId FROM ZnodeGlobalEntity WHERE EntityName = 'Account')
WHERE NOT EXISTS(SELECT * FROM ZnodeGlobalAttribute WHERE  AttributeCode = 'BusinessIdentificationNumber' AND GlobalEntityId = (SELECT TOP 1 GlobalEntityId FROM ZnodeGlobalEntity WHERE EntityName = 'Account'))

INSERT INTO ZnodeGlobalAttributeLocale(LocaleId,GlobalAttributeId,AttributeName,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT 1,GlobalAttributeId,'Business Identification Number',NULL,2,GETDATE(),2,GETDATE()
FROM ZnodeGlobalAttribute a
WHERE AttributeCode = 'BusinessIdentificationNumber' AND GlobalEntityId = (SELECT TOP 1 GlobalEntityId FROM ZnodeGlobalEntity WHERE EntityName = 'Account') and
NOT EXISTS(SELECT * FROM ZnodeGlobalAttributeLocale b WHERE a.GlobalAttributeId = b.GlobalAttributeId AND b.LocaleId = 1)

INSERT INTO ZnodeGlobalAttributeGroup(GroupCode, DisplayOrder, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, IsSystemDefined, GlobalEntityId)
SELECT 'BusinessIdentificationNumber', NULL, 2, GETDATE(), 2, GETDATE(), 0, (SELECT TOP 1 GlobalEntityId FROM ZnodeGlobalEntity WHERE EntityName = 'Account')
WHERE NOT EXISTS(SELECT * FROM ZnodeGlobalAttributeGroup WHERE GroupCode = 'BusinessIdentificationNumber' AND GlobalEntityId = (SELECT TOP 1 GlobalEntityId FROM ZnodeGlobalEntity WHERE EntityName = 'Account'))

INSERT INTO ZnodeGlobalAttributeGroupLocale(LocaleId,GlobalAttributeGroupId,AttributeGroupName,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT 1,GlobalAttributeGroupId,'Business Details',NULL,2,GETDATE(),2,GETDATE()
FROM ZnodeGlobalAttributeGroup a
WHERE a.GroupCode = 'BusinessIdentificationNumber' AND GlobalEntityId = (SELECT TOP 1 GlobalEntityId FROM ZnodeGlobalEntity WHERE EntityName = 'Account') and
NOT EXISTS(SELECT * FROM ZnodeGlobalAttributeGroupLocale b WHERE a.GlobalAttributeGroupId = b.GlobalAttributeGroupId AND b.LocaleId = 1)

INSERT INTO ZnodeGlobalAttributeGroupMapper(GlobalAttributeGroupId,GlobalAttributeId,AttributeDisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (SELECT TOP 1 GlobalAttributeGroupId FROM ZnodeGlobalAttributeGroup WHERE GroupCode = 'BusinessIdentificationNumber'  AND GlobalEntityId = (SELECT TOP 1 GlobalEntityId FROM ZnodeGlobalEntity WHERE EntityName = 'Account')),
      (SELECT TOP 1 GlobalAttributeId FROM ZnodeGlobalAttribute WHERE  AttributeCode = 'BusinessIdentificationNumber' AND GlobalEntityId = (SELECT TOP 1 GlobalEntityId FROM ZnodeGlobalEntity WHERE EntityName = 'Account')),
	  NULL,	2,	GETDATE(),	2,	GETDATE()
WHERE NOT EXISTS(SELECT * FROM  ZnodeGlobalAttributeGroupMapper WHERE GlobalAttributeGroupId =  (SELECT TOP 1 GlobalAttributeGroupId FROM ZnodeGlobalAttributeGroup WHERE GroupCode = 'BusinessIdentificationNumber' AND GlobalEntityId = (SELECT TOP 1 GlobalEntityId FROM ZnodeGlobalEntity WHERE EntityName = 'Account'))
      AND  GlobalAttributeId = (SELECT TOP 1 GlobalAttributeId FROM ZnodeGlobalAttribute WHERE  AttributeCode = 'BusinessIdentificationNumber' AND GlobalEntityId = (SELECT TOP 1 GlobalEntityId FROM ZnodeGlobalEntity WHERE EntityName = 'Account')))

INSERT INTO ZnodeGlobalFamilyGroupMapper (GlobalAttributeFamilyId, GlobalAttributeGroupId, AttributeGroupDisplayOrder,CreatedBy, CreatedDate, ModifiedBy, ModifiedDate)
SELECT
(SELECT TOP 1 GlobalAttributeFamilyId FROM ZnodeGlobalAttributeFamily WHERE FamilyCode = 'Account'),
(SELECT TOP 1 GlobalAttributeGroupId FROM ZnodeGlobalAttributeGroup WHERE GroupCode = 'BusinessIdentificationNumber' AND GlobalEntityId = (SELECT TOP 1 GlobalEntityId FROM ZnodeGlobalEntity WHERE EntityName = 'Account')), 999,
2, GETDATE(), 2, GETDATE()
WHERE NOT EXISTS(SELECT * FROM ZnodeGlobalFamilyGroupMapper where
GlobalAttributeFamilyId = (SELECT TOP 1 GlobalAttributeFamilyId FROM ZnodeGlobalAttributeFamily WHERE FamilyCode = 'Account') and
GlobalAttributeGroupId = (SELECT TOP 1 GlobalAttributeGroupId FROM ZnodeGlobalAttributeGroup WHERE GroupCode = 'BusinessIdentificationNumber' AND GlobalEntityId = (SELECT TOP 1 GlobalEntityId FROM ZnodeGlobalEntity WHERE EntityName = 'Account'))
)


INSERT INTO ZnodeGlobalAttributeValidation (GlobalAttributeId,InputValidationId,InputValidationRuleId,Name,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (SELECT TOP 1 GlobalAttributeId FROM ZnodeGlobalAttribute WHERE AttributeCode = 'BusinessIdentificationNumber' AND GlobalEntityId = (SELECT TOP 1 GlobalEntityId FROM ZnodeGlobalEntity WHERE EntityName = 'Account')),(SELECT TOP 1 InputValidationId FROM znodeattributeinputvalidation  WHERE Name = 'ValidationRule')
,NULL,NULL,2,GETDATE(),2,GETDATE()
WHERE NOT EXISTS(SELECT * FROM ZnodeGlobalAttributeValidation WHERE GlobalAttributeId = (SELECT TOP 1 GlobalAttributeId FROM ZnodeGlobalAttribute WHERE AttributeCode = 'BusinessIdentificationNumber' AND GlobalEntityId = (SELECT TOP 1 GlobalEntityId FROM ZnodeGlobalEntity WHERE EntityName = 'Account'))
AND InputValidationId = (SELECT TOP 1 InputValidationId FROM znodeattributeinputvalidation  WHERE Name = 'ValidationRule'))

INSERT INTO ZnodeGlobalAttributeValidation (GlobalAttributeId,InputValidationId,InputValidationRuleId,Name,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (SELECT TOP 1 GlobalAttributeId FROM ZnodeGlobalAttribute WHERE AttributeCode = 'BusinessIdentificationNumber' AND GlobalEntityId = (SELECT TOP 1 GlobalEntityId FROM ZnodeGlobalEntity WHERE EntityName = 'Account')),(SELECT TOP 1 InputValidationId FROM znodeattributeinputvalidation  WHERE Name = 'RegularExpression')
,NULL,'',2,GETDATE(),2,GETDATE()
WHERE NOT EXISTS(SELECT * FROM ZnodeGlobalAttributeValidation WHERE GlobalAttributeId = (SELECT TOP 1 GlobalAttributeId FROM ZnodeGlobalAttribute WHERE AttributeCode = 'BusinessIdentificationNumber' AND GlobalEntityId = (SELECT TOP 1 GlobalEntityId FROM ZnodeGlobalEntity WHERE EntityName = 'Account'))
AND InputValidationId = (SELECT TOP 1 InputValidationId FROM znodeattributeinputvalidation  WHERE Name = 'RegularExpression'))


INSERT INTO ZnodeGlobalAttributeValidation (GlobalAttributeId,InputValidationId,InputValidationRuleId,Name,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (SELECT TOP 1 GlobalAttributeId FROM ZnodeGlobalAttribute WHERE AttributeCode = 'BusinessIdentificationNumber' AND GlobalEntityId = (SELECT TOP 1 GlobalEntityId FROM ZnodeGlobalEntity WHERE EntityName = 'Account')),(SELECT TOP 1 InputValidationId FROM znodeattributeinputvalidation  WHERE Name = 'MaxCharacters' AND AttributeTypeId = 5)
,NULL,'',2,GETDATE(),2,GETDATE()
WHERE NOT EXISTS(SELECT * FROM ZnodeGlobalAttributeValidation WHERE GlobalAttributeId = (SELECT TOP 1 GlobalAttributeId FROM ZnodeGlobalAttribute WHERE AttributeCode = 'BusinessIdentificationNumber' AND GlobalEntityId = (SELECT TOP 1 GlobalEntityId FROM ZnodeGlobalEntity WHERE EntityName = 'Account'))
AND InputValidationId = (SELECT TOP 1 InputValidationId FROM znodeattributeinputvalidation  WHERE Name = 'MaxCharacters' AND AttributeTypeId = 5) )


INSERT INTO ZnodeGlobalAttributeValidation (GlobalAttributeId,InputValidationId,InputValidationRuleId,Name,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (SELECT TOP 1 GlobalAttributeId FROM ZnodeGlobalAttribute WHERE AttributeCode = 'BusinessIdentificationNumber' AND GlobalEntityId = (SELECT TOP 1 GlobalEntityId FROM ZnodeGlobalEntity WHERE EntityName = 'Account')),(SELECT TOP 1 InputValidationId FROM znodeattributeinputvalidation  WHERE Name = 'UniqueValue')
,NULL,'false',2,GETDATE(),2,GETDATE()
WHERE NOT EXISTS(SELECT * FROM ZnodeGlobalAttributeValidation WHERE GlobalAttributeId = (SELECT TOP 1 GlobalAttributeId FROM ZnodeGlobalAttribute WHERE AttributeCode = 'BusinessIdentificationNumber' AND GlobalEntityId = (SELECT TOP 1 GlobalEntityId FROM ZnodeGlobalEntity WHERE EntityName = 'Account'))
AND InputValidationId = (SELECT TOP 1 InputValidationId FROM znodeattributeinputvalidation  WHERE Name = 'UniqueValue'))

DECLARE @AccountGlobalAttributeValue TABLE (AccountId INT,AccountGlobalAttributeValueId  INT)
INSERT INTO ZnodeAccountGlobalAttributeValue(AccountId,GlobalAttributeId,GlobalAttributeDefaultValueId,AttributeValue
,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
OUTPUT INSERTED.AccountGlobalAttributeValueId,INSERTED.AccountId INTO @AccountGlobalAttributeValue(AccountGlobalAttributeValueId,AccountId)
SELECT ZA.AccountId, (SELECT TOP 1 GlobalAttributeId FROM ZnodeGlobalAttribute WHERE  AttributeCode = 'BusinessIdentificationNumber' AND GlobalEntityId = (SELECT TOP 1 GlobalEntityId FROM ZnodeGlobalEntity WHERE EntityName = 'Account')),
	NULL,NULL,2,GETDATE(),2,GETDATE()
FROM ZnodeAccount ZA
WHERE NOT EXISTS(SELECT * FROM ZnodeAccountGlobalAttributeValue Z WHERE ZA.AccountId = Z.AccountId
	AND GlobalAttributeId = (SELECT TOP 1 GlobalAttributeId FROM ZnodeGlobalAttribute WHERE  AttributeCode = 'BusinessIdentificationNumber' AND GlobalEntityId = (SELECT TOP 1 GlobalEntityId FROM ZnodeGlobalEntity WHERE EntityName = 'Account'))
	)

INSERT INTO ZnodeAccountGlobalAttributeValueLocale(AccountGlobalAttributeValueId,LocaleId,AttributeValue,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,GlobalAttributeDefaultValueId,MediaId,MediaPath)
SELECT AccountGlobalAttributeValueId,1,NULL,2,GETDATE(),2,GETDATE(),NULL,NULL,NULL
FROM @AccountGlobalAttributeValue

Update ZnodeGlobalAttributeGroupLocale set AttributeGroupName = 'Business Details'
where AttributeGroupName = 'Buisness Identification Number'

UPDATE ZnodeGlobalAttribute SET HelpDescription = 'The input saved against this field will be used by all the customer accounts associated with this Account.'
WHERE AttributeCode = 'BusinessIdentificationNumber'

UPDATE ZnodeGlobalAttributeGrouplocale 
SET AttributeGroupName = 'Business Details'
WHERE GlobalAttributeGroupId = (SELECT TOP 1 GlobalAttributeGroupId FROM ZnodeGlobalAttributeGroup WHERE GroupCode = 'BusinessIdentificationNumber')
AND LocaleId = 1
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'ImportDuty' AND TABLE_NAME = 'ZnodeOmsOrderDetails')
BEGIN
	ALTER TABLE ZnodeOmsOrderDetails ADD [ImportDuty] NUMERIC(28,6) NULL
END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'ImportDuty' AND TABLE_NAME = 'ZnodeOmsQuote')
BEGIN
	ALTER TABLE ZnodeOmsQuote ADD [ImportDuty] NUMERIC(28,6) NULL
END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'ImportDuty' AND TABLE_NAME = 'ZnodeOmsTaxOrderDetails')
BEGIN
	ALTER TABLE ZnodeOmsTaxOrderDetails ADD [ImportDuty] NUMERIC(28,6) NULL
END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'ImportDuty' AND TABLE_NAME = 'ZnodeOmsTaxOrderLineDetails')
BEGIN
	ALTER TABLE ZnodeOmsTaxOrderLineDetails ADD [ImportDuty] NUMERIC(28,6) NULL
END
GO
IF NOT EXISTS(SELECT * FROM SYS.TABLES WHERE NAME = 'ZnodeOmsTaxOrderSummary')
BEGIN
	CREATE TABLE [ZnodeOmsTaxOrderSummary] 
	(
		[OmsTaxOrderSummaryId] INT IDENTITY,
		[OmsOrderDetailsId] INT,
		[Tax] NUMERIC(28,6),
		[Rate] NUMERIC(28,6),
		[TaxName] VARCHAR(600),
		[TaxTypeName] VARCHAR(600),
		CONSTRAINT [PK_ZnodeOmsTaxOrderSummary] PRIMARY KEY CLUSTERED ([OmsTaxOrderSummaryId]) ,
		CONSTRAINT [FK_ZnodeOmsTaxOrderSummary_ZnodeOmsOrderDetails] FOREIGN KEY ([OmsOrderDetailsId]) REFERENCES [dbo].[ZnodeOmsOrderDetails] ([OmsOrderDetailsId])
	)
END
GO
IF NOT EXISTS(SELECT * FROM SYS.TABLES WHERE NAME = 'ZnodeOmsTaxQuoteSummary')
BEGIN
	CREATE TABLE [ZnodeOmsTaxQuoteSummary] 
	(
		[OmsTaxQuoteSummaryId] INT IDENTITY,
		[OmsQuoteId] INT,
		[Tax] NUMERIC(28,6),
		[Rate] NUMERIC(28,6),
		[TaxName] VARCHAR(600),
		[TaxTypeName] VARCHAR(600),
		CONSTRAINT [PK_ZnodeOmsTaxQuoteSummary] PRIMARY KEY CLUSTERED ([OmsTaxQuoteSummaryId]) ,
		CONSTRAINT [FK_ZnodeOmsTaxQuoteSummary_ZnodeOmsQuote] FOREIGN KEY ([OmsQuoteId]) REFERENCES [dbo].[ZnodeOmsQuote] ([OmsQuoteId])
	)
END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'ImportDuty' AND TABLE_NAME = 'ZnodeRmaReturnDetails')
BEGIN
	ALTER TABLE ZnodeRmaReturnDetails ADD [ImportDuty] NUMERIC(28,6) NULL
END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'ImportDuty' AND TABLE_NAME = 'ZnodeRmaReturnLineItems')
BEGIN
	ALTER TABLE ZnodeRmaReturnLineItems ADD [ImportDuty] NUMERIC(28,6) NULL
END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'AvataxIsTaxIncluded' AND TABLE_NAME = 'ZnodeTaxPortal')
BEGIN
	ALTER TABLE ZnodeTaxPortal ADD  [AvataxIsTaxIncluded] BIT
END
GO

UPDATE ZnodeTaxPortal SET AvataxIsTaxIncluded = 0 WHERE AvataxIsTaxIncluded IS NULL

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'AvataxIsTaxIncluded' AND TABLE_NAME = 'ZnodeTaxPortal')
BEGIN
	IF NOT EXISTS(SELECT * FROM SYS.default_constraints WHERE NAME = 'DF_ZnodeTaxPortal_AvataxIsTaxIncluded')
	BEGIN
		ALTER TABLE ZnodeTaxPortal ADD CONSTRAINT DF_ZnodeTaxPortal_AvataxIsTaxIncluded DEFAULT 0 FOR AvataxIsTaxIncluded
	END
END
GO
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'AvataxIsTaxIncluded' AND TABLE_NAME = 'ZnodeTaxPortal')
BEGIN
	ALTER TABLE ZnodeTaxPortal ALTER COLUMN AvataxIsTaxIncluded BIT NOT NULL
END

UPDATE ZnodeTaxRuleTypes SET ClassName = 'AvataxClient' WHERE ClassName = 'AvataxTaxSales' AND NAME = 'Avatax Tax Class'
GO
IF EXISTS(SELECT * FROM sys.procedures WHERE Name = 'Znode_GetOmsOrderDetail')
	DROP PROC Znode_GetOmsOrderDetail
GO
CREATE PROCEDURE [dbo].[Znode_GetOmsOrderDetail]
( @WhereClause NVARCHAR(MAX),
  @Rows        INT            = 100,
  @PageNo      INT            = 1,
  @Order_BY    VARCHAR(1000)  = '',
  @RowsCount   INT OUT			,
  @UserId	   INT = 0 ,
  @IsFromAdmin int=0,
  @SalesRepUserId int = 0 
 )
AS
    /*
     Summary : This procedure is used to get the oms order detils
			   Records are fetched for those users who placed the order i.e UserId is Present in ZnodeUser and  ZnodeOmsOrderDetails tables
	 Unit Testing:

    EXEC [Znode_GetOmsOrderDetail_SCT] 'PortalId =1',@Order_BY = '',@RowsCount= 0, @UserId = 0 ,@Rows = 50, @PageNo = 1

	declare @p7 int
	set @p7=4
	exec sp_executesql N'Znode_GetOmsOrderDetail_SCT @WhereClause, @Rows,@PageNo,@Order_By,@RowCount OUT,@UserId,@IsFromAdmin',N'@WhereClause nvarchar(30),@Rows int,@PageNo int,@Order_By nvarchar(14),@RowCount int output,@UserId int,@IsFromAdmin int',@WhereClause=N'(PortalId in(''1'',''4'',''5'',''6''))',@Rows=50,@PageNo=1,@Order_By=N'orderdate desc',@RowCount=@p7 output,@UserId=0,@IsFromAdmin=1
	select @p7



   */
BEGIN
    BEGIN TRY
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
		DECLARE @SQL NVARCHAR(MAX), @ProcessType  varchar(50)='Order'
		DECLARE @OrderLineItemRelationshipTypeId INT
		SET @OrderLineItemRelationshipTypeId = ( SELECT top 1 OrderLineItemRelationshipTypeId  FROM ZnodeOmsOrderLineItemRelationshipType where Name = 'AddOns' )

		----Verifying that the @SalesRepUserId is having 'Sales Rep' role
		IF NOT EXISTS
		(
			SELECT * FROM ZnodeUser ZU
			INNER JOIN AspNetZnodeUser ANZU ON ZU.UserName = ANZU.UserName
			INNER JOIN AspNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName
			INNER JOIN AspNetUserRoles ANUR ON ANU.Id = ANUR.UserId
			Where Exists(select * from AspNetRoles ANR Where Name = 'Sales Rep' AND ANUR.RoleId = ANR.Id) 
			AND ZU.UserId = @SalesRepUserId
		)   
		Begin
			SET @SalesRepUserId = 0
		End

		DECLARE @Fn_GetPaginationWhereClause VARCHAR(500) = dbo.Fn_GetPaginationWhereClause(@PageNo,@Rows),
		@Fn_GetFilterWhereClause NVARCHAR(MAX) = ''
		set @Fn_GetFilterWhereClause=dbo.Fn_GetFilterWhereClause(@WhereClause)
			
		IF @Order_BY = ''
			set @Order_BY = 'OrderDate desc'

			set @Order_BY = replace(@Order_BY,'PortalId','ZODD.PortalId')
			set @Order_BY = replace(@Order_BY,'UserName','ISNULL(RTRIM(LTRIM(ZODD.FirstName)),'''')+'' ''+ISNULL(RTRIM(LTRIM(ZODD.LastName)),'''')')
			set @Order_BY = replace(@Order_BY,'email','ZODD.Email')
			set @Order_BY = replace(@Order_BY,'OrderState','case when ZOS.IsShowToCustomer=0 and '+cast( @IsFromAdmin as varchar(50))+' = 0 then ZOSC.Description else  ZOS.Description end')
			set @Order_BY = replace(@Order_BY,'PaymentStatus','ZOPS.Name')
			set @Order_BY = replace(@Order_BY,'PublishState','ZODPS.DisplayName')
			set @Order_BY = replace(@Order_BY,'StoreName','ZP.StoreName')

			Declare @Fn_GetPagingRowId NVARCHAR(MAX) = ' DENSE_RANK()Over('+ ' Order By '+CASE WHEN Isnull(@Order_BY,'') = '' THEN 'OmsOrderId DESC' ELSE @Order_BY + ',OmsOrderId DESC' END  + ') RowId '
						
			IF OBJECT_ID('tempdb..#TBL_RowCount') is not null
				DROP TABLE #TBL_RowCount

			IF OBJECT_ID('tempdb..#Portal') is not null
				DROP TABLE #Portal
			
			Create table #TBL_RowCount(RowsCount int )
			CREATE TABLE #tbl_GetRecurciveUserId  (ID INT IDENTITY(1,1) Primary key,UserId INT,ParentUserId INT)
			INSERT INTO #tbl_GetRecurciveUserId
			SELECT UserId,ParentUserId FROM dbo.Fn_GetRecurciveUserId (CAST(@UserId AS VARCHAR(50)),@ProcessType ) FNRU
			 
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'PortalId','ZODD.PortalId')
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'UserName','ISNULL(RTRIM(LTRIM(ZODD.FirstName)),'''')+'' ''+ISNULL(RTRIM(LTRIM(ZODD.LastName)),'''')')
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'email','ZODD.Email')
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'OrderState','case when ZOS.IsShowToCustomer=0 and '+cast( @IsFromAdmin as varchar(50))+' = 0 then ZOSC.Description else  ZOS.Description end')
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'PaymentStatus','ZOPS.Name')
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'PublishState','ZODPS.DisplayName')
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'StoreName','ZP.StoreName')

			set @Order_BY = replace(@Order_BY,'ShippingPostalCode','BillingPostalCode')

			set @Fn_GetPagingRowId = replace(@Fn_GetPagingRowId,'OmsOrderId','Zoo.OmsOrderId')

			set @Rows = @PageNo * @Rows

			CREATE TABLE #Portal (PortalId int,StoreName varchar(200))
			insert into #Portal
			select PortalId,StoreName
			from ZnodePortal

		SET @SQL = '
		SELECT Distinct top '+cast(@Rows as varchar(10))+' Zoo.OmsOrderId,Zoo.OrderNumber, ZODD.PortalId,ZP.StoreName ,ZODD.CurrencyCode,
		case when ZOS.IsShowToCustomer=0 and '+cast( @IsFromAdmin as varchar(50))+' = 0 then ZOSC.Description else  ZOS.Description end  OrderState,ZODD.ShippingId,ZODD.PaymentTypeId,ZODD.PaymentSettingId
		,ZOPS.Name PaymentStatus,ZPS.Name PaymentType,CAST(1 AS BIT) ShippingStatus ,ZODD.OrderDate,ZODD.UserId,ISNULL(RTRIM(LTRIM(ZODD.FirstName)),'''')
		+'' ''+ISNULL(RTRIM(LTRIM(ZODD.LastName)),'''') UserName ,ZODD.PaymentTransactionToken ,ZODD.Total ,ZODD.OmsOrderDetailsId,ZODD.PoDocument,
		ZODD.Email ,ZODD.PhoneNumber ,ZODD.SubTotal ,ZODD.TaxCost ,ZODD.ShippingCost,ZODD.BillingPostalCode,
		ZODD.ModifiedDate AS OrderModifiedDate,  ZODD.PaymentDisplayName  ,isnull(Zoo.ExternalId,0) ExternalId,ZODD.CreditCardExpMonth,ZODD.CultureCode--,ZODD.TotalAdditionalCost
		,ZODD.CreditCardExpYear,ZODD.CardType,ZODD.CreditCardNumber,ZODD.PaymentExternalId,ZODPS.DisplayName as PublishState,
		'''' ProductName, 0 CountId, CAST (0 as bit) IsInRMA, '+@Fn_GetPagingRowId+' 
		INTO #Cte_OrderLineDescribe
		FROM ZnodeOmsOrder (nolock) ZOO 
		INNER JOIN ZnodeOmsOrderDetails (nolock) ZODD ON (ZODD.OmsOrderId = ZOO.OmsOrderId AND  ZODD.IsActive = 1)
		INNER JOIN ZnodePublishState ZODPS ON (ZODPS.PublishStateId = ZOO.PublishStateId)
		INNER JOIN #Portal ZP (nolock) ON ZODD.PortalId = ZP.PortalId
		LEFT JOIN ZnodePaymentType (nolock) ZPS ON (ZPS.PaymentTypeId = ZODD.PaymentTypeId )
		LEFT JOIN ZnodeOmsOrderStateShowToCustomer (nolock) ZOSC ON (ZOSC.OmsOrderStateId = ZODD.OmsOrderStateId)
		LEFT JOIN ZnodeOmsOrderState (nolock) ZOS ON (ZOS.OmsOrderStateId = ZODD.OmsOrderStateId)
		LEFT JOIN ZnodeOmsPaymentState (nolock) ZOPS ON (ZOPS.OmsPaymentStateId = ZODD.OmsPaymentStateId)
		WHERE (exists(select * from ZnodeSalesRepCustomerUserPortal SalRep where SalRep.SalesRepUserId = '+cast(@SalesRepUserId as varchar(10))+' and ZODD.UserId = SalRep.CustomerUserid) or '+cast(@SalesRepUserId as varchar(10))+' = 0 )  
		AND (EXISTS (SELECT TOP 1 1 FROM #tbl_GetRecurciveUserId FNRU WHERE FNRU.UserId = ZODD.UserId ) OR '+cast(@UserId as varchar(10))+'  =0 )'
		+ @Fn_GetFilterWhereClause+' 

		Insert Into #TBL_RowCount 
		SELECT count(*)
		FROM ZnodeOmsOrder (nolock) ZOO 
		INNER JOIN ZnodeOmsOrderDetails (nolock) ZODD ON (ZODD.OmsOrderId = ZOO.OmsOrderId AND  ZODD.IsActive = 1)
		INNER JOIN ZnodePublishState ZODPS ON (ZODPS.PublishStateId = ZOO.PublishStateId)
		INNER JOIN #Portal ZP (nolock) ON ZODD.PortalId = ZP.PortalId
		LEFT JOIN ZnodePaymentType (nolock) ZPS ON (ZPS.PaymentTypeId = ZODD.PaymentTypeId )
		LEFT JOIN ZnodeOmsOrderStateShowToCustomer (nolock) ZOSC ON (ZOSC.OmsOrderStateId = ZODD.OmsOrderStateId)
		LEFT JOIN ZnodeOmsOrderState (nolock) ZOS ON (ZOS.OmsOrderStateId = ZODD.OmsOrderStateId)
		LEFT JOIN ZnodeOmsPaymentState (nolock) ZOPS ON (ZOPS.OmsPaymentStateId = ZODD.OmsPaymentStateId)
		WHERE (EXISTS(select * from ZnodeSalesRepCustomerUserPortal SalRep where SalRep.SalesRepUserId = '+cast(@SalesRepUserId as varchar(10))+' and ZODD.UserId = SalRep.CustomerUserid) or '+cast(@SalesRepUserId as varchar(10))+' = 0 )  
		AND (EXISTS (SELECT TOP 1 1 FROM #tbl_GetRecurciveUserId FNRU WHERE FNRU.UserId = ZODD.UserId ) OR '+cast(@UserId as varchar(10))+'  =0 )'
		+ @Fn_GetFilterWhereClause+' 
			
		Create index Ind_OrderLineDescribe_RowId on #Cte_OrderLineDescribe(RowId )

		SELECT OmsOrderId,OrderNumber,PortalId,StoreName,CurrencyCode,OrderState,ShippingId,
		PaymentTypeId,PaymentSettingId,PaymentStatus,PaymentType,ShippingStatus,OrderDate,UserId,UserName,PaymentTransactionToken,Total,
		ProductName OrderItem,OmsOrderDetailsId,CountId ItemCount, PoDocument AS PODocumentPath,IsInRMA ,
		Email,PhoneNumber,SubTotal,TaxCost,ShippingCost,BillingPostalCode
		,OrderModifiedDate,PaymentDisplayName, 
		ExternalId,CreditCardExpMonth,CreditCardExpYear,CardType,CreditCardNumber,PaymentExternalId,CultureCode,PublishState --TotalAdditionalCost
		FROM #Cte_OrderLineDescribe
		' + @Fn_GetPaginationWhereClause +' order by RowId '

		print @SQL
		EXEC(@SQL)
		Select @RowsCount= isnull(RowsCount  ,0) from #TBL_RowCount
		
		IF OBJECT_ID('tempdb..#TBL_RowCount') is not null
				DROP TABLE #TBL_RowCount

		IF OBJECT_ID('tempdb..#Portal') is not null
			DROP TABLE #Portal
		
    END TRY
    BEGIN CATCH
        DECLARE @Status BIT ;
		SET @Status = 0;
		DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(),
		@ErrorCall NVARCHAR(MAX)= 'EXEC Znode_GetOmsOrderDetail @WhereClause = '''+ISNULL(CAST(@WhereClause AS VARCHAR(max)),'''''')+''',@Rows='''+ISNULL(CAST(@Rows AS VARCHAR(50)),'''''')+''',@PageNo='+ISNULL(CAST(@PageNo AS VARCHAR(50)),'''')+',
		@Order_BY='+ISNULL(@Order_BY,'''''')+',@UserId = '+ISNULL(CAST(@UserId AS VARCHAR(50)),'''')+',@RowsCount='+ISNULL(CAST(@RowsCount AS VARCHAR(50)),'''')+',@IsFromAdmin='+ISNULL(CAST(@IsFromAdmin AS VARCHAR(10)),'''');
              			 
        SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                    
		  
        EXEC Znode_InsertProcedureErrorLog
		@ProcedureName = 'Znode_GetOmsOrderDetail',
		@ErrorInProcedure = 'Znode_GetOmsOrderDetail',
		@ErrorMessage = @ErrorMessage,
		@ErrorLine = @ErrorLine,
		@ErrorCall = @ErrorCall;
    END CATCH;
END;
	 
GO
IF EXISTS(SELECT * FROM sys.procedures WHERE Name = 'Znode_InsertUpdateSaveCartLineItemQuantityWrapper')
	DROP PROC Znode_InsertUpdateSaveCartLineItemQuantityWrapper
GO
CREATE PROCEDURE [dbo].[Znode_InsertUpdateSaveCartLineItemQuantityWrapper]
(
	@CartLineItemXML xml, 
	@UserId int,
	@PortalId Int,
	@OmsCookieMappingId INT = 0 
)
AS 
   /* 
    Summary: THis Procedure is USed to save and edit the saved cart line item      
    Unit Testing 
	begin tran  
    Exec Znode_InsertUpdateSaveCartLineItem @CartLineItemXML= '<ArrayOfSavedCartLineItemModel>
  <SavedCartLineItemModel>
    <OmsSavedCartLineItemId>0</OmsSavedCartLineItemId>
    <ParentOmsSavedCartLineItemId>0</ParentOmsSavedCartLineItemId>
    <OmsSavedCartId>1259</OmsSavedCartId>
    <SKU>BlueGreenYellow</SKU>
    <Quantity>1.000000</Quantity>
    <OrderLineItemRelationshipTypeId>0</OrderLineItemRelationshipTypeId>
    <Sequence>1</Sequence>
    <AddonProducts>YELLOW</AddonProducts>
    <BundleProducts />
    <ConfigurableProducts>GREEN</ConfigurableProducts>
  </SavedCartLineItemModel>
  <SavedCartLineItemModel>
    <OmsSavedCartLineItemId>0</OmsSavedCartLineItemId>
    <ParentOmsSavedCartLineItemId>0</ParentOmsSavedCartLineItemId>
    <OmsSavedCartId>1259</OmsSavedCartId>
    <SKU>ap1534</SKU>
    <Quantity>1.0</Quantity>
    <OrderLineItemRelationshipTypeId>0</OrderLineItemRelationshipTypeId>
    <Sequence>2</Sequence>
    <AddonProducts >PINK</AddonProducts>
    <BundleProducts />
    <ConfigurableProducts />
    <PersonaliseValuesList>Address~Hello</PersonaliseValuesList>
  </SavedCartLineItemModel>
</ArrayOfSavedCartLineItemModel>' , @UserId=1 ,@Status=0
	rollback tran
*/
BEGIN
BEGIN TRAN InsertUpdateSaveCartLineItem;
BEGIN TRY
SET NOCOUNT ON;
	--Declared the variables
	DECLARE @GetDate datetime= dbo.Fn_GetDate();
	DECLARE @AddOnQuantity numeric(28, 6)= 0;
	DECLARE @IsAddProduct   BIT = 0 
	DECLARE @OmsSavedCartLineItemId INT = 0
	
	DECLARE @TBL_SavecartLineitems TABLE
	( 
		RowId int , OmsSavedCartLineItemId int, ParentOmsSavedCartLineItemId int, OmsSavedCartId int, SKU nvarchar(600), Quantity numeric(28, 6), OrderLineItemRelationshipTypeID int, CustomText nvarchar(max), 
		CartAddOnDetails nvarchar(max), Sequence int, AddOnValueIds varchar(max), BundleProductIds varchar(max), ConfigurableProductIds varchar(max), GroupProductIds varchar(max), PersonalisedAttribute XML, 
		AutoAddon varchar(max), OmsOrderId int, ItemDetails nvarchar(max),
		Custom1	nvarchar(max),Custom2 nvarchar(max),Custom3 nvarchar(max),Custom4
		nvarchar(max),Custom5 nvarchar(max),GroupId NVARCHAR(max) ,ProductName Nvarchar(1000) , Description NVARCHAR(max),AddOnQuantity NVARCHAR(max)
	);

	--Set the OrderLineItemRelationshipTypeId for Addons product
	DECLARE @OrderLineItemRelationshipTypeIdAddon int =
	(
		SELECT TOP 1 OrderLineItemRelationshipTypeId
		FROM ZnodeOmsOrderLineItemRelationshipType
		WHERE [Name] = 'AddOns'
	);
	--Set the OrderLineItemRelationshipTypeId for simple product
	DECLARE @OrderLineItemRelationshipTypeIdSimple int =
	(
		SELECT TOP 1 OrderLineItemRelationshipTypeId
		FROM ZnodeOmsOrderLineItemRelationshipType
		WHERE [Name] = 'Simple'
	);
	--Set the OrderLineItemRelationshipTypeId for group product
	DECLARE @OrderLineItemRelationshipTypeIdGroup int=
	(
		SELECT TOP 1 OrderLineItemRelationshipTypeId
		FROM ZnodeOmsOrderLineItemRelationshipType
		WHERE [Name] = 'Group'
	);
	--Set the OrderLineItemRelationshipTypeId for Configurable product
	DECLARE @OrderLineItemRelationshipTypeIdConfigurable int=
	(
		SELECT TOP 1 OrderLineItemRelationshipTypeId
		FROM ZnodeOmsOrderLineItemRelationshipType
		WHERE [Name] = 'Configurable'
	);
	--Set the OrderLineItemRelationshipTypeId for Bundles product	
	DECLARE @OrderLineItemRelationshipTypeIdBundle int=
	(
		SELECT TOP 1 OrderLineItemRelationshipTypeId
		FROM ZnodeOmsOrderLineItemRelationshipType
		WHERE [Name] = 'Bundles'
	);

	-----Getting OmsSaveCartId FROM @OmsCookieMappingId
	DECLARE @OmsSavedCartId int
	--Getting OmsCookieMappingId on basis of @UserId and portalid if @UserId > 0 (Not a guest User)
	IF isnull(@OmsCookieMappingId,0)=0 and isnull(@UserId,0) <> 0  
	Begin
		SET @OmsCookieMappingId = (SELECT top 1 OmsCookieMappingId FROM ZnodeOmsCookieMapping with (nolock) where isnull(UserId,0) = @UserID AND isnull(PortalId,0) = @PortalId)
	END

	IF Not Exists(SELECT top 1 OmsCookieMappingId FROM ZnodeOmsCookieMapping with (nolock) Where OmsCookieMappingId = @OmsCookieMappingId)
	Begin
		INSERT INTO ZnodeOmsCookieMapping (UserId,PortalId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		SELECT case when @UserId = 0 then null else @UserId end ,@PortalId,@UserId,@GetDate,@UserId,@GetDate

		SET @OmsCookieMappingId = @@IDENTITY
	End
	----To get the oms savecartid on basis of @OmsCookieMappingId 
	SET @OmsSavedCartId = (SELECT top 1 OmsSavedCartId FROM ZnodeOmsSavedCart with (nolock) where OmsCookieMappingId = @OmsCookieMappingId)
	
	----If omssavecartid not present in ZnodeOmsSavedCart table then inserting new record to generated omssavecartid 
	IF isnull(@OmsSavedCartId,0) = 0
	BEGIN
		INSERT INTO ZnodeOmsSavedCart(OmsCookieMappingId,SalesTax,RecurringSalesTax,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		SELECT @OmsCookieMappingId,Null,Null,@UserId,@GetDate,@UserId,@GetDate

		SET @OmsSavedCartId = @@IDENTITY
	END

	--Fetching data FROM xml to table format and inserted into table @TBL_SavecartLineitems
	INSERT INTO @TBL_SavecartLineitems( RowId,OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, SKU, Quantity, OrderLineItemRelationshipTypeID, CustomText, CartAddOnDetails, Sequence, AddOnValueIds, BundleProductIds, ConfigurableProductIds, GroupProductIds, PersonalisedAttribute, AutoAddon, OmsOrderId, ItemDetails,
	Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName,Description,AddOnQuantity )
	SELECT DENSE_RANK()Over(Order BY Tbl.Col.value( 'SKU[1]', 'NVARCHAR(2000)' )) RowId ,Tbl.Col.value( 'OmsSavedCartLineItemId[1]', 'NVARCHAR(2000)' ) AS OmsSavedCartLineItemId, Tbl.Col.value( 'ParentOmsSavedCartLineItemId[1]', 'NVARCHAR(2000)' ) AS ParentOmsSavedCartLineItemId, @OmsSavedCartId AS OmsSavedCartId, Tbl.Col.value( 'SKU[1]', 'NVARCHAR(2000)' ) AS SKU, Tbl.Col.value( 'Quantity[1]', 'NVARCHAR(2000)' ) AS Quantity
	, Tbl.Col.value( 'OrderLineItemRelationshipTypeID[1]', 'NVARCHAR(2000)' ) AS OrderLineItemRelationshipTypeID, Tbl.Col.value( 'CustomText[1]', 'NVARCHAR(2000)' ) AS CustomText, Tbl.Col.value( 'CartAddOnDetails[1]', 'NVARCHAR(2000)' ) AS CartAddOnDetails, Tbl.Col.value( 'Sequence[1]', 'NVARCHAR(2000)' ) AS Sequence, Tbl.Col.value( 'AddonProducts[1]', 'NVARCHAR(2000)' ) AS AddOnValueIds, ISNULL(Tbl.Col.value( 'BundleProducts[1]', 'NVARCHAR(2000)' ),'') AS BundleProductIds, ISNULL(Tbl.Col.value( 'ConfigurableProducts[1]', 'NVARCHAR(2000)' ),'') AS ConfigurableProductIds, ISNULL(Tbl.Col.value( 'GroupProducts[1]', 'NVARCHAR(Max)' ),'') AS GroupProductIds, 
			Tbl.Col.query('(PersonaliseValuesDetail/node())') AS PersonaliseValuesDetail, Tbl.Col.value( 'AutoAddon[1]', 'NVARCHAR(Max)' ) AS AutoAddon, Tbl.Col.value( 'OmsOrderId[1]', 'NVARCHAR(Max)' ) AS OmsOrderId,
			Tbl.Col.value( 'ItemDetails[1]', 'NVARCHAR(Max)' ) AS ItemDetails,
			Tbl.Col.value( 'Custom1[1]', 'NVARCHAR(Max)' ) AS Custom1,
			Tbl.Col.value( 'Custom2[1]', 'NVARCHAR(Max)' ) AS Custom2,
			Tbl.Col.value( 'Custom3[1]', 'NVARCHAR(Max)' ) AS Custom3,
			Tbl.Col.value( 'Custom4[1]', 'NVARCHAR(Max)' ) AS Custom4,
			Tbl.Col.value( 'Custom5[1]', 'NVARCHAR(Max)' ) AS Custom5,
			Tbl.Col.value( 'GroupId[1]', 'NVARCHAR(Max)' ) AS GroupId,
			Tbl.Col.value( 'ProductName[1]', 'NVARCHAR(Max)' ) AS ProductName,
			Tbl.Col.value( 'Description[1]', 'NVARCHAR(Max)' ) AS Description, 
			Tbl.Col.value( 'AddOnQuantity[1]', 'NVARCHAR(2000)' ) AS AddOnQuantity
	FROM @CartLineItemXML.nodes( '//ArrayOfSavedCartLineItemModel/SavedCartLineItemModel' ) AS Tbl(Col);
			  
	IF OBJECT_ID('tempdb..#TBL_SavecartLineitems') is not null
		DROP TABLE #TBL_SavecartLineitems

	IF OBJECT_ID('tempdb..#OldValueForAddon') is not null
		DROP TABLE #OldValueForAddon

	SELECT * INTO #TBL_SavecartLineitems FROM @TBL_SavecartLineitems
			
	UPDATE ZnodeOmsSavedCart
	SET ModifiedDate = GETDATE()
	WHERE OmsSavedCartId = (SELECT TOP 1  OmsSavedCartId FROM @TBL_SavecartLineitems)

	UPDATE  @TBL_SavecartLineitems
	SET 	Description = ISNUll(Description,'') 

	--Save cart execution code for bundle product
	IF EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems WHERE BundleProductIds <> '' )
	BEGIN 				
		IF EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems WHERE BundleProductIds <> '' AND OmsSavedCartLineItemId <> 0  ) 
		BEGIN 
			SET @OmsSavedCartLineItemId  = (SELECT TOP 1 OmsSavedCartLineItemId FROM @TBL_SavecartLineitems WHERE BundleProductIds <> '' AND OmsSavedCartLineItemId <> 0 )

			UPDATE ZnodeOmsSavedCartLineItem 
			SET Quantity = (SELECT TOP 1 Quantity FROM @TBL_SavecartLineitems WHERE BundleProductIds <> '' AND OmsSavedCartLineItemId <> 0)
			, ModifiedDate = @GetDate
			WHERE ( OmsSavedCartLineItemId = @OmsSavedCartLineItemId  
			OR ParentOmsSavedCartLineItemId =  @OmsSavedCartLineItemId   ) 

			UPDATE ZnodeOmsSavedCartLineItem 
			SET Quantity = AddOnQuantity, ModifiedDate = @GetDate
			FROM ZnodeOmsSavedCartLineItem ZOSCLI with (nolock)
			INNER JOIN @TBL_SavecartLineitems SCLI ON ZOSCLI.ParentOmsSavedCartLineItemId = SCLI.OmsSavedCartLineItemId AND ZOSCLI.OmsSavedCartId = SCLI.OmsSavedCartId AND ZOSCLI.SKU = SCLI.AddOnValueIds
			WHERE ZOSCLI.OrderLineItemRelationshipTypeId = @OrderLineItemRelationshipTypeIdAddon
			AND SCLI.BundleProductIds <> ''

			--After update the existing cart with new save cart then deleting those records which are updated
			DELETE	FROM @TBL_SavecartLineitems WHERE BundleProductIds <> '' AND OmsSavedCartLineItemId <> 0
		END 

		--Getting bundle save cart line item entries into table to pass for bundle prodcut sp
		DECLARE @TBL_bundleProduct TT_SavecartLineitems 
		INSERT INTO @TBL_bundleProduct 
		SELECT *  
		FROM @TBL_SavecartLineitems 
		WHERE ISNULL(BundleProductIds,'') <> '' 
				
		EXEC Znode_InsertUpdateSaveCartLineItemBundle @TBL_bundleProduct,@userId,@OrderLineItemRelationshipTypeIdBundle,@OrderLineItemRelationshipTypeIdAddon
				 
		DELETE FROM  @TBL_SavecartLineitems WHERE ISNULL(BundleProductIds,'') <> '' 
		SET @OmsSavedCartLineItemId = 0 
	END 
	--Save cart execution code for configurable product
	IF EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems WHERE ConfigurableProductIds <> '' )
	BEGIN 				
		IF EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems WHERE ConfigurableProductIds <> '' AND OmsSavedCartLineItemId <> 0  ) 
		BEGIN 

			SET @OmsSavedCartLineItemId  = (SELECT TOP 1 OmsSavedCartLineItemId FROM @TBL_SavecartLineitems WHERE ConfigurableProductIds <> '' AND OmsSavedCartLineItemId <> 0 )
				 
			UPDATE ZnodeOmsSavedCartLineItem 
			SET Quantity = (SELECT TOP 1 Quantity FROM @TBL_SavecartLineitems WHERE ConfigurableProductIds <> '' AND OmsSavedCartLineItemId = @OmsSavedCartLineItemId )
			, ModifiedDate = @GetDate
			WHERE OmsSavedCartLineItemId = @OmsSavedCartLineItemId

			UPDATE ZnodeOmsSavedCartLineItem 
			SET Quantity = AddOnQuantity, ModifiedDate = @GetDate
			FROM ZnodeOmsSavedCartLineItem ZOSCLI with (nolock)
			INNER JOIN @TBL_SavecartLineitems SCLI ON ZOSCLI.ParentOmsSavedCartLineItemId = SCLI.OmsSavedCartLineItemId AND ZOSCLI.OmsSavedCartId = SCLI.OmsSavedCartId AND ZOSCLI.SKU = SCLI.AddOnValueIds
			WHERE ZOSCLI.OrderLineItemRelationshipTypeId = @OrderLineItemRelationshipTypeIdAddon
			AND SCLI.ConfigurableProductIds <> ''

			--After update the existing cart with new save cart then deleting those records which are updated
			DELETE	FROM @TBL_SavecartLineitems WHERE ConfigurableProductIds <> '' AND OmsSavedCartLineItemId <> 0
		END 
		--Getting bundle save cart line item entries into table to pass for configurable prodcut sp
		DECLARE @TBL_Configurable TT_SavecartLineitems 
		INSERT INTO @TBL_Configurable 
		SELECT *  
		FROM @TBL_SavecartLineitems 
		WHERE ISNULL(ConfigurableProductIds,'') <> '' 
		  
		EXEC Znode_InsertUpdateSaveCartLineItemConfigurable @TBL_Configurable,@userId,@OrderLineItemRelationshipTypeIdConfigurable,@OrderLineItemRelationshipTypeIdAddon
				  
		DELETE FROM @TBL_SavecartLineitems 
		WHERE ISNULL(ConfigurableProductIds,'') <> ''
		SET @OmsSavedCartLineItemId = 0  
	END 
	--Save cart execution code for group product
	IF EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems WHERE GroupProductIds <> '' )
	BEGIN 				
		IF EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems WHERE GroupProductIds <> '' AND OmsSavedCartLineItemId <> 0  ) 
		BEGIN 
			--Updating the existing save cart for group product
			SET @OmsSavedCartLineItemId  = (SELECT TOP 1 OmsSavedCartLineItemId FROM @TBL_SavecartLineitems WHERE GroupProductIds <> '' AND OmsSavedCartLineItemId <> 0 )
			UPDATE ZnodeOmsSavedCartLineItem 
			SET Quantity = (SELECT TOP 1 Quantity FROM @TBL_SavecartLineitems WHERE GroupProductIds <> '' AND OmsSavedCartLineItemId = @OmsSavedCartLineItemId )
			WHERE OmsSavedCartLineItemId = @OmsSavedCartLineItemId

			UPDATE ZnodeOmsSavedCartLineItem 
			SET Quantity = AddOnQuantity, ModifiedDate = @GetDate
			FROM ZnodeOmsSavedCartLineItem ZOSCLI with (nolock)
			INNER JOIN @TBL_SavecartLineitems SCLI ON ZOSCLI.ParentOmsSavedCartLineItemId = SCLI.OmsSavedCartLineItemId AND ZOSCLI.OmsSavedCartId = SCLI.OmsSavedCartId AND ZOSCLI.SKU = SCLI.AddOnValueIds
			WHERE ZOSCLI.OrderLineItemRelationshipTypeId = @OrderLineItemRelationshipTypeIdAddon
			AND SCLI.GroupProductIds <> ''

			--After update the existing cart with new save cart then deleting those records which are updated
			DELETE	FROM @TBL_SavecartLineitems WHERE GroupProductIds <> '' AND OmsSavedCartLineItemId <> 0
		END 
		--Getting bundle save cart line item entries into table to pass for group prodcut sp
		DECLARE @TBL_Group TT_SavecartLineitems 
		INSERT INTO @TBL_Group 
		SELECT *  
		FROM @TBL_SavecartLineitems 
		WHERE ISNULL(GroupProductIds,'') <> '' 
		
		EXEC Znode_InsertUpdateSaveCartLineItemGroup @TBL_Group,@userId,@OrderLineItemRelationshipTypeIdGroup,@OrderLineItemRelationshipTypeIdAddon
		
		--After update the existing cart with new save cart then deleting those records which are updated
		DELETE FROM @TBL_SavecartLineitems WHERE ISNULL(GroupProductIds,'') <> ''
		SET @OmsSavedCartLineItemId = 0  
	END 
	
	--This part is for updating the cart for existing line items for simple product
	IF EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems WHERE  OmsSavedCartLineItemId <> 0  ) 
	BEGIN 
				 
		SET @OmsSavedCartLineItemId  = (SELECT TOP 1 OmsSavedCartLineItemId FROM @TBL_SavecartLineitems WHERE  OmsSavedCartLineItemId <> 0 )
		UPDATE ZnodeOmsSavedCartLineItem 
		SET Quantity = (SELECT TOP 1 Quantity FROM @TBL_SavecartLineitems WHERE  OmsSavedCartLineItemId = @OmsSavedCartLineItemId )
		, ModifiedDate = @GetDate
		WHERE OmsSavedCartLineItemId = @OmsSavedCartLineItemId

		UPDATE ZnodeOmsSavedCartLineItem 
		SET Quantity = AddOnQuantity, ModifiedDate = @GetDate
		FROM ZnodeOmsSavedCartLineItem ZOSCLI with (nolock)
		INNER JOIN @TBL_SavecartLineitems SCLI ON ZOSCLI.ParentOmsSavedCartLineItemId = @OmsSavedCartLineItemId AND ZOSCLI.OmsSavedCartId = SCLI.OmsSavedCartId AND ZOSCLI.SKU = SCLI.AddOnValueIds
		WHERE ZOSCLI.OrderLineItemRelationshipTypeId = @OrderLineItemRelationshipTypeIdAddon
					
		DELETE	FROM @TBL_SavecartLineitems WHERE OmsSavedCartLineItemId <> 0
	END 
	--Save cart execution code for Simple product
	DECLARE @OmsInsertedData TABLE (OmsSavedCartLineItemId INT )
	--Inserting the personalise data into variable table @TBL_Personalise for inserting the personalise data for new products
	DECLARE @TBL_Personalise TABLE (OmsSavedCartLineItemId INT, ParentOmsSavedCartLineItemId int,SKU Varchar(600) ,PersonalizeCode NVARCHAr(max),PersonalizeValue NVARCHAr(max),DesignId NVARCHAr(max), ThumbnailURL NVARCHAr(max))
	INSERT INTO @TBL_Personalise
	SELECT DISTINCT Null, a.ParentOmsSavedCartLineItemId,a.SKU
			,Tbl.Col.value( 'PersonalizeCode[1]', 'NVARCHAR(Max)' ) AS PersonalizeCode
			,Tbl.Col.value( 'PersonalizeValue[1]', 'NVARCHAR(Max)' ) AS PersonalizeValue
			,Tbl.Col.value( 'DesignId[1]', 'NVARCHAR(Max)' ) AS DesignId
			,Tbl.Col.value( 'ThumbnailURL[1]', 'NVARCHAR(Max)' ) AS ThumbnailURL
	FROM @TBL_SavecartLineitems a 
	CROSS APPLY a.PersonalisedAttribute.nodes( '//PersonaliseValueModel' ) AS Tbl(Col) 
			  
	----To update saved cart item personalise value FROM given line item
	DECLARE @TBL_Personalise1 TABLE (OmsSavedCartLineItemId INT ,PersonalizeCode NVARCHAr(max),PersonalizeValue NVARCHAr(max),DesignId NVARCHAr(max), ThumbnailURL NVARCHAr(max))
	INSERT INTO @TBL_Personalise1
	SELECT DISTINCT a.OmsSavedCartLineItemId 
			,Tbl.Col.value( 'PersonalizeCode[1]', 'NVARCHAR(Max)' ) AS PersonalizeCode
			,Tbl.Col.value( 'PersonalizeValue[1]', 'NVARCHAR(Max)' ) AS PersonalizeValue
			,Tbl.Col.value( 'DesignId[1]', 'NVARCHAR(Max)' ) AS DesignId
			,Tbl.Col.value( 'ThumbnailURL[1]', 'NVARCHAR(Max)' ) AS ThumbnailURL
	FROM (SELECT TOP 1 OmsSavedCartLineItemId,PersonalisedAttribute Valuex FROM  #TBL_SavecartLineitems TRTR ) a 
	CROSS APPLY	a.Valuex.nodes( '//PersonaliseValueModel' ) AS Tbl(Col)  
		    
			
	CREATE TABLE #NewSavecartLineitemDetails 
	(
		GenId INT IDENTITY(1,1),RowId	INT	,OmsSavedCartLineItemId	INT	 ,ParentOmsSavedCartLineItemId	INT,OmsSavedCartId	INT
		,SKU	NVARCHAR(MAX) ,Quantity	NUMERIC(28,6)	,OrderLineItemRelationshipTypeID	INT	,CustomText	NVARCHAR(MAX)
		,CartAddOnDetails	NVARCHAR(MAX),Sequence	int	,AutoAddon	varchar(MAX)	,OmsOrderId	INT	,ItemDetails	NVARCHAR(MAX)
		,Custom1	NVARCHAR(MAX)  ,Custom2	NVARCHAR(MAX),Custom3	NVARCHAR(MAX),Custom4	NVARCHAR(MAX),Custom5	NVARCHAR(MAX)
		,GroupId	NVARCHAR(MAX) ,ProductName	NVARCHAR(MAX),Description	NVARCHAR(MAX),Id	INT,ParentSKU NVARCHAR(MAX)
	)
	
	--Getting new save cart data
	INSERT INTO #NewSavecartLineitemDetails
	SELECT  Min(RowId )RowId ,OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, SKU
		,Quantity, OrderLineItemRelationshipTypeID, CustomText, CartAddOnDetails, Sequence
		,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,  GroupId ,ProductName,min(Description)Description	,0 Id,NULL ParentSKU 
	FROM @TBL_SavecartLineitems a 
	GROUP BY  OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, SKU
		,Quantity, OrderLineItemRelationshipTypeID, CustomText, CartAddOnDetails, Sequence
		,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName
	
	--Getting new simple product save cart data
	INSERT INTO #NewSavecartLineitemDetails
	SELECT  Min(RowId )RowId ,OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, SKU
		,Quantity, @OrderLineItemRelationshipTypeIdSimple, CustomText, CartAddOnDetails, Sequence
		,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName,min(Description)Description	,1 Id,SKU ParentSKU 
	FROM @TBL_SavecartLineitems  a 
	WHERE ISNULL(BundleProductIds,'') =  '' 
	AND  ISNULL(GroupProductIds,'') = ''	AND ISNULL(	ConfigurableProductIds,'') = ''
		GROUP BY  OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, SKU
		,Quantity,  CustomText, CartAddOnDetails, Sequence
		,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName
			
	--Getting new Group,Bundle and Configurable products save cart data if addon is present for any line item
	INSERT INTO #NewSavecartLineitemDetails
	SELECT  Min(RowId )RowId ,OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, AddOnValueIds
		,AddOnQuantity, @OrderLineItemRelationshipTypeIdAddon, CustomText, CartAddOnDetails, Sequence
		,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName,min(Description)Description	,1 Id 
		,CASE WHEN ConfigurableProductIds <> ''  THEN ConfigurableProductIds
		WHEN  GroupProductIds <> '' THEN GroupProductIds 
		WHEN BundleProductIds <> '' THEN BundleProductIds 
			ELSE SKU END     ParentSKU 
	FROM @TBL_SavecartLineitems  a 
	WHERE AddOnValueIds <> ''
	GROUP BY  OmsSavedCartLineItemId, ParentOmsSavedCartLineItemId, OmsSavedCartId, AddOnValueIds
	,AddOnQuantity,  CustomText, CartAddOnDetails, Sequence ,ConfigurableProductIds,GroupProductIds,	BundleProductIds
	,AutoAddon, OmsOrderId, ItemDetails,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName,SKU
	
	CREATE TABLE #OldSavecartLineitemDetails (OmsSavedCartId INT ,OmsSavedCartLineItemId INT,ParentOmsSavedCartLineItemId INT , SKU  NVARCHAr(2000),OrderLineItemRelationshipTypeID INT  )
	--Getting the old save cart data if present for same SKU in the new save cart data for simple product	 
	INSERT INTO #OldSavecartLineitemDetails  
	SELECT  a.OmsSavedCartId,a.OmsSavedCartLineItemId,a.ParentOmsSavedCartLineItemId , a.SKU  ,a.OrderLineItemRelationshipTypeID 
	FROM ZnodeOmsSavedCartLineItem a with (nolock)  
	WHERE EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems  TY WHERE TY.OmsSavedCartId = a.OmsSavedCartId AND ISNULL(a.SKU,'') = ISNULL(TY.SKU,'')   )   
	AND a.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdSimple   

	--Getting the old save cart Parent data 
	INSERT INTO #OldSavecartLineitemDetails 
	SELECT DISTINCT b.OmsSavedCartId,b.OmsSavedCartLineItemId,b.ParentOmsSavedCartLineItemId , b.SKU  ,b.OrderLineItemRelationshipTypeID  
	FROM ZnodeOmsSavedCartLineItem b with (nolock)
	INNER JOIN #OldSavecartLineitemDetails c ON (c.ParentOmsSavedCartLineItemId  = b.OmsSavedCartLineItemId AND c.OmsSavedCartId = b.OmsSavedCartId)
	WHERE EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems  TY WHERE TY.OmsSavedCartId = b.OmsSavedCartId AND ISNULL(b.SKU,'') = ISNULL(TY.SKU,'') AND ISNULL(b.Groupid,'-') = ISNULL(TY.Groupid,'-')  )
	AND  b.OrderLineItemRelationshipTypeID IS NULL 
		 
	DELETE a FROM #OldSavecartLineitemDetails a WHERE NOT EXISTS (SELECT TOP 1 1  FROM #OldSavecartLineitemDetails b WHERE b.ParentOmsSavedCartLineItemId IS NULL AND b.OmsSavedCartLineItemId = a.ParentOmsSavedCartLineItemId)
	AND a.ParentOmsSavedCartLineItemId IS NOT NULL 
		
	------Merge Addon for same product
	SELECT * INTO #OldValueForAddon FROM #OldSavecartLineitemDetails
		
	--Getting the old save cart addon data for old line items if present
	INSERT INTO #OldSavecartLineitemDetails 
	SELECT b.OmsSavedCartId,b.OmsSavedCartLineItemId,b.ParentOmsSavedCartLineItemId , b.SKU  ,b.OrderLineItemRelationshipTypeID  
	FROM ZnodeOmsSavedCartLineItem b with (nolock)
	INNER JOIN #OldSavecartLineitemDetails c ON (c.OmsSavedCartLineItemId  = b.ParentOmsSavedCartLineItemId AND c.OmsSavedCartId = b.OmsSavedCartId)
	WHERE EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems  TY WHERE TY.OmsSavedCartId = b.OmsSavedCartId AND ISNULL(b.SKU,'') = ISNULL(TY.AddOnValueIds,'') )
	AND  b.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon

	------Merge Addon for same product
	IF EXISTS(SELECT * FROM @TBL_SavecartLineitems WHERE ISNULL(AddOnValueIds,'') <> '' )
	BEGIN

		INSERT INTO #OldValueForAddon 
		SELECT b.OmsSavedCartId,b.OmsSavedCartLineItemId,b.ParentOmsSavedCartLineItemId , b.SKU  ,b.OrderLineItemRelationshipTypeID  
		FROM ZnodeOmsSavedCartLineItem b with (nolock)
		INNER JOIN #OldValueForAddon c ON (c.OmsSavedCartLineItemId  = b.ParentOmsSavedCartLineItemId AND c.OmsSavedCartId = b.OmsSavedCartId)
		WHERE EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems  TY WHERE TY.OmsSavedCartId = b.OmsSavedCartId )--AND ISNULL(b.SKU,'') = ISNULL(TY.AddOnValueIds,'') )
		AND  b.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon

		SELECT distinct SKU, STUFF(
								( SELECT  ', ' + SKU FROM    
									( SELECT DISTINCT SKU FROM     #OldValueForAddon b 
										where a.OmsSavedCartLineItemId=b.ParentOmsSavedCartLineItemId and OrderLineItemRelationshipTypeID = 1 ) x 
										FOR XML PATH('')
								), 1, 2, ''
								) AddOns
		INTO #AddOnsExists
		FROM #OldValueForAddon a where a.ParentOmsSavedCartLineItemId is not null and OrderLineItemRelationshipTypeID<>1

		SELECT distinct a.SKU, STUFF(
									( SELECT  ', ' + x.AddOnValueIds FROM    
									( SELECT DISTINCT b.AddOnValueIds FROM @TBL_SavecartLineitems b
										where a.SKU=b.SKU ) x
										FOR XML PATH('')
									), 1, 2, ''
								) AddOns
		INTO #AddOnAdded
		FROM @TBL_SavecartLineitems a

		IF NOT EXISTS(SELECT * FROM #AddOnsExists a INNER JOIN #AddOnAdded b on a.SKU = b.SKU and a.AddOns = b.AddOns )
		BEGIN
			DELETE FROM #OldSavecartLineitemDetails
		END

	END

	--If addon present in new and old save cart data and not matches the addon data (old and new for merge) then removing the old save cart data FROM #OldSavecartLineitemDetails
	IF NOT EXISTS (SELECT TOP 1 1  FROM @TBL_SavecartLineitems ty WHERE EXISTS (SELECT TOP 1 1 FROM 	#OldSavecartLineitemDetails a WHERE	
	ISNULL(TY.AddOnValueIds,'') = a.SKU AND  a.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon ))
	AND EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems WHERE ISNULL(AddOnValueIds,'')  <> '' )
	BEGIN 
		
		DELETE FROM #OldSavecartLineitemDetails 
	END 
	ELSE 
	BEGIN 
	    
		IF EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems WHERE ISNULL(AddOnValueIds,'')  <> '' )
		BEGIN 
		 
			DECLARE @parenTofAddon  TABLE( ParentOmsSavedCartLineItemId INT  )  
			INSERT INTO  @parenTofAddon 
			SELECT  ParentOmsSavedCartLineItemId FROM #OldSavecartLineitemDetails WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon  

			DELETE FROM #OldSavecartLineitemDetails WHERE OmsSavedCartLineItemId NOT IN (SELECT ParentOmsSavedCartLineItemId FROM  @parenTofAddon)   
				AND ParentOmsSavedCartLineItemId IS NOT NULL  
				AND OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdAddon

			DELETE FROM #OldSavecartLineitemDetails WHERE OmsSavedCartLineItemId NOT IN (SELECT ISNULL(m.ParentOmsSavedCartLineItemId,0) FROM #OldSavecartLineitemDetails m)
			AND ParentOmsSavedCartLineItemId IS  NULL  
		 
		END 
		ELSE IF (SELECT COUNT (OmsSavedCartLineItemId) FROM #OldSavecartLineitemDetails WHERE ParentOmsSavedCartLineItemId IS NULL ) > 1 
		BEGIN 

			DECLARE @TBL_deleteParentOmsSavedCartLineItemId TABLE (OmsSavedCartLineItemId INT )
			INSERT INTO @TBL_deleteParentOmsSavedCartLineItemId 
			SELECT ParentOmsSavedCartLineItemId
			FROM ZnodeOmsSavedCartLineItem a with (nolock)
			WHERE ParentOmsSavedCartLineItemId IN (SELECT OmsSavedCartLineItemId FROM #OldSavecartLineitemDetails WHERE OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdSimple  )
			AND OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon 

			DELETE FROM #OldSavecartLineitemDetails WHERE OmsSavedCartLineItemId IN (SELECT OmsSavedCartLineItemId FROM @TBL_deleteParentOmsSavedCartLineItemId)
			OR ParentOmsSavedCartLineItemId IN (SELECT OmsSavedCartLineItemId FROM @TBL_deleteParentOmsSavedCartLineItemId)
		    
			DELETE FROM #OldSavecartLineitemDetails WHERE OmsSavedCartLineItemId NOT IN (SELECT ISNULL(m.ParentOmsSavedCartLineItemId,0) FROM #OldSavecartLineitemDetails m)
			AND ParentOmsSavedCartLineItemId IS  NULL  

		END
		ELSE IF  EXISTS (SELECT TOP 1 1 FROM ZnodeOmsSavedCartLineItem Wt WHERE EXISTS (SELECT TOP 1 1 FROM #OldSavecartLineitemDetails ty WHERE ty.OmsSavedCartId = wt.OmsSavedCartId AND ty.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdSimple AND wt.ParentOmsSavedCartLineItemId= ty.OmsSavedCartLineItemId  ) AND wt.OrderLineItemRelationshipTypeId = @OrderLineItemRelationshipTypeIdAddon)
		AND EXISTS (SELECT TOP 1 1 FROM @TBL_SavecartLineitems WHERE ISNULL(AddOnValueIds,'')  = '' )
		BEGIN 

			DELETE FROM #OldSavecartLineitemDetails
		END 
	END 

	--Getting the personalise data for old save cart if present
	DECLARE @TBL_Personaloldvalues TABLE (OmsSavedCartLineItemId INT , PersonalizeCode NVARCHAr(max), PersonalizeValue NVARCHAr(max))
	INSERT INTO @TBL_Personaloldvalues
	SELECT OmsSavedCartLineItemId , PersonalizeCode, PersonalizeValue
	FROM ZnodeOmsPersonalizeCartItem  a 
	WHERE EXISTS (SELECT TOP 1 1 FROM #OldSavecartLineitemDetails TY WHERE TY.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId)
	AND EXISTS (SELECT TOP 1 1 FROM @TBL_Personalise TU WHERE TU.PersonalizeCode = a.PersonalizeCode AND TU.PersonalizeValue = a.PersonalizeValue)

	IF  NOT EXISTS (SELECT TOP 1 1 FROM @TBL_Personaloldvalues)
	AND EXISTS (SELECT TOP 1 1 FROM @TBL_Personalise )
	BEGIN 
		DELETE FROM #OldSavecartLineitemDetails
	END 
	ELSE 
	BEGIN 
		IF EXISTS (SELECT TOP 1 1 FROM @TBL_Personaloldvalues)
		AND (SELECT COUNT (DISTINCT OmsSavedCartLineItemId ) FROM #OldSavecartLineitemDetails WHERE ParentOmsSavedCartLineItemId IS nULL ) > 1 
		BEGIN 
		   
			DELETE FROM #OldSavecartLineitemDetails WHERE OmsSavedCartLineItemId IN (
			SELECT OmsSavedCartLineItemId FROM #OldSavecartLineitemDetails WHERE OmsSavedCartLineItemId NOT IN (SELECT OmsSavedCartLineItemId FROM @TBL_Personaloldvalues )
			AND ParentOmsSavedCartLineItemId NOT IN (SELECT OmsSavedCartLineItemId FROM @TBL_Personaloldvalues ) ) 
			OR OmsSavedCartLineItemId IN ( SELECT ParentOmsSavedCartLineItemId FROM #OldSavecartLineitemDetails WHERE OmsSavedCartLineItemId NOT IN (SELECT OmsSavedCartLineItemId FROM @TBL_Personaloldvalues )
			AND ParentOmsSavedCartLineItemId NOT IN (SELECT OmsSavedCartLineItemId FROM @TBL_Personaloldvalues ))
		      
		END 
		ELSE IF NOT EXISTS (SELECT TOP 1 1 FROM @TBL_Personaloldvalues)
		AND (SELECT COUNT (DISTINCT OmsSavedCartLineItemId ) FROM #OldSavecartLineitemDetails WHERE ParentOmsSavedCartLineItemId IS nULL ) > 1 
		BEGIN 

			DELETE n FROM #OldSavecartLineitemDetails n WHERE OmsSavedCartLineItemId  IN (SELECT OmsSavedCartLineItemId FROM ZnodeOmsPersonalizeCartItem WHERE n.OmsSavedCartLineItemId = ZnodeOmsPersonalizeCartItem.OmsSavedCartLineItemId  )
			OR ParentOmsSavedCartLineItemId  IN (SELECT OmsSavedCartLineItemId FROM ZnodeOmsPersonalizeCartItem   )
	   
		END 
		ELSE IF NOT EXISTS (SELECT TOP 1 1  FROM @TBL_Personalise)
		AND EXISTS (SELECT TOP 1 1 FROM ZnodeOmsPersonalizeCartItem m WHERE EXISTS (SELECT Top 1 1 FROM #OldSavecartLineitemDetails YU WHERE YU.OmsSavedCartLineItemId = m.OmsSavedCartLineItemId )) 
		AND (SELECT COUNT (DISTINCT OmsSavedCartLineItemId ) FROM #OldSavecartLineitemDetails WHERE ParentOmsSavedCartLineItemId IS nULL ) = 1
		BEGIN 
			DELETE FROM #OldSavecartLineitemDetails WHERE NOT EXISTS (SELECT TOP 1 1  FROM @TBL_Personalise)
		END 
	  
	END 

	--If already exists cart 
	IF EXISTS (SELECT TOP 1 1 FROM #OldSavecartLineitemDetails )
	BEGIN
		----DELETE old value FROM table which having personalise data in ZnodeOmsPersonalizeCartItem but same SKU not having personalise value for new cart item
		;WITH cte AS
		(
			SELECT distinct b.*
			FROM @TBL_SavecartLineitems a 
			INNER JOIN #OldSavecartLineitemDetails b on ( a.SKU = b.sku)
			where isnull(cast(a.PersonalisedAttribute AS varchar(max)),'') = ''
		)
		,cte2 AS
		(
			SELECT c.ParentOmsSavedCartLineItemId
			FROM #OldSavecartLineitemDetails a
			INNER JOIN ZnodeOmsSavedCartLineItem c on a.OmsSavedCartLineItemId = c.ParentOmsSavedCartLineItemId
			INNER JOIN ZnodeOmsPersonalizeCartItem b on b.OmsSavedCartLineItemId = c.OmsSavedCartLineItemId
		)
		DELETE a FROM #OldSavecartLineitemDetails a
		INNER JOIN cte b on a.OmsSavedCartLineItemId = b.OmsSavedCartLineItemId
		INNER JOIN cte2 c on (a.OmsSavedCartLineItemId = c.ParentOmsSavedCartLineItemId or a.ParentOmsSavedCartLineItemId = c.ParentOmsSavedCartLineItemId)

		----DELETE old value FROM table which having personalise data in ZnodeOmsPersonalizeCartItem but same SKU having personalise value for new cart item
		;WITH cte AS
		(
			SELECT distinct b.*, 
				a.PersonalizeCode
				,a.PersonalizeValue
			FROM @TBL_Personalise a 
			INNER JOIN #OldSavecartLineitemDetails b on ( a.SKU = b.sku)
			where a.PersonalizeValue <> ''
		)
		,cte2 AS
		(
			SELECT a.ParentOmsSavedCartLineItemId, b.PersonalizeCode, b.PersonalizeValue
			FROM #OldSavecartLineitemDetails a
			INNER JOIN ZnodeOmsPersonalizeCartItem b on b.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId
			WHERE NOT EXISTS(SELECT * FROM cte c where b.OmsSavedCartLineItemId = c.OmsSavedCartLineItemId and b.PersonalizeCode = c.PersonalizeCode 
						and b.PersonalizeValue = c.PersonalizeValue )
		)
		DELETE a FROM #OldSavecartLineitemDetails a
		INNER JOIN cte b on a.OmsSavedCartLineItemId = b.OmsSavedCartLineItemId
		INNER JOIN cte2 c on (a.OmsSavedCartLineItemId = c.ParentOmsSavedCartLineItemId or a.ParentOmsSavedCartLineItemId = c.ParentOmsSavedCartLineItemId)

		;WITH cte AS
		(
			SELECT b.OmsSavedCartLineItemId ,b.ParentOmsSavedCartLineItemId , a.SKU AS SKU
				,a.PersonalizeCode
				,a.PersonalizeValue
				,a.DesignId
				,a.ThumbnailURL
			FROM @TBL_Personalise a 
			INNER JOIN #OldSavecartLineitemDetails b on a.SKU = b.SKU
			INNER JOIN ZnodeOmsPersonalizeCartItem c on b.OmsSavedCartLineItemId = c.OmsSavedCartLineItemId
			WHERE a.OmsSavedCartLineItemId = 0
		)
		DELETE b1
		FROM #OldSavecartLineitemDetails b1 
		WHERE NOT EXISTS(SELECT * FROM cte c where (b1.OmsSavedCartLineItemId = c.ParentOmsSavedCartLineItemId or b1.ParentOmsSavedCartLineItemId = c.ParentOmsSavedCartLineItemId))
		AND EXISTS(SELECT * FROM cte)

		--------If lineitem present in ZnodeOmsPersonalizeCartItem and personalize value is different for same line item then New lineItem will generate
		--------If lineitem present in ZnodeOmsPersonalizeCartItem and personalize value is same for same line item then Quantity will added
	
		;WITH cte AS
		(
			SELECT b.OmsSavedCartLineItemId ,a.ParentOmsSavedCartLineItemId , a.SKU
						,d.PersonalizeCode
				,d.PersonalizeValue
				,d.DesignId
				,d.ThumbnailURL
				FROM @TBL_SavecartLineitems a 
				INNER JOIN #OldSavecartLineitemDetails b on a.SKU = b.SKU
				INNER JOIN @TBL_Personalise d on d.SKU = a.SKU
				INNER JOIN ZnodeOmsPersonalizeCartItem  c  with (nolock)  on b.OmsSavedCartLineItemId = c.OmsSavedCartLineItemId
				WHERE a.OmsSavedCartLineItemId = 0
		)
		DELETE b1
		FROM cte a1		  
		INNER JOIN #OldSavecartLineitemDetails b1 on a1.sku = b1.SKU
		WHERE NOT EXISTS(SELECT * FROM ZnodeOmsPersonalizeCartItem c where a1.OmsSavedCartLineItemId = c.OmsSavedCartLineItemId and a1.PersonalizeValue = c.PersonalizeValue)

		--Updating the cart if old and new cart data matches 
		UPDATE a
		SET a.Quantity = a.Quantity+ty.Quantity,
		a.Custom1 = ty.Custom1,
		a.Custom2 = ty.Custom2,
		a.Custom3 = ty.Custom3,
		a.Custom4 = ty.Custom4,
		a.Custom5 = ty.Custom5, 
		a.ModifiedDate = @GetDate
		FROM ZnodeOmsSavedCartLineItem a
		INNER JOIN #OldSavecartLineitemDetails b ON (a.OmsSavedCartLineItemId = b.OmsSavedCartLineItemId)
		INNER JOIN #NewSavecartLineitemDetails ty ON (ty.SKU = b.SKU)

	END 
	--Inserting the new save cart data if old and new cart data not match
	ELSE 
	BEGIN 
	    --Getting the new save cart data and generating row no. for new save cart insert
		SELECT RowId, Id ,Row_number()Over(Order BY RowId, Id,GenId) NewRowId , ParentOmsSavedCartLineItemId ,OmsSavedCartId,SKU,Quantity,OrderLineItemRelationshipTypeId  
		,CustomText,CartAddOnDetails,ROW_NUMBER()Over(Order BY NewId() ) Sequence ,AutoAddon  
		,OmsOrderId,Custom1,Custom2,Custom3 ,Custom4 ,Custom5,GroupId,ProductName ,min(Description)Description  ,ParentSKU  
		INTO #InsertNewSavecartLineitem   
		FROM  #NewSavecartLineitemDetails  
		GROUP BY ParentOmsSavedCartLineItemId ,OmsSavedCartId,SKU,Quantity,OrderLineItemRelationshipTypeId  
		,CustomText,CartAddOnDetails ,AutoAddon  
		,OmsOrderId,Custom1,Custom2,Custom3 ,Custom4 ,Custom5,GroupId,ProductName ,RowId, Id ,GenId,ParentSKU   
		ORDER BY RowId, Id   
       	
		--Removing the line item having Quantity <=0	     
		DELETE FROM #InsertNewSavecartLineitem WHERE Quantity <=0  
  
		--Updating the rowid into new save cart line item as new line item is merged into existing save cart item
		;WITH VTTY AS   
		(  
			SELECT m.RowId OldRowId , TY1.RowId , TY1.SKU   
			FROM #InsertNewSavecartLineitem m  
			INNER JOIN  #InsertNewSavecartLineitem TY1 ON TY1.SKU = m.ParentSKU   
			WHERE m.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon   
		)   
		UPDATE m1   
		SET m1.RowId = TYU.RowId  
		FROM #InsertNewSavecartLineitem m1   
		INNER JOIN VTTY TYU ON (TYU.OldRowId = m1.RowId)  
        
		--Deleting the new save cart line item if cart line item is merged
		;WITH VTRET AS   
		(  
			SELECT RowId,id,Min(NewRowId) NewRowId ,SKU ,ParentSKU ,OrderLineItemRelationshipTypeID  
			FROM #InsertNewSavecartLineitem   
			GROUP BY RowId,id ,SKU ,ParentSKU ,OrderLineItemRelationshipTypeID
			Having  SKU = ParentSKU  AND OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdSimple
		)   
		DELETE FROM #InsertNewSavecartLineitem WHERE NewRowId IN (SELECT NewRowId FROM VTRET)  

		--Inserting the new cart line item if not merged in existing save cart line item
		INSERT INTO  ZnodeOmsSavedCartLineItem (ParentOmsSavedCartLineItemId ,OmsSavedCartId,SKU,Quantity,OrderLineItemRelationshipTypeId  
		,CustomText,CartAddOnDetails,Sequence,CreatedBY,CreatedDate,ModifiedBy ,ModifiedDate,AutoAddon  
		,OmsOrderId,Custom1,Custom2,Custom3 ,Custom4 ,Custom5,GroupId,ProductName ,Description)  
		OUTPUT INSERTED.OmsSavedCartLineItemId  INTO @OmsInsertedData 
		SELECT NULL ,OmsSavedCartId,SKU,Quantity,OrderLineItemRelationshipTypeId  
		,CustomText,CartAddOnDetails,ROW_NUMBER()Over(Order BY NewRowId)  sequence,@UserId,@GetDate,@UserId,@GetDate,AutoAddon  
		,OmsOrderId,Custom1,Custom2,Custom3 ,Custom4 ,Custom5,GroupId,ProductName ,Description   
		FROM  #InsertNewSavecartLineitem  TH  

		SELECT  MAX(a.OmsSavedCartLineItemId ) OmsSavedCartLineItemId 
		, b.RowId ,b.GroupId ,b.SKU ,b.ParentSKU  
		INTO #ParentOmsSavedCartId
		FROM ZnodeOmsSavedCartLineItem a with (nolock) 
		INNER JOIN #InsertNewSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.ParentSKU AND ISNULL(b.GroupId,'-') = ISNULL(a.GroupId,'-')  )  
		WHERE ISNULL(a.ParentOmsSavedCartLineItemId,0) =0  
		AND b.OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdAddon
		AND CASE WHEN EXISTS (SELECT TOP 1 1 FROM #InsertNewSavecartLineitem TU WHERE TU.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdSimple)  THEN ISNULL(a.OrderLineItemRelationshipTypeID,0) ELSE 0 END = 0 
		GROUP BY b.RowId ,b.GroupId ,b.SKU	,b.ParentSKU,b.OrderLineItemRelationshipTypeID

		UPDATE a SET a.ParentOmsSavedCartLineItemId = (SELECT TOP 1 OmsSavedCartLineItemId FROM  #ParentOmsSavedCartId  r  
		WHERE  r.RowId = b.RowId AND ISNULL(r.GroupId,'-') = ISNULL(a.GroupId,'-')  Order by b.RowId )   
		FROM ZnodeOmsSavedCartLineItem a  
		INNER JOIN #InsertNewSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.SKU AND b.id =1  )   
		WHERE a.OrderLineItemRelationshipTypeId IS NOT NULL   
		AND b.OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdAddon  
		AND a.ParentOmsSavedCartLineItemId IS nULL   

		SELECT a.OmsSavedCartLineItemId , b.RowId  ,b.SKU ,b.ParentSKU  ,Row_number()Over(Order BY c.OmsSavedCartLineItemId )RowIdNo
		INTO #NewSimpleProduct
		FROM ZnodeOmsSavedCartLineItem a with (nolock) 
		INNER JOIN #InsertNewSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.ParentSKU AND ( b.Id = 1  ))  
		INNER JOIN ZnodeOmsSavedCartLineItem c on b.sku = c.sku and b.OmsSavedCartId=c.OmsSavedCartId and b.Id = 1 
		WHERE ( ISNULL(a.ParentOmsSavedCartLineItemId,0) <> 0   )
		AND b.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon  and c.ParentOmsSavedCartLineItemId is null

		--Updating ParentOmsSavedCartLineItemId for newly added save cart line item
		;WITH table_update AS 
		(
			SELECT * , ROW_NUMBER()Over(Order BY OmsSavedCartLineItemId  ) RowIdNo
			FROM ZnodeOmsSavedCartLineItem a with (nolock)
			WHERE a.OrderLineItemRelationshipTypeId IS NOT NULL   
			AND a.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon  
			AND a.ParentOmsSavedCartLineItemId IS NULL  
			AND EXISTS (SELECT TOP 1 1  FROM  #InsertNewSavecartLineitem ty WHERE ty.OmsSavedCartId = a.OmsSavedCartId )
			AND EXISTS (SELECT TOP 1 1 FROM #NewSimpleProduct TI WHERE TI.SKU = a.SKU)
		)
		UPDATE a SET  
		a.ParentOmsSavedCartLineItemId =(SELECT TOP 1 max(OmsSavedCartLineItemId) 
		FROM #NewSimpleProduct  r  
		WHERE  r.ParentSKU = b.ParentSKU AND a.SKU = r.SKU  GROUP BY r.ParentSKU, r.SKU  )   
		FROM table_update a  
		INNER JOIN #InsertNewSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.SKU AND b.OrderLineItemRelationshipTypeID = @OrderLineItemRelationshipTypeIdAddon AND  b.id =1 )   
		WHERE (SELECT TOP 1 max(OmsSavedCartLineItemId) 
		FROM #NewSimpleProduct  r  
		WHERE  r.ParentSKU = b.ParentSKU AND a.SKU = r.SKU   GROUP BY r.ParentSKU, r.SKU  )    IS NOT NULL 
	 
		;WITH Cte_Th AS   
		(             
			SELECT RowId    
			FROM #InsertNewSavecartLineitem a   
			GROUP BY RowId   
			HAVING COUNT(NewRowId) <= 1   
		)   
		UPDATE a SET a.Quantity =  NULL , a.ModifiedDate = @GetDate  
		FROM ZnodeOmsSavedCartLineItem a  
		INNER JOIN #InsertNewSavecartLineitem b ON (a.OmsSavedCartId = b.OmsSavedCartId AND a.SKU = b.SKU AND b.id =0)   
		WHERE NOT EXISTS (SELECT TOP 1 1  FROM Cte_Th TY WHERE TY.RowId = b.RowId )  
		AND a.OrderLineItemRelationshipTypeId IS NULL   
  
		UPDATE  ZnodeOmsSavedCartLineItem   
		SET GROUPID = NULL   
		WHERE  EXISTS (SELECT TOP 1 1  FROM #InsertNewSavecartLineitem RT WHERE RT.OmsSavedCartId = ZnodeOmsSavedCartLineItem.OmsSavedCartId )  
		AND OrderLineItemRelationshipTypeId IS NOT NULL     

		;WITH Cte_UpdateSequence AS   
		(  
			SELECT OmsSavedCartLineItemId ,Row_Number()Over(Order By OmsSavedCartLineItemId) RowId , Sequence   
			FROM ZnodeOmsSavedCartLineItem with (nolock)  
			WHERE EXISTS (SELECT TOP 1 1 FROM #InsertNewSavecartLineitem TH WHERE TH.OmsSavedCartId = ZnodeOmsSavedCartLineItem.OmsSavedCartId )  
		)   
		UPDATE Cte_UpdateSequence  
		SET  Sequence = RowId  
	
		----To update saved cart item personalise value FROM given line item	
		IF EXISTS(SELECT * FROM @TBL_Personalise1 where isnull(PersonalizeValue,'') <> '' and isnull(OmsSavedCartLineItemId,0) <> 0)
		BEGIN
			DELETE FROM ZnodeOmsPersonalizeCartItem 
			WHERE EXISTS (SELECT TOP 1 1 FROM @TBL_Personalise1 yu WHERE yu.OmsSavedCartLineItemId = ZnodeOmsPersonalizeCartItem.OmsSavedCartLineItemId )

			MERGE INTO ZnodeOmsPersonalizeCartItem TARGET 
			USING @TBL_Personalise1 SOURCE
			ON (TARGET.OmsSavedCartLineItemId = SOURCE.OmsSavedCartLineItemId ) 
			WHEN NOT MATCHED THEN 
			INSERT  ( OmsSavedCartLineItemId,  CreatedBy, CreatedDate, ModifiedBy, ModifiedDate
							,PersonalizeCode, PersonalizeValue,DesignId	,ThumbnailURL )
			VALUES (  SOURCE.OmsSavedCartLineItemId,  @userId, @getdate, @userId, @getdate
							,SOURCE.PersonalizeCode, SOURCE.PersonalizeValue,SOURCE.DesignId	,SOURCE.ThumbnailURL ) ;
		END		
	
		UPDATE @TBL_Personalise
		SET OmsSavedCartLineItemId = b.OmsSavedCartLineItemId
		FROM @OmsInsertedData a 
		INNER JOIN ZnodeOmsSavedCartLineItem b ON (a.OmsSavedCartLineItemId = b.OmsSavedCartLineItemId and b.OrderLineItemRelationshipTypeID <> @OrderLineItemRelationshipTypeIdAddon)
		WHERE b.ParentOmsSavedCartLineItemId IS NOT NULL 
	
		DELETE FROM ZnodeOmsPersonalizeCartItem 
		WHERE EXISTS (SELECT TOP 1 1 FROM @TBL_Personalise yu WHERE yu.OmsSavedCartLineItemId = ZnodeOmsPersonalizeCartItem.OmsSavedCartLineItemId )
						
		MERGE INTO ZnodeOmsPersonalizeCartItem TARGET 
		USING @TBL_Personalise SOURCE
		ON (TARGET.OmsSavedCartLineItemId = SOURCE.OmsSavedCartLineItemId ) 
		WHEN NOT MATCHED THEN 
		INSERT  ( OmsSavedCartLineItemId,  CreatedBy, CreatedDate, ModifiedBy, ModifiedDate
					,PersonalizeCode, PersonalizeValue,DesignId	,ThumbnailURL )
		VALUES (  SOURCE.OmsSavedCartLineItemId,  @userId, @getdate, @userId, @getdate
					,SOURCE.PersonalizeCode, SOURCE.PersonalizeValue,SOURCE.DesignId	,SOURCE.ThumbnailURL ) ;
  
		
		 
	END 

	Declare @OutputTable Table (CartCount numeric(28,6))

	INSERT INTO @OutputTable
	EXEC [Znode_GetOmsSavedCartLineItemCount] @OmsCookieMappingId = @OmsCookieMappingId,@UserId=@UserId,@PortalId=@UserId
	

	SELECT CAST(1 AS bit) AS Status,@OmsSavedCartId AS SavedCartId,@OmsCookieMappingId AS CookieMappingId,CartCount
	FROM @OutputTable

COMMIT TRAN InsertUpdateSaveCartLineItem;
END TRY
BEGIN CATCH
	SELECT ERROR_MESSAGE()	
	DECLARE @Error_procedure varchar(1000)= ERROR_PROCEDURE(), @ErrorMessage nvarchar(max)= ERROR_MESSAGE(), @ErrorLine varchar(100)= ERROR_LINE(), @ErrorCall nvarchar(max)= 'EXEC Znode_InsertUpdateSaveCartLineItem @CartLineItemXML = '+CAST(@CartLineItemXML
	AS varchar(max))+',@UserId = '+CAST(@UserId AS varchar(50))+',@PortalId='+CAST(@PortalId AS varchar(10))+',@OmsCookieMappingId='+CAST(@OmsCookieMappingId AS varchar(10));

	SELECT 0 AS ID, CAST(0 AS bit) AS Status,ERROR_MESSAGE();
	ROLLBACK TRAN InsertUpdateSaveCartLineItem;
	EXEC Znode_InsertProcedureErrorLog @ProcedureName = 'Znode_InsertUpdateSaveCartLineItem', @ErrorInProcedure = @Error_procedure, @ErrorMessage = @ErrorMessage, @ErrorLine = @ErrorLine, @ErrorCall = @ErrorCall;
END CATCH;
END;
GO
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ZnodeRmaReturnLineItems' AND COLUMN_NAME = 'ImportDuty')
BEGIN
	ALTER TABLE ZnodeRmaReturnLineItems DROP COLUMN ImportDuty
END
GO
IF EXISTS(SELECT * FROM SYS.procedures WHERE Name = 'Znode_GetOmsOrderDetail')
	DROP PROC Znode_GetOmsOrderDetail
GO
CREATE PROCEDURE [dbo].[Znode_GetOmsOrderDetail]
( 
	@WhereClause NVARCHAR(MAX),
	@Rows        INT            = 100,
	@PageNo      INT            = 1,
	@Order_BY    VARCHAR(1000)  = '',
	@RowsCount   INT OUT			,
	@UserId	   INT = 0 ,
	@IsFromAdmin int=0,
	@SalesRepUserId int = 0 
 )
AS
    /*
     Summary : This procedure is used to get the oms order detils
			   Records are fetched for those users who placed the order i.e UserId is Present in ZnodeUser and  ZnodeOmsOrderDetails tables
	 Unit Testing:

    EXEC [Znode_GetOmsOrderDetail_SCT] 'PortalId =1',@Order_BY = '',@RowsCount= 0, @UserId = 0 ,@Rows = 50, @PageNo = 1

	declare @p7 int
	set @p7=4
	exec sp_executesql N'Znode_GetOmsOrderDetail_SCT @WhereClause, @Rows,@PageNo,@Order_By,@RowCount OUT,@UserId,@IsFromAdmin',N'@WhereClause nvarchar(30),@Rows int,@PageNo int,@Order_By nvarchar(14),@RowCount int output,@UserId int,@IsFromAdmin int',@WhereClause=N'(PortalId in(''1'',''4'',''5'',''6''))',@Rows=50,@PageNo=1,@Order_By=N'orderdate desc',@RowCount=@p7 output,@UserId=0,@IsFromAdmin=1
	select @p7



   */
BEGIN
    BEGIN TRY
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
		DECLARE @SQL NVARCHAR(MAX), @ProcessType  varchar(50)='Order'
		DECLARE @OrderLineItemRelationshipTypeId INT
		SET @OrderLineItemRelationshipTypeId = ( SELECT top 1 OrderLineItemRelationshipTypeId  FROM ZnodeOmsOrderLineItemRelationshipType where Name = 'AddOns' )

		----Verifying that the @SalesRepUserId is having 'Sales Rep' role
		IF NOT EXISTS
		(
			SELECT * FROM ZnodeUser ZU
			INNER JOIN AspNetZnodeUser ANZU ON ZU.UserName = ANZU.UserName
			INNER JOIN AspNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName
			INNER JOIN AspNetUserRoles ANUR ON ANU.Id = ANUR.UserId
			Where Exists(select * from AspNetRoles ANR Where Name = 'Sales Rep' AND ANUR.RoleId = ANR.Id) 
			AND ZU.UserId = @SalesRepUserId
		)   
		Begin
			SET @SalesRepUserId = 0
		End

		DECLARE @Fn_GetPaginationWhereClause VARCHAR(500) = dbo.Fn_GetPaginationWhereClause(@PageNo,@Rows),
		@Fn_GetFilterWhereClause NVARCHAR(MAX) = ''
		set @Fn_GetFilterWhereClause=dbo.Fn_GetFilterWhereClause(@WhereClause)
			
		IF @Order_BY = ''
			set @Order_BY = 'OrderDate desc'

			set @Order_BY = replace(@Order_BY,'PortalId','ZODD.PortalId')
			set @Order_BY = replace(@Order_BY,'UserName','ISNULL(RTRIM(LTRIM(ZODD.FirstName)),'''')+'' ''+ISNULL(RTRIM(LTRIM(ZODD.LastName)),'''')')
			set @Order_BY = replace(@Order_BY,'email','ZODD.Email')
			set @Order_BY = replace(@Order_BY,'OrderState','case when ZOS.IsShowToCustomer=0 and '+cast( @IsFromAdmin as varchar(50))+' = 0 then ZOSC.Description else  ZOS.Description end')
			set @Order_BY = replace(@Order_BY,'PaymentStatus','ZOPS.Name')
			set @Order_BY = replace(@Order_BY,'PublishState','ZODPS.DisplayName')
			set @Order_BY = replace(@Order_BY,'StoreName','ZP.StoreName')

			Declare @Fn_GetPagingRowId NVARCHAR(MAX) = ' DENSE_RANK()Over('+ ' Order By '+CASE WHEN Isnull(@Order_BY,'') = '' THEN 'OmsOrderId DESC' ELSE @Order_BY + ',OmsOrderId DESC' END  + ') RowId '
						
			IF OBJECT_ID('tempdb..#TBL_RowCount') is not null
				DROP TABLE #TBL_RowCount

			IF OBJECT_ID('tempdb..#Portal') is not null
				DROP TABLE #Portal
			
			Create table #TBL_RowCount(RowsCount int )
			CREATE TABLE #tbl_GetRecurciveUserId  (ID INT IDENTITY(1,1) Primary key,UserId INT,ParentUserId INT)
			INSERT INTO #tbl_GetRecurciveUserId
			SELECT UserId,ParentUserId FROM dbo.Fn_GetRecurciveUserId (CAST(@UserId AS VARCHAR(50)),@ProcessType ) FNRU
			 
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'PortalId','ZODD.PortalId')
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'UserName','ISNULL(RTRIM(LTRIM(ZODD.FirstName)),'''')+'' ''+ISNULL(RTRIM(LTRIM(ZODD.LastName)),'''')')
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'email','ZODD.Email')
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'OrderState','case when ZOS.IsShowToCustomer=0 and '+cast( @IsFromAdmin as varchar(50))+' = 0 then ZOSC.Description else  ZOS.Description end')
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'PaymentStatus','ZOPS.Name')
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'PublishState','ZODPS.DisplayName')
			set @Fn_GetFilterWhereClause = replace(@Fn_GetFilterWhereClause,'StoreName','ZP.StoreName')

			set @Order_BY = replace(@Order_BY,'ShippingPostalCode','BillingPostalCode')

			set @Fn_GetPagingRowId = replace(@Fn_GetPagingRowId,'OmsOrderId','Zoo.OmsOrderId')

			set @Rows = @PageNo * @Rows

			CREATE TABLE #Portal (PortalId int,StoreName varchar(200))
			insert into #Portal
			select PortalId,StoreName
			from ZnodePortal

		SET @SQL = '
		SELECT Distinct top '+cast(@Rows as varchar(10))+' Zoo.OmsOrderId,Zoo.OrderNumber, ZODD.PortalId,ZP.StoreName ,ZODD.CurrencyCode,
		case when ZOS.IsShowToCustomer=0 and '+cast( @IsFromAdmin as varchar(50))+' = 0 then ZOSC.Description else  ZOS.Description end  OrderState,ZODD.ShippingId,ZODD.PaymentTypeId,ZODD.PaymentSettingId
		,ZOPS.Name PaymentStatus,ZPS.Name PaymentType,CAST(1 AS BIT) ShippingStatus ,ZODD.OrderDate,ZODD.UserId,ISNULL(RTRIM(LTRIM(ZODD.FirstName)),'''')
		+'' ''+ISNULL(RTRIM(LTRIM(ZODD.LastName)),'''') UserName ,ZODD.PaymentTransactionToken ,ZODD.Total ,ZODD.OmsOrderDetailsId,ZODD.PoDocument,
		ZODD.Email ,ZODD.PhoneNumber ,ZODD.SubTotal ,ZODD.TaxCost ,ZODD.ShippingCost,ZODD.BillingPostalCode,
		ZODD.ModifiedDate AS OrderModifiedDate,  ZODD.PaymentDisplayName  ,isnull(Zoo.ExternalId,0) ExternalId,ZODD.CreditCardExpMonth,ZODD.CultureCode--,ZODD.TotalAdditionalCost
		,ZODD.CreditCardExpYear,ZODD.CardType,ZODD.CreditCardNumber,ZODD.PaymentExternalId,ZODPS.DisplayName as PublishState,
		'''' ProductName, 0 CountId, CAST (0 as bit) IsInRMA, '+@Fn_GetPagingRowId+' 
		INTO #Cte_OrderLineDescribe
		FROM ZnodeOmsOrder (nolock) ZOO 
		INNER JOIN ZnodeOmsOrderDetails (nolock) ZODD ON (ZODD.OmsOrderId = ZOO.OmsOrderId AND  ZODD.IsActive = 1)
		INNER JOIN ZnodePublishState ZODPS ON (ZODPS.PublishStateId = ZOO.PublishStateId)
		INNER JOIN #Portal ZP (nolock) ON ZODD.PortalId = ZP.PortalId
		LEFT JOIN ZnodePaymentType (nolock) ZPS ON (ZPS.PaymentTypeId = ZODD.PaymentTypeId )
		LEFT JOIN ZnodeOmsOrderStateShowToCustomer (nolock) ZOSC ON (ZOSC.OmsOrderStateId = ZODD.OmsOrderStateId)
		LEFT JOIN ZnodeOmsOrderState (nolock) ZOS ON (ZOS.OmsOrderStateId = ZODD.OmsOrderStateId)
		LEFT JOIN ZnodeOmsPaymentState (nolock) ZOPS ON (ZOPS.OmsPaymentStateId = ZODD.OmsPaymentStateId)
		WHERE (exists(select * from ZnodeSalesRepCustomerUserPortal SalRep where SalRep.SalesRepUserId = '+cast(@SalesRepUserId as varchar(10))+' and ZODD.UserId = SalRep.CustomerUserid) or '+cast(@SalesRepUserId as varchar(10))+' = 0 )  
		AND (EXISTS (SELECT TOP 1 1 FROM #tbl_GetRecurciveUserId FNRU WHERE FNRU.UserId = ZODD.UserId ) OR '+cast(@UserId as varchar(10))+'  =0 )'
		+ @Fn_GetFilterWhereClause+' 
		ORDER BY '+CASE WHEN Isnull(@Order_BY,'') = '' THEN 'OmsOrderId DESC' ELSE @Order_BY + ',OmsOrderId DESC' END +' 

		INSERT INTO #TBL_RowCount 
		SELECT count(*)
		FROM ZnodeOmsOrder (nolock) ZOO 
		INNER JOIN ZnodeOmsOrderDetails (nolock) ZODD ON (ZODD.OmsOrderId = ZOO.OmsOrderId AND  ZODD.IsActive = 1)
		INNER JOIN ZnodePublishState ZODPS ON (ZODPS.PublishStateId = ZOO.PublishStateId)
		INNER JOIN #Portal ZP (nolock) ON ZODD.PortalId = ZP.PortalId
		LEFT JOIN ZnodePaymentType (nolock) ZPS ON (ZPS.PaymentTypeId = ZODD.PaymentTypeId )
		LEFT JOIN ZnodeOmsOrderStateShowToCustomer (nolock) ZOSC ON (ZOSC.OmsOrderStateId = ZODD.OmsOrderStateId)
		LEFT JOIN ZnodeOmsOrderState (nolock) ZOS ON (ZOS.OmsOrderStateId = ZODD.OmsOrderStateId)
		LEFT JOIN ZnodeOmsPaymentState (nolock) ZOPS ON (ZOPS.OmsPaymentStateId = ZODD.OmsPaymentStateId)
		WHERE (EXISTS(select * from ZnodeSalesRepCustomerUserPortal SalRep where SalRep.SalesRepUserId = '+cast(@SalesRepUserId as varchar(10))+' and ZODD.UserId = SalRep.CustomerUserid) or '+cast(@SalesRepUserId as varchar(10))+' = 0 )  
		AND (EXISTS (SELECT TOP 1 1 FROM #tbl_GetRecurciveUserId FNRU WHERE FNRU.UserId = ZODD.UserId ) OR '+cast(@UserId as varchar(10))+'  =0 )'
		+ @Fn_GetFilterWhereClause+' 
			
		Create index Ind_OrderLineDescribe_RowId on #Cte_OrderLineDescribe(RowId )

		SELECT OmsOrderId,OrderNumber,PortalId,StoreName,CurrencyCode,OrderState,ShippingId,
		PaymentTypeId,PaymentSettingId,PaymentStatus,PaymentType,ShippingStatus,OrderDate,UserId,UserName,PaymentTransactionToken,Total,
		ProductName OrderItem,OmsOrderDetailsId,CountId ItemCount, PoDocument AS PODocumentPath,IsInRMA ,
		Email,PhoneNumber,SubTotal,TaxCost,ShippingCost,BillingPostalCode
		,OrderModifiedDate,PaymentDisplayName, 
		ExternalId,CreditCardExpMonth,CreditCardExpYear,CardType,CreditCardNumber,PaymentExternalId,CultureCode,PublishState --TotalAdditionalCost
		FROM #Cte_OrderLineDescribe
		' + @Fn_GetPaginationWhereClause +' order by RowId '

		print @SQL
		EXEC(@SQL)
		Select @RowsCount= isnull(RowsCount  ,0) from #TBL_RowCount
		
		IF OBJECT_ID('tempdb..#TBL_RowCount') is not null
				DROP TABLE #TBL_RowCount

		IF OBJECT_ID('tempdb..#Portal') is not null
			DROP TABLE #Portal
		
    END TRY
    BEGIN CATCH
        DECLARE @Status BIT ;
		SET @Status = 0;
		DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(),
		@ErrorCall NVARCHAR(MAX)= 'EXEC Znode_GetOmsOrderDetail @WhereClause = '''+ISNULL(CAST(@WhereClause AS VARCHAR(max)),'''''')+''',@Rows='''+ISNULL(CAST(@Rows AS VARCHAR(50)),'''''')+''',@PageNo='+ISNULL(CAST(@PageNo AS VARCHAR(50)),'''')+',
		@Order_BY='+ISNULL(@Order_BY,'''''')+',@UserId = '+ISNULL(CAST(@UserId AS VARCHAR(50)),'''')+',@RowsCount='+ISNULL(CAST(@RowsCount AS VARCHAR(50)),'''')+',@IsFromAdmin='+ISNULL(CAST(@IsFromAdmin AS VARCHAR(10)),'''');
              			 
        SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                    
		  
        EXEC Znode_InsertProcedureErrorLog
		@ProcedureName = 'Znode_GetOmsOrderDetail',
		@ErrorInProcedure = 'Znode_GetOmsOrderDetail',
		@ErrorMessage = @ErrorMessage,
		@ErrorLine = @ErrorLine,
		@ErrorCall = @ErrorCall;
    END CATCH;
END; 
GO